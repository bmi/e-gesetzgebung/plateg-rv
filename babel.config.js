// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

const config = require('@leanup/cli-core-babel/babel.config');

config.plugins.push([
  '@babel/plugin-proposal-nullish-coalescing-operator',
  {
    loose: true,
  },
]);
config.plugins.push(['@babel/plugin-proposal-private-property-in-object', { loose: true }]);
module.exports = config;
