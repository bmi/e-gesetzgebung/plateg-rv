// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// Only used to make it work standalone...not sure if this is really making sense
import './shares/i18n';
import './style.less';

import { ConfigProvider } from 'antd';
import deDE from 'antd/es/locale/de_DE';
import i18n from 'i18next';
import React, { version } from 'react';
import { createRoot } from 'react-dom/client';
import { initReactI18next } from 'react-i18next';
import { HashRouter as Router, Redirect } from 'react-router-dom';

import { px2remTransformer, StyleProvider } from '@ant-design/cssinjs';
import { plategThemeConfig, ScrollToTop } from '@plateg/theme';
import { KeycloakAuth } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { LoginRedirect } from '@plateg/theme/src/components/auth/login-redirect';
import { store, StoreProvider } from '@plateg/theme/src/components/store';
import { StoreInit } from '@plateg/theme/src/components/storeInit/component.react';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { AppRv } from './components/app/component.react';
import { messages } from './messages';
import { routes } from './shares/routes';

GlobalDI.register('Framework', {
  name: 'React',
  version,
});
interface TranslationObject {
  [key: string]: string | TranslationObject;
}
const isObject = (item: unknown): item is TranslationObject => {
  return item !== null && typeof item === 'object' && !Array.isArray(item);
};

const deepMerge = (target: TranslationObject, source: TranslationObject): TranslationObject => {
  if (!isObject(target) || !isObject(source)) {
    console.log('Both target and source must be objects');
  }

  const output: TranslationObject = { ...target };

  Object.keys(source).forEach((key) => {
    if (isObject(source[key])) {
      if (!(key in target)) {
        output[key] = source[key];
      } else {
        output[key] = deepMerge(target[key] as TranslationObject, source[key] as TranslationObject);
      }
    } else {
      output[key] = source[key];
    }
  });

  return output;
};
const loadMessages = (userRessort: string) => {
  const msgs = { ...messages };
  if (userRessort === 'bt' || userRessort === 'br') {
    msgs.de.translation.rv = deepMerge(msgs.de.translation.rv, msgs.de.translation.rv[userRessort].rv);
  }
  return i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
      resources: msgs,
      lng: 'de',
      fallbackLng: 'de',
      interpolation: {
        escapeValue: false, // react already safes from xss
      },
    });
};
// Necessary in order to use date-fns instead of moment
deDE.DatePicker.lang.locale = 'de';
const htmlDivElement: HTMLDivElement | null = document.querySelector('div#app');
if (htmlDivElement === null) {
  throw new Error('Root container missing in index.html');
}
const root = createRoot(htmlDivElement);
const px2rem = px2remTransformer({
  rootValue: 10, // 10px = 1rem;
});
root.render(
  <ConfigProvider theme={plategThemeConfig} locale={deDE}>
    <Router>
      <ScrollToTop />
      <StoreProvider store={store}>
        <KeycloakAuth forceLogin>
          <StoreInit loadMessages={loadMessages}>
            <>
              <StyleProvider transformers={[px2rem]}>
                <AppRv />
              </StyleProvider>
              <LoginRedirect>
                <Redirect to={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}`} />
              </LoginRedirect>
            </>
          </StoreInit>
        </KeycloakAuth>
      </StoreProvider>
    </Router>
  </ConfigProvider>,
);
