// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { AjaxError } from 'rxjs/ajax';

import { ErrorController, GlobalDI } from '@plateg/theme';

export class RvErrorController {
  private errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  public handleError(error: AjaxError, errorMsg?: string): void {
    const status = (error?.response as { status: number })?.status;
    switch (status) {
      case 1061:
        this.errorCtrl.displayErrorMsg(error, 'rv.rvErrorController.error1061');
        break;
      case 1062:
        this.errorCtrl.displayErrorMsg(error, 'rv.rvErrorController.error1062');
        break;
      case 1063:
        this.errorCtrl.displayErrorMsg(error, 'rv.rvErrorController.error1063');
        break;
      case 1064:
        this.errorCtrl.displayErrorMsg(error, 'rv.rvErrorController.error1064');
        break;
      case 1065:
        this.errorCtrl.displayErrorMsg(error, 'rv.rvErrorController.error1065');
        break;
      case 1069:
        this.errorCtrl.displayErrorMsg(error, 'rv.rvErrorController.error1069');
        break;
      default:
        this.errorCtrl.displayErrorMsg(error, errorMsg || 'rv.rvErrorController.errorDefault');
        break;
    }
  }
}
