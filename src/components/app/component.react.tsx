// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-rv.less';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import {
  AbstimmungControllerApi,
  BerechtigungControllerApi,
  CodelistenControllerApi,
  Configuration,
  EditorRemoteControllerApi,
  NeuRisControllerApi,
  PrintControllerApi,
  RegelungsvorhabenControllerApi,
  RvWorkaroundControllerApi,
} from '@plateg/rest-api';
import { CodeListenController, configureRestApi, HeaderController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { LayoutWrapperRv } from '../rv/component.react';

export function AppRv(): React.ReactElement {
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  GlobalDI.getOrRegister('rvHeadercontroller', () => new HeaderController());
  configureRestApi(registerRestApis);

  return (
    <Switch>
      <Route path="/regelungsvorhaben">
        <LayoutWrapperRv />
      </Route>
    </Switch>
  );
}

function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('berechtigungControllerApi', () => new BerechtigungControllerApi(configRestCalls));
  GlobalDI.getOrRegister('editorRemoteControllerApi', () => new EditorRemoteControllerApi(configRestCalls));
  GlobalDI.getOrRegister('enormController', () => new AbstimmungControllerApi(configRestCalls));

  // INFO: codelistenControllerApi must be registered before codeListenController.
  GlobalDI.getOrRegister('codelistenControllerApi', () => new CodelistenControllerApi(configRestCalls));
  // INFO: pass dependencies as constructor arguments instead of relying on order here.
  // INFO: remove; codeListenController is not an API and should not be initialized in this block.
  GlobalDI.getOrRegister('codeListenController', () => new CodeListenController());

  GlobalDI.getOrRegister('neuRisControllerApi', () => new NeuRisControllerApi(configRestCalls));
  GlobalDI.getOrRegister('printController', () => new PrintControllerApi(configRestCalls));
  GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('rvWorkaroundController', () => new RvWorkaroundControllerApi(configRestCalls));
}
