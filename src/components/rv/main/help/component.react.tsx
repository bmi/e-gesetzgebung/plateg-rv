// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { useAppSelector } from '@plateg/theme/src/components/store';

export function RvHelpComponent(): React.ReactElement {
  const { t } = useTranslation();
  const appStore = useAppSelector((state) => state.user);
  return (
    <div
      className="rv-help-content"
      dangerouslySetInnerHTML={{
        __html: t('rv.help.section1'),
      }}
    />
  );
}
