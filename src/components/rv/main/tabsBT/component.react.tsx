// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../tabs/my-regelungsvorhaben.less';

import { Col, Row, TabsProps, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  PaginierungDTO,
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenBundestagTableDTO,
  RegelungsvorhabenBundestagTableDTOs,
} from '@plateg/rest-api/models/';
import {
  BreadcrumbComponent,
  HeaderController,
  LoadingStatusController,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import {
  setPaginationFilterInfo,
  setPaginationInitState,
  setPaginationResult,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../shares/routes';
import { RvErrorController } from '../../../../utils/rv-error-controller';
import { RvHelpLink } from '../../component.react';
import { ArchivTab } from '../tabs/archiv-tab/component.react';
import { getArchivBundestagCall, getMyRegelungsvorhabenBundestagCall } from '../tabs/controller.react';
import { MyRegelungsvorhabenBundestagTab } from '../tabsBT/my-regelungsvorhaben-bundestag-tab/component.react';

interface MapTabsContentInterfaceBT {
  [key: string]: MapTabsContentItemInterfaceBT;
}
interface MapTabsContentItemInterfaceBT {
  loadMethod: (paginierungDTO: PaginierungDTO) => Observable<RegelungsvorhabenBundestagTableDTOs>;
  setMethod: Function;
  getTableMethod?: Function;
}
export function TabsRvBT(): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('rvHeadercontroller');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  const { t } = useTranslation();
  const { Title } = Typography;
  const history = useHistory();
  const dispatch = useAppDispatch();
  const btTableRoute = 'meineRegelungsvorhabenBundestag';
  const routeMatcherVorbereitung = useRouteMatch<{ tabKey: string }>('/regelungsvorhaben/:tabKey');
  const initTabKey =
    routeMatcherVorbereitung?.params?.tabKey != null
      ? routeMatcherVorbereitung?.params?.tabKey
      : routes.MEINE_REGELUNGSVORHABEN;
  const [activeTab, setActiveTab] = useState<string>(initTabKey);
  const [loadedTabs, setLoadedTabs] = useState<string[]>([]);
  const appStore = useAppSelector((state) => state.user);

  const [myBTRegelungsvorhabenData, setMyBTRegelungsvorhabenData] = useState<RegelungsvorhabenBundestagTableDTO[]>([]);
  const [archivData, setArchivData] = useState<RegelungsvorhabenBundesregierungTableDTO[]>([]);
  const [currentUserIsStellvertreter, setCurrentUserIsStellvertreter] = useState(false);

  const initialRender = useRef(true);
  const selectedTabkey = routeMatcherVorbereitung?.params?.tabKey || routes.MEINE_REGELUNGSVORHABEN;
  const pagDataRequest = useAppSelector((state) => state.tablePagination.tabs[btTableRoute]).request;

  const mapTabsContentBT: MapTabsContentInterfaceBT = {
    meineRegelungsvorhabenBundestag: {
      loadMethod: getMyRegelungsvorhabenBundestagCall,
      setMethod: setMyBTRegelungsvorhabenData,
    },
    archiv: {
      loadMethod: getArchivBundestagCall,
      setMethod: setArchivData,
    },
  };

  useEffect(() => {
    if (appStore.user?.dto.stellvertreter) {
      setCurrentUserIsStellvertreter(true);
    }
  }, []);

  useEffect(() => {
    setBreadcrumb(activeTab);
  }, [activeTab]);

  useEffect(() => {
    const splitActiveTab = activeTab === routes.MEINE_REGELUNGSVORHABEN ? btTableRoute : activeTab;
    const splitSelectedTab = selectedTabkey === routes.MEINE_REGELUNGSVORHABEN ? btTableRoute : selectedTabkey;
    if (activeTab !== selectedTabkey) {
      setActiveTab(selectedTabkey);
      if (!loadedTabs.includes(selectedTabkey)) {
        initialRender.current = true;
        loadContentBT(splitSelectedTab);
      }
      return;
    }

    loadContentBT(splitActiveTab);
  }, [
    pagDataRequest.currentPage,
    pagDataRequest.columnKey,
    pagDataRequest.sortOrder,
    pagDataRequest.filters,
    selectedTabkey,
  ]);

  const setBreadcrumb = (tabName: string) => {
    const tabText = (
      <span>
        {t('rv.breadcrumbs.projectName')} - {t(`rv.breadcrumbs.tabName.${tabName}`)}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabText]} />],
      headerRight: [],
      headerLast: [<RvHelpLink key="rv-header-help-link" />],
    });
  };

  const loadContentBT = (key: string) => {
    loadingStatusController.setLoadingStatus(true);
    if (mapTabsContentBT.hasOwnProperty(key)) {
      if (initialRender.current) {
        dispatch(setPaginationInitState({ tabKey: key }));
        initialRender.current = false;
      }

      const loadSub = mapTabsContentBT[`${key}`]
        .loadMethod({
          pageNumber: pagDataRequest.currentPage || 0,
          pageSize: 20,
          filters: pagDataRequest.filters as { [key: string]: string },
          sortBy: pagDataRequest.columnKey,
          sortDirection: pagDataRequest.sortOrder,
        })
        .subscribe({
          next: (data) => {
            const { content, totalElements, number } = data.dtos;
            const { filterNames, vorhabenartFilter, allContentEmpty } = data;
            mapTabsContentBT[`${key}`].setMethod(content);
            if (key === btTableRoute) {
              setLoadedTabs([...loadedTabs, routes.MEINE_REGELUNGSVORHABEN]);
            } else {
              setLoadedTabs([...loadedTabs, key]);
            }

            dispatch(
              setPaginationResult({
                tabKey: key,
                totalItems: totalElements,
                currentPage: number,
                allContentEmpty,
              }),
            );
            dispatch(setPaginationFilterInfo({ tabKey: key, vorhabenartFilter, filterNames }));

            loadingStatusController.setLoadingStatus(false);
            loadSub.unsubscribe();
          },
          error: (error: AjaxError) => {
            loadingStatusController.setLoadingStatus(false);
            rvErrorCtrl.handleError(error);
            loadSub.unsubscribe();
          },
        });
    }
  };

  const resetTab = (key: string) => {
    setLoadedTabs(
      loadedTabs.filter((tab: string) => {
        return tab !== key;
      }),
    );
  };

  const tabItems: TabsProps['items'] = [
    {
      key: routes.MEINE_REGELUNGSVORHABEN,
      label: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.tabNav'),
      children: (
        <MyRegelungsvorhabenBundestagTab
          tabKey={btTableRoute}
          myRegelungsvorhabenData={myBTRegelungsvorhabenData}
          setMyRegelungsvorhabenData={setMyBTRegelungsvorhabenData}
          resetTab={resetTab}
          currentUserIsStellvertreter={currentUserIsStellvertreter}
          loadContent={() => loadContentBT(btTableRoute)}
        />
      ),
    },
    {
      key: routes.ARCHIV,
      label: t('rv.myRegelungsvorhaben.tabs.archiv.tabNav'),
      children: <ArchivTab tabKey={routes.ARCHIV} archivData={archivData} />,
    },
  ];

  return (
    <div className="myRegelungsvorhabenPage">
      <TitleWrapperComponent>
        <Row>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className="heading-holder">
              <Title level={1}>{t('rv.myRegelungsvorhaben.mainTitle')}</Title>
            </div>
          </Col>
        </Row>
      </TitleWrapperComponent>
      <Row>
        <Col xs={{ span: 22, offset: 1 }}>
          <TabsWrapper
            items={tabItems}
            activeKey={activeTab}
            className={`my-votes-tabs standard-tabs`}
            onChange={(key: string) => history.push(`/regelungsvorhaben/${key}`)}
            moduleName={t('rv.myRegelungsvorhaben.mainTitle')}
          />
        </Col>
      </Row>
    </div>
  );
}
