// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import i18n from 'i18next';
import React from 'react';

import { RegelungsvorhabenBundestagTableDTO } from '@plateg/rest-api';
import { DropdownMenu } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { getBTTableItems } from '../../tabs/general-items/controller.react';
import { TableCompPropsRvTableWithoutId } from './component.react';

export function getMyRegelungsvorhabenBundestagTableVals(
  content: RegelungsvorhabenBundestagTableDTO[],
  setConfirmArchiveModalIsVisible: (confirmArchiveModalIsVisible: boolean) => void,
  setSelectedRegelungsvorhaben: (selectedAbstimmung: RegelungsvorhabenBundestagTableDTO) => void,
  setIsFreigebenModalVisible: (value: boolean) => void,
  currentUserIsStellvertreter: boolean,
): TableCompPropsRvTableWithoutId {
  const columns: ColumnsType<RegelungsvorhabenBundestagTableDTO> = [
    ...getBTTableItems(),
    {
      title: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.actions').toString(),
      key: 'actions',
      render: (record: RegelungsvorhabenBundestagTableDTO) => {
        return (
          <div className="actions-holder">
            <DropdownMenu
              openLink={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}/${record.id}/${routes.NAECHSTESCHRITTE}`}
              items={[
                {
                  element: (
                    <>
                      <span className="status-change-dd-span">
                        {i18n
                          .t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.BT.table.actions.statusAendern')
                          .toString()}
                      </span>
                    </>
                  ),
                  children: [
                    {
                      element: i18n.t('rv.myRegelungsvorhaben.BTTableItems.statuses.IN_BEARBEITUNG'),
                    },
                    {
                      element: i18n.t('rv.myRegelungsvorhaben.BTTableItems.statuses.WEITERGELEITET'),
                    },
                    {
                      element: i18n.t('rv.myRegelungsvorhaben.BTTableItems.statuses.ARCHIVIERT'),
                    },
                  ],
                },
                {
                  type: 'divider',
                },
                {
                  element: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.BT.table.actions.archivieren'),
                  onClick: () => {
                    setSelectedRegelungsvorhaben(record);
                    setConfirmArchiveModalIsVisible(true);
                  },
                  disabled: () => !record.allowedToArchive,
                },
              ]}
              elementId={record.id}
            />
          </div>
        );
      },
    },
  ];

  return {
    expandable: false,
    columns,
    content,
    filteredColumns: [{ name: 'vorhabentyp', columnIndex: 0 }],
    sorterOptions: [
      {
        columnKey: 'zuleitungAm',
        titleAsc: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.zuleitungsdatumAsc'),
        titleDesc: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.zuleitungsdatumDesc'),
      },
    ],
  };
}
