// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { ErrorPage, SuccessPage } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../shares/routes';
import { CreationRoutesRv } from './regelungsvorhaben/component.react';
import { RegelungsvorhabenTabs } from './regelungsvorhaben/regelungsvorhaben-tabs/component.react';
import { TabsRv } from './tabs/component.react';
import { TabsRvBR } from './tabsBR/component.react';
import { TabsRvBT } from './tabsBT/component.react';

export function MainRoutesRv(): React.ReactElement {
  const { t } = useTranslation();
  const appStore = useAppSelector((state) => state.user);
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>('/regelungsvorhaben/:id');
  const routeMatcher = useRouteMatch<{ action: string }>('/regelungsvorhaben/:id/:action');
  let existingRegelungsvorhabenId: string | undefined;
  if (routeMatcherVorbereitung?.params?.id != null) {
    existingRegelungsvorhabenId = routeMatcherVorbereitung?.params.id;
  }

  /* if archive & naechsteschritte- redirect */
  const routeMatcherAction = useRouteMatch<{ tabName: string; subPath: string; id: string }>(
    `/${routes.REGELUNGSVORHABEN}/:tabName/:id/:subPath`,
  );
  const isArchivePath =
    routeMatcherAction?.params.tabName === routes.ARCHIV &&
    routeMatcherAction?.params.subPath === routes.NAECHSTESCHRITTE;

  if (isArchivePath) {
    return (
      <Redirect
        to={`/${routes.REGELUNGSVORHABEN}/${routes.ARCHIV}/${routeMatcherAction?.params.id}/${routes.DATENBLATT}`}
      />
    );
  }

  const renderTabsBasedOnRessort = () => {
    const kurzbezeichnung = appStore.user?.dto.ressort?.kurzbezeichnung;
    switch (kurzbezeichnung) {
      case 'BR':
        return <TabsRvBR />;
      case 'BT':
        return <TabsRvBT />;
      default:
        return <TabsRv />;
    }
  };

  const getSuccessMsg = (action: string) => {
    let msg;
    switch (action) {
      case routes.ERFOLGUPD:
        msg = 'aktualisiert';
        break;
      case routes.ERFOLGPKP:
        msg = 'aus PKP übertragen und angelegt';
        break;
      default:
        msg = 'angelegt';
    }
    return msg;
  };
  return (
    <Switch>
      <Route exact path={[`/regelungsvorhaben`]}>
        <Redirect to={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}`} />
      </Route>
      <Route
        exact
        path={[
          `/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}`,
          `/regelungsvorhaben/${routes.ENTWURFE}`,
          `/regelungsvorhaben/${routes.ARCHIV}`,
        ]}
      >
        {renderTabsBasedOnRessort()}
      </Route>
      <Route
        exact
        path={[
          `/regelungsvorhaben/:tabName/${routes.PRUEFEN_REGELUNGSVORHABEN}`,
          `/regelungsvorhaben/:tabName/${routes.PRUEFEN_REGELUNGSVORHABEN_ENTWURF}`,
        ]}
      >
        <CreationRoutesRv />
      </Route>
      <Route
        exact
        path={[
          `/regelungsvorhaben/:tabName/:id/${routes.NEUES_REGELUNGSVORHABEN}`,
          `/regelungsvorhaben/:tabName/${routes.NEUES_REGELUNGSVORHABEN}`,
          `/regelungsvorhaben/:tabName/:id/${routes.BEARBEITEN_REGELUNGSVORHABEN}`,
          `/regelungsvorhaben/:tabName/:id/${routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF}`,
        ]}
      >
        {appStore.user?.dto.ressort?.kurzbezeichnung === 'BT' ||
        appStore.user?.dto.ressort?.kurzbezeichnung === 'BR' ? (
          <Redirect to={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}`} />
        ) : (
          <CreationRoutesRv />
        )}
      </Route>
      <Route
        path={[
          `/regelungsvorhaben/:tabName/:id/${routes.DATENBLATT}`,
          `/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}`,
          `/regelungsvorhaben/:tabName/:id/${routes.ZULEITUNGEN}`,
        ]}
      >
        <RegelungsvorhabenTabs />
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.BESTANDSRECHT}`]}>
        {appStore.user?.dto.ressort?.kurzbezeichnung === 'BT' ||
        appStore.user?.dto.ressort?.kurzbezeichnung === 'BR' ? (
          <Redirect to={`${routes.NAECHSTESCHRITTE}`} />
        ) : (
          <RegelungsvorhabenTabs />
        )}
      </Route>
      <Route
        exact
        path={[
          `/regelungsvorhaben/:id/${routes.ERFOLG}`,
          `/regelungsvorhaben/:id/${routes.ERFOLGUPD}`,
          `/regelungsvorhaben/:id/${routes.ERFOLGPKP}`,
        ]}
      >
        <SuccessPage
          title={t('rv.notificationPages.success.title', {
            type: getSuccessMsg(routeMatcher?.params.action || ''),
          })}
          text={
            <span
              dangerouslySetInnerHTML={{
                __html: t('rv.notificationPages.success.text', {
                  interpolation: { escapeValue: false },
                }),
              }}
            />
          }
          link={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}`}
          linkText={t('rv.notificationPages.success.buttonText')}
        />
      </Route>
      <Route exact path={[`/regelungsvorhaben/:id/:tabName/${routes.FEHLER}`]}>
        <ErrorPage
          title={t('rv.notificationPages.error.title')}
          text={t('rv.notificationPages.error.text', { returnObjects: true })}
          link={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}/${existingRegelungsvorhabenId as string}/${
            routes.BEARBEITEN_REGELUNGSVORHABEN
          }`}
          linkText={t('rv.notificationPages.error.buttonText')}
        />
      </Route>
    </Switch>
  );
}
