// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CodelisteDTO } from '@plateg/rest-api';
import { InfoComponent, MultiChoiceComponent } from '@plateg/theme';
import { ChoicesItemInterface } from '@plateg/theme/src/components/multi-choice/choices-list/component.react';

export interface SubjectAreasComponentProps {
  subjectAreas?: CodelisteDTO;
  form: FormInstance;
}

export function SubjectAreasComponent(props: SubjectAreasComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const areasLabel = (
    <span>
      {t('rv.newRegelungsvorhaben.participants.subjectAreas.label')}

      <InfoComponent withLabelRequired title={t('rv.newRegelungsvorhaben.participants.subjectAreas.hint.title')}>
        <p
          dangerouslySetInnerHTML={{
            __html: t('rv.newRegelungsvorhaben.participants.subjectAreas.hint.content'),
          }}
        />
      </InfoComponent>
    </span>
  );
  const areasName = 'sachgebiete';
  const itemType = t('rv.newRegelungsvorhaben.participants.subjectAreas.itemType');
  const [preparedAreas, setPreparedAreas] = useState<ChoicesItemInterface[]>([]);

  useEffect(() => {
    if (props.subjectAreas) {
      const prepared = props.subjectAreas.values.map((item) => {
        return { label: `${Object.values(item)[0] as string}`, value: Object.keys(item)[0] };
      });
      setPreparedAreas(prepared);
    }
  }, [props.subjectAreas]);

  return (
    <>
      <MultiChoiceComponent
        fieldName={areasName}
        fieldLabel={areasLabel}
        choices={preparedAreas}
        listTitle={itemType}
        form={props.form}
        itemType={itemType}
        rules={[
          {
            required: false,
            message: '',
          },
        ]}
      />
    </>
  );
}
