// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { AbteilungDTO } from '@plateg/rest-api';

import { AbteilungReferatController } from './controller';

describe('TEST: Extract Referate from Abteilung', () => {
  const ctrl = new AbteilungReferatController();
  it('no unterabteilungen', () => {
    const abteilung = {
      referate: [{ bezeichnung: 'a' }],
      unterabteilungen: [],
    };
    const referate = ctrl.extractReferateFromAbteilung(abteilung as unknown as AbteilungDTO);
    expect(referate).to.deep.equal([{ bezeichnung: 'a' }]);
  });
  it('no referate', () => {
    const abteilung = {
      referate: [],
      unterabteilungen: [],
    };
    const referate = ctrl.extractReferateFromAbteilung(abteilung as unknown as AbteilungDTO);
    expect(referate).to.deep.equal([]);
  });
  it('no direct referate', () => {
    const abteilung = {
      referate: [],
      unterabteilungen: [{ referate: [{ bezeichnung: 'b' }] }],
    };
    const referate = ctrl.extractReferateFromAbteilung(abteilung as unknown as AbteilungDTO);
    expect(referate).to.deep.equal([{ bezeichnung: 'b' }]);
  });
  it('referate and unterabteilungen', () => {
    const abteilung = {
      referate: [{ bezeichnung: 'a' }],
      unterabteilungen: [{ referate: [{ bezeichnung: 'b' }, { bezeichnung: 'c' }] }],
    };
    const referate = ctrl.extractReferateFromAbteilung(abteilung as unknown as AbteilungDTO);
    expect(referate).to.deep.equal([{ bezeichnung: 'a' }, { bezeichnung: 'b' }, { bezeichnung: 'c' }]);
  });
});
