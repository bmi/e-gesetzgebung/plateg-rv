// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AbteilungDTO, ReferatDTO, RegelungsvorhabenEntityDTO, RessortFullDTO } from '@plateg/rest-api';
import { CheckOutlined, FormItemWithInfo, InfoComponent, MainContentSelectWrapper } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { useRvState } from '../../../regelungsvorhaben-context/component.react';
import { AbteilungReferatController } from './controller';

export interface FederfuehrendeAbteilungReferatComponentProps {
  form: FormInstance;
  ressorts: RessortFullDTO[];
  ressort?: string;
  disabled?: boolean;
  hidden?: boolean;
  abteilungen?: Array<AbteilungDTO>;
  isRvSentToPKP: boolean;
  rv?: RegelungsvorhabenEntityDTO;
}

export function FederfuehrendeAbteilungReferatComponent(
  props: FederfuehrendeAbteilungReferatComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const abteilungReferatCtrl = new AbteilungReferatController();
  const [currentRessort, setCurrentRessort] = useState<RessortFullDTO>();
  const [selectedAbteilung, setSelectedAbteilung] = useState<[] | null | string>(null);
  const [selectedReferat, setSelectedReferat] = useState<[] | null | string>(null);
  const [userAbteilung, setUserAbteilung] = useState<AbteilungDTO | undefined | null>(null);
  const [userFachreferat, setUserFachreferat] = useState<ReferatDTO | undefined | null>(null);
  const [referatList, setReferatList] = useState<Array<ReferatDTO>>();
  const { itemFromPKP } = useRvState();
  const appStore = useAppSelector((state) => state.user);

  const checkAbteilung = (abteilung?: AbteilungDTO) => {
    if (!abteilung) {
      return false;
    }
    if (props.rv?.technischeFfAbteilung) {
      return abteilung.id === props.rv?.technischeFfAbteilung;
    }
    return abteilung.bezeichnung === appStore.user?.dto.abteilung;
  };
  const checkFachreferat = (referat?: ReferatDTO) => {
    if (!referat) {
      return false;
    }
    if (props.rv?.technischesFfReferat) {
      return referat.id === props.rv?.technischesFfReferat;
    }
    return referat.bezeichnung === appStore.user?.dto.fachreferat;
  };
  useEffect(() => {
    if (props.ressort && props.ressorts) {
      const userRessort = props.ressorts.filter((item) => item.id === props.ressort)[0];
      setCurrentRessort(userRessort);

      const abteilung = userRessort?.abteilungen.find(checkAbteilung);
      setUserAbteilung(abteilung);

      if (abteilung) {
        const fachreferat = abteilungReferatCtrl.extractReferateFromAbteilung(abteilung).find(checkFachreferat);
        setUserFachreferat(fachreferat);
      } else {
        setUserFachreferat(null);
      }
    }
  }, [null, props.ressort, props.ressorts]);

  useEffect(() => {
    setSelectedAbteilung(props.form.getFieldValue('technischeFfAbteilung') as string);
    if (selectedAbteilung === null) {
      setReferatList(undefined);
      setSelectedReferat(null);
    }
  }, [props.form.getFieldValue('technischeFfAbteilung')]);

  useEffect(() => {
    if (currentRessort && selectedAbteilung) {
      const chosenOptionForAbteilung = currentRessort?.abteilungen.filter((item) => item.id == selectedAbteilung)[0];
      if (chosenOptionForAbteilung) {
        setReferatList(abteilungReferatCtrl.extractReferateFromAbteilung(chosenOptionForAbteilung));
      }
    }
  }, [selectedAbteilung, currentRessort]);

  useEffect(() => {
    if (userAbteilung?.id) {
      props.form.setFieldsValue({ technischeFfAbteilung: !userAbteilung.deleted ? userAbteilung.id : null });
      setSelectedAbteilung(!userAbteilung.deleted ? userAbteilung.id : null);
    }
  }, [userAbteilung, itemFromPKP]);

  useEffect(() => {
    if (userFachreferat?.id) {
      props.form.setFieldsValue({ technischesFfReferat: !userFachreferat.deleted ? userFachreferat.id : null });
    }
  }, [userFachreferat, itemFromPKP]);

  const abteilungenOptions = useMemo(() => {
    if (currentRessort?.abteilungen.length) {
      return currentRessort.abteilungen
        .filter((abteilung) => !abteilung.deleted)
        .sort((a, b) => a.bezeichnung.localeCompare(b.bezeichnung))
        .map((abteilung) => ({
          value: abteilung.id,
          label: <span aria-label={abteilung.bezeichnung}> {abteilung.bezeichnung}</span>,
          title: abteilung.bezeichnung,
        }));
    }
    return [];
  }, [currentRessort]);

  const referateOptions = useMemo(() => {
    if (referatList?.length) {
      return referatList
        .filter((referat) => !referat.deleted)
        .sort((a, b) => a.bezeichnung.localeCompare(b.bezeichnung))
        .map((referat) => ({
          value: referat.id,
          label: <span aria-label={referat.bezeichnung}>{referat.bezeichnung}</span>,
        }));
    }
    return [];
  }, [referatList]);

  const isItemDisabled = !!itemFromPKP || props.isRvSentToPKP;

  const isEntityDisabled = (department: AbteilungDTO | ReferatDTO | null | undefined) => {
    if (isItemDisabled && department) {
      return true;
    }

    return false;
  };

  return (
    <>
      <FormItemWithInfo
        validateStatus={userAbteilung?.deleted && !selectedAbteilung ? 'error' : undefined}
        help={
          userAbteilung?.deleted && !selectedAbteilung
            ? t('rv.newRegelungsvorhaben.participants.ffAbteilung.deletedDepartmentMsg', {
                abteilung: userAbteilung?.bezeichnung,
              })
            : undefined
        }
        htmlFor="technischeFfAbteilung"
        name="technischeFfAbteilung"
        label={
          <>
            <span>
              {t('rv.newRegelungsvorhaben.participants.ffAbteilung.label')}
              <InfoComponent withLabel title={t('rv.newRegelungsvorhaben.participants.ffAbteilung.hint.title')}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: t('rv.newRegelungsvorhaben.participants.ffAbteilung.hint.content', {
                      link1: 'https://example.com',
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </span>
          </>
        }
        rules={[{ required: true, message: t('rv.newRegelungsvorhaben.participants.ffAbteilung.error') }]}
      >
        <MainContentSelectWrapper
          options={abteilungenOptions}
          id="technischeFfAbteilung"
          placeholder={t('rv.newRegelungsvorhaben.participants.ffAbteilung.placeholder')}
          menuItemSelectedIcon={<CheckOutlined />}
          disabled={isEntityDisabled(userAbteilung)}
          onChange={(abteilung: null | string) => {
            props.form?.resetFields(['technischesFfReferat']);
            setSelectedAbteilung(abteilung);
            setSelectedReferat(null);
          }}
        />
      </FormItemWithInfo>

      <FormItemWithInfo
        validateStatus={userFachreferat?.deleted && !selectedReferat ? 'error' : undefined}
        help={
          userFachreferat?.deleted && !selectedReferat
            ? t('rv.newRegelungsvorhaben.participants.ffReferat.deletedDepartmentMsg', {
                referat: userFachreferat?.bezeichnung,
              })
            : undefined
        }
        htmlFor="technischesFfReferat"
        name="technischesFfReferat"
        label={
          <>
            <span>
              {t('rv.newRegelungsvorhaben.participants.ffReferat.label')}
              <InfoComponent withLabel title={t('rv.newRegelungsvorhaben.participants.ffReferat.hint.title')}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: t('rv.newRegelungsvorhaben.participants.ffReferat.hint.content', {
                      link1: 'https://example.com',
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </span>
          </>
        }
        rules={[{ required: true, message: t('rv.newRegelungsvorhaben.participants.ffReferat.error') }]}
      >
        <MainContentSelectWrapper
          id="technischesFfReferat"
          placeholder={t('rv.newRegelungsvorhaben.participants.ffReferat.placeholder')}
          menuItemSelectedIcon={<CheckOutlined />}
          options={referateOptions}
          disabled={isEntityDisabled(userFachreferat)}
          onChange={(referat) => {
            setSelectedReferat(referat);
          }}
        />
      </FormItemWithInfo>
    </>
  );
}
