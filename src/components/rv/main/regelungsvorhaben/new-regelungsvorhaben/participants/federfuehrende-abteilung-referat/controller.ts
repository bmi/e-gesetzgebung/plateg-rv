// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { AbteilungDTO, ReferatDTO } from '@plateg/rest-api';

export class AbteilungReferatController {
  public extractReferateFromAbteilung(abteilung: AbteilungDTO): ReferatDTO[] {
    const abteilungsReferate = abteilung.referate;
    const unterabteilungsReferate = abteilung.unterabteilungen.map((item) => item.referate);
    return abteilungsReferate.concat(unterabteilungsReferate.flat());
  }
}
