// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenTypType, RessortDTO } from '@plateg/rest-api';
import { MultiChoiceComponent } from '@plateg/theme';
import { ChoicesItemInterface } from '@plateg/theme/src/components/multi-choice/choices-list/component.react';

import { useRvState } from '../../../regelungsvorhaben-context/component.react';
import { prepareRessorts } from '../component.react';

export interface FederfuehrerComponentProps {
  form: FormInstance;
  ressorts?: RessortDTO[];
  ressort?: string;
  disabled: boolean;
  vorhabenart: RegelungsvorhabenTypType;
}

export function FederfuehrerComponent(props: FederfuehrerComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const ressortsLabel = <span>{t('rv.newRegelungsvorhaben.participants.ffRessort.label')}</span>;
  const ressortsName = 'allFfRessorts';
  const itemType = t('rv.newRegelungsvorhaben.participants.ffRessort.itemType');
  const [preparedRessorts, setPreparedRessorts] = useState<ChoicesItemInterface[]>([]);
  const { itemFromPKP } = useRvState();

  useEffect(() => {
    prepareRessorts(props.ressorts, setPreparedRessorts, props.vorhabenart);
  }, [props.vorhabenart, props.ressorts, itemFromPKP]);

  useEffect(() => {
    if (props.ressort) {
      if (!props.form.getFieldValue(ressortsName)) {
        props.form.setFieldsValue({ [ressortsName]: [props.ressort] });
      }
    }
  }, [props.ressort]);

  return (
    <>
      <MultiChoiceComponent
        fieldName={ressortsName}
        fieldLabel={ressortsLabel}
        disabled={props.disabled}
        choices={preparedRessorts}
        listTitle={itemType}
        form={props.form}
        itemType={itemType}
        rules={[
          {
            required: true,
            message: t('rv.newRegelungsvorhaben.participants.ffRessort.error'),
          },
        ]}
      />
    </>
  );
}
