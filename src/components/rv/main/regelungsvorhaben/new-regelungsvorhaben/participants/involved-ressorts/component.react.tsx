// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { BASE_PATH, RegelungsvorhabenTypType, RessortDTO } from '@plateg/rest-api';
import { InfoComponent, MultiChoiceComponent } from '@plateg/theme';
import { ChoicesItemInterface } from '@plateg/theme/src/components/multi-choice/choices-list/component.react';

import { useRvState } from '../../../regelungsvorhaben-context/component.react';
import { prepareRessorts } from '../component.react';

const GGO_LINK = BASE_PATH + '/arbeitshilfen/download/34#page=';

export interface InvolvedRessortsComponentProps {
  form: FormInstance;
  ressorts?: RessortDTO[];
  vorhabenart: RegelungsvorhabenTypType;
}
export function InvolvedRessortsComponent(props: InvolvedRessortsComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const EVIR_PATH = history.createHref({ pathname: '/evir' });
  const ressortsLabel = (
    <span>
      {t('rv.newRegelungsvorhaben.participants.involvedRessorts.label')}
      <InfoComponent withLabelRequired title={t('rv.newRegelungsvorhaben.participants.involvedRessorts.hint.title')}>
        <span
          dangerouslySetInnerHTML={{
            __html: t('rv.newRegelungsvorhaben.participants.involvedRessorts.hint.content', {
              interpolation: { escapeValue: false },
              link_GGO45_Seite36: GGO_LINK + '36',
              link_GGO74_Seite55: GGO_LINK + '55',
              link_GGO45_Anlage6: GGO_LINK + '68',
              link_gesetz_113_materialrecherche:
                EVIR_PATH + '/gesetz/regierungsinitiative/phaseTitle1/vorueberlegungen11/materialrecherche113',
              link_gesetz_141_ressortabstimmung:
                EVIR_PATH +
                '/gesetz/regierungsinitiative/phaseTitle1/ressortabstimmungundweiterebeteiligungen14/ressortabstimmung141',
              link_rechtsverordnung_113_materialrecherche:
                EVIR_PATH +
                '/rechtsverordnung/rechtsverordnungDerBundesregierungOderEinesOderMehrererBundesminister/phaseTitle1/vorueberlegungen11/materialrecherche113',
              link_rechtsverordnung_141_ressortabstimmung:
                EVIR_PATH +
                '/rechtsverordnung/rechtsverordnungDerBundesregierungOderEinesOderMehrererBundesminister/phaseTitle1/ressortabstimmungundweiterebeteiligungen14/ressortabstimmung141',
              link_verwaltungsvorschrift_15_verfahren:
                EVIR_PATH +
                '/verwaltungsvorschrift/verwaltungsvorschrift/phaseTitle1/verfahrenbeimerlassvonverwaltungsvorschriften15',
            }),
          }}
        />
      </InfoComponent>
    </span>
  );
  const ressortsName = 'beteiligteRessorts';
  const itemType = t('rv.newRegelungsvorhaben.participants.involvedRessorts.itemType');
  const [preparedRessorts, setPreparedRessorts] = useState<ChoicesItemInterface[]>([]);
  const { itemFromPKP } = useRvState();
  useEffect(() => {
    prepareRessorts(props.ressorts, setPreparedRessorts, props.vorhabenart);
  }, [props.vorhabenart, props.ressorts, itemFromPKP]);

  return (
    <>
      <MultiChoiceComponent
        fieldName={ressortsName}
        fieldLabel={ressortsLabel}
        choices={preparedRessorts}
        listTitle={itemType}
        form={props.form}
        itemType={itemType}
        rules={[
          {
            required: false,
            message: '',
          },
        ]}
      />
    </>
  );
}
