// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Radio } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { InitiantType, RegelungsvorhabenTypType } from '@plateg/rest-api';

interface InitiatorComponentProps {
  form: FormInstance;
  vorhabenart: RegelungsvorhabenTypType;
}

export function InitiatorComponent(props: InitiatorComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const regelungsinitiativeOnly = props.vorhabenart !== RegelungsvorhabenTypType.Gesetz;

  useEffect(() => {
    if (regelungsinitiativeOnly) {
      props.form.setFieldsValue({ initiant: InitiantType.Bundesregierung });
    }
  }, [regelungsinitiativeOnly]);

  return (
    <>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t('rv.newRegelungsvorhaben.participants.initiator.label')}</legend>
        <Form.Item
          name="initiant"
          label={t('rv.newRegelungsvorhaben.participants.initiator.label')}
          rules={[
            { required: true, whitespace: true, message: t('rv.newRegelungsvorhaben.participants.initiator.error') },
          ]}
          initialValue={InitiantType.Bundesregierung}
        >
          <Radio.Group name={'radiogroup-' + uuidv4()} className="horizontal-radios">
            <Radio id="rv-newRegelungsvorhabenOptionBundesregierung-radio" value={InitiantType.Bundesregierung}>
              {t('rv.newRegelungsvorhaben.participants.initiator.options.bundesregierung')}
            </Radio>
            <Radio
              id="rv-newRegelungsvorhabenOptionBundestag-radio"
              value={InitiantType.Bundestag}
              disabled={regelungsinitiativeOnly}
            >
              {t('rv.newRegelungsvorhaben.participants.initiator.options.bundestag')}
            </Radio>
            <Radio
              id="rv-newRegelungsvorhabenOptionBundesrat-radio"
              value={InitiantType.Bundesrat}
              disabled={regelungsinitiativeOnly}
            >
              {t('rv.newRegelungsvorhaben.participants.initiator.options.bundesrat')}
            </Radio>
          </Radio.Group>
        </Form.Item>
      </fieldset>
    </>
  );
}
