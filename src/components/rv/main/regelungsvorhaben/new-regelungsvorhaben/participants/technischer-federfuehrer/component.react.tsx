// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../new-regelungsvorhaben.less';

import { FormInstance } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { RessortDTO } from '@plateg/rest-api';
import { CheckOutlined, FormItemWithInfo, InfoComponent, MainContentSelectWrapper } from '@plateg/theme';

export interface TechnischerFederfuehrerComponentProps {
  form: FormInstance;
  ressorts?: RessortDTO[];
  ressort?: string;
  disabled?: boolean;
  hidden?: boolean;
}

export function TechnischerFederfuehrerComponent(props: TechnischerFederfuehrerComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const disabled = props.disabled || !(props.ressorts?.length && props.ressorts.length > 1);

  useEffect(() => {
    if (props.ressorts?.length === 1) {
      props.form?.setFieldsValue({
        ['technischesFfRessort']: props.ressorts[0].id,
      });
    } else if (
      !props.ressorts?.find((entry) => {
        return entry.id === props.form.getFieldValue('technischesFfRessort');
      })
    ) {
      props.form?.setFieldsValue({
        ['technischesFfRessort']: undefined,
      });
    }
  }, [props.ressorts]);

  useEffect(() => {
    if (props.ressort) {
      props.form?.setFieldsValue({
        ['technischesFfRessort']: props.ressort,
      });
    } else {
      props.form?.setFieldsValue({
        ['technischesFfRessort']: undefined,
      });
    }
  }, [props.ressort]);

  return (
    <>
      <FormItemWithInfo
        name="technischesFfRessort"
        validateTrigger="onBlur"
        rules={[{ required: true, message: t('rv.newRegelungsvorhaben.participants.techFfRessort.error') }]}
        label={
          <span>
            {t('rv.newRegelungsvorhaben.participants.techFfRessort.label')}

            <InfoComponent
              withLabel
              withLabelRequired
              title={t('rv.newRegelungsvorhaben.participants.techFfRessort.hint.title')}
            >
              <p
                dangerouslySetInnerHTML={{
                  __html: t('rv.newRegelungsvorhaben.participants.techFfRessort.hint.content', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              />
            </InfoComponent>
          </span>
        }
      >
        <MainContentSelectWrapper
          id="technischesFfRessort"
          disabled={disabled}
          placeholder={t('rv.newRegelungsvorhaben.participants.techFfRessort.placeholder')}
          listHeight={256}
          menuItemSelectedIcon={<CheckOutlined />}
          options={props.ressorts?.map((item) => ({
            value: item.id,
            label: <span aria-label={item.bezeichnung}>{`${item.bezeichnung} (${item.kurzbezeichnung})`}</span>,
            title: `${item.bezeichnung} (${item.kurzbezeichnung})`,
          }))}
        />
      </FormItemWithInfo>
    </>
  );
}
