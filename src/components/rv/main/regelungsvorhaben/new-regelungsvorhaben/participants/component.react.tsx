// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Tag } from 'antd';
import Title from 'antd/lib/typography/Title';
import { CustomTagProps } from 'rc-select/lib/BaseSelect';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  CodelistenResponseDTO,
  RegelungsvorhabenEntityDTO,
  RegelungsvorhabenTypType,
  RessortDTO,
  RessortFullDTO,
} from '@plateg/rest-api';
import { CloseOutlined } from '@plateg/theme';
import { ChoicesItemInterface } from '@plateg/theme/src/components/multi-choice/choices-list/component.react';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { FederfuehrendeAbteilungReferatComponent } from './federfuehrende-abteilung-referat/component.react';
import { FederfuehrerComponent } from './federfuehrer/component.react';
import { InitiatorComponent } from './initiator/component.react';
import { InvolvedRessortsComponent } from './involved-ressorts/component.react';
import { TechnischerFederfuehrerComponent } from './technischer-federfuehrer/component.react';

export interface ParticipantsComponentProps {
  form: FormInstance;
  ressort?: string;
  codeList?: CodelistenResponseDTO;
  action: string;
  hidden?: boolean;
  isRvSentToPKP: boolean;
  rv?: RegelungsvorhabenEntityDTO;
}

export const participantTagRender = (tagProps: CustomTagProps): React.ReactElement => {
  const { label, closable, onClose } = tagProps;
  return (
    <Tag color="blue" closable={closable} onClose={onClose} style={{ marginRight: 3 }} closeIcon={<CloseOutlined />}>
      {label}
    </Tag>
  );
};

export const EXCLUDED_LIST = {
  [RegelungsvorhabenTypType.Gesetz]: ['BKM', 'BPräsA', 'BR', 'BT', 'BND', 'StBA', 'BAMF', 'BDBOS'],
  [RegelungsvorhabenTypType.Rechtsverordnung]: ['BPräsA', 'BR', 'BT', 'BND', 'StBA', 'BAMF', 'BDBOS'],
  [RegelungsvorhabenTypType.Verwaltungsvorschrift]: ['BND', 'StBA', 'BAMF', 'BDBOS'],
};

const SORT_RESSORTS_PATTERN = [
  'BKAmt',
  'BMWK',
  'BMF',
  'BMI',
  'AA',
  'BMJ',
  'BMAS',
  'BMVg',
  'BMEL',
  'BMFSFJ',
  'BMG',
  'BMDV',
  'BMUV',
  'BMBF',
  'BMZ',
  'BMWSB',
  'BKM',
  'BR',
  'BT',
  'BPräsA',
];

export const prepareRessorts = (
  initialRessorts: RessortDTO[] = [],
  setPreparedRessorts: (list: ChoicesItemInterface[]) => void,
  vorhabenart?: RegelungsvorhabenTypType,
) => {
  if (initialRessorts) {
    const filteredRessorts = vorhabenart
      ? initialRessorts.filter((item) => !EXCLUDED_LIST[vorhabenart].includes(item.kurzbezeichnung))
      : initialRessorts;
    const prepared = filteredRessorts
      .filter((ressort) => ressort.obersteBundesbehoerde)
      .sort(customSortRessorts)
      .map((ressort) => {
        return { label: `${ressort.bezeichnung} (${ressort.kurzbezeichnung})`, value: ressort.id };
      });
    setPreparedRessorts(prepared);
  }
};
export const customSortRessorts = (ressortA: RessortDTO, ressortB: RessortDTO) => {
  const indexA = SORT_RESSORTS_PATTERN.indexOf(ressortA.kurzbezeichnung);
  const indexB = SORT_RESSORTS_PATTERN.indexOf(ressortB.kurzbezeichnung);

  // If both elements 'a' and 'b' are not found in pattern, maintain their relative order
  if (indexA === -1 && indexB === -1) return 0;
  // If element 'a' is not found in pattern, place it after 'b'
  if (indexA === -1) return 1;
  // If element 'b' is not found in pattern, place it before 'a'
  if (indexB === -1) return -1;

  return indexA - indexB;
};

export function ParticipantsComponent(props: ParticipantsComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [federfuehrerDisabled, setFederfuehrerDisabled] = useState<boolean>(false);
  const [ressorstList, setRessorstList] = useState<RessortFullDTO[]>([]);
  const appStore = useAppSelector((state) => state.user);
  useEffect(() => {
    if (props.codeList?.ressorts) {
      // Remove Presse- und Informationsamt der Bundesregierung (BPA) from available ressorts
      const filteredList = props.codeList?.ressorts.filter((ressort) => ressort.kurzbezeichnung !== 'BPA');
      setRessorstList(filteredList);
    }
  }, [props.codeList?.ressorts]);

  useEffect(() => {
    if (appStore.user?.dto.stellvertreter && props.action === 'bearbeitenRegelungsvorhaben') {
      setFederfuehrerDisabled(true);
    }
  }, [appStore.user]);

  return (
    <div className="rv-participants">
      <Title level={2}>{t('rv.newRegelungsvorhaben.participants.title')}</Title>
      <Form.Item shouldUpdate noStyle>
        {() => (
          <InitiatorComponent
            form={props.form}
            vorhabenart={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
          />
        )}
      </Form.Item>
      <TechnischerFederfuehrerComponent
        ressort={props.rv?.technischesFfRessort || props.ressort}
        form={props.form}
        disabled={true}
        ressorts={ressorstList}
      />
      <FederfuehrendeAbteilungReferatComponent
        ressort={props.rv?.technischesFfRessort || props.ressort}
        form={props.form}
        ressorts={ressorstList}
        isRvSentToPKP={props.isRvSentToPKP}
        rv={props.rv}
      />
      <Form.Item shouldUpdate noStyle>
        {() => (
          <FederfuehrerComponent
            ressort={props.rv?.technischesFfRessort || props.ressort}
            ressorts={ressorstList}
            form={props.form}
            disabled={federfuehrerDisabled}
            vorhabenart={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
          />
        )}
      </Form.Item>
      <Form.Item shouldUpdate noStyle>
        {() => (
          <InvolvedRessortsComponent
            ressorts={ressorstList}
            form={props.form}
            vorhabenart={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
          />
        )}
      </Form.Item>
    </div>
  );
}
