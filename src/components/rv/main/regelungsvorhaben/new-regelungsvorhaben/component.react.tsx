// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './new-regelungsvorhaben.less';

import { Button, Col, Form, Row } from 'antd';
import Paragraph from 'antd/lib/typography/Paragraph';
import React, { useEffect, useState } from 'react';
import { flushSync } from 'react-dom';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  CodelistenResponseDTO,
  PkpGesendetStatusType,
  PkpWorkflowStatusType,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityDTO,
  RegelungsvorhabenEntityResponseDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  CodeListenController,
  CombinedTitle,
  displayMessage,
  DropdownMenu,
  ErrorBox,
  GeneralFormWrapper,
  HeaderController,
  InfoComponent,
  LoadingStatusController,
  RouteLeavingGuard,
  SaveOutlined,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../../shares/routes';
import { RvErrorController } from '../../../../../utils/rv-error-controller';
import { BreadcrumbTitle, ElementLink, TabLink } from '../../../component.react';
import { NewRegelungsvorhabenController } from '../controller';
import { useRvState } from '../regelungsvorhaben-context/component.react';
import { DesignationComponent } from './designation/component.react';
import { ParticipantsComponent } from './participants/component.react';
import { VorhabenartComponent } from './vorhabenart/component.react';

export interface NewRegelungsvorhabenComponentProps {
  regelungsvorhaben?: RegelungsvorhabenEntityDTO;
  setRegelungsvorhaben: (regelungsvorhaben: RegelungsvorhabenEntityDTO) => void;
  setRegelungsvorhabenId: (regelungsvorhabenId: string) => void;
  tabName: string;
  action: string;
  cachingAllowed: boolean;
  setCachingAllowed: (cachingAllowed: boolean) => void;
  id?: string;
}

export function NewRegelungsvorhabenComponent(props: NewRegelungsvorhabenComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const [isLeavingPageBlocked, setIsLeavingPageBlocked] = useState(true);
  const existingRvId = props.id;
  const tabName: string = props.tabName;
  const action: string = props.action;
  const errorMessage = (error: AjaxError) => {
    rvErrorCtrl.handleError(error);
    console.error('Error sub', error);
    loadingStatusController.setLoadingStatus(false);
  };

  const [form] = Form.useForm();
  const [codeList, setCodeList] = useState<CodelistenResponseDTO>();
  const [ressort, setRessort] = useState<string>();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const ctrl = GlobalDI.getOrRegister('rvNewRegelungsvorhabenController', () => new NewRegelungsvorhabenController());
  const codeListenCtrl = GlobalDI.getOrRegister('codeListenController', () => new CodeListenController());
  const rvCtrl = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  const headerController = GlobalDI.get<HeaderController>('rvHeadercontroller');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  const [allowedToModify, setAllowedToModify] = useState<boolean>(true);
  const [cachingEnabled, setCachingEnabled] = useState<boolean>(false);
  const [regelungsvorhabenStatus, setRegelungsvorhabenStatus] = useState<VorhabenStatusType>();
  const [isRvSentToPKP, setIsRvSentToPKP] = useState<boolean>(false);
  const appStore = useAppSelector((state) => state.user);
  const { itemFromPKP, setInitialRv } = useRvState();

  useEffect(() => {
    setBreadcrumb(props.regelungsvorhaben?.abkuerzung as string);
    let rvSub: Subscription;
    if (props.regelungsvorhaben) {
      form.setFieldsValue(props.regelungsvorhaben);
      props.setRegelungsvorhaben(props.regelungsvorhaben);
      setAllowedToModify(props.regelungsvorhaben.allowedToModify || true);
      setBreadcrumb(props.regelungsvorhaben.abkuerzung || '');
      setCodelistenAndRessort();
      setRegelungsvorhabenStatus(props.regelungsvorhaben.status);
      setIsRvSentToPKP(
        props.regelungsvorhaben.pkpStatus === PkpGesendetStatusType.Aktualisiert ||
          props.regelungsvorhaben.pkpStatus === PkpGesendetStatusType.Erstellt,
      );
    } else if (existingRvId) {
      const id = existingRvId;
      loadingStatusController.setLoadingStatus(true);
      rvSub = rvCtrl.getRegelungsvorhaben({ id }).subscribe({
        next: (data: RegelungsvorhabenEntityResponseDTO) => {
          const status =
            data.dto.status === VorhabenStatusType.Entwurf ? VorhabenStatusType.InBearbeitung : data.dto.status;
          form.setFieldsValue({ ...data.dto, status });
          props.setRegelungsvorhaben(data.dto);
          props.setRegelungsvorhabenId(existingRvId);
          setAllowedToModify(data.dto.allowedToModify);
          setBreadcrumb(data.dto.abkuerzung || '');
          setCodelistenAndRessort();
          setRegelungsvorhabenStatus(data.dto.status);
          setIsRvSentToPKP(
            data.dto.pkpStatus === PkpGesendetStatusType.Aktualisiert ||
              data.dto.pkpStatus === PkpGesendetStatusType.Erstellt,
          );
        },
        error: errorMessage,
      });
    }
    return () => {
      rvSub?.unsubscribe();
    };
  }, [existingRvId]);

  useEffect(() => {
    if (!existingRvId && !props.regelungsvorhaben && appStore.user) {
      setCodelistenAndRessort();
    }
  }, [appStore.user]);

  useEffect(() => {
    const rightHeaderButtons = [];
    if (props.cachingAllowed) {
      rightHeaderButtons.push(
        <Button
          id="rv-newRegelungsvorhaben-continueLater-btn"
          key="zwischenspeichern-button"
          onClick={cacheRv}
          disabled={!cachingEnabled}
        >
          {t('rv.newRegelungsvorhaben.btns.continueLater.bntText')}
        </Button>,
      );
      rightHeaderButtons.push(
        <DropdownMenu
          items={[
            {
              element: t('rv.newRegelungsvorhaben.btns.continueLater.bntText'),
              onClick: () => {
                cacheRv();
              },
              disabled: () => !cachingEnabled,
            },
          ]}
          elementId={'headerRightAlternative'}
          overlayClass={'headerRightAlternative-overlay'}
        />,
      );
    }
    headerController.setHeaderProps({
      headerRight: rightHeaderButtons,
    });
  }, [props.cachingAllowed, cachingEnabled]);

  useEffect(() => {
    setCachingAllowedForRV();
  }, [regelungsvorhabenStatus, itemFromPKP]);

  const setCachingAllowedForRV = () => {
    if (!existingRvId || (regelungsvorhabenStatus === VorhabenStatusType.Entwurf && !itemFromPKP)) {
      props.setCachingAllowed(true);
    }
  };

  useEffect(() => {
    if (itemFromPKP) {
      props.setCachingAllowed(false);
    } else {
      setCachingAllowedForRV();
    }
  }, [itemFromPKP]);

  const setCodelistenAndRessort = () => {
    loadingStatusController.setLoadingStatus(true);
    const codeListsub: Subscription = codeListenCtrl.getCodeListen().subscribe({
      next: (codeListResponse: CodelistenResponseDTO) => {
        setCodeList(codeListResponse);
        setRessort(appStore.user?.dto.ressort?.id);
        loadingStatusController.setLoadingStatus(false);
      },
      error: errorMessage,
    });
    return () => {
      codeListsub?.unsubscribe();
    };
  };

  const setBreadcrumb = (abkuerzung: string) => {
    const breadcrumbsItems = [];
    const tabLink = <TabLink tabName={tabName} />;
    breadcrumbsItems.push(tabLink);
    if (props.id) {
      const elementLink = <ElementLink tabName={tabName} elId={props.id} abkuerzung={abkuerzung} />;
      breadcrumbsItems.push(elementLink);
    }
    const breadcrumbTitle = <BreadcrumbTitle abkuerzung={''} action={action} />;
    breadcrumbsItems.push(breadcrumbTitle);
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[...breadcrumbsItems]} />],
    });
  };

  const saveRv = (successMsg: string, shouldGotoReviewPage = false) => {
    const formVals = form.getFieldsValue() as RegelungsvorhabenEntityDTO;
    formVals.status = VorhabenStatusType.InBearbeitung;
    // join old regelungsvorhaben with new from form just to be sure what pkp fields are not lost
    props.setRegelungsvorhaben({ ...props.regelungsvorhaben, ...formVals });
    if (shouldGotoReviewPage) {
      goToReviewPage();
    }
  };

  useEffect(() => {
    if (!props.regelungsvorhaben) {
      setInitialRv({ ...form.getFieldsValue(true), technischesFfRessort: ressort });
    }
  }, [null, ressort]);

  const cacheRv = () => {
    setIsLeavingPageBlocked(false);
    const formVals = form.getFieldsValue() as RegelungsvorhabenEntityDTO;
    formVals.status = VorhabenStatusType.Entwurf;
    loadingStatusController.setLoadingStatus(true);
    const apiCall = existingRvId ? ctrl.modifyRvCall(formVals, existingRvId) : ctrl.saveRvCall(formVals);
    apiCall.subscribe({
      next: (rv: RegelungsvorhabenEntityResponseDTO) => {
        loadingStatusController.setLoadingStatus(false);
        displayMessage(t('rv.newRegelungsvorhaben.msgs.successCached'), 'success');
        setCachingEnabled(false);

        const editLocationPath = `/regelungsvorhaben/${routes.ENTWURFE}/${rv.base.id}/${routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF}`;
        if (history.location.pathname !== editLocationPath) {
          history.push(editLocationPath);
        }
      },
      error: errorMessage,
    });
  };

  const goToReviewPage = () => {
    flushSync(() => {
      setIsLeavingPageBlocked(false);
    });
    action === routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF
      ? history.push(`/regelungsvorhaben/${tabName}/${routes.PRUEFEN_REGELUNGSVORHABEN_ENTWURF}`)
      : history.push(`/regelungsvorhaben/${tabName}/${routes.PRUEFEN_REGELUNGSVORHABEN}`);
  };

  const onFinishFailed = () => {
    document?.getElementById('errorBox')?.focus();
  };

  const onFormValuesChanged = () => {
    setIsLeavingPageBlocked(true);
    setCachingEnabled(true);
  };

  const onFinish = () => {
    const successMsg = existingRvId
      ? t('rv.newRegelungsvorhaben.msgs.successUpdatedMsg')
      : t('rv.newRegelungsvorhaben.msgs.successCreatedMsg');
    saveRv(successMsg, true);
  };

  const isPKPWorkflowStatusUVorKMGGV =
    props.regelungsvorhaben?.pkpWorkflowStatus === PkpWorkflowStatusType.Uv ||
    props.regelungsvorhaben?.pkpWorkflowStatus === PkpWorkflowStatusType.Kmggv;
  return (
    <Row className="rv-form">
      {allowedToModify && (
        <Col
          xs={{ span: 22, offset: 1 }}
          md={{ span: 14, offset: 2 }}
          lg={{ span: 16, offset: 3 }}
          xl={{ span: 12, offset: 3 }}
          xxl={{ span: 9, offset: 8 }}
        >
          <RouteLeavingGuard
            navigate={(path: string) => history.push(path)}
            shouldBlockNavigation={() => {
              return isLeavingPageBlocked;
            }}
            title={t('rv.newRegelungsvorhaben.confirmModal.title')}
            btnOk={t('rv.newRegelungsvorhaben.confirmModal.btnCancelConfirmOk')}
            btnCancel={t('rv.newRegelungsvorhaben.confirmModal.btnCancelConfirmNo')}
          />
          <GeneralFormWrapper
            form={form}
            layout="vertical"
            scrollToFirstError={true}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            className="rv-form"
            onValuesChange={onFormValuesChanged}
            onFieldsChange={onFormValuesChanged}
            noValidate
          >
            <CombinedTitle
              title={props.regelungsvorhaben?.abkuerzung}
              suffix={t(`rv.newRegelungsvorhaben.${action}.mainTitle`)}
            />
            <p className="ant-typography p-no-style">
              Pflichtfelder sind mit einem <sup>*</sup> gekennzeichnet.
            </p>
            <Form.Item shouldUpdate noStyle>
              {() => <ErrorBox fieldsErrors={form.getFieldsError()} />}
            </Form.Item>
            <DesignationComponent
              isPKPWorkflowStatusUVorKMGGV={isPKPWorkflowStatusUVorKMGGV}
              form={form}
              ressort={ressort}
              isRvSentToPKP={isRvSentToPKP}
              codeList={codeList}
              existingRvId={existingRvId}
            />
            <ParticipantsComponent
              codeList={codeList}
              ressort={ressort}
              form={form}
              action={props.action}
              isRvSentToPKP={isRvSentToPKP}
              rv={props.regelungsvorhaben}
            />
            <VorhabenartComponent form={form} codeList={codeList} ressort={ressort} isRvSentToPKP={isRvSentToPKP} />

            <div className="form-control-buttons">
              <Button id="rv-newRegelungsvorhaben-submit-btn" type="primary" htmlType="submit" size={'large'}>
                {t('rv.newRegelungsvorhaben.btns.submit')}
              </Button>

              <Button
                id="rv-newRegelungsvorhaben-cancel-btn"
                size={'large'}
                onClick={() => history.push(`/regelungsvorhaben/${tabName}`)}
              >
                {t('rv.newRegelungsvorhaben.btns.cancel')}
              </Button>
            </div>
            {props.cachingAllowed && (
              <>
                <hr />
                <Paragraph>
                  {t('rv.newRegelungsvorhaben.btns.continueLater.title')}
                  <InfoComponent title={t('rv.newRegelungsvorhaben.btns.continueLater.hint.title')}>
                    <p>{t('rv.newRegelungsvorhaben.btns.continueLater.hint.content')}</p>
                  </InfoComponent>
                  &nbsp;&nbsp;&nbsp;
                  <Button
                    id="rv-newRegelungsvorhaben-zwischenspeichern-btn"
                    type="text"
                    className="blue-text-button"
                    disabled={!cachingEnabled}
                    icon={<SaveOutlined />}
                    onClick={cacheRv}
                  >
                    {t('rv.newRegelungsvorhaben.btns.continueLater.bntText')}
                  </Button>
                </Paragraph>
              </>
            )}
          </GeneralFormWrapper>
        </Col>
      )}
    </Row>
  );
}
