// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import i18n from 'i18next';
import React from 'react';

import { RegelungsvorhabenBundesregierungTableDTO, RegelungsvorhabenPpkTableDTO } from '@plateg/rest-api';
import { CheckOutlined, TableComponentProps } from '@plateg/theme';

export function getRvsInPKPTableVals(
  content: RegelungsvorhabenPpkTableDTO[],
  applySelectedItem: (item: RegelungsvorhabenPpkTableDTO) => void,
  selectdItem: RegelungsvorhabenPpkTableDTO | undefined,
): TableComponentProps<RegelungsvorhabenPpkTableDTO> {
  const columns: ColumnsType<RegelungsvorhabenPpkTableDTO> = [
    {
      title: i18n.t('rv.newRegelungsvorhaben.designation.rvsInPKP.columns.kurzbezeichnung').toString(),
      key: 'kurzbezeichnung',
      fixed: 'left',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): React.ReactElement => {
        return <span>{record.kurzbezeichnung}</span>;
      },
    },
    {
      title: i18n.t('rv.newRegelungsvorhaben.designation.rvsInPKP.columns.vorhabenart').toString(),
      key: 'vorhabenart',
      fixed: 'left',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): React.ReactElement => {
        return (
          <span>
            {i18n
              .t(`rv.newRegelungsvorhaben.vorhabenart.art.options.${(record.vorhabenart || '').toLowerCase()}`)
              .toString()}
          </span>
        );
      },
    },
    {
      title: i18n.t('rv.newRegelungsvorhaben.designation.rvsInPKP.columns.aktion').toString(),
      key: 'aktion',
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        return (
          <div className="actions-holder">
            {selectdItem && record.id === selectdItem.id ? (
              <CheckOutlined />
            ) : (
              <Button
                type="link"
                aria-label={`${record.kurzbezeichnung} verwenden`}
                onClick={() => applySelectedItem(record)}
                disabled={selectdItem !== undefined}
              >
                {i18n.t('rv.newRegelungsvorhaben.designation.rvsInPKP.btnApply').toString()}
              </Button>
            )}
          </div>
        );
      },
    },
  ];

  return {
    id: '',
    expandable: false,
    columns,
    content,
  };
}
