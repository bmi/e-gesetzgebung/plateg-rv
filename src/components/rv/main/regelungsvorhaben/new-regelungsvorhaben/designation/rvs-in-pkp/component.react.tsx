// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './rvs-in-pkp.less';

import { Button, FormInstance, Image, Switch, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { RegelungsvorhabenControllerApi, RegelungsvorhabenPpkTableDTO } from '@plateg/rest-api';
import {
  CloseOutlined,
  GlobalDI,
  ImageModule,
  InfoComponent,
  LoadingStatusController,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { RvErrorController } from '../../../../../../../utils/rv-error-controller';
import { useRvState } from '../../../regelungsvorhaben-context/component.react';
import { getRvsInPKPTableVals } from './controller.react';

export interface VorhabenartComponentProps {
  form: FormInstance;
}

export function RvsInPKPComponent(props: VorhabenartComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [isChecked, setIsChecked] = useState(false);
  const { itemFromPKP, setItemFromPKP, initialRv } = useRvState();
  const rvCtrl = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const appStore = useAppSelector((state) => state.user);
  const errorMessage = (error: AjaxError) => {
    rvErrorCtrl.handleError(error);
    console.error('Error sub', error);
    loadingStatusController.setLoadingStatus(false);
  };
  const [noItemsFromPKP, setNoItemsFromPKP] = useState('');
  const [rvsData, setRvsData] = useState<RegelungsvorhabenPpkTableDTO[]>([]);
  const [rvsTableVals, setRvsTableVals] = useState<TableComponentProps<RegelungsvorhabenPpkTableDTO>>({
    id: 'Regelungsvorhaben PKP Tabelle',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const deleteMsg = t(`theme.mail.msg.delete`);
  const defaultImage = {
    imgSrc:
      (require('../../../../../../../media/empty-content-images/myRegelungsvorhaben/empty-state-table.svg') as ImageModule) ||
      '',
    height: 215,
    imgAlt: t('rv.newRegelungsvorhaben.designation.rvsInPKP.noItems'),
  };

  useEffect(() => {
    setIsChecked(!!itemFromPKP);
  }, []);

  useEffect(() => {
    let sub: Subscription;
    if (isChecked) {
      loadingStatusController.setLoadingStatus(true);
      sub = rvCtrl.getRegelungsvorhabenFromPkpList().subscribe({
        next: (data: RegelungsvorhabenPpkTableDTO[]) => {
          setNoItemsFromPKP(!data.length ? t(`rv.newRegelungsvorhaben.designation.rvsInPKP.noItems`) : '');
          setRvsData(data);
          setRvsTableVals(getRvsInPKPTableVals(data, applySelectedItem, itemFromPKP));
          loadingStatusController.setLoadingStatus(false);
        },
        error: errorMessage,
      });
    }
    return () => {
      sub?.unsubscribe();
    };
  }, [isChecked]);

  useEffect(() => {
    setRvsTableVals(getRvsInPKPTableVals(rvsData, applySelectedItem, itemFromPKP));
  }, [rvsData, itemFromPKP]);

  const onChange = (checked: boolean) => {
    setIsChecked(checked);
    if (!checked) {
      setNoItemsFromPKP('');
      removeSelectedItem();
    }
  };
  const applySelectedItem = (item: RegelungsvorhabenPpkTableDTO) => {
    setItemFromPKP(item);
    props.form.setFieldsValue({
      ...item,
    });
  };

  const removeSelectedItem = () => {
    props.form.setFieldsValue({
      ...initialRv,
      kurzbeschreibung: '',
      kurzbezeichnung: '',
      langtitel: '',
      abkuerzung: '',
      grundtyp: undefined,
    });
    setItemFromPKP(undefined);
  };

  return (
    <div className="rvs-in-pkp">
      <div className="switch-holder">
        <Switch
          className="switch-component"
          checked={isChecked}
          onChange={onChange}
          aria-label={t(`rv.newRegelungsvorhaben.designation.rvsInPKP.switchLabel`)}
          disabled={appStore.user?.dto.stellvertreter}
        />
        <span>{t(`rv.newRegelungsvorhaben.designation.rvsInPKP.switchLabel`)}</span>
        <InfoComponent
          isContactPerson={false}
          title={t(`rv.newRegelungsvorhaben.designation.rvsInPKP.drawerSwitchTitle`)}
        >
          <p>{t(`rv.newRegelungsvorhaben.designation.rvsInPKP.drawerSwitchText`)}</p>
          <p>{t(`rv.newRegelungsvorhaben.designation.rvsInPKP.drawerSwitchTextStellvertreter`)}</p>
        </InfoComponent>
      </div>

      {isChecked && rvsTableVals.content.length > 0 && (
        <>
          <div className={'table-component'}>
            <Table
              key="pkp-table"
              columns={rvsTableVals.columns}
              dataSource={rvsTableVals.content}
              pagination={{ defaultPageSize: 10, position: ['bottomCenter'] }}
              rowKey={(record) => record.id}
            />
          </div>
          <div className="selected-item">
            <strong>
              {t(`rv.newRegelungsvorhaben.designation.rvsInPKP.selectedItem.title`)}
              <InfoComponent
                isContactPerson={false}
                title={t(`rv.newRegelungsvorhaben.designation.rvsInPKP.selectedItem.title`)}
              >
                {t(`rv.newRegelungsvorhaben.designation.rvsInPKP.selectedItem.drawerText`)}
              </InfoComponent>
            </strong>
            <div>
              {itemFromPKP ? (
                <div className={`tag-element`} id={`selected-address-${itemFromPKP.id}`}>
                  {itemFromPKP.kurzbezeichnung}
                  <Button
                    className="btn-close"
                    type="text"
                    onClick={removeSelectedItem}
                    aria-label={`${deleteMsg} ${itemFromPKP.kurzbezeichnung}`}
                    id={`delete-address-${itemFromPKP.id}`}
                  >
                    <CloseOutlined />
                  </Button>
                </div>
              ) : (
                <p>{t(`rv.newRegelungsvorhaben.designation.rvsInPKP.selectedItem.noItem`)}</p>
              )}
            </div>
          </div>
        </>
      )}

      {noItemsFromPKP && (
        <div className="no-items">
          <Image
            preview={false}
            src={defaultImage.imgSrc.default}
            height={defaultImage.height}
            alt={defaultImage.imgAlt}
            loading={'lazy'}
          />
          <div className="subheadline-description">
            <p className="ant-typography p-no-style">{noItemsFromPKP}</p>
          </div>
        </div>
      )}
    </div>
  );
}
