// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox, Form, FormInstance, Radio } from 'antd';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { BeteiligungBundesratType, RegelungsvorhabenTypType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

export interface BeteiligungsBundesratComponentProps {
  form: FormInstance;
  selectedType?: RegelungsvorhabenTypType;
}
export function BeteiligungsBundesratComponent(props: BeteiligungsBundesratComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const CheckboxGroup = Checkbox.Group;
  const [checkedList, setCheckedList] = useState<CheckboxValueType[]>([]);
  const beteiligungBundesratRadioList = [
    BeteiligungBundesratType.Einspruchsgesetz,
    BeteiligungBundesratType.Zustimmungsgesetz,
    BeteiligungBundesratType.BeteiligungserfordernisSpaeterFestlegen,
  ];
  const EVIR_PATH = history.createHref({ pathname: '/evir' });
  const link_gg = (id: string) => {
    return `https://www.gesetze-im-internet.de/gg/art_${id}.html`;
  };

  return (
    <Form.Item
      name="beteiligungBundesrat"
      label={
        <span className="label-text">
          {t('rv.newRegelungsvorhaben.beteiligungBundesrat.title')}{' '}
          <InfoComponent title={t('rv.newRegelungsvorhaben.beteiligungBundesrat.hint.title')}>
            <span
              dangerouslySetInnerHTML={{
                __html: t('rv.newRegelungsvorhaben.beteiligungBundesrat.hint.content', {
                  interpolation: { escapeValue: false },
                  link_art79gg: link_gg('79'),
                  link_art80gg: link_gg('80'),
                  link_art84gg: link_gg('84'),
                  link_art85gg: link_gg('85'),
                  link_art86gg: link_gg('86'),
                  link_art104agg: link_gg('104a'),
                  link_art105gg: link_gg('105'),
                  link_art108gg: link_gg('108'),
                  link_evir22beratungbundesrat:
                    EVIR_PATH + '/gesetz/regierungsinitiative/phaseTitle2/beratungbundesrat22',
                  link_evir18befassungbundesrat:
                    EVIR_PATH +
                    '/rechtsverordnung/rechtsverordnungDerBundesregierungOderEinesOderMehrererBundesminister/phaseTitle1/bundesrat18',
                  link_evir152befassungbundesrat:
                    EVIR_PATH +
                    '/verwaltungsvorschrift/verwaltungsvorschrift/phaseTitle1/verfahrenbeimerlassvonverwaltungsvorschriften15/beteiligungdesbundesrates152',
                }),
              }}
            />
          </InfoComponent>
        </span>
      }
    >
      {props.selectedType === RegelungsvorhabenTypType.Gesetz && (
        <Radio.Group name={'radiogroup-beteiligungBundesrat'} className="horizontal-radios">
          {beteiligungBundesratRadioList.map((item) => {
            return (
              <Radio key={item} id={`rv-${item}-radio`} value={item}>
                {t(`rv.newRegelungsvorhaben.beteiligungBundesrat.options.${item}.label`)}
              </Radio>
            );
          })}
        </Radio.Group>
      )}
      {props.selectedType !== RegelungsvorhabenTypType.Gesetz && (
        <CheckboxGroup
          value={checkedList}
          onChange={(list: CheckboxValueType[]) => {
            setCheckedList(list);
            if (list.length) {
              props.form.setFieldsValue({ ['beteiligungBundesrat']: list[0] as BeteiligungBundesratType });
            }
          }}
          role="list"
        >
          <Checkbox
            aria-label={t(
              `rv.newRegelungsvorhaben.beteiligungBundesrat.options.${BeteiligungBundesratType.Zustimmungerfordernis}.label`,
            )}
            value={BeteiligungBundesratType.Zustimmungerfordernis}
          >
            {t(
              `rv.newRegelungsvorhaben.beteiligungBundesrat.options.${BeteiligungBundesratType.Zustimmungerfordernis}.label`,
            )}
          </Checkbox>
        </CheckboxGroup>
      )}
    </Form.Item>
  );
}
