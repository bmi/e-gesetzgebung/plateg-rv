// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../new-regelungsvorhaben.less';

import { Input } from 'antd';
import { Rule } from 'antd/lib/form';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Constants, FormItemWithInfo, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

interface CustomRvInputProps {
  name: string;
  identifier: string;
  required: boolean;
  links: string[];
  isTextArea: boolean;
  isPKPWorkflowStatusUVorKMGGV?: boolean;
}

export function CustomRvInput(props: CustomRvInputProps): React.ReactElement {
  const { t } = useTranslation();

  const generateMaxCharRule = (identifier: string) => {
    const maxChars = props.isTextArea ? Constants.TEXT_AREA_LENGTH : Constants.TEXT_BOX_LENGTH;
    return {
      max: maxChars,
      message: t('rv.newRegelungsvorhaben.designation.regDesignation.lengthError', {
        maxChars: Filters.numberToDecimalString(maxChars),
        fieldname: t(`rv.newRegelungsvorhaben.designation.${identifier}.label`),
      }),
    };
  };
  const rules: Rule[] = [generateMaxCharRule(props.identifier)];
  if (props.required) {
    rules.push({
      required: true,
      whitespace: true,
      message: t(`rv.newRegelungsvorhaben.designation.${props.identifier}.error`),
    });
  }

  const bezOrKurzBez = props.name === 'langtitel' || props.name === 'kurzbezeichnung';
  const enableInfoIconPkp = props.isPKPWorkflowStatusUVorKMGGV && bezOrKurzBez;
  const disableInfoIcon = enableInfoIconPkp ? false : props.isPKPWorkflowStatusUVorKMGGV;

  const transformListToObject = (list: string[]): { [key: string]: string } => {
    const result: { [key: string]: string } = {};
    for (let i = 0; i < list.length; i++) {
      result[`link${i + 1}`] = list[i];
    }
    return result;
  };

  return (
    <FormItemWithInfo
      disabled={disableInfoIcon}
      name={props.name}
      label={
        <span>
          {t(`rv.newRegelungsvorhaben.designation.${props.identifier}.label`)}
          <InfoComponent
            withLabel
            withLabelRequired={props.required}
            title={t(`rv.newRegelungsvorhaben.designation.${props.identifier}.hint.title`)}
          >
            {enableInfoIconPkp ? (
              <p>{t(`rv.newRegelungsvorhaben.designation.${props.identifier}.hint.contentInfoDisabled`)}</p>
            ) : (
              <p
                dangerouslySetInnerHTML={{
                  __html: t(`rv.newRegelungsvorhaben.designation.${props.identifier}.hint.content`, {
                    interpolation: { escapeValue: false },
                    ...transformListToObject(props.links),
                  }),
                }}
              />
            )}
          </InfoComponent>
        </span>
      }
      rules={rules}
    >
      <Input disabled={props.isPKPWorkflowStatusUVorKMGGV} />
    </FormItemWithInfo>
  );
}
