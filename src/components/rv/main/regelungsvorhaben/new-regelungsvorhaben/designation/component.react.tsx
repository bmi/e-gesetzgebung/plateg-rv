// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  BeteiligungBundesratType,
  BeteiligungBundestagType,
  CodelistenResponseDTO,
  RegelungsvorhabenTypType,
} from '@plateg/rest-api';
import { Constants, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { RvTypeComponent } from '../vorhabenart/rv-type/component.react';
import { BeteiligungsBundesratComponent } from './beteiligungs-bundesrat/component.react';
import { BeteiligungsBundestagComponent } from './beteiligungs-bundestag/component.react';
import { CustomRvInput } from './custom-rv-input/component.react';
import { RvsInPKPComponent } from './rvs-in-pkp/component.react';

interface DesignationComponentProps {
  isPKPWorkflowStatusUVorKMGGV?: boolean;
  form: FormInstance;
  ressort?: string;
  isRvSentToPKP: boolean;
  codeList?: CodelistenResponseDTO;
  existingRvId?: string;
}

export function DesignationComponent(props: DesignationComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [selectedType, setSelectedType] = useState<RegelungsvorhabenTypType>(RegelungsvorhabenTypType.Gesetz);

  useEffect(() => {
    const defaultValue = selectedType === RegelungsvorhabenTypType.Gesetz ? [BeteiligungBundestagType.Bundestag] : [];
    const defaultValueBundesrat =
      selectedType === RegelungsvorhabenTypType.Gesetz
        ? BeteiligungBundesratType.BeteiligungserfordernisSpaeterFestlegen
        : null;
    props.form.setFieldsValue({
      beteiligungBundestag: defaultValue,
      beteiligungBundesrat: defaultValueBundesrat,
    });
  }, [selectedType]);

  return (
    <>
      <Title level={2}>{t('rv.newRegelungsvorhaben.designation.title')}</Title>
      {!props.existingRvId && <RvsInPKPComponent form={props.form} />}
      <RvTypeComponent
        selectedRessorts={props.form.getFieldValue('allFfRessorts') as string[]}
        form={props.form}
        ressort={props.ressort}
        isRvSentToPKP={props.isRvSentToPKP}
        updateSelectedType={setSelectedType}
      />
      <CustomRvInput
        name="langtitel"
        required={true}
        identifier="amtlDesignation"
        links={['https://hdr.bmj.de/page_c.1.html#an_324']}
        isTextArea={true}
        isPKPWorkflowStatusUVorKMGGV={props.isPKPWorkflowStatusUVorKMGGV}
      />
      <CustomRvInput
        name="kurzbezeichnung"
        required={true}
        identifier="regDesignation"
        links={[
          'https://www.hdr.bmj.de/page_c.1.html#an_331',
          'https://www.hdr.bmj.de/page_d.2.html#an_508',
          'https://www.hdr.bmj.de/page_d.3.html#an_532',
          'https://www.hdr.bmj.de/page_d.4.html#an_724',
          'https://www.hdr.bmj.de/page_d.5.html',
        ]}
        isTextArea={true}
        isPKPWorkflowStatusUVorKMGGV={props.isPKPWorkflowStatusUVorKMGGV}
      />
      <CustomRvInput
        name="abkuerzung"
        required={true}
        identifier="amtlAbbreviation"
        links={[
          'https://www.hdr.bmj.de/page_c.1.html#an_341',
          'https://www.hdr.bmj.de/page_d.2.html#an_509',
          'https://www.hdr.bmj.de/page_d.3.html#an_533',
          'https://www.hdr.bmj.de/page_d.4.html#an_729',
          'https://www.hdr.bmj.de/page_d.5.html',
        ]}
        isTextArea={false}
      />
      <Form.Item
        name="kurzbeschreibung"
        label={
          <span className="label-text">
            {t('rv.newRegelungsvorhaben.designation.shortDescription.label')}{' '}
            <InfoComponent title={t('rv.newRegelungsvorhaben.designation.shortDescription.hint.title')}>
              <p>{t('rv.newRegelungsvorhaben.designation.shortDescription.hint.content')}</p>
            </InfoComponent>
          </span>
        }
        rules={[
          {
            max: Constants.TEXT_AREA_LENGTH,
            message: t('rv.newRegelungsvorhaben.designation.regDesignation.lengthError', {
              maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
              fieldname: t('rv.newRegelungsvorhaben.designation.shortDescription.label'),
            }),
          },
        ]}
      >
        <TextArea autoSize={{ minRows: 4, maxRows: 4 }} />
      </Form.Item>

      <Form.Item shouldUpdate noStyle>
        {() => (
          <>
            <BeteiligungsBundestagComponent
              form={props.form}
              selectedType={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
            />
            <BeteiligungsBundesratComponent
              form={props.form}
              selectedType={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
            />
          </>
        )}
      </Form.Item>
    </>
  );
}
