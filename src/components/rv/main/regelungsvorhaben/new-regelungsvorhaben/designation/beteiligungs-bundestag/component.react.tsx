// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './beteiligungs-bundestag.less';

import { Checkbox, Form, FormInstance } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { BeteiligungBundestagType, RegelungsvorhabenTypType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

export interface BeteiligungsBundestagComponentProps {
  form: FormInstance;
  selectedType?: RegelungsvorhabenTypType;
  initialType?: RegelungsvorhabenTypType;
}
export function BeteiligungsBundestagComponent(props: BeteiligungsBundestagComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const CheckboxGroup = Checkbox.Group;
  const [checkedList, setCheckedList] = useState<BeteiligungBundestagType[]>([]);
  const rechtsverordnungList = [
    BeteiligungBundestagType.Kenntnisvorbehalt,
    BeteiligungBundestagType.Zustimmungsvorbehalt,
    BeteiligungBundestagType.Ablehnungsvorbehalt,
    BeteiligungBundestagType.Aufhebungsvorbehalt,
  ];
  const EVIR_PATH = history.createHref({ pathname: '/evir' });

  return (
    <Form.Item
      name="beteiligungBundestag"
      id="beteiligungBundestag"
      label={
        <span className="label-text">
          {t('rv.newRegelungsvorhaben.beteiligungBundestag.title')}{' '}
          <InfoComponent title={t('rv.newRegelungsvorhaben.beteiligungBundestag.hint.title')}>
            <span
              dangerouslySetInnerHTML={{
                __html: t('rv.newRegelungsvorhaben.beteiligungBundestag.hint.content', {
                  interpolation: { escapeValue: false },
                  link_evir21beratungbundestag:
                    EVIR_PATH + '/gesetz/regierungsinitiative/phaseTitle2/beratungbundestag21',
                  link_evir17befassungbundestag:
                    EVIR_PATH +
                    '/rechtsverordnung/rechtsverordnungDerBundesregierungOderEinesOderMehrererBundesminister/phaseTitle1/bundestag17',
                }),
              }}
            />
          </InfoComponent>
        </span>
      }
      hidden={props.selectedType === RegelungsvorhabenTypType.Verwaltungsvorschrift}
    >
      <CheckboxGroup
        value={checkedList}
        onChange={(list: BeteiligungBundestagType[]) => {
          setCheckedList(list);
        }}
        role="list"
        className="beteiligungBundestag-checkboxes-list"
      >
        {props.selectedType === RegelungsvorhabenTypType.Gesetz && (
          <Checkbox
            disabled={true}
            checked
            aria-label={`${t(
              `rv.newRegelungsvorhaben.beteiligungBundestag.options.${BeteiligungBundestagType.Bundestag}.label`,
            )} 1 von 1`}
            value={BeteiligungBundestagType.Bundestag}
          >
            {t(`rv.newRegelungsvorhaben.beteiligungBundestag.options.${BeteiligungBundestagType.Bundestag}.label`)}
          </Checkbox>
        )}
        {props.selectedType === RegelungsvorhabenTypType.Rechtsverordnung && (
          <>
            {rechtsverordnungList.map((item, index) => {
              return (
                <Checkbox
                  key={item}
                  aria-label={`${t(
                    `rv.newRegelungsvorhaben.beteiligungBundestag.options.${item}.label`,
                  )} ${index + 1} von ${rechtsverordnungList.length}`}
                  value={item}
                >
                  {t(`rv.newRegelungsvorhaben.beteiligungBundestag.options.${item}.label`)}
                </Checkbox>
              );
            })}
          </>
        )}
      </CheckboxGroup>
    </Form.Item>
  );
}
