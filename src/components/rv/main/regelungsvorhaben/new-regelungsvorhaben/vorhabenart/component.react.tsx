// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  BesondereUmstaendeType,
  CodelistenResponseDTO,
  CodelisteType,
  GrundtypType,
  RegelungsvorhabenTypType,
} from '@plateg/rest-api';
import { HinweisComponent } from '@plateg/theme';

import { useRvState } from '../../regelungsvorhaben-context/component.react';
import { SubjectAreasComponent } from '../participants/subject-areas/component.react';
import { BaseTypeSelectComponent } from './base-type-select/component.react';
import { EuLawComponent } from './eu-law/component.react';
import { SpecialCircumstancesSelectComponent } from './special-circumstances/component.react';

export interface VorhabenartComponentProps {
  codeList?: CodelistenResponseDTO;
  form: FormInstance;
  ressort?: string;
  isRvSentToPKP: boolean;
}

export function VorhabenartComponent(props: VorhabenartComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { itemFromPKP } = useRvState();
  return (
    <div className="rv-participants">
      <Title level={2}>{t('rv.newRegelungsvorhaben.vorhabenart.title')}</Title>
      {itemFromPKP?.besondereUmstaende?.some((item) => item === BesondereUmstaendeType.GesetzEilbeduerftigeVorlage) ? (
        <HinweisComponent
          mode="warning"
          content={<span>{t(`rv.newRegelungsvorhaben.designation.rvsInPKP.besondereUmstaende.hinweis.content`)}</span>}
        />
      ) : (
        ''
      )}
      <Form.Item shouldUpdate noStyle>
        {() => (
          <>
            <BaseTypeSelectComponent
              basetype={props.codeList?.codelisten.filter((dto) => {
                return (
                  dto.type === CodelisteType.GrundTypGesetz ||
                  dto.type === CodelisteType.GrundTypRechtsverordnung ||
                  dto.type === CodelisteType.GrundTypVerwaltungsvorschrift
                );
              })}
              vorhabenart={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
              form={props.form}
            />
          </>
        )}
      </Form.Item>
      <Form.Item shouldUpdate noStyle>
        {() => (
          <>
            <EuLawComponent
              vorhabenart={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
              form={props.form}
            />
            <SubjectAreasComponent
              subjectAreas={props.codeList?.codelisten.find((dto) => {
                return dto.type === CodelisteType.Sachgebiet;
              })}
              form={props.form}
            />
            <SpecialCircumstancesSelectComponent
              specialCircumstances={props.codeList?.codelisten.filter((dto) => {
                return (
                  dto.type === CodelisteType.BesondereUmstaendeGesetz ||
                  dto.type === CodelisteType.BesondereUmstaendeRechtsverordnung ||
                  dto.type === CodelisteType.BesondereUmstaendeVerwaltungsvorschrift
                );
              })}
              vorhabenart={props.form.getFieldValue('vorhabenart') as RegelungsvorhabenTypType}
              grundtyp={props.form.getFieldValue('grundtyp') as GrundtypType}
              form={props.form}
            />
          </>
        )}
      </Form.Item>
    </div>
  );
}
