// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Rule } from 'antd/lib/form';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { PairStringString } from '@plateg/rest-api';
import { CheckOutlined, CloseOutlined, FormItemWithInfo, InfoComponent, MainContentSelectWrapper } from '@plateg/theme';

export interface CustomRvSelectProps {
  name: string;
  identifier: string;
  required?: boolean;
  items: PairStringString[] | undefined;
  disabled: boolean;
}

export function defaultIfBlank(s: string): string {
  return s?.trim() || '';
}

export function CustomRvSelect(props: CustomRvSelectProps): React.ReactElement {
  const { t } = useTranslation();

  let rules: Rule[] = [];
  if (props.required) {
    rules = [
      {
        required: true,
        whitespace: true,
        message: t(`rv.newRegelungsvorhaben.vorhabenart.${props.identifier}.error`),
      },
    ];
  }
  return (
    <FormItemWithInfo
      className={props.disabled ? 'select-disabled' : ''}
      name={props.name}
      rules={rules}
      disabled={props.disabled}
      label={
        <span>
          {t(`rv.newRegelungsvorhaben.vorhabenart.${props.identifier}.label`)}

          <InfoComponent
            withLabelRequired
            title={t(`rv.newRegelungsvorhaben.vorhabenart.${props.identifier}.hint.title`)}
          >
            <span
              dangerouslySetInnerHTML={{
                __html: t(`rv.newRegelungsvorhaben.vorhabenart.${props.identifier}.hint.content`, {
                  interpolation: { escapeValue: false },
                }),
              }}
            />
          </InfoComponent>
        </span>
      }
    >
      <MainContentSelectWrapper
        id={props.name}
        disabled={props.disabled}
        placeholder={t(`rv.newRegelungsvorhaben.vorhabenart.${props.identifier}.placeholder`)}
        listHeight={256}
        menuItemSelectedIcon={<CheckOutlined />}
        allowClear={!props.required}
        clearIcon={<CloseOutlined />}
        options={props.items?.map((item) => {
          const label = String(Object.values(item)[0]);
          return {
            label: <span aria-label={defaultIfBlank(label)}>{label}</span>,
            value: Object.keys(item)[0],
            title: label,
          };
        })}
      />
    </FormItemWithInfo>
  );
}
