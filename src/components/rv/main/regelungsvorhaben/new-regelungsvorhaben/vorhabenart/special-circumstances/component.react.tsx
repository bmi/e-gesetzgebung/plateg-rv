// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  BesondereUmstaendeType,
  CodelisteDTO,
  GrundtypType,
  PairStringString,
  RegelungsvorhabenTypType,
} from '@plateg/rest-api';
import { InfoComponent, MultiChoiceComponent } from '@plateg/theme';
import { ChoicesItemInterface } from '@plateg/theme/src/components/multi-choice/choices-list/component.react';

import { useRvState } from '../../../regelungsvorhaben-context/component.react';

export interface SpecialCircumstancesSelectComponentProps {
  specialCircumstances?: CodelisteDTO[];
  vorhabenart: RegelungsvorhabenTypType;
  form: FormInstance;
  grundtyp: GrundtypType;
}
export function SpecialCircumstancesSelectComponent(
  props: SpecialCircumstancesSelectComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const [items, setItems] = useState<PairStringString[] | undefined>();
  const [isContentVisible, setIsContentVisible] = useState(false);
  const exceptionsList = [GrundtypType.GesetzAenderungGrundgesetz, GrundtypType.GesetzUebertragungHoheit];
  const itemsToExclude = [
    BesondereUmstaendeType.GesetzNichtZutreffend,
    BesondereUmstaendeType.GesetzAlsEilbeduerftigeVorlage,
    BesondereUmstaendeType.VerwaltungsvorschriftNichtZutreffend,
    BesondereUmstaendeType.RechtsverordnungNichtZutreffend,
  ];
  const { itemFromPKP } = useRvState();

  useEffect(() => {
    if ((props.form.getFieldValue('besondereUmstaende') as PairStringString[])?.length) {
      setIsContentVisible(true);
    } else {
      setIsContentVisible(false);
    }
  }, [props.form.getFieldValue('besondereUmstaende')]);

  useEffect(() => {
    let preparedList = props.specialCircumstances?.find(
      (codeListe) => codeListe.type.toString() === 'BESONDERE_UMSTAENDE_' + props.vorhabenart,
    )?.values;

    if (exceptionsList.includes(props.grundtyp)) {
      itemsToExclude.push(BesondereUmstaendeType.GesetzEilbeduerftigeVorlage);
      const valueBesondereUmstaende: BesondereUmstaendeType[] = props.form.getFieldValue('besondereUmstaende') || [];
      if (valueBesondereUmstaende.includes(BesondereUmstaendeType.GesetzEilbeduerftigeVorlage)) {
        props.form.setFieldsValue({
          besondereUmstaende: valueBesondereUmstaende.filter(
            (item) => item !== BesondereUmstaendeType.GesetzEilbeduerftigeVorlage,
          ),
        });
      }
    }
    preparedList = preparedList?.filter((item) => {
      const parsedObject = parseObject(item);
      return !itemsToExclude.some((itemToExclude) => itemToExclude === parsedObject.key);
    });
    setItems(preparedList);
    prepareRessorts(preparedList || []);
  }, [props.specialCircumstances, props.vorhabenart, props.grundtyp, itemFromPKP]);

  useEffect(() => {
    if (items !== undefined && !items?.includes(props.form.getFieldValue('besondereUmstaende') as PairStringString)) {
      props.form.setFieldsValue({ besondereUmstaende: [] });
    }
  }, [props.vorhabenart]);

  const [preparedList, setPreparedList] = useState<ChoicesItemInterface[]>([]);
  const prepareRessorts = (list: PairStringString[]) => {
    if (list) {
      const prepared = list?.map((item) => {
        const parsedObj = parseObject(item);

        return { label: parsedObj.value as string, value: parsedObj.key as string };
      });
      setPreparedList(prepared || []);
    }
  };

  const parseObject = (obj: Object) => {
    let itemValue;
    let itemKey;
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        itemKey = key;
        itemValue = obj[key] as string;
      }
    }
    return {
      key: itemKey,
      value: itemValue,
    };
  };

  const specialCircumstancesLabel = (
    <span>
      {t('rv.newRegelungsvorhaben.vorhabenart.specialCircumstances.label')}
      <InfoComponent withLabelRequired title={t('rv.newRegelungsvorhaben.vorhabenart.specialCircumstances.hint.title')}>
        <span
          dangerouslySetInnerHTML={{
            __html: t('rv.newRegelungsvorhaben.vorhabenart.specialCircumstances.hint.content', {
              interpolation: { escapeValue: false },
            }),
          }}
        />
      </InfoComponent>
    </span>
  );
  const itemType = t('rv.newRegelungsvorhaben.vorhabenart.specialCircumstances.itemType');

  return (
    <>
      <Button
        type="link"
        onClick={() => setIsContentVisible(true)}
        id={`btn-show-specialCircumstances`}
        disabled={!props.grundtyp}
        hidden={isContentVisible}
        size="large"
      >
        {t('rv.newRegelungsvorhaben.vorhabenart.specialCircumstances.btnAdd')}
      </Button>

      <div hidden={!isContentVisible}>
        <MultiChoiceComponent
          fieldName={'besondereUmstaende'}
          fieldLabel={specialCircumstancesLabel}
          disabled={false}
          choices={preparedList}
          listTitle={itemType}
          form={props.form}
          itemType={itemType}
          rules={[
            {
              required: false,
              message: '',
            },
          ]}
          isCheckAllHidden={true}
        />
      </div>
    </>
  );
}
