// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Radio } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EuVeranlasstType, RegelungsvorhabenTypType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

export interface EuLawComponentProps {
  vorhabenart: RegelungsvorhabenTypType;
  form: FormInstance;
}

export function EuLawComponent(props: EuLawComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [isDisabled, setIsDisabled] = useState<boolean>(false);
  useEffect(() => {
    if (
      props.vorhabenart === RegelungsvorhabenTypType.Gesetz ||
      props.vorhabenart === RegelungsvorhabenTypType.Rechtsverordnung
    ) {
      setIsDisabled(false);
    } else {
      props.form.setFieldsValue({ euVeranlasst: EuVeranlasstType.Leer });
      setIsDisabled(true);
    }
  }, [props.vorhabenart]);

  return (
    <>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.label')}</legend>
        <Form.Item
          name="euVeranlasst"
          initialValue={EuVeranlasstType.Leer}
          label={
            <span>
              {t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.label')}
              <InfoComponent title={t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.hint.title')}>
                <span
                  dangerouslySetInnerHTML={{
                    __html: t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.hint.content', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </span>
          }
        >
          <Radio.Group name="radiogroup-eu-veranlasst" className="horizontal-radios" disabled={isDisabled}>
            <Radio
              id="rv-newRegelungsvorhabenDeadlinesOptionVorbereitung-euVeranlasst-radio"
              value={EuVeranlasstType.Vorbereitung}
            >
              {t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.options.vorbereitung')}
            </Radio>
            <Radio
              id={'rv-newRegelungsvorhabenDeadlinesOptionTrue-euVeranlasst-radio'}
              value={EuVeranlasstType.Umsetzung}
            >
              {t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.options.umsetzung')}
            </Radio>
            <Radio id={'rv-newRegelungsvorhabenDeadlinesOptionFalse-euVeranlasst-radio'} value={EuVeranlasstType.Leer}>
              {t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.options.leer')}
            </Radio>
          </Radio.Group>
        </Form.Item>
      </fieldset>
    </>
  );
}
