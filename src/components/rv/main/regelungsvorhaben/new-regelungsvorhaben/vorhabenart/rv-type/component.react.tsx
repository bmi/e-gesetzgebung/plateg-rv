// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { RegelungsvorhabenTypType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

export interface RvTypeComponentProps {
  form: FormInstance;
  selectedRessorts: string[];
  ressort?: string;
  isRvSentToPKP: boolean;
  updateSelectedType: (type: RegelungsvorhabenTypType) => void;
}
export function RvTypeComponent(props: RvTypeComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t('rv.newRegelungsvorhaben.vorhabenart.art.label')}</legend>
        <Form.Item
          name="vorhabenart"
          label={
            <span>
              {t('rv.newRegelungsvorhaben.vorhabenart.art.label')}
              <span style={{ position: 'absolute', marginLeft: '10px' }}>
                <InfoComponent title={t('rv.newRegelungsvorhaben.vorhabenart.art.hint.title')}>
                  <p>{t('rv.newRegelungsvorhaben.vorhabenart.art.hint.content')}</p>
                </InfoComponent>
              </span>
            </span>
          }
          rules={[{ required: true, whitespace: true, message: t('rv.newRegelungsvorhaben.vorhabenart.art.error') }]}
          initialValue={RegelungsvorhabenTypType.Gesetz}
        >
          <Radio.Group
            name={'radiogroup-' + uuidv4()}
            className="horizontal-radios"
            onChange={(e) => {
              props.updateSelectedType(e.target.value as RegelungsvorhabenTypType);
            }}
            disabled={props.isRvSentToPKP}
          >
            <Radio
              id="rv-newRegelungsvorhabenVorhabenartOptionGesetz-radio"
              disabled={props.isRvSentToPKP}
              value={RegelungsvorhabenTypType.Gesetz}
              checked={true}
            >
              {t('rv.newRegelungsvorhaben.vorhabenart.art.options.gesetz')}
            </Radio>
            <Radio
              id="rv-newRegelungsvorhabenVorhabenartOptionRechtsverordnung-radio"
              disabled={props.isRvSentToPKP}
              value={RegelungsvorhabenTypType.Rechtsverordnung}
            >
              {t('rv.newRegelungsvorhaben.vorhabenart.art.options.rechtsverordnung')}
            </Radio>
            <Radio
              id="rv-newRegelungsvorhabenVorhabenartOptionVerwaltungsvorschrift-radio"
              disabled={props.isRvSentToPKP}
              value={RegelungsvorhabenTypType.Verwaltungsvorschrift}
            >
              {t('rv.newRegelungsvorhaben.vorhabenart.art.options.verwaltungsvorschrift')}
            </Radio>
          </Radio.Group>
        </Form.Item>
      </fieldset>
    </>
  );
}
