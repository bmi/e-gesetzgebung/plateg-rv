// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';

import { CodelisteDTO, PairStringString, RegelungsvorhabenTypType } from '@plateg/rest-api';

import { CustomRvSelect } from '../custom-rv-select/component.react';

export interface BaseTypeSelectComponentProps {
  basetype?: CodelisteDTO[];
  vorhabenart: RegelungsvorhabenTypType;
  form: FormInstance;
}
export function BaseTypeSelectComponent(props: BaseTypeSelectComponentProps): React.ReactElement {
  const disabled = !props.vorhabenart;
  const [items, setItems] = useState<PairStringString[] | undefined>();

  useEffect(() => {
    setItems(
      props.basetype?.find((codeListe) => codeListe.type.toString() === 'GRUND_TYP_' + props.vorhabenart)?.values,
    );
  }, [props.basetype, props.vorhabenart]);

  useEffect(() => {
    if (items !== undefined && !items?.includes(props.form.getFieldValue('grundtyp') as PairStringString)) {
      props.form.setFieldsValue({ grundtyp: undefined });
    }
  }, [props.vorhabenart]);
  return (
    <>
      <CustomRvSelect name="grundtyp" required={true} items={items} disabled={disabled} identifier="baseType" />
    </>
  );
}
