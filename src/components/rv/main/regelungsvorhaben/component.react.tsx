// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState } from 'react';
import { Route, Switch } from 'react-router';

import { RegelungsvorhabenEntityDTO } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';
import { CreateRegelungsvorhabenComponent } from './create-regelungsvorhaben/component.react';
import { EditRegelungsvorhabenComponent } from './edit-regelungsvorhaben/component.react';
import { PruefenRegelungsvorhabenComponent } from './pruefen-regelungsvorhaben/component.react';
import { RvStateProvider } from './regelungsvorhaben-context/component.react';

export function CreationRoutesRv(): React.ReactElement {
  const [regelungsvorhaben, setRegelungsvorhaben] = useState<RegelungsvorhabenEntityDTO | undefined>(undefined);
  const [regelungsvorhabenId, setRegelungsvorhabenId] = useState<string | null>(null);
  const [cachingAllowed, setCachingAllowed] = useState(false);
  return (
    <RvStateProvider>
      <Switch>
        <Route exact path={[`/regelungsvorhaben/:tabName/${routes.NEUES_REGELUNGSVORHABEN}`]}>
          <CreateRegelungsvorhabenComponent
            regelungsvorhaben={regelungsvorhaben}
            setRegelungsvorhaben={setRegelungsvorhaben}
            setRegelungsvorhabenId={setRegelungsvorhabenId}
            cachingAllowed={cachingAllowed}
            setCachingAllowed={setCachingAllowed}
          />
        </Route>
        <Route
          exact
          path={[
            `/regelungsvorhaben/:tabName/:id/${routes.NEUES_REGELUNGSVORHABEN}`,
            `/regelungsvorhaben/:tabName/:id/${routes.BEARBEITEN_REGELUNGSVORHABEN}`,
            `/regelungsvorhaben/:tabName/:id/${routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF}`,
          ]}
        >
          <EditRegelungsvorhabenComponent
            regelungsvorhaben={regelungsvorhaben}
            setRegelungsvorhaben={setRegelungsvorhaben}
            setRegelungsvorhabenId={setRegelungsvorhabenId}
            cachingAllowed={cachingAllowed}
            setCachingAllowed={setCachingAllowed}
          />
        </Route>
        <Route
          exact
          path={[
            `/regelungsvorhaben/:tabName/${routes.PRUEFEN_REGELUNGSVORHABEN}`,
            `/regelungsvorhaben/:tabName/${routes.PRUEFEN_REGELUNGSVORHABEN_ENTWURF}`,
          ]}
        >
          <PruefenRegelungsvorhabenComponent
            regelungsvorhaben={regelungsvorhaben as RegelungsvorhabenEntityDTO}
            regelungsvorhabenId={regelungsvorhabenId}
          />
        </Route>
      </Switch>
    </RvStateProvider>
  );
}
