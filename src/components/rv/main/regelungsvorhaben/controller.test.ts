// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
import sinon from 'sinon';

import {
  CodelistenControllerApi,
  CodelistenResponseDTO,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityDTO,
  RegelungsvorhabenEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { isRelevantKurzbezeichnung, NewRegelungsvorhabenController } from './controller';

describe('TEST: NewRegelungsvorhabenController', () => {
  const ctrl = GlobalDI.getOrRegister('newRegelungsvorhabenController', () => new NewRegelungsvorhabenController());
  const regelungsvorhabenController = GlobalDI.getOrRegister<RegelungsvorhabenControllerApi>(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );
  const codelistenControllerApi = GlobalDI.getOrRegister(
    'codelistenControllerApi',
    () => new CodelistenControllerApi(),
  );
  const getCodelistenStub = sinon.stub(codelistenControllerApi, 'getCodelisten');
  const createRegelungsvorhabenStub = sinon.stub(regelungsvorhabenController, 'createRegelungsvorhaben');
  const modifyRegelungsvorhabenStub = sinon.stub(regelungsvorhabenController, 'modifyRegelungsvorhaben');

  after(() => {
    getCodelistenStub.reset();
    createRegelungsvorhabenStub.reset();
    modifyRegelungsvorhabenStub.reset();
  });

  it('TEST: getCodelistenStub success - check if correct function is called', () => {
    getCodelistenStub.callsFake(() => {
      return new Observable<AjaxResponse<CodelistenResponseDTO>>((observer) => {
        observer.next();
      });
    });
    ctrl.getCodelistenCall();
    sinon.assert.calledOnce(getCodelistenStub);
  });

  it('TEST: saveRvCall success - check if correct function is called', () => {
    createRegelungsvorhabenStub.callsFake(() => {
      return new Observable<AjaxResponse<RegelungsvorhabenEntityResponseDTO>>((observer) => {
        observer.next();
      });
    });
    ctrl.saveRvCall({} as RegelungsvorhabenEntityDTO);
    sinon.assert.calledOnce(createRegelungsvorhabenStub);
  });

  it('TEST: modifyRegelungsvorhaben success - check if correct function is called', () => {
    modifyRegelungsvorhabenStub.callsFake(() => {
      return new Observable<AjaxResponse<RegelungsvorhabenEntityResponseDTO>>((observer) => {
        observer.next();
      });
    });
    ctrl.modifyRvCall({} as RegelungsvorhabenEntityDTO, '0');
    sinon.assert.calledOnce(modifyRegelungsvorhabenStub);
  });
});

describe('TEST: isRelevantKurzbezeichnung', () => {
  it('should return true for relevant kurzbezeichnung "BT"', () => {
    const result = isRelevantKurzbezeichnung('BT');
    expect(result).to.be.true;
  });

  it('should return true for relevant kurzbezeichnung "BR"', () => {
    const result = isRelevantKurzbezeichnung('BR');
    expect(result).to.be.true;
  });

  it('should return false for irrelevant kurzbezeichnung "XYZ"', () => {
    const result = isRelevantKurzbezeichnung('XYZ');
    expect(result).to.be.false;
  });

  it('should return false for undefined kurzbezeichnung', () => {
    const result = isRelevantKurzbezeichnung(undefined);
    expect(result).to.be.false;
  });

  it('should return false for empty string kurzbezeichnung', () => {
    const result = isRelevantKurzbezeichnung('');
    expect(result).to.be.false;
  });

  it('should handle nullish values properly', () => {
    const result = isRelevantKurzbezeichnung(null as unknown as string | undefined);
    expect(result).to.be.false;
  });
});
