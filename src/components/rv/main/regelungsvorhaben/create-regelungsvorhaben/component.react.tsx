// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useRouteMatch } from 'react-router';

import { RegelungsvorhabenEntityDTO } from '@plateg/rest-api';

import { NewRegelungsvorhabenComponent } from '../new-regelungsvorhaben/component.react';

export interface CreateRegelungsvorhabenComponentProps {
  regelungsvorhaben?: RegelungsvorhabenEntityDTO;
  setRegelungsvorhaben: (regelungsvorhaben: RegelungsvorhabenEntityDTO) => void;
  setRegelungsvorhabenId: (regelungsvorhabenId: string) => void;
  cachingAllowed: boolean;
  setCachingAllowed: (cachingAllowed: boolean) => void;
}

export function CreateRegelungsvorhabenComponent(props: CreateRegelungsvorhabenComponentProps): React.ReactElement {
  const routeMatcher = useRouteMatch<{ tabName: string; action: string }>('/regelungsvorhaben/:tabName/:action');

  const tabName = routeMatcher?.params.tabName ?? '';
  const action = routeMatcher?.params.action ?? '';
  return (
    <NewRegelungsvorhabenComponent
      regelungsvorhaben={props.regelungsvorhaben}
      setRegelungsvorhaben={props.setRegelungsvorhaben}
      setRegelungsvorhabenId={props.setRegelungsvorhabenId}
      tabName={tabName}
      action={action}
      cachingAllowed={props.cachingAllowed}
      setCachingAllowed={props.setCachingAllowed}
    />
  );
}
