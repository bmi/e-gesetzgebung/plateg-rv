// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';

import {
  CodelistenControllerApi,
  CodelistenResponseDTO,
  GetTagesordnungsrelevanzRequest,
  PkpGesendetStatusType,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityDTO,
  RegelungsvorhabenEntityResponseDTO,
  TagesordnungsrelevanzDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

export const relevantKurzbezeichnungen = ['BT', 'BR'];
export const isRelevantKurzbezeichnung = (kurzbezeichnung: string | undefined): boolean => {
  return relevantKurzbezeichnungen.includes(kurzbezeichnung ?? '');
};

export class NewRegelungsvorhabenController {
  private readonly regelungsvorhabenController = GlobalDI.getOrRegister(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );

  private readonly codelistenControllerApi = GlobalDI.getOrRegister(
    'codelistenControllerApi',
    () => new CodelistenControllerApi(),
  );

  public getCodelistenCall(): Observable<CodelistenResponseDTO> {
    return this.codelistenControllerApi.getCodelisten();
  }

  public saveRvCall(
    regelungsvorhaben: RegelungsvorhabenEntityDTO,
    pkpRvId?: string,
  ): Observable<RegelungsvorhabenEntityResponseDTO> {
    return this.regelungsvorhabenController.createRegelungsvorhaben({
      regelungsvorhabenEntityDTO: regelungsvorhaben,
      pkpRvId,
    });
  }

  public modifyRvCall(
    regelungsvorhaben: RegelungsvorhabenEntityDTO,
    id: string,
  ): Observable<RegelungsvorhabenEntityResponseDTO> {
    return this.regelungsvorhabenController.modifyRegelungsvorhaben({
      regelungsvorhabenEntityDTO: regelungsvorhaben,
      id: id,
    });
  }

  public sendRVToPKPCall(id: string): Observable<PkpGesendetStatusType> {
    return this.regelungsvorhabenController.sendRvToPkp({
      id: id,
    });
  }

  public getTagesordnungsRelevanzCall(dto: GetTagesordnungsrelevanzRequest): Observable<TagesordnungsrelevanzDTO> {
    return this.regelungsvorhabenController.getTagesordnungsrelevanz(dto);
  }
}
