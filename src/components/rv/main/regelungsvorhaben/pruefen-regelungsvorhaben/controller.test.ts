// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { should, use } from 'chai';
import * as chaiArrays from 'chai-arrays';

import {
  BeteiligungBundesratType,
  BeteiligungBundestagType,
  CodelistenResponseDTO,
  CodelisteType,
  InitiantType,
  RegelungsvorhabenTypType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { RVPruefenController } from './controller';

use(chaiArrays.default);

const ctrl = GlobalDI.getOrRegister('RVPruefenController', () => new RVPruefenController());
const codeliste: CodelistenResponseDTO = {
  codelisten: [
    {
      type: CodelisteType.Beteiligungserfordernis,
      values: [{ BUNDESTAG: BeteiligungBundestagType.Bundestag }],
    },
    {
      type: CodelisteType.Initiant,
      values: [
        {
          BUNDESTAG: BeteiligungBundestagType.Bundestag,
        },
        {
          BUNDESRAT: InitiantType.Bundesrat,
        },
        {
          BUNDESREGIERUNG: InitiantType.Bundesregierung,
        },
      ],
    },
    { type: CodelisteType.BesondereUmstaendeVerwaltungsvorschrift, values: [] },
  ],
  ressorts: [
    {
      id: '0',
      kurzbezeichnung: 'BMVg',
      bezeichnung: 'Bundesministerium der Verteidigung',
      aktiv: true,
      abteilungen: [],
    },
    {
      id: '1',
      kurzbezeichnung: 'BMI',
      bezeichnung: 'Bundesministerium des Innern und für Heimat',
      aktiv: true,
      abteilungen: [
        {
          id: '7ceba0c6-d693-4d70-8862-7c0cc29032e8',
          bezeichnung: 'D',
          unterabteilungen: [],
          referate: [],
        },
        {
          id: '161af421-516c-4d12-bc6f-2ea14ebf5b35',
          bezeichnung: 'G',
          unterabteilungen: [],
          referate: [],
        },
        {
          id: '14a67ec6-7077-4366-8e40-a58db2907274',
          bezeichnung: 'Z',
          unterabteilungen: [
            {
              id: '8d3dccdb-4003-4880-bdf5-0a80b108135d',
              bezeichnung: 'Z II',
              unterabteilungen: [],
              referate: [
                {
                  id: '0d488328-8009-4ae1-8a89-aaca9505e08d',
                  bezeichnung: 'Z II 1',
                },
                {
                  id: '3d6ce95a-0af4-414c-aef4-69b9d36378fd',
                  bezeichnung: 'Z II 2',
                },
              ],
            },
            {
              id: 'be04e225-bd50-44d1-b107-7a8b92598ecc',
              bezeichnung: 'Z I',
              unterabteilungen: [],
              referate: [],
            },
            {
              id: 'e0a8e60d-4405-4064-a80c-e284df3793b2',
              bezeichnung: 'Z III',
              unterabteilungen: [],
              referate: [],
            },
          ],
          referate: [],
        },
      ],
    },
    {
      id: '2',
      kurzbezeichnung: 'BKAmt',
      bezeichnung: 'Bundeskanzleramt',
      aktiv: true,
      abteilungen: [
        {
          id: 'cf94e3c3-e526-4eed-8108-31a0d7414a21',
          bezeichnung: '3',
          unterabteilungen: [
            {
              id: '04191bfd-ac49-4d91-adc7-4deeb62d3fc6',
              bezeichnung: '32',
              unterabteilungen: [],
              referate: [],
            },
            {
              id: '9fcbd2aa-ad66-44e7-8f2f-c69ec5dac815',
              bezeichnung: '31',
              unterabteilungen: [],
              referate: [],
            },
          ],
          referate: [
            {
              id: '06768090-99ab-47e0-8343-2f4027516b3a',
              bezeichnung: '33',
            },
          ],
        },
        {
          id: '00ac7d67-8d87-4cc0-a0d3-714d1f05a891',
          bezeichnung: '1',
          unterabteilungen: [
            {
              id: '666c0dd3-d1c7-4c41-ae74-6814174082c4',
              bezeichnung: '11',
              unterabteilungen: [],
              referate: [],
            },
          ],
          referate: [],
        },
        {
          id: 'db108d4c-1ef0-49a7-af63-e51fa694c163',
          bezeichnung: '2',
          unterabteilungen: [
            {
              id: '552c719b-44a4-4775-8f8f-c6a17268c0b0',
              bezeichnung: '21',
              unterabteilungen: [],
              referate: [
                {
                  id: '7328f67d-7717-4eaf-b786-9de19ec4aebc',
                  bezeichnung: '211',
                },
                {
                  id: 'bdbe4c4b-dcca-49af-a04b-f7ed835d1df5',
                  bezeichnung: '212',
                },
              ],
            },
            {
              id: 'd8bbf4d3-3b5e-4d75-8d63-48d973e65e20',
              bezeichnung: '22',
              unterabteilungen: [],
              referate: [
                {
                  id: '8a937d24-a8c9-46b6-a5c2-7089957c7594',
                  bezeichnung: '221',
                },
                {
                  id: '3e74c80b-2859-487d-9cbf-20cb45d1dd73',
                  bezeichnung: '222',
                },
              ],
            },
          ],
          referate: [],
        },
      ],
    },
  ],
};

describe('TEST getRessortNames', () => {
  it('getRessortNames returns correct names, ids exist', () => {
    const result = ctrl.getRessortNames(codeliste.ressorts, ['0', '1', '2']);
    should().equal(
      result,
      'Bundesministerium der Verteidigung, Bundesministerium des Innern und für Heimat, Bundeskanzleramt',
    );
  });
  it('getRessortNames returns correct names, one id exists', () => {
    const result = ctrl.getRessortNames(codeliste.ressorts, ['0', '4', '5']);
    should().equal(result, 'Bundesministerium der Verteidigung');
  });
  it('getRessortNames returns undefined, no id exists', () => {
    const result = ctrl.getRessortNames(codeliste.ressorts, ['9', '4', '5']);
    should().equal(result, undefined);
  });
  it('getRessortNames returns undefined, no codeList passed in', () => {
    const result = ctrl.getRessortNames(undefined, ['0', '1', '2']);
    should().equal(result, undefined);
  });
  it('getRessortNames returns undefined, no ids passed in', () => {
    const result = ctrl.getRessortNames(codeliste.ressorts);
    should().equal(result, undefined);
  });
  it('getRessortNames returns undefined, nothing passed in', () => {
    const result = ctrl.getRessortNames();
    should().equal(result, undefined);
  });
});

describe('TEST getCodeNames', () => {
  it('getRessortNames returns correct name (one), enums exist, codeList not undefined, type exists', () => {
    const result = ctrl.getCodeNames(
      [BeteiligungBundestagType.Bundestag],
      codeliste,
      CodelisteType.Beteiligungserfordernis,
    );
    should().equal(result, BeteiligungBundestagType.Bundestag);
  });
  it('getRessortNames returns correct names (two), enums exist, codeList not undefined, type exists', () => {
    const result = ctrl.getCodeNames(
      [BeteiligungBundestagType.Bundestag, InitiantType.Bundesrat],
      codeliste,
      CodelisteType.Initiant,
    );
    should().equal(result, `${BeteiligungBundestagType.Bundestag}, ${InitiantType.Bundesrat}`);
  });
  it('getRessortNames returns undefined, enums undefined', () => {
    const result = ctrl.getCodeNames(undefined, codeliste, CodelisteType.BesondereUmstaendeRechtsverordnung);
    should().equal(result, undefined);
  });
  it('getRessortNames returns undefined, codeList undefined', () => {
    const result = ctrl.getCodeNames(
      [BeteiligungBundesratType.BeteiligungserfordernisSpaeterFestlegen],
      undefined,
      CodelisteType.BesondereUmstaendeRechtsverordnung,
    );
    should().equal(result, undefined);
  });
  it('getRessortNames returns undefined, type undefined', () => {
    const result = ctrl.getCodeNames(
      [BeteiligungBundesratType.BeteiligungserfordernisSpaeterFestlegen],
      codeliste,
      undefined,
    );
    should().equal(result, undefined);
  });
});

describe('TEST getBesondereUmstaendeType', () => {
  it('getBesondereUmstaendeType returns correct string for RegelungsvorhabenTypType.Gesetz', () => {
    const result = ctrl.getBesondereUmstaendeType(RegelungsvorhabenTypType.Gesetz);
    should().equal(result, CodelisteType.BesondereUmstaendeGesetz);
  });
  it('getBesondereUmstaendeType returns correct string for RegelungsvorhabenTypType.Rechtsverordnung', () => {
    const result = ctrl.getBesondereUmstaendeType(RegelungsvorhabenTypType.Rechtsverordnung);
    should().equal(result, CodelisteType.BesondereUmstaendeRechtsverordnung);
  });
  it('getBesondereUmstaendeType returns correct string for RegelungsvorhabenTypType.Verwaltungsvorschrift', () => {
    const result = ctrl.getBesondereUmstaendeType(RegelungsvorhabenTypType.Verwaltungsvorschrift);
    should().equal(result, CodelisteType.BesondereUmstaendeVerwaltungsvorschrift);
  });
});
