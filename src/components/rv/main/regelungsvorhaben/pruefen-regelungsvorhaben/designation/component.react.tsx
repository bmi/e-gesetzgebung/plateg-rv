// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenEntityFullDTO, RegelungsvorhabenTypType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

import { PruefenRegelungsvorhabenComponentProps } from '../component.react';

export function DesignationPruefenRegelungsvorhabenComponent(
  props: Readonly<PruefenRegelungsvorhabenComponentProps>,
): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const regelungsvorhaben = props.regelungsvorhaben as RegelungsvorhabenEntityFullDTO;
  const noAnswer = <em>{t('rv.na')}</em>;
  const noAnswerDatenblattNr = <em>{t('rv.naDatenblattNr')}</em>;

  return (
    <section className="review-section">
      <Title level={2}>{t('rv.newRegelungsvorhaben.designation.title')}</Title>
      <dl>
        <dt>{t('rv.newRegelungsvorhaben.vorhabenart.art.label')}</dt>
        <dd>
          <strong>
            {regelungsvorhaben?.vorhabenart !== undefined && regelungsvorhaben?.vorhabenart !== null
              ? t(`rv.newRegelungsvorhaben.vorhabenart.art.options.${regelungsvorhaben.vorhabenart.toLowerCase()}`)
              : noAnswer}
          </strong>
        </dd>
        <dt>{t('rv.newRegelungsvorhaben.designation.amtlDesignation.label')}</dt>
        <dd className="dd-breakable-word">
          <strong>{regelungsvorhaben?.langtitel !== undefined ? regelungsvorhaben.langtitel : noAnswer}</strong>
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.designation.regDesignation.label')}</dt>
        <dd className="dd-breakable-word">
          <strong>
            {regelungsvorhaben?.kurzbezeichnung !== undefined ? regelungsvorhaben.kurzbezeichnung : noAnswer}
          </strong>
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.designation.amtlAbbreviation.label')}</dt>
        <dd>
          <strong>{regelungsvorhaben?.abkuerzung !== undefined ? regelungsvorhaben.abkuerzung : noAnswer}</strong>
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.designation.shortDescription.label')}</dt>
        <dd className="dd-breakable-word">
          {regelungsvorhaben?.kurzbeschreibung !== undefined ? regelungsvorhaben.kurzbeschreibung : noAnswer}
        </dd>

        <dt>
          {t('rv.newRegelungsvorhaben.designation.datenblattNr.label')}
          <InfoComponent
            tooltipBtnStyle={{ marginLeft: '4px' }}
            title={t('rv.newRegelungsvorhaben.designation.datenblattNr.label')}
          >
            <p>{t('rv.newRegelungsvorhaben.designation.datenblattNr.infoText')}</p>
          </InfoComponent>
        </dt>
        <dd>{regelungsvorhaben?.datenblattNummer ? regelungsvorhaben.datenblattNummer : noAnswerDatenblattNr}</dd>
        {props.regelungsvorhaben.vorhabenart !== RegelungsvorhabenTypType.Verwaltungsvorschrift && (
          <>
            <dt>{t('rv.newRegelungsvorhaben.beteiligungBundestag.title')}</dt>
            <dd>
              {regelungsvorhaben?.beteiligungBundestag.length
                ? regelungsvorhaben?.beteiligungBundestag.map((item) => {
                    return (
                      <React.Fragment key={item}>
                        {t(`rv.newRegelungsvorhaben.beteiligungBundestag.options.${item}.label`)}
                        <br />
                      </React.Fragment>
                    );
                  })
                : noAnswer}
            </dd>
          </>
        )}
        <dt>{t('rv.newRegelungsvorhaben.beteiligungBundesrat.title')}</dt>
        <dd>
          {regelungsvorhaben?.beteiligungBundesrat
            ? t(`rv.newRegelungsvorhaben.beteiligungBundesrat.options.${regelungsvorhaben?.beteiligungBundesrat}.label`)
            : noAnswer}
        </dd>
      </dl>
    </section>
  );
}
