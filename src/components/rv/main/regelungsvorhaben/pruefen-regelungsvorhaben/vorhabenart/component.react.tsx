// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { CodelistenResponseDTO, CodelisteType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { PruefenRegelungsvorhabenComponentProps } from '../component.react';
import { RVPruefenController } from '../controller';

export interface VorhabenartPruefenRegelungsvorhabenComponentProps extends PruefenRegelungsvorhabenComponentProps {
  codeList?: CodelistenResponseDTO;
}
export function VorhabenartPruefenRegelungsvorhabenComponent(
  props: VorhabenartPruefenRegelungsvorhabenComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const regelungsvorhaben = props.regelungsvorhaben;
  const ctrl = GlobalDI.getOrRegister('RVPruefenController', () => new RVPruefenController());
  const noAnswer = <em>{t('rv.na')}</em>;

  const euVeranlasstStr = regelungsvorhaben?.euVeranlasst?.toString().toLowerCase();
  const executionEuLawEntry =
    regelungsvorhaben?.euVeranlasst !== undefined && regelungsvorhaben?.euVeranlasst !== null
      ? t(`rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.options.${euVeranlasstStr}`)
      : noAnswer;

  return (
    <section className="review-section">
      <Title level={2}>{t('rv.newRegelungsvorhaben.vorhabenart.title')}</Title>
      <dl>
        <dt>{t('rv.newRegelungsvorhaben.vorhabenart.baseType.label')}</dt>
        <dd>
          {regelungsvorhaben?.grundtyp && props.codeList
            ? ctrl.getCodeNames(
                [regelungsvorhaben.grundtyp],
                props.codeList,
                `GRUND_TYP_${regelungsvorhaben?.vorhabenart || ''}` as CodelisteType,
              )
            : noAnswer}
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.participants.subjectAreas.label')}</dt>
        <dd>
          {ctrl.getCodeNames(props.regelungsvorhaben?.sachgebiete, props.codeList, CodelisteType.Sachgebiet) ??
            noAnswer}
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.vorhabenart.specialCircumstances.label')}</dt>
        <dd>
          {regelungsvorhaben?.besondereUmstaende.length && props.codeList
            ? ctrl.getCodeNames(
                [...regelungsvorhaben.besondereUmstaende],
                props.codeList,
                ctrl.getBesondereUmstaendeType(regelungsvorhaben.vorhabenart),
              )
            : noAnswer}
        </dd>
        <dt>{t('rv.newRegelungsvorhaben.vorhabenart.executionEuLaw.label')}</dt>
        <dd>{executionEuLawEntry}</dd>
      </dl>
    </section>
  );
}
