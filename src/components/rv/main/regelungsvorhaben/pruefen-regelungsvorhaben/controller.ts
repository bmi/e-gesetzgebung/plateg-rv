// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CodelistenResponseDTO, CodelisteType, RegelungsvorhabenTypType, RessortFullDTO } from '@plateg/rest-api';

export class RVPruefenController {
  public getRessortNames(ressorts?: RessortFullDTO[], ids?: string[]): string | undefined {
    if (!ressorts?.length || !ids) {
      return undefined;
    }
    let names = '';
    ressorts.forEach((ressort) => {
      if (ids.includes(ressort.id)) {
        if (names.length > 0) {
          names = names + ', ';
        }
        names = names + ressort.bezeichnung;
      }
    });
    return names.length > 0 ? names : undefined;
  }

  public getCodeNames(enums?: string[], codeList?: CodelistenResponseDTO, type?: CodelisteType): string | undefined {
    if (!codeList || !enums || !type) {
      return undefined;
    }
    let names = '';
    codeList?.codelisten
      .filter((list) => {
        return list.type === type;
      })
      .forEach((list) => {
        list.values.forEach((element) => {
          if (enums.includes(Object.keys(element)[0])) {
            if (names.length > 0) {
              names = names + ', ';
            }
            names = names + (Object.values(element)[0] as string);
          }
        });
      });
    return names.length > 0 ? names : undefined;
  }

  public getBesondereUmstaendeType(regelungsvorhabenTypType?: RegelungsvorhabenTypType): CodelisteType | undefined {
    if (regelungsvorhabenTypType === RegelungsvorhabenTypType.Gesetz) {
      return CodelisteType.BesondereUmstaendeGesetz;
    } else if (regelungsvorhabenTypType === RegelungsvorhabenTypType.Rechtsverordnung) {
      return CodelisteType.BesondereUmstaendeRechtsverordnung;
    } else if (regelungsvorhabenTypType === RegelungsvorhabenTypType.Verwaltungsvorschrift) {
      return CodelisteType.BesondereUmstaendeVerwaltungsvorschrift;
    } else {
      return undefined;
    }
  }
}
