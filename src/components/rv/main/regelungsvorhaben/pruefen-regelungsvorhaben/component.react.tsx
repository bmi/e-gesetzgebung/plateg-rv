// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './pruefen-regelungsvorhaben.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  CodelistenResponseDTO,
  RegelungsvorhabenEntityDTO,
  RegelungsvorhabenEntityFullDTO,
  RegelungsvorhabenEntityResponseDTO,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  CombinedTitle,
  HeaderController,
  HinweisComponent,
  LoadingStatusController,
} from '@plateg/theme';
import { useAppDispatch } from '@plateg/theme/src/components/store';
import { setRequestCurrentPage } from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../../shares/routes';
import { RvErrorController } from '../../../../../utils/rv-error-controller';
import { BreadcrumbTitle, EditElementLink, ElementLink, TabLink } from '../../../component.react';
import { NewRegelungsvorhabenController } from '../controller';
import { useRvState } from '../regelungsvorhaben-context/component.react';
import { DesignationPruefenRegelungsvorhabenComponent } from './designation/component.react';
import { ParticipantsPruefenRegelungsvorhabenComponent } from './participants/component.react';
import { VorhabenartPruefenRegelungsvorhabenComponent } from './vorhabenart/component.react';

export interface PruefenRegelungsvorhabenComponentProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullDTO | RegelungsvorhabenEntityDTO;
  regelungsvorhabenId?: string | null;
}

export function PruefenRegelungsvorhabenComponent(props: PruefenRegelungsvorhabenComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const existingRvId = props.regelungsvorhabenId;
  const ctrl = GlobalDI.getOrRegister('rvNewRegelungsvorhabenController', () => new NewRegelungsvorhabenController());
  const headerController = GlobalDI.get<HeaderController>('rvHeadercontroller');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  const regelungsvorhaben = props.regelungsvorhaben;
  const [codeList, setCodeList] = useState<CodelistenResponseDTO>();
  const { itemFromPKP } = useRvState();
  const routeMatcherVorbereitung = useRouteMatch<{ tabName: string; action: string }>(
    '/regelungsvorhaben/:tabName/:action',
  );
  const tabName = routeMatcherVorbereitung?.params?.tabName ?? '';
  const action = routeMatcherVorbereitung?.params?.action ?? '';
  const route = () => {
    if (!existingRvId) {
      return `${tabName}/${routes.NEUES_REGELUNGSVORHABEN}`;
    } else if (action === routes.PRUEFEN_REGELUNGSVORHABEN_ENTWURF) {
      return `${tabName}/${existingRvId}/${routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF}`;
    } else {
      return `${tabName}/${existingRvId}/${routes.BEARBEITEN_REGELUNGSVORHABEN}`;
    }
  };
  const dispatch = useAppDispatch();

  const errorCallback = (error: AjaxError) => {
    rvErrorCtrl.handleError(error);
    console.error('Error sub', error);
    loadingStatusController.setLoadingStatus(false);
  };

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    setBreadcrumb(regelungsvorhaben.abkuerzung || '');
    const codeListsub: Subscription = ctrl.getCodelistenCall().subscribe({
      next: (codeListResponse: CodelistenResponseDTO) => {
        setCodeList(codeListResponse);
      },
      error: errorCallback,
      complete: () => {
        loadingStatusController.setLoadingStatus(false);
      },
    });
    return () => {
      codeListsub?.unsubscribe();
    };
  }, []);

  const setBreadcrumb = (abkuerzung: string) => {
    const breadcrumbsItems = [];
    const tabLink = <TabLink tabName={tabName} />;
    breadcrumbsItems.push(tabLink);

    const elementLink = <ElementLink tabName={tabName} elId={existingRvId} abkuerzung={abkuerzung} />;
    breadcrumbsItems.push(elementLink);

    const editLinkAction =
      action === routes.PRUEFEN_REGELUNGSVORHABEN
        ? routes.BEARBEITEN_REGELUNGSVORHABEN
        : routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF;
    const editLink = <EditElementLink tabName={tabName} action={editLinkAction} elId={existingRvId} />;
    breadcrumbsItems.push(editLink);

    const breadcrumbTitle = <BreadcrumbTitle abkuerzung={''} action={action} />;
    breadcrumbsItems.push(breadcrumbTitle);

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[...breadcrumbsItems]} />],
      headerRight: [],
    });
  };

  const saveRv = () => {
    if (props.regelungsvorhaben) {
      dispatch(setRequestCurrentPage({ tabKey: tabName, currentPage: 1 }));
      loadingStatusController.setLoadingStatus(true);
      const apiCall = existingRvId
        ? ctrl.modifyRvCall(props.regelungsvorhaben as RegelungsvorhabenEntityDTO, existingRvId)
        : ctrl.saveRvCall(props.regelungsvorhaben as RegelungsvorhabenEntityDTO, itemFromPKP?.id);
      apiCall.subscribe({
        next: (rv: RegelungsvorhabenEntityResponseDTO) => {
          if (itemFromPKP) {
            history.push(`/regelungsvorhaben/${rv.base.id}/${routes.ERFOLGPKP}`);
          } else {
            existingRvId && action !== routes.PRUEFEN_REGELUNGSVORHABEN_ENTWURF
              ? history.push(`/regelungsvorhaben/${rv.base.id}/${routes.ERFOLGUPD}`)
              : history.push(`/regelungsvorhaben/${rv.base.id}/${routes.ERFOLG}`);
          }
        },
        error: errorCallback,
        complete: () => {
          loadingStatusController.setLoadingStatus(false);
        },
      });
    }
  };
  let submitBtnText =
    existingRvId && action !== routes.PRUEFEN_REGELUNGSVORHABEN_ENTWURF
      ? t('rv.pruefenRegelungsvorhaben.btns.submitEdit')
      : t('rv.pruefenRegelungsvorhaben.btns.submit');

  submitBtnText = itemFromPKP ? t('rv.pruefenRegelungsvorhaben.btns.submitItemFromPKP') : submitBtnText;

  return (
    <div className="pruefen-seite">
      <CombinedTitle title={props.regelungsvorhaben?.abkuerzung} suffix={t('rv.pruefenRegelungsvorhaben.mainTitle')} />
      <DesignationPruefenRegelungsvorhabenComponent regelungsvorhaben={regelungsvorhaben} />
      <ParticipantsPruefenRegelungsvorhabenComponent codeList={codeList} regelungsvorhaben={regelungsvorhaben} />
      <VorhabenartPruefenRegelungsvorhabenComponent codeList={codeList} regelungsvorhaben={regelungsvorhaben} />
      {itemFromPKP ? (
        <HinweisComponent content={<span>{t(`rv.pruefenRegelungsvorhaben.itemFromPKPHinweis`)}</span>} />
      ) : (
        ''
      )}

      <div className="form-control-buttons">
        <Button
          id="rv-pruefenRegelungsvorhaben-submit-btn"
          type="primary"
          onClick={saveRv}
          htmlType="submit"
          size={'large'}
        >
          {submitBtnText}
        </Button>

        <Button
          id="rv-pruefenRegelungsvorhaben-cancel-btn"
          size={'large'}
          onClick={() => {
            history.push(`/regelungsvorhaben/${route()}`);
          }}
        >
          {t('rv.pruefenRegelungsvorhaben.btns.cancel')}
        </Button>
      </div>
    </div>
  );
}
