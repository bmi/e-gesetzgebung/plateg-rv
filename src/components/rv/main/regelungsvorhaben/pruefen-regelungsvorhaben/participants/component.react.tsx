// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { CodelistenResponseDTO } from '@plateg/rest-api';
import { CodeListenController } from '@plateg/theme/src/controllers/CodeListenController';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { customSortRessorts } from '../../new-regelungsvorhaben/participants/component.react';
import { PruefenRegelungsvorhabenComponentProps } from '../component.react';
import { RVPruefenController } from '../controller';

export interface ParticipantsPruefenRegelungsvorhabenComponentProps extends PruefenRegelungsvorhabenComponentProps {
  codeList?: CodelistenResponseDTO;
}
export function ParticipantsPruefenRegelungsvorhabenComponent(
  props: ParticipantsPruefenRegelungsvorhabenComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const ctrl = GlobalDI.getOrRegister('RVPruefenController', () => new RVPruefenController());
  const ctrlCheckRv = GlobalDI.getOrRegister('CodeListenController', () => new CodeListenController());
  const noAnswer = <em>{t('rv.na')}</em>;
  const sortedCodelistRessorts = props.codeList?.ressorts.sort(customSortRessorts);

  const abteilungRv = ctrlCheckRv.getAbteilungName(props.codeList, props.regelungsvorhaben.technischeFfAbteilung);
  const referatRv = ctrlCheckRv.getReferatName(props.codeList, props.regelungsvorhaben.technischesFfReferat);

  return (
    <section className="review-section">
      <Title level={2}>{t('rv.newRegelungsvorhaben.participants.title')}</Title>
      <dl>
        <dt>{t('rv.newRegelungsvorhaben.participants.initiator.label')}</dt>
        <dd>
          {props.regelungsvorhaben?.initiant
            ? t(
                `rv.newRegelungsvorhaben.participants.initiator.options.${props.regelungsvorhaben.initiant.toLowerCase()}`,
              )
            : noAnswer}
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.participants.ffRessort.label')}</dt>
        <dd>{ctrl.getRessortNames(sortedCodelistRessorts, props.regelungsvorhaben?.allFfRessorts) ?? noAnswer}</dd>

        <dt>{t('rv.newRegelungsvorhaben.participants.techFfRessort.label')}</dt>
        <dd>
          {ctrl.getRessortNames(
            sortedCodelistRessorts,
            props.regelungsvorhaben?.technischesFfRessort ? [props.regelungsvorhaben?.technischesFfRessort] : undefined,
          ) ?? noAnswer}
        </dd>

        <dt>{t('rv.newRegelungsvorhaben.participants.ffAbteilung.label')}</dt>
        <dd>{abteilungRv ?? noAnswer}</dd>

        <dt>{t('rv.newRegelungsvorhaben.participants.ffReferat.label')}</dt>
        <dd>{referatRv ?? noAnswer}</dd>

        <dt>{t('rv.newRegelungsvorhaben.participants.involvedRessorts.label')}</dt>
        <dd>{ctrl.getRessortNames(sortedCodelistRessorts, props.regelungsvorhaben?.beteiligteRessorts) ?? noAnswer}</dd>
      </dl>
    </section>
  );
}
