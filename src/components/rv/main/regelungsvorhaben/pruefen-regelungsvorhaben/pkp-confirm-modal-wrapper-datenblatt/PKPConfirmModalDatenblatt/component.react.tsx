// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './pkp-confirm-modal.less';

import React from 'react';

import { ExclamationCircleFilled, ModalWrapper } from '@plateg/theme';

interface PKPConfirmModalProps {
  okText: string;
  cancelText: string;
  modalTitle?: string;
  actionTitle: string;
  visible: boolean;
  setVisible: (visible: boolean) => void;
  setPKPsent: () => void;
}

// currently not used
export function PKPConfirmModal(props: PKPConfirmModalProps): React.ReactElement {
  const modalTitle = (
    <span className="modal-content">
      <span className="title-icon">
        <ExclamationCircleFilled />
      </span>
      <span className="title-text">{props.actionTitle}</span>
    </span>
  );
  return (
    <ModalWrapper
      open={props.visible}
      onOk={props.setPKPsent}
      onCancel={() => props.setVisible(!props.visible)}
      okText={props.okText}
      cancelText={props.cancelText}
      title={<h3>{modalTitle}</h3>}
      className={'pkp-confirm-modal'}
    />
  );
}
