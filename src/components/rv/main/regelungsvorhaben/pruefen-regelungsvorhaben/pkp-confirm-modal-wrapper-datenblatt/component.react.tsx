// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Typography } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, RegelungsvorhabenEntityFullResponseDTO } from '@plateg/rest-api';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { PkpSentStatus } from '../../../tabs/general-items/pkp-sent-status/component.react';
import { RvPkpConfirmModal } from '../../../tabs/my-regelungsvorhaben-tab/rv-pkp-confirm-modal/component.react';
import { isRelevantKurzbezeichnung } from '../../controller';

const { Title } = Typography;

interface Props {
  refreshRV: () => void;
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  currentUserIsStellvertreter?: boolean;
}

export function PkpComfirmModalWrapperDatenblatt(props: Props): React.ReactElement {
  const { t } = useTranslation();
  const appStore = useAppSelector((state) => state.user);
  const isBRorBTuser = isRelevantKurzbezeichnung(appStore.user?.dto.ressort?.kurzbezeichnung);

  const [confirmPKPModalIsVisible, setConfirmPKPModalIsVisible] = useState(false);

  return (
    <>
      <RvPkpConfirmModal
        visible={confirmPKPModalIsVisible}
        setVisible={setConfirmPKPModalIsVisible}
        rv={props.regelungsvorhaben.dto}
        rvId={props.regelungsvorhaben.base.id}
        onClickModal={() => props.refreshRV()}
      />

      <section className="review-section">
        <Title level={2}>{t('rv.newRegelungsvorhaben.status.title')}</Title>
        <dl>
          <dt>{t('rv.newRegelungsvorhaben.status.label')}</dt>
          <dd>{t(`rv.newRegelungsvorhaben.status.options.${props.regelungsvorhaben.dto.status}`)}</dd>
          <dt>{t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.tableColLabel')}</dt>
          <dd className="inline-flex-cell">
            <span>
              {props.regelungsvorhaben.dto.pkpStatus && (
                <PkpSentStatus
                  pkpStatus={props.regelungsvorhaben.dto.pkpStatus}
                  pkpGesendetAm={props.regelungsvorhaben.dto.pkpGesendetAm}
                  pkpWorkflowStatus={props.regelungsvorhaben.dto.pkpWorkflowStatus}
                />
              )}
            </span>
            <span>
              {!isBRorBTuser && (
                <Button
                  className="btn-datenblatt-sent-pkp"
                  id="rv-datenblattRegelungsvorhaben-PKP-btn"
                  size={'middle'}
                  type="primary"
                  onClick={() => setConfirmPKPModalIsVisible(true)}
                  disabled={
                    !props.regelungsvorhaben.dto.aktionen?.includes(AktionType.AnPkpSenden) ||
                    props.currentUserIsStellvertreter
                  }
                >
                  {t('rv.datenblattRegelungsvorhaben.btns.PKP')}
                </Button>
              )}
            </span>
          </dd>
        </dl>
      </section>
    </>
  );
}
