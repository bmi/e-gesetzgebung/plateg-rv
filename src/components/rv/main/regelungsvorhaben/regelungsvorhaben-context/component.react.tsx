// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { createContext, ReactNode, useContext, useState } from 'react';

import { RegelungsvorhabenEntityDTO, RegelungsvorhabenPpkTableDTO } from '@plateg/rest-api';

// Define the shape of your state
interface RvStateContextType {
  itemFromPKP: RegelungsvorhabenPpkTableDTO | undefined;
  setItemFromPKP: (newItemFromPKP: RegelungsvorhabenPpkTableDTO | undefined) => void;
  initialRv: RegelungsvorhabenEntityDTO | undefined;
  setInitialRv: (newItemFromPKP: RegelungsvorhabenEntityDTO | undefined) => void;
}

// Create the context with a default value
const RvStateContext = createContext<RvStateContextType | undefined>(undefined);

interface MyStateProviderProps {
  children: ReactNode;
}

export function RvStateProvider({ children }: MyStateProviderProps) {
  const [itemFromPKP, setItemFromPKP] = useState<RegelungsvorhabenPpkTableDTO | undefined>(undefined);
  const [initialRv, setInitialRv] = useState<RegelungsvorhabenEntityDTO | undefined>(undefined);

  return (
    <RvStateContext.Provider value={{ itemFromPKP, setItemFromPKP, initialRv, setInitialRv }}>
      {children}
    </RvStateContext.Provider>
  );
}

export function useRvState(): RvStateContextType {
  const context = useContext(RvStateContext);
  if (!context) {
    throw new Error('useRvState must be used within a RvStateProvider');
  }
  return context;
}
