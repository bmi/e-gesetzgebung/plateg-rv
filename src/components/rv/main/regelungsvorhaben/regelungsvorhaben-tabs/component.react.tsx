// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './regelungsvorhaben-tabs.less';

import { Button, Col, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { forkJoin, Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AktionType,
  CodelistenResponseDTO,
  RegelungsvorhabenBestandsrechtTableDTO,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityFullResponseDTO,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
  RolleLokalType,
  RvProzesshilfeOverviewDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';
import {
  BerechtigungenOutlined,
  BreadcrumbComponent,
  DropdownMenu,
  HeaderController,
  InfoComponent,
  LoadingStatusController,
  RVInfoComponent,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { Briefcase } from '@plateg/theme/src/components/icons/Briefcase';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../../shares/routes';
import { RvErrorController } from '../../../../../utils/rv-error-controller';
import { BreadcrumbTitle, RvHelpLink, TabLink } from '../../../component.react';
import { FreigabeModalRV } from '../../tabs/freigabe-modal/component.react';
import { isRelevantKurzbezeichnung, NewRegelungsvorhabenController } from '../controller';
import { Bestandsrecht } from './bestandsrecht/component.react';
import { Datenblatt } from './datenblatt/component.react';
import { NaechsteSchritte } from './naechste-schritte/component.react';
import { Zuleitungen } from './zuleitungen/component.react';

import type { TabsProps } from 'antd';

interface RegelungsvorhabenTabsState extends RvProzesshilfeOverviewDTO {
  codeList?: CodelistenResponseDTO;
  restricted?: boolean;
  bestandsrechtData?: RegelungsvorhabenBestandsrechtTableDTO[];
}

interface RvSubResp {
  prozesshilfeUebersicht: RvProzesshilfeOverviewDTO;
  bestandsrechts?: RegelungsvorhabenBestandsrechtTableDTO[];
}

export function RegelungsvorhabenTabs(): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcher = useRouteMatch<{ id: string; tabName: string; action: string }>(
    '/regelungsvorhaben/:tabName/:id/:action',
  );

  const tabName = routeMatcher?.params.tabName ? routeMatcher?.params.tabName : '';
  const action = routeMatcher?.params.action ? routeMatcher?.params.action : '';
  const ctrl = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  const newRvCtrl = GlobalDI.getOrRegister(
    'rvNewRegelungsvorhabenController',
    () => new NewRegelungsvorhabenController(),
  );
  const history = useHistory();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('rvHeadercontroller');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  const [state, setState] = useState<RegelungsvorhabenTabsState>({} as RegelungsvorhabenTabsState);
  const [freigabeModalIsVisible, setFreigabeModalIsVisible] = useState(false);
  const [currentUserIsStellvertreter, setCurrentUserIsStellvertreter] = useState(false);
  const [activeIndex, setActiveIndex] = useState<string>(action);
  const appStore = useAppSelector((state) => state.user);
  const isBRorBTuser = isRelevantKurzbezeichnung(appStore.user?.dto.ressort?.kurzbezeichnung);

  const bypassAuthForBrUser = 'BR' === appStore.user?.dto.ressort?.kurzbezeichnung;

  const checkRvOwner = (rv: RegelungsvorhabenEntityFullResponseDTO): boolean => {
    const restricted =
      !appStore.user?.dto.stellvertreter &&
      appStore.user?.dto.ressort?.kurzbezeichnung !== 'BR' &&
      ((appStore.user?.dto.ressort?.kurzbezeichnung !== 'BT' && rv.dto.rolleTyp !== RolleLokalType.Federfuehrer) ||
        (appStore.user?.dto.ressort?.kurzbezeichnung === 'BT' && rv.dto.rolleTyp === RolleLokalType.Gast));
    const RBACwhitelist = {
      [routes.BESTANDSRECHT]: state.regelungsvorhaben?.dto.aktionen?.includes(AktionType.BestandsrechtLesen),
    };
    if (restricted && !RBACwhitelist[action]) {
      setActiveIndex(routes.DATENBLATT);
      history.replace(`/regelungsvorhaben/${tabName}/${rv.base.id}/${routes.DATENBLATT}`);
    } else {
      setActiveIndex(action);
    }
    return restricted;
  };

  useEffect(() => {
    const id = routeMatcher?.params.id;
    let codeListsub: Subscription;
    let rvSub: Subscription;

    if (id) {
      const sources = {
        prozesshilfeUebersicht: bypassAuthForBrUser
          ? 
            ctrl.getProzesshilfeUebersichtNoAuth({ id })
          : ctrl.getProzesshilfeUebersicht({ id }),
        ...(appStore.user?.dto.ressort?.kurzbezeichnung === 'BT'
          ? undefined
          : { bestandsrechts: ctrl.getRegelungsvorhabenBestandsrechtTable({ id }) }),
      };

      loadingStatusController.setLoadingStatus(true);
      rvSub = forkJoin({ ...sources }).subscribe({
        next: (results: RvSubResp) => {
          codeListsub = newRvCtrl.getCodelistenCall().subscribe({
            next: (codeListResponse: CodelistenResponseDTO) => {
              const restricted = checkRvOwner(results.prozesshilfeUebersicht.regelungsvorhaben);
              setState({
                ...state,
                codeList: codeListResponse,
                ...results.prozesshilfeUebersicht,
                restricted,
                bestandsrechtData:
                  appStore.user?.dto.ressort?.kurzbezeichnung === 'BT' ? undefined : results.bestandsrechts,
              });
              setBreadcrumb(results.prozesshilfeUebersicht.regelungsvorhaben, action, codeListResponse);
            },
            error: (error: AjaxError) => {
              rvErrorCtrl.handleError(error);
              loadingStatusController.setLoadingStatus(false);
              setState({ ...state, ...results.prozesshilfeUebersicht });
            },
            complete: () => {
              loadingStatusController.setLoadingStatus(false);
            },
          });
        },
        error: (error: AjaxError) => {
          loadingStatusController.setLoadingStatus(false);
          console.error(error, error);
          rvErrorCtrl.handleError(error);
        },
      });
    }

    return () => {
      codeListsub?.unsubscribe();
      rvSub?.unsubscribe();
    };
  }, []);
  useEffect(() => {
    if (state.regelungsvorhaben) {
      checkRvOwner(state.regelungsvorhaben);
      setBreadcrumb(state.regelungsvorhaben, activeIndex, state.codeList);
    }
  }, [action]);
  useEffect(() => {
    if (appStore.user?.dto.stellvertreter) {
      setCurrentUserIsStellvertreter(true);
    }
  }, [appStore.user]);

  const isArchived =
    state.regelungsvorhaben?.dto?.bearbeitetAm !== undefined &&
    state.regelungsvorhaben?.dto?.bearbeitetAm !== null &&
    state.regelungsvorhaben.dto.status === VorhabenStatusType.Archiviert;

  const refreshRv = () => {
    const id = routeMatcher?.params.id;
    if (id) {
      loadingStatusController.setLoadingStatus(true);

      const ctrlSub = (
        bypassAuthForBrUser
          ? 
            ctrl.getProzesshilfeUebersichtNoAuth({ id })
          : ctrl.getProzesshilfeUebersicht({ id })
      ).subscribe({
        next: (result) => {
          setState({
            ...state,
            ...result,
          });
          loadingStatusController.setLoadingStatus(false);
          ctrlSub?.unsubscribe();
        },
        error: (error: AjaxError) => {
          loadingStatusController.setLoadingStatus(false);
          console.error(error, error);
          rvErrorCtrl.handleError(error);
          ctrlSub?.unsubscribe();
        },
      });
    }
  };

  const setBreadcrumb = (
    response: RegelungsvorhabenEntityFullResponseDTO,
    newAction: string,
    codeList?: CodelistenResponseDTO,
  ) => {
    const tabLink = <TabLink tabName={tabName} />;
    const breadcrumbTitle = <BreadcrumbTitle action={newAction} abkuerzung={response.dto.abkuerzung || ''} />;
    const rightHeaderButtons = [];
    if (response.dto.status !== VorhabenStatusType.Entwurf) {
      const rvInfoDrawer = (
        <InfoComponent
          isContactPerson={false}
          title={t('rv.regelungsvorhabenTabs.rvInfoDrawer.infoRvTitle')}
          buttonText={t('rv.regelungsvorhabenTabs.rvInfoDrawer.infoRvLabel')}
          key="egfa-header-rv-info"
          titleWithoutPrefix={true}
        >
          <RVInfoComponent
            id="rv-info"
            regelungsvorhaben={response as RegelungsvorhabenEntityWithDetailsResponseDTO}
            codeList={codeList}
          />
        </InfoComponent>
      );
      rightHeaderButtons.push(
        <Button
          key="rv-freigabe-btn"
          id="rv-freigabe-btn"
          className="no-wrap blue-text-button"
          type="text"
          onClick={() => {
            setFreigabeModalIsVisible(true);
          }}
        >
          <BerechtigungenOutlined /> {t('rv.header.freigabe')}
        </Button>,
      );
      rightHeaderButtons.push(rvInfoDrawer);
      rightHeaderButtons.push(
        <DropdownMenu
          key={'dropdown-menu-rv-freigabe'}
          items={[
            {
              element: t('rv.header.freigabe'),
              onClick: () => {
                setFreigabeModalIsVisible(true);
              },
            },
            {
              element: rvInfoDrawer,
            },
          ]}
          elementId={'headerRightAlternative'}
          overlayClass={'headerRightAlternative-overlay'}
        />,
      );
    }

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, breadcrumbTitle]} />],
      headerRight: rightHeaderButtons,
      headerLast: [<RvHelpLink key="rv-header-help-link" />],
    });
  };

  const { codeList, restricted, ...nextStepsState } = state;

  const tabItems: TabsProps['items'] = state?.regelungsvorhaben && [
    {
      key: `${routes.NAECHSTESCHRITTE}`,
      label: t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.title'),
      disabled: state.regelungsvorhaben.dto.status === VorhabenStatusType.Entwurf || state.restricted || isArchived,
      children: (
        <NaechsteSchritte
          {...nextStepsState}
          bestandsrechtData={state.bestandsrechtData || []}
          setBestandsrechtData={(vals: RegelungsvorhabenBestandsrechtTableDTO[]) =>
            setState({ ...state, bestandsrechtData: vals })
          }
        />
      ),
    },
    {
      key: `${routes.DATENBLATT}`,
      label: t('rv.regelungsvorhabenTabs.tabs.datenblatt.title'),
      children: (
        <Datenblatt
          regelungsvorhaben={state.regelungsvorhaben}
          tabName={tabName}
          codeList={state.codeList}
          currentUserIsStellvertreter={currentUserIsStellvertreter}
          refreshRV={refreshRv}
        />
      ),
    },
    {
      key: `${routes.ZULEITUNGEN}`,
      label: t('rv.regelungsvorhabenTabs.tabs.zuleitungen.title'),
      disabled: state.regelungsvorhaben.dto.status === VorhabenStatusType.Entwurf || state.restricted,
      children: <Zuleitungen regelungsvorhaben={state.regelungsvorhaben} isArchived={isArchived} />,
    },
    ...(!isBRorBTuser
      ? [
          {
            key: routes.BESTANDSRECHT,
            disabled: !state.regelungsvorhaben.dto.aktionen?.includes(AktionType.BestandsrechtLesen),
            label: `${t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.title')}`,
            children: (
              <Bestandsrecht
                regelungsvorhaben={state.regelungsvorhaben}
                bestandsrechtData={state.bestandsrechtData || []}
                setBestandsrechtData={(vals: RegelungsvorhabenBestandsrechtTableDTO[]) =>
                  setState({ ...state, bestandsrechtData: vals })
                }
              />
            ),
          },
        ]
      : []),
  ];

  return (
    <>
      {state?.regelungsvorhaben && (
        <div className="regelungsvorhaben-details">
          <TitleWrapperComponent>
            <Row>
              <Col xs={{ span: 22, offset: 1 }}>
                <div className="heading-holder">
                  <Title className="main-title" level={1}>
                    <Briefcase />
                    {state.regelungsvorhaben.dto.abkuerzung}
                  </Title>
                </div>
              </Col>
            </Row>
          </TitleWrapperComponent>
          <FreigabeModalRV
            isVisible={freigabeModalIsVisible}
            setIsVisible={(val: boolean) => setFreigabeModalIsVisible(val)}
            disabled={!state.regelungsvorhaben.dto.aktionen?.includes(AktionType.BerechtigungenAendern)}
            objectTitle={state.regelungsvorhaben.dto.kurzbezeichnung}
            objectId={state.regelungsvorhaben.base.id}
            selectedRvId={state.regelungsvorhaben.base.id}
            allowChanges={
              state.regelungsvorhaben.dto.aktionen?.includes(AktionType.BerechtigungenAendern) ||
              state.regelungsvorhaben.dto.aktionen?.includes(AktionType.EigeneBerechtigungenLoeschen)
            }
            revokeObserverRight={state.regelungsvorhaben.dto.aktionen?.includes(
              AktionType.EigeneBerechtigungenLoeschen,
            )}
            loadContent={() => history.push(`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}`)}
          />
          <Row>
            <Col xs={{ span: 22, offset: 1 }}>
              <TabsWrapper
                items={tabItems}
                activeKey={`${activeIndex}`}
                className="standard-tabs"
                onChange={(key: string) => {
                  if (state.regelungsvorhaben) {
                    setActiveIndex(key);
                    history.push(`/regelungsvorhaben/${tabName}/${state.regelungsvorhaben.base.id}/${key}`);
                    setBreadcrumb(state.regelungsvorhaben, key, state.codeList);
                  }
                }}
              />
            </Col>
          </Row>
        </div>
      )}
    </>
  );
}
