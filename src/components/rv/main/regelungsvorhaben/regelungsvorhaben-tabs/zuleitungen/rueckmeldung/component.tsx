// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenZuleitungTableDTO, RueckmeldungType } from '@plateg/rest-api';
import { CheckOutlinedGreen, DocumentListPopover, ExclamationCircleFilled } from '@plateg/theme';

import { getLabelZuleitungDiffCases } from '../controller';
import { getDateFormat } from '../freigegeben/component';

interface Props {
  record: RegelungsvorhabenZuleitungTableDTO;
}

export function ZuleitungRuckmeldung(props: Props): React.ReactElement {
  const { t } = useTranslation();
  const checkStatus = (status: RueckmeldungType) => {
    if (status === RueckmeldungType.Zugestellt) {
      return <></>;
    } else if (
      status === RueckmeldungType.AenderungGewuenschtPkp ||
      status === RueckmeldungType.AenderungGewuenschtFederfuehrer
    ) {
      return <ExclamationCircleFilled />;
    } else {
      return <CheckOutlinedGreen />;
    }
  };
  return (
    <>
      <div className="zuleitung-display-flex">
        {checkStatus(props.record.rueckmeldung)}
        <Text
          className={`zuleitung-docs-title ${
            props.record.rueckmeldung === RueckmeldungType.Zugestellt ? 'zuleitung-no-padding' : ''
          }`}
        >
          {getLabelZuleitungDiffCases(props.record.rueckmeldung)}
        </Text>
      </div>

      {props.record.letzteRueckmeldungAm && <Text>{getDateFormat(props.record.letzteRueckmeldungAm)}</Text>}
      <br />
      {props.record.rueckmeldung === RueckmeldungType.AenderungGewuenschtPkp &&
        props.record.rueckmeldungDokumente.length > 0 && (
          <DocumentListPopover
            documentsList={props.record.rueckmeldungDokumente}
            title={t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.docs.showDocsReceived')}
            recordId={props.record.id}
          />
        )}
    </>
  );
}
