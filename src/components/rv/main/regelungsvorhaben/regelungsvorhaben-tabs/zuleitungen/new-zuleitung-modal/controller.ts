// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd';
import { RcFile } from 'antd/lib/upload';
import i18n from 'i18next';
import { forkJoin, Observable, Observer, of, switchMap } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  EntwurfstypType,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenZuleitungCreateDTO,
  RvFileEntityCreateDTO,
} from '@plateg/rest-api';
import { displayMessage, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { RvErrorController } from '../../../../../../../utils/rv-error-controller';
import { ZuleitungValuesInterface } from './component.react';

export class NewZuleitungModalController {
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private readonly regelungsvorhabenController =
    GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  private readonly rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  public zuleitungSenden(
    rvId: string,
    setIsVisible: (isVisible: boolean) => void,
    fetchZuleitungen: () => void,
    zuleitungValues: ZuleitungValuesInterface,
  ): void {
    setIsVisible(false);
    this.loadingStatusController.setLoadingStatus(true);

    // Prepare files as Byte Array
    const filesList = this.manageFiles(zuleitungValues.filesData.fileList, EntwurfstypType.Regelungsentwurf);
    const additionalList = this.manageFiles(zuleitungValues.filesData.additionalFileList, EntwurfstypType.Zusatzinfo);

    forkJoin([filesList, additionalList])
      .pipe(
        switchMap((data: RvFileEntityCreateDTO[][]) => {
          return this.regelungsvorhabenController.createRegelungsvorhabenZuleitung({
            id: rvId,
            regelungsvorhabenZuleitungCreateDTO: {
              freigegebenFuer: zuleitungValues.typ,
              documentTyp: zuleitungValues.documentTyp,
              adressaten: [],
              fileDtos: data.flat(),
            } as RegelungsvorhabenZuleitungCreateDTO,
          });
        }),
      )
      .subscribe({
        next: () => {
          this.loadingStatusController.setLoadingStatus(false);
          displayMessage(i18n.t('rv.newZuleitung.modal.succsessMsg'), 'success');
          fetchZuleitungen();
        },
        error: (error: AjaxError) => {
          this.loadingStatusController.setLoadingStatus(false);
          console.error(error, error);
          this.rvErrorCtrl.handleError(error);
        },
      });
  }

  // Prepare observable of list for each type of files REGELUNGSENTWURF or ZUSATZINFO
  private manageFiles(files: UploadFile[], statusType: EntwurfstypType): Observable<RvFileEntityCreateDTO[]> {
    if (!files.length) {
      return of([]);
    }
    const filesObsList = files.map((file) => {
      return this.transformFile(file, statusType);
    });
    return forkJoin(filesObsList);
  }

  // Prepare observable file as Byte Array
  // All of these manipulations are requed because fileReader.onload is async method.
  private transformFile(file: UploadFile, statusType: EntwurfstypType) {
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file.originFileObj as RcFile);
    const fileByteArray: number[] = [];
    return new Observable((observer: Observer<RvFileEntityCreateDTO>) => {
      const fileDto: RvFileEntityCreateDTO = {
        fileContent: [],
        fileName: file.name,
        entwurfStatusType: statusType,
      };
      fileReader.onload = function (ev) {
        const array = new Uint8Array(ev.target?.result as ArrayBufferLike);
        for (const item of array) {
          fileByteArray.push(item);
        }
        fileDto.fileContent = fileByteArray;
        observer.next(fileDto);
        observer.complete();
      };
    });
  }
}
