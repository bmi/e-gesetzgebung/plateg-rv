// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, UploadFile } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType } from '@plateg/rest-api';
import {
  AdditionalUploadComponent,
  FolderDefault,
  HiddenInfoComponent,
  HinweisComponent,
  UploadComponent,
} from '@plateg/theme';
import { getUploadMessagesConfig } from '@plateg/theme/src/components/file-upload/controller';

import { DocumentTypeRadiogroup } from './document-type-radiogroup/component.react';

export type FormTypePartial = { regelungsvorhabenId: string };
export const standardMaximumSize = 20000000;
interface FileUploadComponentProps {
  rvId: string;
  fileList: UploadFile[];
  setFileList: (uploadedList: UploadFile[]) => void;
  additionalFileList: UploadFile[];
  setAdditionalFileList: (additionalUploadedList: UploadFile[]) => void;
  documentType: DocumentType;
  dmTitle?: string;
  setValues: Function;
}
export function FileUploadComponent(props: FileUploadComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <DocumentTypeRadiogroup disabled={false} setValues={props.setValues} />
      {props.documentType === DocumentType.Editor && (
        <Form.Item label={t('rv.fileUpload.dmTitle')} required>
          {props.dmTitle && (
            <div>
              <FolderDefault />
              <span>{props.dmTitle}</span>
            </div>
          )}
          {!props.dmTitle && (
            <HinweisComponent
              title={t('rv.fileUpload.missingDmHintTitle')}
              mode="warning"
              content={<span>{t('rv.fileUpload.missingDmHintContent')}</span>}
            />
          )}
        </Form.Item>
      )}

      {props.documentType === DocumentType.Enorm && (
        <>
          <Form.Item shouldUpdate>
            {() => (
              <UploadComponent
                messagesConfig={{
                  ...(getUploadMessagesConfig() as { [k: string]: string }),
                  title: (
                    <>
                      {t('rv.fileUpload.uploadDraftTitle')}*
                      <HiddenInfoComponent
                        title={t('rv.fileUpload.uploadDraftDrawerTitle')}
                        text={t('rv.fileUpload.uploadDraftDrawerText')}
                      ></HiddenInfoComponent>
                    </>
                  ),
                  subtitle: <>{t('rv.fileUpload.uploadedDraftTitle')}</>,
                  uploadBtnRegelungsentwurf: <>{t('rv.fileUpload.uploadBtnRegelungsentwurf')}</>,
                  uploadHintRegelungsentwurf: <>{t('rv.fileUpload.uploadHintRegelungsentwurf')}</>,
                  uploadedDraftTitle: <>{t('rv.fileUpload.uploadedDraftTitle')}</>,
                }}
                fileList={props.fileList}
                setFileList={props.setFileList}
                fileFormat=".doc,.docx,.pdf"
                fileSize={standardMaximumSize}
                onlySingleFileAllowed={true}
                restrictRenaming={false}
                required={true}
                // version={props.version}
                isBearbeiten={false}
                errorProps={{
                  errorTitle: t('rv.fileUpload.uploadDraftErrorTitle'),
                  formatString: t('rv.fileUpload.uploadDraftFormatShort'),
                }}
                // onFormChange={onFormChange}
                uniqueKey="MainUpload"
              />
            )}
          </Form.Item>
          <div className="zuleitungen-upload-section">
            <AdditionalUploadComponent
              // onFormChange={onFormChange}
              fileList={props.additionalFileList}
              setFileList={props.setAdditionalFileList}
              fileFormat=".doc,.docx,.pdf"
              fileSize={standardMaximumSize}
              singleFile={false}
              messagesConfig={{
                title: t('rv.fileUpload.uploadAttachmentsTitle'),
                subtitle: <>{t('rv.fileUpload.uploadedAttachmentsTitle')}</>,
                ...(getUploadMessagesConfig() as { [k: string]: string }),
              }}
            />
          </div>
        </>
      )}
    </>
  );
}
