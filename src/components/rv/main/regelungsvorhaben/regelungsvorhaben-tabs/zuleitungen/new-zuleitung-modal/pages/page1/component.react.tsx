// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio, UploadFile } from 'antd';
import Title from 'antd/lib/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FreigegebenType } from '@plateg/rest-api';
import { ErrorBox, FormItemWithInfo, GeneralFormWrapper } from '@plateg/theme';

import { ZuleitungValuesInterface } from '../../component.react';
import { FileUploadComponent } from '../file-upload/component.react';

export interface Page1SelectComponentProps {
  name: string;
  title: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  rvId: string;
  values: ZuleitungValuesInterface;
  setValues: Function;
}
export function Page1SelectComponent(props: Page1SelectComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [uploadedList, setUploadedList] = useState<UploadFile[]>(props.values.filesData.fileList);
  const [additionalUploadedFileList, setAdditionalUploadedList] = useState<UploadFile[]>(
    props.values.filesData.additionalFileList,
  );

  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function }).focus();
  };

  // dmTitle needs to be set explicitly because it is not part of the form
  const onFinish = (vals: any) => {
    props.setActivePageIndex(props.activePageIndex + 1);
    props.setValues({
      ...vals,
      dmTitle: props.values.dmTitle,
      filesData: {
        ...props.values.filesData,
        fileList: uploadedList,
        additionalFileList: additionalUploadedFileList,
      },
    });
  };

  return (
    <>
      <GeneralFormWrapper
        onFinishFailed={handleError}
        form={form}
        id={props.name}
        name={props.name}
        className="new-zuleitung"
        layout="vertical"
        initialValues={props.values}
        onFinish={onFinish}
      >
        <Title level={2}>{props.title}</Title>
        <p className="ant-typography p-no-style">{t('rv.newZuleitung.modal.page1.requiredInfo')}</p>
        <Form.Item shouldUpdate noStyle>
          {() => <ErrorBox fieldsErrors={form.getFieldsError()} />}
        </Form.Item>
        <FormItemWithInfo
          customName={`${props.name}_new-zuleitung`}
          label={<>{t('rv.newZuleitung.modal.page1.item1.label')}</>}
          name="typ"
          rules={[{ required: true, whitespace: true, message: t('rv.newZuleitung.modal.page1.item1.error') }]}
        >
          <Radio.Group className="horizontal-radios" name="typ">
            <Radio id="rv-newZuleitungModalPage1Item1OptionBkamt-radio" value={FreigegebenType.Bkamt}>
              {t('rv.newZuleitung.modal.page1.item1.options.BKAMT')}
            </Radio>
            <Radio
              id="rv-newZuleitungModalPage1Item1OptionBundesrat-radio"
              value={FreigegebenType.Bundesrat}
              disabled={true}
            >
              {t('rv.newZuleitung.modal.page1.item1.options.BUNDESRAT')}
            </Radio>
          </Radio.Group>
        </FormItemWithInfo>

        {/* Fileupload section */}
        <FileUploadComponent
          rvId={props.rvId}
          fileList={uploadedList}
          setFileList={setUploadedList}
          additionalFileList={additionalUploadedFileList}
          setAdditionalFileList={setAdditionalUploadedList}
          documentType={props.values.documentTyp}
          dmTitle={props.values.dmTitle}
          setValues={props.setValues}
        />
      </GeneralFormWrapper>
    </>
  );
}
