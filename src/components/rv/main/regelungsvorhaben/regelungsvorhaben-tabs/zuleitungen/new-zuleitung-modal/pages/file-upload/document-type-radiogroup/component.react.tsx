// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Radio, RadioChangeEvent } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType } from '@plateg/rest-api';
import { FormItemWithInfo } from '@plateg/theme';

import { ZuleitungValuesInterface } from '../../../component.react';

interface DocumentTypeProps {
  setValues?: Function;
  disabled: boolean;
  disabledItem?: DocumentType;
}
export function DocumentTypeRadiogroup(props: DocumentTypeProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <fieldset className="fieldset-form-items">
      <legend className="seo">{t('rv.fileUpload.typeOfDocument.label')}</legend>
      <FormItemWithInfo
        name="documentTyp"
        label={<span>{t('rv.fileUpload.typeOfDocument.label')}</span>}
        initialValue={DocumentType.Editor}
        rules={[{ required: true, message: t('rv.fileUpload.typeOfDocument.error') }]}
      >
        <Radio.Group
          disabled={props.disabled}
          onChange={(event: RadioChangeEvent) => {
            props.setValues?.((oldValues: ZuleitungValuesInterface) => {
              // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
              return { ...oldValues, documentTyp: event.target.value };
            });
          }}
          className="horizontal-radios"
          name="documentTyp"
        >
          <Radio
            id="fileupload-editorDocument-radio"
            value={DocumentType.Editor}
            disabled={props.disabledItem == DocumentType.Editor}
          >
            {t('rv.fileUpload.typeOfDocument.typeEditorDocument')}
          </Radio>
          <Radio
            id="fileupload-enormDocument-radio"
            value={DocumentType.Enorm}
            disabled={props.disabledItem == DocumentType.Enorm}
          >
            {t('rv.fileUpload.typeOfDocument.typeEnormDocument')}
          </Radio>
        </Radio.Group>
      </FormItemWithInfo>
    </fieldset>
  );
}
