// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './page-2-review.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType } from '@plateg/rest-api';
import { FileListComponent, FolderDefault } from '@plateg/theme';

import { ZuleitungValuesInterface } from '../../component.react';

interface Page2ReviewComponentProps {
  titel: string;
  activePageIndex: number;
  setActivePageIndex: (pageIndex: number) => void;
  rvId: string;
  values: ZuleitungValuesInterface;
}

export function Page2ReviewComponent(props: Page2ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const noAnswer = <em>{t('rv.newZuleitung.modal.page2.noAnswer')}</em>;
  return (
    <div className="page-2-review">
      <div className="review-section">
        <Title level={2}>{props.titel}</Title>
        <dl>
          <dt>{t('rv.newZuleitung.modal.page2.anliegen')}</dt>
          <dd>
            {t(`rv.newZuleitung.modal.page1.item1.options.${props.values.typ || ''}`, { defaultValue: noAnswer })}
          </dd>

          <dt>{t('rv.newZuleitung.modal.page2.formatDocument')}</dt>
          <dd>
            {t(`rv.newZuleitung.modal.page2.typeDoc.${props.values.documentTyp as DocumentType}`, {
              defaultValue: noAnswer,
            })}
          </dd>

          {props.values?.documentTyp === DocumentType.Editor ? (
            <>
              <dt>{t('rv.newZuleitung.modal.page2.selectedRE')}</dt>
              <dd>
                <FolderDefault />
                {props.values.dmTitle}
              </dd>
            </>
          ) : (
            <>
              <dt>{t('rv.newZuleitung.modal.page2.uploadedDraft')}</dt>
              <dd>
                <FileListComponent files={props.values.filesData.fileList} />
              </dd>
            </>
          )}
          {props.values.filesData.additionalFileList.length ? (
            <>
              <dt>{t('rv.newZuleitung.modal.page2.uploadedAttachments')}</dt>
              <dd>
                <FileListComponent files={props.values.filesData.additionalFileList} />
              </dd>
            </>
          ) : null}
        </dl>
      </div>
    </div>
  );
}
