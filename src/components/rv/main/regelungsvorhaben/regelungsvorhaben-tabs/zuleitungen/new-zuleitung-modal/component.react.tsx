// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  DocumentType,
  EditorRemoteControllerApi,
  FreigegebenType,
  RegelungsvorhabenEntityFullResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI, MultiPageModalComponent } from '@plateg/theme';

import { NewZuleitungModalController } from './controller';
import { Page2ReviewComponent } from './pages/page-2-review/component.react';
import { Page1SelectComponent } from './pages/page1/component.react';

export interface Files {
  files: UploadFile[];
  additionalFiles: UploadFile[];
}
interface FilesDataInterface {
  fileList: UploadFile[];
  initalFileList: UploadFile[];
  additionalFileList: UploadFile[];
  initalAdditionalFileList: UploadFile[];
}
export interface NewZuleitungModalProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  fetchZuleitungen: () => void;
}

export interface ZuleitungValuesInterface {
  typ?: FreigegebenType;
  dmTitle?: string;
  documentTyp: DocumentType;
  filesData: FilesDataInterface;
}
export function NewZuleitungModal(props: NewZuleitungModalProps): React.ReactElement {
  const { t } = useTranslation();
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const defaultValues: ZuleitungValuesInterface = {
    typ: FreigegebenType.Bkamt,
    dmTitle: undefined,
    documentTyp: DocumentType.Editor,
    filesData: { fileList: [], initalFileList: [], additionalFileList: [], initalAdditionalFileList: [] },
  };
  const [zuleitungValues, setZuleitungValues] = useState<ZuleitungValuesInterface>(defaultValues);

  const newZuleitungModalController = GlobalDI.getOrRegister(
    'newZuleitungModalController',
    () => new NewZuleitungModalController(),
  );
  const editorRemoteController = GlobalDI.getOrRegister(
    'editorRemoteControllerApi',
    () => new EditorRemoteControllerApi(),
  );

  useEffect(() => {
    if (!props.isVisible) {
      setActivePageIndex(0);
      setZuleitungValues(defaultValues);
    }
  }, [props.isVisible]);

  useEffect(() => {
    if (props.isVisible) {
      editorRemoteController
        .getDokumentenmappeBereitFuerKabinettverfahren({ rvId: props.regelungsvorhaben?.base.id })
        .subscribe({
          next: (response) => {
            setZuleitungValues((values) => ({
              ...values,
              dmTitle: response?.titel,
            }));
          },
        });
    }
  }, [props.isVisible]);

  return (
    <>
      <MultiPageModalComponent
        key={`New-Zuleitung-Modal-${props.isVisible.toString()}`}
        isVisible={props.isVisible}
        setIsVisible={props.setIsVisible}
        activePageIndex={activePageIndex}
        setActivePageIndex={setActivePageIndex}
        title={t('rv.newZuleitung.modal.title', { rvName: props.regelungsvorhaben.dto.abkuerzung })}
        cancelBtnText={t('rv.newZuleitung.modal.cancelBtnText')}
        nextBtnText={t('rv.newZuleitung.modal.nextBtnText')}
        prevBtnText={t('rv.newZuleitung.modal.prevBtnText')}
        componentReferenceContext={`newZuleitung-${activePageIndex}`}
        pages={[
          {
            formName: 'page1',
            content: (
              <Page1SelectComponent
                name="page1"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                title={t('rv.newZuleitung.modal.page1.title')}
                rvId={props.regelungsvorhaben.base.id}
                values={zuleitungValues}
                setValues={setZuleitungValues}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('rv.newZuleitung.modal.pruefenBtn'),
              disabled: zuleitungValues.documentTyp === DocumentType.Editor && !zuleitungValues.dmTitle,
            },
          },
          {
            content: (
              <Page2ReviewComponent
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                titel={t('rv.newZuleitung.modal.page2.title')}
                rvId={props.regelungsvorhaben.base.id}
                values={zuleitungValues}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('rv.newZuleitung.modal.page2.pruefenBtn'),
            },
            nextOnClick: () => {
              newZuleitungModalController.zuleitungSenden(
                props.regelungsvorhaben.base.id,
                props.setIsVisible,
                props.fetchZuleitungen,
                zuleitungValues,
              );
            },
          },
        ]}
      />
    </>
  );
}
