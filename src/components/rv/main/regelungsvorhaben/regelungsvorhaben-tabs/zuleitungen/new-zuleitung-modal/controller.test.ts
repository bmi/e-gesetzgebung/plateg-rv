// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import { DocumentType, FreigegebenType, RegelungsvorhabenControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../../../../general.test';
import { ZuleitungValuesInterface } from './component.react';
import { NewZuleitungModalController } from './controller';

describe('NewZuleitungModalController testen', () => {
  let ctrl: NewZuleitungModalController;
  let httpRequestStub: sinon.SinonStub;
  let setIsVisibleStub: sinon.SinonStub;
  let fetchZuleitungenStub: sinon.SinonStub;
  const regelungsvorhabenController = GlobalDI.getOrRegister<RegelungsvorhabenControllerApi>(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );
  before(() => {
    ctrl = new NewZuleitungModalController();
    setIsVisibleStub = sinon.stub();
    fetchZuleitungenStub = sinon.stub();
    httpRequestStub = sinon.stub(regelungsvorhabenController, 'createRegelungsvorhabenZuleitung');
  });
  after(() => {
    httpRequestStub.restore();
  });
  describe('TEST: zuleitungSenden - Success', () => {
    before(() => {
      httpRequestStub.returns(of(void 0));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      setLoadingStatusStub.reset();
      httpRequestStub.resetBehavior();
    });
    it('check if REST API is called ', (done) => {
      const rvId = 'abc';
      const zuleitungValues: ZuleitungValuesInterface = {
        typ: FreigegebenType.Bkamt,
        dokumentenmappeId: undefined,
        documentTyp: DocumentType.Editor,
        filesData: { fileList: [], initalFileList: [], additionalFileList: [], initalAdditionalFileList: [] },
      };
      ctrl.zuleitungSenden(rvId, setIsVisibleStub, fetchZuleitungenStub, zuleitungValues);
      setTimeout(() => {
        sinon.assert.calledTwice(setLoadingStatusStub);
        sinon.assert.notCalled(displayErrorMsgStub);
        sinon.assert.calledOnce(setIsVisibleStub);
        sinon.assert.calledOnce(fetchZuleitungenStub);
        done();
      }, 20);
    });
  });
  describe('TEST: zuleitungSenden - Failure', () => {
    before(() => {
      httpRequestStub.returns(throwError({ status: 404 }));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      httpRequestStub.resetBehavior();
    });
    it('check if REST API call failed', (done) => {
      const rvId = 'abc';
      const zuleitungValues: ZuleitungValuesInterface = {
        typ: FreigegebenType.Bkamt,
        dokumentenmappeId: undefined,
        documentTyp: DocumentType.Editor,
        filesData: { fileList: [], initalFileList: [], additionalFileList: [], initalAdditionalFileList: [] },
      };
      ctrl.zuleitungSenden(rvId, setIsVisibleStub, fetchZuleitungenStub, zuleitungValues);
      setTimeout(() => {
        sinon.assert.calledTwice(setLoadingStatusStub);
        sinon.assert.calledOnce(displayErrorMsgStub);
        sinon.assert.calledOnce(displayErrorMsgStub);
        done();
      }, 20);
    });
  });
});
