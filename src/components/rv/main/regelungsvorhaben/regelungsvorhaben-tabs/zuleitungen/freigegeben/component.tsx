// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { format } from 'date-fns';
import { de } from 'date-fns/locale';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenZuleitungTableDTO } from '@plateg/rest-api';
import { TwoLineText } from '@plateg/theme';

import { getLabelZuleitungDiffCases, getLabelZustellungApproved } from '../controller';

interface Props {
  record: RegelungsvorhabenZuleitungTableDTO;
}

export const getDateFormat = (date: string) => {
  return format(new Date(date), 'dd.MM.yyyy · HH:mm', { locale: de });
};

export function ZuleitungFreigegeben(props: Props): React.ReactElement {
  const { t } = useTranslation();

  return (
    <TwoLineText
      elementId={`ferigegeben-${props.record.id}`}
      firstRow={`
        ${getLabelZuleitungDiffCases(props.record.freigegebenFuer)} 
        ${getLabelZustellungApproved(props.record.freigegebenFuer)}
      `}
      firstRowBold={false}
      secondRow={props.record.erstelltAm ? getDateFormat(props.record.erstelltAm) : ''}
      secondRowBold={false}
      secondRowLight={false}
    ></TwoLineText>
  );
}
