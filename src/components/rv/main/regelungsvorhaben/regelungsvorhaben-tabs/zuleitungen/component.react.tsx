// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zuleitungen.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityFullResponseDTO,
  RegelungsvorhabenZuleitungTableDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { RvErrorController } from '../../../../../../utils/rv-error-controller';
import { getZuleitungenTableVals } from './controller';
import { NewZuleitungModal } from './new-zuleitung-modal/component.react';

interface ZuleitungenProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  isArchived?: boolean | string;
}

export type TableCompPropsRvZuleitungen = TableComponentProps<RegelungsvorhabenZuleitungTableDTO>;

export function Zuleitungen(props: ZuleitungenProps): React.ReactElement {
  const { t } = useTranslation();
  const [zuleitungen, setZuleitungen] = useState<RegelungsvorhabenZuleitungTableDTO[]>();
  const [zuleitungenTableVals, setZuleitungenTableVals] = useState<TableCompPropsRvZuleitungen>({
    columns: [],
    content: [],
    id: '',
    expandable: false,
  });
  const appStore = useAppSelector((state) => state.user);

  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [currentUserIsStellvertreter, setCurrentUserIsStellvertreter] = useState(false);

  const rvCtrl = GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi());
  const loadingStatusCtrl = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  let rvSub: Subscription;
  useEffect(() => {
    const mandant = appStore.user?.dto.ressort?.kurzbezeichnung;
    if (mandant === 'BR') {
      fetchBRZuleitungen();
    } else if (mandant === 'BT') {
      fetchBTZuleitungen();
    } else {
      fetchBRegZuleitungen();
    }
    return () => {
      rvSub.unsubscribe();
    };
  }, []);

  const fetchBRegZuleitungen = () => {
    if (rvSub) {
      rvSub.unsubscribe();
    }
    loadingStatusCtrl.setLoadingStatus(true);

    rvSub = rvCtrl.getRegelungsvorhabenZuleitungTable({ id: props.regelungsvorhaben.base.id }).subscribe({
      next: (data) => {
        setZuleitungen(data);
      },
      complete: () => {
        loadingStatusCtrl.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        rvErrorCtrl.handleError(error);
      },
    });
  };

  const fetchBRZuleitungen = () => {
    if (rvSub) {
      rvSub.unsubscribe();
    }
    loadingStatusCtrl.setLoadingStatus(true);

    /**
     * EGBR-529: Filters out RueckmeldungType.AENDERUNG_GEWUENSCHT_PKP.
     * This is already implemented by getRegelungsvorhabenZuleitungTableBundestag, which is a specialized version of getRegelungsvorhabenZuleitungTable.
     */
    rvSub = rvCtrl.getRegelungsvorhabenZuleitungTableBundestag({ id: props.regelungsvorhaben.base.id }).subscribe({
      next: (data) => {
        setZuleitungen(data);
      },
      complete: () => {
        loadingStatusCtrl.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        rvErrorCtrl.handleError(error);
      },
    });
  };

  const fetchBTZuleitungen = () => {
    if (rvSub) {
      rvSub.unsubscribe();
    }
    loadingStatusCtrl.setLoadingStatus(true);
    rvSub = rvCtrl.getRegelungsvorhabenZuleitungTableBundestag({ id: props.regelungsvorhaben.base.id }).subscribe({
      next: (data) => {
        setZuleitungen(data);
      },
      complete: () => {
        loadingStatusCtrl.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        rvErrorCtrl.handleError(error);
      },
    });
  };

  useEffect(() => {
    if (appStore.user?.dto.stellvertreter) {
      setCurrentUserIsStellvertreter(true);
    }
  }, [appStore.user]);

  useEffect(() => {
    if (zuleitungen) {
      setZuleitungenTableVals(
        getZuleitungenTableVals(
          zuleitungen,
          props.regelungsvorhaben.base.id,
          appStore.user?.dto.ressort?.kurzbezeichnung,
        ),
      );
    }
  }, [zuleitungen]);

  return (
    <div className="zuleitungen-container">
      <div className="zuleitung-entries-div">
        {zuleitungen && (
          <span>
            {`${zuleitungen.length} ${
              zuleitungen.length === 1
                ? t('rv.regelungsvorhabenTabs.tabs.zuleitungen.counter.sgl')
                : t('rv.regelungsvorhabenTabs.tabs.zuleitungen.counter.pl')
            }
          `}
          </span>
        )}
        {appStore.user?.dto.ressort?.kurzbezeichnung !== 'BT' && (
          <>
            <Button
              type="primary"
              onClick={() => setIsVisible(true)}
              disabled={
                !props.regelungsvorhaben.dto.pkpGesendetAm ||
                currentUserIsStellvertreter ||
                (props.isArchived as boolean) ||
                props.regelungsvorhaben.dto.status === VorhabenStatusType.Bundestag
              }
            >
              {t('rv.regelungsvorhabenTabs.tabs.zuleitungen.sendZultngBtn')}
            </Button>
            <NewZuleitungModal
              isVisible={isVisible}
              setIsVisible={setIsVisible}
              regelungsvorhaben={props.regelungsvorhaben}
              fetchZuleitungen={fetchBRegZuleitungen}
            />
          </>
        )}
      </div>
      <div className="table-container">
        {zuleitungenTableVals?.content.length > 0 && (
          <TableComponent
            hiddenRowCount
            columns={zuleitungenTableVals.columns}
            content={zuleitungenTableVals.content}
            expandable={zuleitungenTableVals.expandable}
            id={zuleitungenTableVals.id}
            key="rv-zuleitungen-table"
          />
        )}
      </div>
    </div>
  );
}
