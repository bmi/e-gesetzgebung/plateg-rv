// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType, RegelungsvorhabenZuleitungTableDTO } from '@plateg/rest-api';
import { DocumentListPopover } from '@plateg/theme';

interface ZuleitungDokumenteInterface {
  record: RegelungsvorhabenZuleitungTableDTO;
}
// this is hardcoded for first iter.

export function ZuleitungDokumente(props: ZuleitungDokumenteInterface): React.ReactElement {
  const { t } = useTranslation();
  const docListTitle = t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.docs.showDocsSent');
  return (
    <div className="zuleitung-docs-container">
      {props.record.documentTyp === DocumentType.Enorm && (
        <DocumentListPopover
          documentsList={props.record.gesendeteDokumente}
          title={docListTitle}
          recordId={props.record.id}
        />
      )}
      {props.record.documentTyp === DocumentType.Editor && props.record.dokumentenmappe && (
        <DocumentListPopover
          dokumentenmappe={props.record.dokumentenmappe}
          title={docListTitle}
          recordId={props.record.id}
        />
      )}
    </div>
  );
}
