// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import i18n from 'i18next';
import React from 'react';
import { AjaxError } from 'rxjs/ajax';

import {
  FreigegebenType,
  ProzesschrittType,
  RegelungsvorhabenZuleitungTableDTO,
  RueckmeldungType,
  RvWorkaroundControllerApi,
  Status,
  StatusStatusEnum,
} from '@plateg/rest-api';
import { displayMessage, DropdownMenu, GlobalDI, LoadingStatusController, TwoLineText } from '@plateg/theme';

import { RvErrorController } from '../../../../../../utils/rv-error-controller';
import { TableCompPropsRvZuleitungen } from './component.react';
import { ZuleitungDokumente } from './dokumente/component';
import { ZuleitungFreigegeben } from './freigegeben/component';
import { ZuleitungRuckmeldung } from './rueckmeldung/component';

export const getLabelZuleitungDiffCases = (label: ProzesschrittType | FreigegebenType | RueckmeldungType) => {
  return i18n
    .t(
      `rv.regelungsvorhabenTabs.tabs.zuleitungen.table.labels.${label as ProzesschrittType | FreigegebenType | RueckmeldungType}`,
    )
    .toString();
};

export const getLabelZustellungApproved = (approved: FreigegebenType) => {
  let labelZustellungApproved;
  switch (approved) {
    case FreigegebenType.Bundestag:
      labelZustellungApproved = i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.approved.pd1');
      break;
    case FreigegebenType.Bundesrat:
      labelZustellungApproved = i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.approved.p1');
      break;
    default:
      labelZustellungApproved = i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.approved.pkp');
      break;
  }
  return labelZustellungApproved.toString();
};

export function setRVBRWeitergeleitetCall(rvId: string) {
  // Composes status DTO like it can be sent by PKP.
  const status: Status = {
    status: StatusStatusEnum.BrWeitergeleitet,
    kommentar: 'gesetzt von Frontend als Workaroud für PKP',
    datei_liste: [],
  };

  const ctrl = GlobalDI.get<RvWorkaroundControllerApi>('rvWorkaroundController');

  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  loadingStatusController.setLoadingStatus(true);

  ctrl.setRegelungsvorhabenStatus({ id: rvId, status }).subscribe({
    next: () => {
      displayMessage(
        i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.brWeitergeleitet.successMessage'),
        'success',
      );
      loadingStatusController.setLoadingStatus(false);
    },
    error: (error: AjaxError) => {
      console.error('Error sub', error);
      loadingStatusController.setLoadingStatus(false);
      rvErrorCtrl.handleError(error);
    },
  });
}

export function setRVBTWeitergeleitetCall(rvId: string) {
  const ctrl = GlobalDI.get<RvWorkaroundControllerApi>('rvWorkaroundController');

  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  loadingStatusController.setLoadingStatus(true);
  ctrl.setRegelungsvorhabenStatusBtWeitergeleitet({ id: rvId }).subscribe({
    next: () => {
      displayMessage(
        i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.btWeitergeleitet.successMessage'),
        'success',
      );
      loadingStatusController.setLoadingStatus(false);
    },
    error: (error: AjaxError) => {
      console.error('Error sub', error);
      loadingStatusController.setLoadingStatus(false);
      rvErrorCtrl.handleError(error);
    },
  });
}

export const getZuleitungenTableVals = (
  content: RegelungsvorhabenZuleitungTableDTO[],
  RvId: string,
  ressortKurzbezeichnung: string,
): TableCompPropsRvZuleitungen => {
  const columns: ColumnsType<RegelungsvorhabenZuleitungTableDTO> = [
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.header.process').toString(),
      fixed: 'left',
      key: 'prozessschritt',
      render: (record: RegelungsvorhabenZuleitungTableDTO) => {
        return (
          <TwoLineText
            firstRow={getLabelZuleitungDiffCases(record.prozesschritt)}
            firstRowBold
            elementId={`prozess-${record.id}`}
            // later edit hard coded val
            secondRow={i18n
              .t(`rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.processSteps.${record.freigegebenFuer}`)
              .toString()}
            secondRowBold={false}
            secondRowLight={false}
          />
        );
      },
    },
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.header.docs').toString(),
      key: 'dokumente',
      render: (record: RegelungsvorhabenZuleitungTableDTO) => {
        return <ZuleitungDokumente key={`docs-${record.id}`} record={record} />;
      },
    },
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.header.approved').toString(),
      key: 'freigegeben',
      render: (record: RegelungsvorhabenZuleitungTableDTO) => {
        return <ZuleitungFreigegeben record={record} />;
      },
    },
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.header.status').toString(),
      key: 'rueckmeldung',
      render: (record: RegelungsvorhabenZuleitungTableDTO) => {
        return <ZuleitungRuckmeldung record={record} />;
      },
    },
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.header.actions').toString(),
      key: 'aktionen',
      render: (record: RegelungsvorhabenZuleitungTableDTO) => {
        // removed entries for Weiterleitung BT/BR but kept code as per PLATEG-6709, will be removed in PLATEG-6711
        const showZuleitungWorkaround = false;
        return (
          <div className="actions-holder">
            <DropdownMenu
              items={[
                {
                  element: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.actions.feedbackDocFolder'),
                  disabled: () => true,
                },
                {
                  element: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.actions.openFeedback'),
                  disabled: () => true,
                },
                {
                  element: i18n.t('rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.actions.versionHistory'),
                  disabled: () => true,
                },
                ...(showZuleitungWorkaround && ressortKurzbezeichnung !== 'BT' && ressortKurzbezeichnung !== 'BR'
                  ? [
                      {
                        element: i18n.t(
                          'rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.actions.statusSetzenBTWeitergeleitet',
                        ),
                        disabled: () =>
                          record.rueckmeldung === RueckmeldungType.AenderungGewuenschtPkp ||
                          record.rueckmeldung === RueckmeldungType.ZuleitungEmpfangen ||
                          record.rueckmeldung === RueckmeldungType.AenderungGewuenschtFederfuehrer,
                        onClick: () => {
                          setRVBTWeitergeleitetCall(RvId);
                        },
                      },
                    ]
                  : []),
                ...(showZuleitungWorkaround && ressortKurzbezeichnung !== 'BT' && ressortKurzbezeichnung !== 'BR'
                  ? [
                      {
                        disabled: () =>
                          record.rueckmeldung === RueckmeldungType.AenderungGewuenschtPkp ||
                          record.rueckmeldung === RueckmeldungType.ZuleitungEmpfangen ||
                          record.rueckmeldung === RueckmeldungType.AenderungGewuenschtFederfuehrer,
                        element: i18n.t(
                          'rv.regelungsvorhabenTabs.tabs.zuleitungen.table.body.actions.statusSetzenBRWeitergeleitet',
                        ),
                        onClick: () => {
                          setRVBRWeitergeleitetCall(RvId);
                        },
                      },
                    ]
                  : []),
              ]}
              elementId={`zuleitung-action-${record.id}`}
            />
          </div>
        );
      },
    },
  ];

  return {
    id: 'rv-zuleitungen-table',
    expandable: false,
    columns,
    content,
  };
};
