// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../pruefen-regelungsvorhaben/pruefen-regelungsvorhaben.less';

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';

import {
  BASE_PATH,
  CodelistenResponseDTO,
  RegelungsvorhabenEntityFullResponseDTO,
  RolleLokalType,
} from '@plateg/rest-api';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';

import { routes } from '../../../../../../shares/routes';
import { DesignationPruefenRegelungsvorhabenComponent } from '../../pruefen-regelungsvorhaben/designation/component.react';
import { ParticipantsPruefenRegelungsvorhabenComponent } from '../../pruefen-regelungsvorhaben/participants/component.react';
import { PkpComfirmModalWrapperDatenblatt } from '../../pruefen-regelungsvorhaben/pkp-confirm-modal-wrapper-datenblatt/component.react';
import { VorhabenartPruefenRegelungsvorhabenComponent } from '../../pruefen-regelungsvorhaben/vorhabenart/component.react';
import { isRelevantKurzbezeichnung } from '../../controller';

interface DatenblattProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  codeList: CodelistenResponseDTO | undefined;
  tabName: string;
  currentUserIsStellvertreter?: boolean;
  refreshRV: () => void;
}
export function Datenblatt(props: DatenblattProps): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcher = useRouteMatch<{ id: string; tabName: string; action: string }>(
    '/regelungsvorhaben/:tabName/:id/:action',
  );
  const appStore = useAppSelector((state) => state.user);
  const history = useHistory();
  const isBRorBTuser = isRelevantKurzbezeichnung(appStore.user?.dto.ressort?.kurzbezeichnung);

  return (
    <>
      {props.regelungsvorhaben && (
        <div className="pruefen-seite rv-datenblatt">
          <PkpComfirmModalWrapperDatenblatt
            currentUserIsStellvertreter={
              props.currentUserIsStellvertreter ||
              props.regelungsvorhaben.dto.rolleTyp === RolleLokalType.Gast ||
              props.regelungsvorhaben.dto.rolleTyp === RolleLokalType.Keine
            }
            regelungsvorhaben={props.regelungsvorhaben}
            refreshRV={props.refreshRV}
          />
          <DesignationPruefenRegelungsvorhabenComponent regelungsvorhaben={props.regelungsvorhaben.dto} />
          <ParticipantsPruefenRegelungsvorhabenComponent
            codeList={props.codeList}
            regelungsvorhaben={props.regelungsvorhaben.dto}
          />
          <VorhabenartPruefenRegelungsvorhabenComponent
            codeList={props.codeList}
            regelungsvorhaben={props.regelungsvorhaben.dto}
          />
          <div className="form-control-buttons">
            {!isBRorBTuser && (
              <Button
                id="rv-datenblattRegelungsvorhaben-edit-btn"
                disabled={!props.regelungsvorhaben.dto.allowedToModify || props.regelungsvorhaben.dto.zuleitungOffen}
                size={'large'}
                onClick={() => {
                  history.push(
                    `/regelungsvorhaben/${props.tabName}/${routeMatcher?.params.id as string}/${
                      props.tabName === routes.ENTWURFE
                        ? routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF
                        : routes.BEARBEITEN_REGELUNGSVORHABEN
                    }`,
                  );
                }}
              >
                {t('rv.datenblattRegelungsvorhaben.btns.edit')}
              </Button>
            )}
            <Button
              className="ant-btn-lg"
              id="datenblatt-rv-regelungsvorhabenexport-btn"
              key="export-button"
              type="primary"
              onClick={(e) =>
                openFile(`${BASE_PATH}/file/regelungsvorhabenexport/${props.regelungsvorhaben.base.id || ''}/pdf`, {
                  target: '_blank',
                  fileName: `${props.regelungsvorhaben.dto.kurzbezeichnung}.pdf`,
                  onStart: () => (e.target.disabled = true),
                  onSuccess: () => (e.target.disabled = false),
                  onError: (error) => {
                    e.target.disabled = false;
                    console.error(error);
                  },
                })
              }
            >
              {t('rv.header.exportButton')}
            </Button>
          </div>
        </div>
      )}
    </>
  );
}
