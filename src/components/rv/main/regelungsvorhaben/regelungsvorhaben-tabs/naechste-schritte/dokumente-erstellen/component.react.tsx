// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  RegelungsvorhabenEntityFullResponseDTO,
  RvProzesshilfeEgfaDTO,
  RvProzesshilfeZeitplanungDTO,
} from '@plateg/rest-api';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface DokumenteErstellenProps {
  zeitplanung?: RvProzesshilfeZeitplanungDTO;
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  egfa?: RvProzesshilfeEgfaDTO;
  hideProgress?: boolean;
}

export function DokumenteErstellenComponent(props: DokumenteErstellenProps): React.ReactElement {
  const { t } = useTranslation();
  const isArchivedZeitpl = props.zeitplanung?.archiviertAm ? 'archiv/meineZeitplanungen' : 'meineZeitplanungen';
  const isZeitplanungActive = props.zeitplanung && props.zeitplanung.archiviertAm === null;
  const isEgfaActive = props.egfa && props.egfa.archiviertAm === null;

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.DOKUMENTEERSTELLEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.DOKUMENTEERSTELLEN].ZEITPLANUNG,
      isZeitplanungActive
        ? `/zeit/${isArchivedZeitpl}/${props.zeitplanung?.zeitplanungId}/tabellensicht`
        : `/zeit/neueZeitplanung/regelungsvorhaben/${props.regelungsvorhaben.base.id}`,
      !!isZeitplanungActive,
      undefined,
      true,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.DOKUMENTEERSTELLEN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.DOKUMENTEERSTELLEN].ZEITPLANUNG
        }.${isZeitplanungActive ? 'aktuellenZeitplanung' : 'createLink'}`,
      ),
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.DOKUMENTEERSTELLEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.DOKUMENTEERSTELLEN].REGELUNGSALTERNATIVEN,
      '/evor',
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.DOKUMENTEERSTELLEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.DOKUMENTEERSTELLEN].GESETZESFOLGENABSCHAETZUNG,
      isEgfaActive
        ? `/egfa/${props.egfa?.egfaId}/modulUebersicht`
        : `/egfa/neueGesetzesfolgenabschaetzung/regelungsvorhaben/${props.regelungsvorhaben.base.id}`,
      !!isEgfaActive,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.DOKUMENTEERSTELLEN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.DOKUMENTEERSTELLEN].GESETZESFOLGENABSCHAETZUNG
        }.${isEgfaActive ? 'aktuellenEgfa' : 'createLink'}`,
      ),
      props.hideProgress,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.DOKUMENTEERSTELLEN}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.DOKUMENTE_ERSTELLEN}
    />
  );
}
