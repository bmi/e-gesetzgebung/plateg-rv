// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { RegelungsvorhabenEntityFullResponseDTO } from '@plateg/rest-api';

import { getCollapseItem, NEXT_STEP_SUB_TYPE, NEXT_STEP_TYPE, NextStepsContentMain, NextStepsTabsNameEnum } from '..';

interface ParlamentsverfahrenVorbereitenProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  hideProgress?: boolean;
}

export function ParlamentsverfahrenEndgueltigeFassungComponent(
  props: ParlamentsverfahrenVorbereitenProps,
): React.ReactElement {
  const collapseItems: CollapseProps[] = [
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG].DRUCKFAHNEERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG].VORLAGELEKTORIEREN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG].ANDERUNGENABSTIMMEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG].FASSUNGERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG].DRUCKAUFTRAGGEBEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG].FASSUNGVERTEILEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.PARLAMENTSVERFAHRENENDGUELTIGEFASSUNG}
    />
  );
}
