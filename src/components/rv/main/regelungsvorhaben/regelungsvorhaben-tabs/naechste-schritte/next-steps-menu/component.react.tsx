// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { RegelungsvorhabenBestandsrechtTableDTO, RvProzesshilfeOverviewDTO } from '@plateg/rest-api';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../../../../../shares/routes';
import {
  AusschussverfahrenDurchfuehrenComponent,
  BeratungPlenumVorbereitenComponent,
  DokumenteErstellenComponent,
  KabinettverfahrenDurchfuehren,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  NotifizierungUebersendenComponent,
  ParlamentsverfahrenEndgueltigeFassungComponent,
  ParlamentsverfahrenVorabfassungComponent,
  PlenarsitzungDurchfuehrenComponent,
  RegelungsentwurfAbstimmenComponent,
  RegelungsentwurfErarbeitenComponent,
  RegierungsentwuerfeComponent,
} from '../index';

interface NaechsteSchritteProps extends RvProzesshilfeOverviewDTO {
  bestandsrechtData: RegelungsvorhabenBestandsrechtTableDTO[];
  setBestandsrechtData: (vals: RegelungsvorhabenBestandsrechtTableDTO[]) => void;
}

export function MainRoutesNaechsteSchritte(props: NaechsteSchritteProps): React.ReactElement {
  const appStore = useAppSelector((state) => state.user);
  const [currentUserIsBTUser, setCurrentUserIsBTUser] = useState<boolean>(false);
  const [currentUserIsBRUser, setCurrentUserIsBRUser] = useState<boolean>(false);

  const BRegDefaultStep: React.ReactElement = (
    <DokumenteErstellenComponent
      zeitplanung={props.letzteZeitplanung}
      regelungsvorhaben={props.regelungsvorhaben}
      egfa={props.letzteEgfa}
      hideProgress={currentUserIsBTUser}
    />
  );

  const BTDefaultStep: React.ReactElement = (
    <ParlamentsverfahrenVorabfassungComponent regelungsvorhaben={props.regelungsvorhaben} />
  );

  const BRatDefaultStep: React.ReactElement = <RegierungsentwuerfeComponent />;

  const [defaultStep, setDefaultStep] = useState<React.ReactElement>(BRegDefaultStep);
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>('/regelungsvorhaben/:id');
  let existingRegelungsvorhabenId: string | undefined;
  const { t } = useTranslation();
  if (routeMatcherVorbereitung?.params?.id != null) {
    existingRegelungsvorhabenId = routeMatcherVorbereitung?.params.id;
  }

  useEffect(() => {
    switch (appStore.user?.dto.ressort?.kurzbezeichnung) {
      case 'BT':
        setCurrentUserIsBTUser(true);
        setCurrentUserIsBRUser(false);
        setDefaultStep(BTDefaultStep);
        break;
      case 'BR':
        setCurrentUserIsBTUser(false);
        setCurrentUserIsBRUser(true);
        setDefaultStep(BRatDefaultStep);
        break;
      default:
        setCurrentUserIsBTUser(false);
        setCurrentUserIsBRUser(false);
        setDefaultStep(BRegDefaultStep);
    }
  }, [appStore.user]);

  return (
    <Switch>
      <Route exact path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}`]}>
        {defaultStep}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.DOKUMENTEERSTELLEN}`]}>
        {currentUserIsBTUser || currentUserIsBRUser ? (
          defaultStep
        ) : (
          <DokumenteErstellenComponent
            zeitplanung={props.letzteZeitplanung}
            regelungsvorhaben={props.regelungsvorhaben}
            egfa={props.letzteEgfa}
            hideProgress={currentUserIsBTUser || currentUserIsBRUser}
          />
        )}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.REGELUNGSENTWURFERARBEITEN}`]}>
        {currentUserIsBTUser || currentUserIsBRUser ? (
          defaultStep
        ) : (
          <RegelungsentwurfErarbeitenComponent
            regelungsvorhaben={props.regelungsvorhaben}
            letzteDokumentmappe={props.letzteDokumentenmappe}
            bestandsrechtData={props.bestandsrechtData}
            setBestandsrechtData={props.setBestandsrechtData}
            hideProgress={currentUserIsBTUser || currentUserIsBRUser}
          />
        )}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.REGELUNGSENTWURFABSTIMMEN}`]}>
        {currentUserIsBTUser || currentUserIsBRUser ? (
          defaultStep
        ) : (
          <RegelungsentwurfAbstimmenComponent
            regelungsvorhaben={props.regelungsvorhaben}
            hausabstimmung={props.letzteHausabstimmung}
            ressortabstimmung={props.letzteRessortabstimmung}
            vorhabenclearing={props.letztesVorhabenclearing}
            hideProgress={currentUserIsBTUser || currentUserIsBRUser}
          />
        )}
      </Route>
      <Route
        path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.KABINETTVERFAHREN_DURCHFUEHREN}`]}
      >
        {currentUserIsBTUser || currentUserIsBRUser ? (
          defaultStep
        ) : (
          <KabinettverfahrenDurchfuehren
            regelungsvorhaben={props.regelungsvorhaben}
            letzteDokumentmappe={props.letzteDokumentenmappe}
            letzteZuleitung={props.letzteZuleitung}
            letzteSchlussabstimmung={props.letzteSchlussabstimmung}
            letzteAbstimmungHausleitung={props.letzteAbstimmungDerVorlageFuerDenRegierungsentwurf}
            hideProgress={currentUserIsBTUser || currentUserIsBRUser}
          />
        )}
      </Route>
      <Route
        path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.PARLAMENTSVERFAHREN_VORABFASSUNG}`]}
      >
        {currentUserIsBTUser ? (
          <ParlamentsverfahrenVorabfassungComponent
            regelungsvorhaben={props.regelungsvorhaben}
            hideProgress={!currentUserIsBTUser}
          />
        ) : (
          defaultStep
        )}
      </Route>
      <Route
        path={[
          `/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG}`,
        ]}
      >
        {currentUserIsBTUser ? (
          <ParlamentsverfahrenEndgueltigeFassungComponent
            regelungsvorhaben={props.regelungsvorhaben}
            hideProgress={!currentUserIsBTUser}
          />
        ) : (
          defaultStep
        )}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.REGIERUNGSENTWUERFE}`]}>
        {currentUserIsBRUser ? <RegierungsentwuerfeComponent /> : defaultStep}
      </Route>
      <Route
        path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.AUSSCHUSSVERFAHREN_DURCHFUEHREN}`]}
      >
        {currentUserIsBRUser ? <AusschussverfahrenDurchfuehrenComponent /> : defaultStep}
      </Route>
      <Route
        path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.BERATUNG_PLENUM_VORBEREITEN}`]}
      >
        {currentUserIsBRUser ? <BeratungPlenumVorbereitenComponent /> : defaultStep}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.PLENARSITZUNG_DURCHFUEHREN}`]}>
        {currentUserIsBRUser ? <PlenarsitzungDurchfuehrenComponent /> : defaultStep}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.NOTIFIZIERUNG_UEBERSENDEN}`]}>
        {currentUserIsBRUser ? <NotifizierungUebersendenComponent /> : defaultStep}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.EINLEITUNGSVERFAHREN}`]}>
        {currentUserIsBTUser || currentUserIsBRUser ? (
          <NextStepsContentMain
            stepType={NEXT_STEP_TYPE.EINLEITUNGSVERFAHREN}
            evirSectionTabname={NextStepsTabsNameEnum.EINLEITUNGSVERFAHREN}
            collapseItems={undefined}
            lockReason={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.lockReason.bRegReason')}
          />
        ) : (
          BRegDefaultStep
        )}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.STELLUNGNAHMEBR}`]}>
        {!currentUserIsBRUser ? (
          <NextStepsContentMain
            stepType={NEXT_STEP_TYPE.STELLUNGNAHMEBR}
            evirSectionTabname={NextStepsTabsNameEnum.STELLUNGNAHMEBR}
            collapseItems={undefined}
            lockReason={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.lockReason.bRatReason')}
          />
        ) : (
          defaultStep
        )}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.GESETZGEBUNGSVERFAHRENBRAT}`]}>
        <NextStepsContentMain
          stepType={NEXT_STEP_TYPE.GESETZGEBUNGSVERFAHRENBRAT}
          evirSectionTabname={NextStepsTabsNameEnum.GESETZGEBUNGSVERFAHRENBRAT}
          collapseItems={undefined}
          lockReason={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.lockReason.bRatReason')}
        />
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.ABSCHLUSSVERFAHREN}`]}>
        {currentUserIsBTUser || currentUserIsBRUser ? (
          <NextStepsContentMain
            stepType={NEXT_STEP_TYPE.ABSCHLUSSVERFAHREN}
            evirSectionTabname={NextStepsTabsNameEnum.ABSCHLUSSVERFAHREN}
            collapseItems={undefined}
            lockReason={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.lockReason.bAmtReason')}
          />
        ) : (
          BRegDefaultStep
        )}
      </Route>
      <Route path={[`/regelungsvorhaben/:tabName/:id/${routes.NAECHSTESCHRITTE}/${routes.GESETZGEBUNGSVERFAHRENBT}`]}>
        {!currentUserIsBTUser ? (
          <NextStepsContentMain
            stepType={NEXT_STEP_TYPE.GESETZGEBUNGSVERFAHRENBTAG}
            evirSectionTabname={NextStepsTabsNameEnum.GESETZGEBUNGSVERFAHRENBTAG}
            collapseItems={undefined}
            lockReason={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.lockReason.bTagReason')}
          />
        ) : (
          BTDefaultStep
        )}
      </Route>
    </Switch>
  );
}
