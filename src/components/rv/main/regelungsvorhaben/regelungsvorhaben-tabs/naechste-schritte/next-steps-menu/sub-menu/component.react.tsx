// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { MenuItem } from '../../../naechste-schritte/component.react';

interface SubmenuProps {
  activeIndex: number;
  clickableMenuItems: MenuItem[];
  changeActiveTabs: (index: number) => void;
}

export function NaechsteSchritteSubmenu({
  activeIndex,
  clickableMenuItems,
  changeActiveTabs,
}: SubmenuProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <Row className="naechste-schritte-collapse">
      <Col span={16}>
        <Row justify="space-between">
          <Col span={4}>
            <Button
              className={`prev-link`}
              disabled={activeIndex <= 0}
              type="text"
              onClick={() => changeActiveTabs(-1)}
            >
              <span>{t('rv.newZuleitung.modal.prevBtnText')}</span>
            </Button>
          </Col>
          <Col span={4}>
            <Button
              className={`next-link`}
              disabled={activeIndex >= clickableMenuItems.length - 1}
              type="text"
              onClick={() => changeActiveTabs(1)}
            >
              <span>{t('rv.newZuleitung.modal.nextBtnText')}</span>
            </Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
