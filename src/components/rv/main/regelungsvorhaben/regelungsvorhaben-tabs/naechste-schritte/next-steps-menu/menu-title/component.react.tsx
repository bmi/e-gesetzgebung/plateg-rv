// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { CheckOutlinedGreen, IconLocked } from '@plateg/theme';
import { CheckInProgress } from '@plateg/theme/src/components/icons/CheckInProgress';

export enum ProgressLevel {
  complete = 'COMPLETE',
  partial = 'PARTIAL',
  locked = 'LOCKED',
}
interface MenuTitleProps {
  title: string;
  disabled?: boolean;
  progress?: ProgressLevel;
  hideIcon?: boolean;
}

export function MenuTitle(props: MenuTitleProps): React.ReactElement {
  const { t } = useTranslation();
  let progressIcon: React.ReactNode | null;
  switch (props.progress) {
    case ProgressLevel.complete:
      progressIcon = (
        <span>
          <CheckOutlinedGreen />
        </span>
      );
      break;
    case ProgressLevel.partial:
      progressIcon = (
        <span>
          <CheckInProgress />
        </span>
      );
      break;
    case ProgressLevel.locked:
      progressIcon = (
        <span>
          <IconLocked />
        </span>
      );
      break;
    default:
      progressIcon = null;
      break;
  }
  return (
    <>
      {(!props.hideIcon || props.progress === ProgressLevel.locked) && (
        <div className="tab-icon-holder">{progressIcon}</div>
      )}
      <div>
        <Title className="next-steps-side-title" level={3}>
          {props.title}
          {props.progress === ProgressLevel.locked &&
            ` ${t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.keinZugriff')}`}
        </Title>
        {props.disabled && (
          <Title level={3} className="next-steps-side-title">
            {t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.kuenftigVerfuegbar')}
          </Title>
        )}
      </div>
    </>
  );
}
