// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { DokumentenmappeDTOStatusEnum, RueckmeldungType, RvProzesshilfeOverviewDTO } from '@plateg/rest-api';

import { StepItemBody, StepItemTitle } from './';

export const NEXT_STEP_TYPE = {
  DOKUMENTEERSTELLEN: 'dokumenteErstellen',
  REGELUNGSENTWURFERARBEITEN: 'regelungsentwurfErarbeiten',
  REGELUNGSENTWURFABSTIMMEN: 'regelungsentwurfAbstimmen',
  KABINETTVERFAHREN: 'kabinettverfahren',
  PARLAMENTSVERFAHREN_VORABFASSUNG: 'parlamentsverfahrenVorabfassung',
  PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG: 'parlamentsverfahrenEndgueltigeFassung',
  EINLEITUNGSVERFAHREN: 'einleitungsverfahrenKeinZugriff',
  GESETZGEBUNGSVERFAHRENBRAT: 'gesetzgebungsverfahrenBRATZugriff',
  ABSCHLUSSVERFAHREN: 'abschlussverfahrenZugriff',
  GESETZGEBUNGSVERFAHRENBTAG: 'gesetzgebungsverfahrenBTAGZugriff',
  STELLUNGNAHMEBR: 'stellungnahmeBRKeinZugriff',
  REGIERUNGSENTWUERFE: 'regierungsentwuerfe',
  AUSSCHUSSVERFAHREN_DURCHFUEHREN: 'ausschussverfahrenDurchfuehren',
  BERATUNG_PLENUM_VORBEREITEN: 'beratungPlenumVorbereiten',
  PLENARSITZUNG_DURCHFUEHREN: 'plenarsitzungDurchfuehren',
  NOTIFIZIERUNG_UEBERSENDEN: 'notifizierungUebersenden',
} as const;

export const NEXT_STEP_SUB_TYPE = {
  [NEXT_STEP_TYPE.DOKUMENTEERSTELLEN]: {
    ZEITPLANUNG: 'zeitplanung',
    REGELUNGSALTERNATIVEN: 'regelungsalternativen',
    GESETZESFOLGENABSCHAETZUNG: 'gesetzesfolgenabschaetzung',
  },
  [NEXT_STEP_TYPE.REGELUNGSENTWURFERARBEITEN]: {
    REGELUNGSTEXT: 'regelungstext',
    BESTANDSRECHT: 'bestandsrecht',
  },
  [NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN]: {
    HAUSABSTIMMUNG: 'hausabstimmung',
    RESSORTABSTIMMUNG: 'ressortabstimmung',
    VORHABENCLEARING: 'vorhabenclearing',
  },
  [NEXT_STEP_TYPE.KABINETTVERFAHREN]: {
    SCHLUSSABSTIMMUNG: 'schlussabstimmung',
    VORLAGEFREGIERUNGSENTWURF_ERSTELLEN: 'vorlageFRegierungsentwurf_Erstellen',
    VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN: 'vorlageFRegierungsentwurf_HausleitungSenden',
    VORLAGEFREGIERUNGSENTWURF_PKPSENDEN: 'vorlageFRegierungsentwurf_PKPSenden',
    KABINETTBESCHLUSS: 'kabinettbeschluss',
  },
  [NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG]: {
    VORLAGEEMPFANGEN: 'vorlageEmpfangen',
    DATENERFASSEN: 'datenErfassen',
    BUNDESTAGSDRUCKSACHEERSTELLEN: 'bundestagsDrucksacheErstellen',
    VORPRUEFUNGDURCHFUEHREN: 'vorpruefungDurchfuehren',
    VORABFASSUNGERSTELLEN: 'vorabfassungErstellen',
    ZUSAETZLICHEPRUEFUNG: 'zusaetzlicheVorpruefungDurchfuehren',
    VORABFASSUNGVERTEILEN: 'vorabfassungVerteilen',
    SCHNELLDRUCKGEBEN: 'schnelldruckFuerVorabfassungGeben',
  },
  [NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG]: {
    DRUCKFAHNEERSTELLEN: 'druckfahneErstellen',
    VORLAGELEKTORIEREN: 'vorlageLektorieren',
    ANDERUNGENABSTIMMEN: 'aenderungenAbstimmen',
    FASSUNGERSTELLEN: 'endgueltigeFassungErstellen',
    DRUCKAUFTRAGGEBEN: 'druckAuftragGeben',
    FASSUNGVERTEILEN: 'fassungVerteilen',
  },
  [NEXT_STEP_TYPE.REGIERUNGSENTWUERFE]: {
    REGIERUNGSENTWURFERHALTEN: 'regierungsentwurfErhalten',
    DATENGESETZESVORLAGEERFASSEN: 'datenGesetzesvorlageErfassen',
    UMLAUFDURCHFUEHREN: 'umlaufDurchfuehren',
    BRGRUNDDRUCKSACHEERSTELLEN: 'brGrunddrucksacheErstellen',
    BRDRUCKSACHEVERTEILEN: 'brDrucksacheVerteilen',
    NACHTRAEGLICHEMITBERATUNGSBITTE: 'nachtraeglicheMitberatungsbitte',
  },
  [NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN]: {
    TAGESORDNUNG_ERSTELLEN: 'tagesordnungErstellen',
    NACHTRAEGETAGESORDNUNG_ERSTELLEN: 'nachtraegeTagesordnungErstellen',
    UNTERAUSSCHUSSSITZUNG_DURCHFUEHREN: 'unterausschusssitzungDurchfuehren',
    ENTWURFNIEDERSCHRIFT_ERSTELLEN: 'entwurfNiederschriftErstellen',
    SITZUNGEN_DURCHFUEHREN: 'sitzungenDurchfuehren',
    NIEDERSCHRIFT_FINALISIEREN: 'niederschriftFinalisieren',
    UMFRAGEVERFAHREN_DURCHFUEHREN: 'umfrageverfahrenDurchfuehren',
    EMPFEHLUNGEN_AUSSCHUESSE: 'empfehlungenAusschuesse',
    EMPFEHLUNGSDRUCKSACHE_PRUEFEN: 'empfehlungsdrucksachePruefen',
    EMPFEHLUNGSDRUCKSACHE_VERTEILEN: 'empfehlungsdrucksacheVerteilen',
  },
  [NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN]: {
    TAGESORDNUNG_ANLEGEN: 'tagesordnungAnlegen',
    ERLAEUTERUNGEN_TAGESORDNUNG_ERSTELLEN: 'erlaeuterungenTagesordnungErstellen',
    TAGESORDNUNG_VEROEFFENTLICHEN: 'tagesordnungVeroeffentlichen',
    SPRECHZETTEL_UND_REDNERLISTE: 'sprechzettelUndRednerliste',
  },
  [NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN]: {
    PLENARANTRAG_STELLEN: 'plenarantragStellen',
    PLENARANTRAG_PRUEFEN: 'plenarantragPruefen',
    TAGESORDNUNG_FREIGEBEN: 'tagesordnungFreigeben',
    SPRECHZETTEL_UND_REDNERLISTE_ANPASSEN: 'sprechzettelUndRednerlisteAnpassen',
    BESCHLUESSE_ERFASSEN: 'beschluesseErfassen',
    BESCHLUSSDRUCKSACHE_ERZEUGEN: 'beschlussdrucksacheErzeugen',
    BESCHLUSSDRUCKSACHE_PRUEFEN: 'beschlussdrucksachePruefen',
    BESCHLUSSDRUCKSACHE_VERSENDEN: 'beschlussdrucksacheVersenden',
  },
  [NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN]: {
    NOTIFIZIERUNG_VORBEREITEN: 'notifizierungVorbereiten',
    NOTIFIZIERUNG_AN_BKAMT_VERSENDEN: 'notifizierungAnBKAmtVersenden',
  },
};

export const NEXT_STEP_PROGRESS_TYPE = {
  [NEXT_STEP_TYPE.KABINETTVERFAHREN]: {
    SCHLUSSABSTIMMUNG_DURCHFUEHREN: (schlussabstimmung: RvProzesshilfeOverviewDTO['letzteSchlussabstimmung']) =>
      !!schlussabstimmung,
    VORLAGEFREGIERUNGSENTWURF_ERSTELLEN: (letzteDokMappe: RvProzesshilfeOverviewDTO['letzteDokumentenmappe']) =>
      letzteDokMappe?.status === DokumentenmappeDTOStatusEnum.BereitFuerKabinettverfahren ||
      letzteDokMappe?.status === DokumentenmappeDTOStatusEnum.ZugeleitetPkp,
    VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN: (
      abstimmungHausleitung: RvProzesshilfeOverviewDTO['letzteAbstimmungDerVorlageFuerDenRegierungsentwurf'],
    ) => !!abstimmungHausleitung,
    VORLAGEFREGIERUNGSENTWURF_PKPSENDEN: (letzteZuleitung: RvProzesshilfeOverviewDTO['letzteZuleitung']) =>
      !!letzteZuleitung,
    KABINETTBESCHLUSS: (letzteZuleitung: RvProzesshilfeOverviewDTO['letzteZuleitung']) =>
      letzteZuleitung?.rueckmeldungType === RueckmeldungType.KabinettvorlageBeschlossen,
  },
};

export const getCollapseItem = (
  step: string,
  subStep: string,
  linkPath?: string,
  ready?: boolean,
  linkDisabled?: boolean,
  tipTextDanger?: boolean,
  textBodyDanger?: boolean,
  linkText?: string,
  hideProgress?: boolean,
  optional?: boolean,
  linkOnClick?: (e?: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void,
) => {
  const itemProps = {
    step,
    subStep,
    linkDisabled,
    linkPath,
    tipTextDanger,
    textBodyDanger,
    linkText,
    linkOnClick,
    hideProgress,
  };

  const itemTitleProps = {
    ready,
    step,
    subStep,
    hideProgress,
    optional,
  };

  return {
    key: subStep,
    label: <StepItemTitle {...itemTitleProps} />,
    children: <StepItemBody {...itemProps} />,
    className: optional ? 'optional-collapse' : '',
  };
};

export const translStringCollapse = (tab: string) =>
  `rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.${tab}.collapse`;
