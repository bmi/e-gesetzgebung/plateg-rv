// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface BeratungPlenumVorbereitenProps {
  hideProgess?: boolean;
}

export function BeratungPlenumVorbereitenComponent(props: BeratungPlenumVorbereitenProps): React.ReactElement {
  const { t } = useTranslation();

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].TAGESORDNUNG_ANLEGEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].TAGESORDNUNG_ANLEGEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].ERLAEUTERUNGEN_TAGESORDNUNG_ERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].ERLAEUTERUNGEN_TAGESORDNUNG_ERSTELLEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].TAGESORDNUNG_VEROEFFENTLICHEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].TAGESORDNUNG_VEROEFFENTLICHEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].SPRECHZETTEL_UND_REDNERLISTE,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN].SPRECHZETTEL_UND_REDNERLISTE}`,
      ),
      props.hideProgess,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.BERATUNG_PLENUM_VORBEREITEN}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.BERATUNG_PLENUM_VORBEREITEN}
    />
  );
}
