// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

export enum NextStepsTabsNameEnum {
  DOKUMENTE_ERSTELLEN = 'DOKUMENTE_ERSTELLEN',
  REGELUNGSENTWURF_ERARBEITEN = 'REGELUNGSENTWURF ERARBEITEN',
  REGELUNGSENTWURF_ABSTIMMEN = 'REGELUNGSENTWURF ABSTIMMEN',
  KABINETTVERFAHREN = 'KABINETTVERFAHREN',
  PARLAMENTSVERFAHRENVORABFASSUNG = 'PARLAMENTSVERFAHREN VORBEREITEN VORABFASSUNG',
  PARLAMENTSVERFAHRENENDGUELTIGEFASSUNG = 'PARLAMENTSVERFAHREN VORBEREITEN ENDGUELTIGEFASSUNG',
  EINLEITUNGSVERFAHREN = 'EINLEITUNGSVERFAHREN ZUGRIFF',
  GESETZGEBUNGSVERFAHRENBRAT = 'GESETZGEBUNGSVERFAHREN BUNDESRAT',
  ABSCHLUSSVERFAHREN = 'ABSCHLUSSVERFAHREN',
  GESETZGEBUNGSVERFAHRENBTAG = 'GESETZGEBUNGSVERFAHRENBTAG',
  STELLUNGNAHMEBR = 'STELLUNGNAHMEBR',
  REGIERUNGSENTWUERFE = 'REGIERUNGSENTWUERFE',
  AUSSCHUSSVERFAHREN_DURCHFUEHREN = 'AUSSCHUSSVERFAHREN DURCHFUEHREN',
  BERATUNG_PLENUM_VORBEREITEN = 'BERATUNG PLENUM VORBEREITEN',
  PLENARSITZUNG_DURCHFUEHREN = 'PLENARSITZUNG DURCHFUEHREN',
  NOTIFIZIERUNG_UEBERSENDEN = 'NOTIFIZIERUNG UEBERSENDEN',
}

type LinkContentType = {
  linkText: string;
  sublink: string;
};

type EvirLinkContentType = {
  [key in NextStepsTabsNameEnum]: LinkContentType[];
};

interface Props {
  tabname: NextStepsTabsNameEnum;
}

export function NextStepsEvirSectionComponent({ tabname }: Props): React.ReactElement {
  const { t } = useTranslation();

  const evirLinksContent: EvirLinkContentType = {
    [NextStepsTabsNameEnum.REGELUNGSENTWURF_ABSTIMMEN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regelungsentwurfAbstimmen.evir.list.hausabstimmung',
        ),
        sublink: '/phaseTitle1/hausabstimmung13',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regelungsentwurfAbstimmen.evir.list.entscheidung',
        ),
        sublink: '/phaseTitle1/hausabstimmung13/entscheidunghausleitung132',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regelungsentwurfAbstimmen.evir.list.ressortabstimmungen',
        ),
        sublink: '/phaseTitle1/ressortabstimmungundweiterebeteiligungen14',
      },
    ],
    [NextStepsTabsNameEnum.REGELUNGSENTWURF_ERARBEITEN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regelungsentwurfErarbeiten.evir.list.ersterEntwurf',
        ),
        sublink: '/phaseTitle1/arbeitsentwurf12/ersterentwurf121',
      },
    ],
    [NextStepsTabsNameEnum.DOKUMENTE_ERSTELLEN]: [
      {
        linkText: t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.dokumenteErstellen.evir.list.zeitplanung'),
        sublink: '/phaseTitle1/vorueberlegungen11/zeitplanung112',
      },
      {
        linkText: t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.dokumenteErstellen.evir.list.eckpunkte'),
        sublink: '/phaseTitle1/vorueberlegungen11/eckpunkteundprospektivefolgenabschaetzung114',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.dokumenteErstellen.evir.list.gesetzesfolgenabschaetzung',
        ),
        sublink: '/phaseTitle1/arbeitsentwurf12/gesetzesfolgenabschaetzung122',
      },
    ],
    [NextStepsTabsNameEnum.KABINETTVERFAHREN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.kabinettverfahren.evir.list.schlussabstimmung',
        ),
        sublink: '/phaseTitle1/kabinettbeschluss16/schlussabstimmung161',
      },
      {
        linkText: t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.kabinettverfahren.evir.list.kvHausleitung'),
        sublink: '/phaseTitle1/kabinettbeschluss16/kabinettvorlageanhausleitung162',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.kabinettverfahren.evir.list.kvBundeskanzleramt',
        ),
        sublink: '/phaseTitle1/kabinettbeschluss16/kabinettvorlageanbundeskanzleramt163',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.kabinettverfahren.evir.list.kabinettbeschluss',
        ),
        sublink: '/phaseTitle1/kabinettbeschluss16/kabinettbeschluss164',
      },
    ],
    [NextStepsTabsNameEnum.PARLAMENTSVERFAHRENVORABFASSUNG]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.parlamentsverfahrenVorabfassung.evir.list.einbringungBundestag',
        ),
        sublink: '/phaseTitle1/bundestag19',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.parlamentsverfahrenVorabfassung.evir.list.beratungBundestag',
        ),
        sublink: '/phaseTitle2/beratungbundestag21',
      },
    ],
    [NextStepsTabsNameEnum.PARLAMENTSVERFAHRENENDGUELTIGEFASSUNG]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.parlamentsverfahrenVorabfassung.evir.list.einbringungBundestag',
        ),
        sublink: '/phaseTitle1/bundestag19',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.parlamentsverfahrenVorabfassung.evir.list.beratungBundestag',
        ),
        sublink: '/phaseTitle2/beratungbundestag21',
      },
    ],
    [NextStepsTabsNameEnum.EINLEITUNGSVERFAHREN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.vorueberlegungen',
        ),
        sublink: '/phaseTitle1/vorueberlegungen11',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.arbeitsentwurf',
        ),
        sublink: '/phaseTitle1/arbeitsentwurf12',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.hausabstimmung',
        ),
        sublink: '/phaseTitle1/hausabstimmung13',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.ressortabstimmung',
        ),
        sublink: '/phaseTitle1/ressortabstimmungundweiterebeteiligungen14',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.laender',
        ),
        sublink: '/phaseTitle1/laender15',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.kabinettbeschluss',
        ),
        sublink: '/phaseTitle1/kabinettbeschluss16',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.stellungnahme',
        ),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.gegenaeusserung',
        ),
        sublink: '/phaseTitle1/gegenaeusserungbundesregierung18',
      },
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahrenKeinZugriff.evir.list.einbringung',
        ),
        sublink: '/phaseTitle1/bundestag19',
      },
    ],
    [NextStepsTabsNameEnum.GESETZGEBUNGSVERFAHRENBRAT]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.gesetzgebungsverfahrenBRATZugriff.evir.list.beratung',
        ),
        sublink: '/phaseTitle2/beratungbundesrat22',
      },
    ],
    [NextStepsTabsNameEnum.ABSCHLUSSVERFAHREN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.abschlussverfahrenZugriff.evir.list.ausfertigung',
        ),
        sublink: '/phaseTitle3/ausfertigungundverkuendung31',
      },
    ],
    [NextStepsTabsNameEnum.GESETZGEBUNGSVERFAHRENBTAG]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.gesetzgebungsverfahrenBTAGZugriff.evir.list.beratung',
        ),
        sublink: '/phaseTitle2/beratungbundestag21',
      },
    ],
    [NextStepsTabsNameEnum.STELLUNGNAHMEBR]: [
      {
        linkText: t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regierungsentwuerfe.evir.list.stellungnahme'),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
    ],
    [NextStepsTabsNameEnum.REGIERUNGSENTWUERFE]: [
      {
        linkText: t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regierungsentwuerfe.evir.list.stellungnahme'),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
    ],
    [NextStepsTabsNameEnum.AUSSCHUSSVERFAHREN_DURCHFUEHREN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.ausschussverfahrenDurchfuehren.evir.list.stellungnahme',
        ),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
    ],
    [NextStepsTabsNameEnum.BERATUNG_PLENUM_VORBEREITEN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.beratungPlenumVorbereiten.evir.list.stellungnahme',
        ),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
    ],
    [NextStepsTabsNameEnum.PLENARSITZUNG_DURCHFUEHREN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.beratungPlenumVorbereiten.evir.list.stellungnahme',
        ),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
    ],
    [NextStepsTabsNameEnum.NOTIFIZIERUNG_UEBERSENDEN]: [
      {
        linkText: t(
          'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.beratungPlenumVorbereiten.evir.list.stellungnahme',
        ),
        sublink: '/phaseTitle1/stellungnahmebundesrat17',
      },
    ],
  };

  const mainLink = '/evir/gesetz/regierungsinitiative';

  const displayLinks = evirLinksContent[tabname].map((item, index) => {
    return (
      <li key={`${item.linkText}-${index}`}>
        <Link to={`${mainLink}${item.sublink}`}>{item.linkText}</Link>
      </li>
    );
  });

  return (
    <>
      <Title className="evir-title" level={4}>
        {t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.dokumenteErstellen.evir.title')}
      </Title>
      <ul className="evir-list">{displayLinks}</ul>
    </>
  );
}
