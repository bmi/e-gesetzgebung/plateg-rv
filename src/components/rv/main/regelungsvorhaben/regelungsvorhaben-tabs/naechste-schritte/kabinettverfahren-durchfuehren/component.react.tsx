// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenEntityFullResponseDTO, RvProzesshilfeOverviewDTO } from '@plateg/rest-api';

import {
  getCollapseItem,
  NEXT_STEP_PROGRESS_TYPE,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';
import { routes } from '../../../../../../../shares/routes';

interface Props {
  letzteDokumentmappe?: RvProzesshilfeOverviewDTO['letzteDokumentenmappe'];
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  letzteZuleitung?: RvProzesshilfeOverviewDTO['letzteZuleitung'];
  letzteSchlussabstimmung?: RvProzesshilfeOverviewDTO['letzteSchlussabstimmung'];
  letzteAbstimmungHausleitung?: RvProzesshilfeOverviewDTO['letzteAbstimmungDerVorlageFuerDenRegierungsentwurf'];
  hideProgress?: boolean;
}

export function KabinettverfahrenDurchfuehren({
  letzteDokumentmappe,
  regelungsvorhaben,
  letzteZuleitung,
  letzteSchlussabstimmung,
  letzteAbstimmungHausleitung,
  hideProgress,
}: Props): React.ReactElement {
  const { t } = useTranslation();

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.KABINETTVERFAHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].SCHLUSSABSTIMMUNG,
      letzteSchlussabstimmung
        ? `/hra/meineAbstimmung/${letzteSchlussabstimmung.abstimmungId}/abstimmungsrunde`
        : `/hra/meineAbstimmung/abstimmungAnlegen/schlussabstimmung/${regelungsvorhaben.base.id}`,
      NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].SCHLUSSABSTIMMUNG_DURCHFUEHREN(letzteSchlussabstimmung),
      false,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.KABINETTVERFAHREN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].SCHLUSSABSTIMMUNG
        }.${letzteSchlussabstimmung ? 'aktuellenSchlussabstimmung' : 'createLink'}`,
      ),
      hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.KABINETTVERFAHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_ERSTELLEN,
      '/editor',
      NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_ERSTELLEN(
        letzteDokumentmappe,
      ),
      false,
      true,
      undefined,
      undefined,
      hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.KABINETTVERFAHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN,
      letzteAbstimmungHausleitung
        ? `/hra/meineAbstimmung/${letzteAbstimmungHausleitung.abstimmungId}/abstimmungsrunde`
        : `/hra/meineAbstimmung/abstimmungAnlegen/abstimmungHausleitung/${regelungsvorhaben.base.id}`,
      NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN(
        letzteAbstimmungHausleitung,
      ),
      false,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.KABINETTVERFAHREN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN
        }.${letzteAbstimmungHausleitung ? 'aktuelleAbstimmung' : 'createLink'}`,
      ),
      hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.KABINETTVERFAHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_PKPSENDEN,
      `/${routes.REGELUNGSVORHABEN}/${routes.MEINE_REGELUNGSVORHABEN}/${regelungsvorhaben.base.id}/${routes.ZULEITUNGEN}`,
      NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_PKPSENDEN(letzteZuleitung),
      false,
      false,
      true,
      undefined,
      hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.KABINETTVERFAHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].KABINETTBESCHLUSS,
      `/${routes.REGELUNGSVORHABEN}/${routes.MEINE_REGELUNGSVORHABEN}/${regelungsvorhaben.base.id}/${routes.ZULEITUNGEN}`,
      NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].KABINETTBESCHLUSS(letzteZuleitung),
      false,
      undefined,
      true,
      undefined,
      hideProgress,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.KABINETTVERFAHREN}
      evirSectionTabname={NextStepsTabsNameEnum.KABINETTVERFAHREN}
      collapseItems={collapseItems}
    />
  );
}
