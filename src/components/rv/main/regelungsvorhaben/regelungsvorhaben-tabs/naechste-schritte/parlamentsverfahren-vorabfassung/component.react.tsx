// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { RegelungsvorhabenEntityFullResponseDTO, TagesordnungsrelevanzDTO } from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';
import { RvErrorController } from '../../../../../../../utils/rv-error-controller';
import { NewRegelungsvorhabenController } from '../../../../../main/regelungsvorhaben/controller';
import { ItemType } from 'rc-collapse/es/interface';

interface ParlamentsverfahrenVorbereitenProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  hideProgress?: boolean;
}

export function ParlamentsverfahrenVorabfassungComponent(
  props: ParlamentsverfahrenVorbereitenProps,
): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('rvNewRegelungsvorhabenController', () => new NewRegelungsvorhabenController());
  const loadingStatusCtrl = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  const [showOptionalSteps, setShowOptionalSteps] = useState<boolean | undefined>(false);
  let rvSub: Subscription;

  useEffect(() => {
    fetchOptionaleSchritte();
    return () => {
      rvSub?.unsubscribe();
    };
  }, []);

  const fetchOptionaleSchritte = () => {
    loadingStatusCtrl.setLoadingStatus(true);
    const id = props.regelungsvorhaben.base.id;
    const rvSub: Subscription = ctrl.getTagesordnungsRelevanzCall({ id }).subscribe({
      next: (data: TagesordnungsrelevanzDTO) => {
        setShowOptionalSteps(data.tagesordnungsrelevant);
      },
      complete: () => {
        loadingStatusCtrl.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        rvErrorCtrl.handleError(error);
      },
    });
    return () => {
      rvSub?.unsubscribe();
    };
  };

  const collapseItems: ItemType[] = [
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].VORLAGEEMPFANGEN,
      undefined,
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].DATENERFASSEN,
      `/drucksachen/annahmestelle/${props.regelungsvorhaben.dto.drucksachenInfo?.drucksachenId}/bundestagsdrucksache`,
      !!props.regelungsvorhaben.dto.drucksachenInfo?.drucksachenNummer,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].DATENERFASSEN
        }.datenblattAnzeigen`,
      ),
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].BUNDESTAGSDRUCKSACHEERSTELLEN,
      undefined,
      undefined,
      true,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].VORPRUEFUNGDURCHFUEHREN,
      undefined,
      undefined,
      true,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].VORABFASSUNGERSTELLEN,
      undefined,
      undefined,
      true,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    ...(showOptionalSteps
      ? [
          getCollapseItem(
            NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
            NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].ZUSAETZLICHEPRUEFUNG,
            undefined,
            undefined,
            true,
            undefined,
            undefined,
            undefined,
            props.hideProgress,
            true,
          ),
        ]
      : []),
    getCollapseItem(
      NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].VORABFASSUNGVERTEILEN,
      undefined,
      undefined,
      true,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    ...(showOptionalSteps
      ? [
          getCollapseItem(
            NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG,
            NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG].SCHNELLDRUCKGEBEN,
            undefined,
            undefined,
            true,
            undefined,
            undefined,
            undefined,
            props.hideProgress,
            true,
          ),
        ]
      : []),
  ];

  return (
    <NextStepsContentMain
      showOptionalSteps={showOptionalSteps}
      stepType={NEXT_STEP_TYPE.PARLAMENTSVERFAHREN_VORABFASSUNG}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.PARLAMENTSVERFAHRENVORABFASSUNG}
    />
  );
}
