// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface PlenarsitzungDurchfuehrenProps {
  hideProgess?: boolean;
}

export function PlenarsitzungDurchfuehrenComponent(props: PlenarsitzungDurchfuehrenProps): React.ReactElement {
  const { t } = useTranslation();

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].PLENARANTRAG_STELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].PLENARANTRAG_STELLEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].PLENARANTRAG_PRUEFEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].PLENARANTRAG_PRUEFEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].TAGESORDNUNG_FREIGEBEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].TAGESORDNUNG_FREIGEBEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].SPRECHZETTEL_UND_REDNERLISTE_ANPASSEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].SPRECHZETTEL_UND_REDNERLISTE_ANPASSEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUESSE_ERFASSEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUESSE_ERFASSEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUSSDRUCKSACHE_ERZEUGEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUSSDRUCKSACHE_ERZEUGEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUSSDRUCKSACHE_PRUEFEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUSSDRUCKSACHE_PRUEFEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUSSDRUCKSACHE_VERSENDEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN].BESCHLUSSDRUCKSACHE_VERSENDEN}`,
      ),
      props.hideProgess,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.PLENARSITZUNG_DURCHFUEHREN}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.PLENARSITZUNG_DURCHFUEHREN}
    />
  );
}
