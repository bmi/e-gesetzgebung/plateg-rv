// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { PraxisTipp, translStringCollapse } from '../../..';

interface Props {
  textBodyDanger?: boolean;
  linkText?: string;
  linkOnClick?: (e?: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  linkPath?: string;
  linkDisabled?: boolean;
  tipTextDanger?: boolean;
  step: string;
  subStep: string;
  hideProgress?: boolean;
}

export function StepItemBody({
  textBodyDanger,
  linkOnClick,
  linkText,
  tipTextDanger,
  linkDisabled,
  linkPath,
  step,
  subStep,
  hideProgress,
}: Props): React.ReactElement {
  const linkStylesDisabled = {
    textDecoration: 'none',
    color: 'gray',
    cursor: 'not-allowed',
  };

  const { t } = useTranslation();

  return (
    <>
      {textBodyDanger ? (
        <p
          dangerouslySetInnerHTML={{
            __html: t(`${translStringCollapse(step)}.${subStep}.text`),
          }}
        />
      ) : (
        <p>{t(`${translStringCollapse(step)}.${subStep}.text`)}</p>
      )}
      {tipTextDanger && <PraxisTipp step={step} subStep={subStep} />}
      {!hideProgress && linkPath && (
        <Link
          style={linkDisabled ? linkStylesDisabled : {}}
          className="collapse-create-link"
          to={!linkDisabled ? linkPath || '#' : '#'}
          onClick={
            !linkDisabled
              ? linkOnClick
              : (e) => {
                  e.preventDefault();
                }
          }
        >
          {linkText ? linkText : t(`${translStringCollapse(step)}.${subStep}.createLink`)}
        </Link>
      )}
    </>
  );
}
