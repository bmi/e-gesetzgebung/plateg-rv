// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Col, Collapse, Row, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';

import { AriaController, IconLocked } from '@plateg/theme';
import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';

import { NEXT_STEP_TYPE, NextStepsEvirSectionComponent, NextStepsTabsNameEnum } from '../';
import { LockReason } from './step-item/lock-reason/component.react';
import { ItemType } from 'rc-collapse/es/interface';
const { Title } = Typography;

interface Props {
  stepType: (typeof NEXT_STEP_TYPE)[keyof typeof NEXT_STEP_TYPE];
  collapseItems: ItemType[];
  evirSectionTabname: NextStepsTabsNameEnum;
  showOptionalSteps?: boolean;
  lockReason?: string;
}

export function NextStepsContentMain({
  collapseItems,
  evirSectionTabname,
  stepType,
  showOptionalSteps,
  lockReason,
}: Props): React.ReactElement {
  const location = useLocation();
  const { t } = useTranslation();

  const [collapseKey, setCollapseKey] = useState<string | string[]>('');

  useEffect(() => {
    setCollapseKey('');
  }, [location.pathname]);

  useEffect(() => {
    AriaController.setAttrByClassName('ant-collapse', 'role', 'list');
    AriaController.setAttrByClassName('ant-collapse-item', 'role', 'listitem');
  }, []);

  return (
    <>
      <Row>
        <Col className="naechste-schritte-collapse" span={16}>
          <div className="naechste-schritte-collapse-title">
            {lockReason && <IconLocked />}
            <Title level={2}>{t(`rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.${stepType}.title`)}</Title>
          </div>

          {lockReason && <LockReason lockReason={lockReason} />}
          <Collapse
            onChange={(key: string | string[]) => setCollapseKey(key)}
            activeKey={collapseKey}
            ghost
            items={collapseItems}
            expandIcon={({ isActive }) => (
              <DirectionRightOutlinedNew
                style={{ width: 29, height: 29 }}
                className={isActive ? 'svg-transition svg-rotate-down' : 'svg-transition'}
              />
            )}
          />
        </Col>
        <Col className="naechste-schritte-evir" span={8}>
          <NextStepsEvirSectionComponent tabname={evirSectionTabname} />
        </Col>
      </Row>
    </>
  );
}
