// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { LampOutlined } from '@plateg/theme/src/components/icons/LampOutlined';

import { translStringCollapse } from '../../..';

interface Props {
  step: string;
  subStep: string;
}

export function PraxisTipp({ step, subStep }: Props): React.ReactElement {
  const { t } = useTranslation();
  const [showTip, setShowTip] = useState(false);

  return (
    <>
      <Title className="praxistipp" level={4}>
        <LampOutlined />
        {t(`${translStringCollapse('dokumenteErstellen')}.zeitplanung.praxistipp`)}
      </Title>
      {showTip && (
        <p
          className="praxis-tip-p-danger"
          dangerouslySetInnerHTML={{ __html: t(`${translStringCollapse(step)}.${subStep}.textTip`) }}
        />
      )}
      <Button className="link-btn" type="text" onClick={() => setShowTip((state) => !state)}>
        {t(
          `${translStringCollapse('dokumenteErstellen')}.zeitplanung.${!showTip ? 'textEinblenden' : 'textAusblenden'}`,
        )}
      </Button>
    </>
  );
}
