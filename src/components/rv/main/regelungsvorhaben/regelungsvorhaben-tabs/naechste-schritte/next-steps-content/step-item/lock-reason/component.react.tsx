// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Col, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
  lockReason: string;
}

export function LockReason({ lockReason }: Props): React.ReactElement {
  const { t } = useTranslation();

  return (
    <Row className="lock-reason-box">
      <Col>
        <Title level={4}>{t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.lockReason.title')}</Title>
        <p>{lockReason}</p>
      </Col>
    </Row>
  );
}
