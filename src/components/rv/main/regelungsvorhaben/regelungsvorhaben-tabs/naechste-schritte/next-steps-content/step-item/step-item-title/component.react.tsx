// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { CheckOutlinedGreen } from '@plateg/theme';
import { DocumentClockOutlined } from '@plateg/theme/src/components/icons/DocumentClockOutlined';

interface Props {
  step: string;
  subStep: string;
  ready?: boolean;
  hideProgress?: boolean;
  optional?: boolean;
}

export function StepItemTitle({ step, subStep, ready, hideProgress, optional }: Props): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div className="tab-content-title">
      {!hideProgress && ready && (
        <span className="tab-content-svg-span">
          <CheckOutlinedGreen />
        </span>
      )}
      {optional && (
        <span className="tab-content-svg-span">
          <DocumentClockOutlined />
        </span>
      )}
      <span>{t(`rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.${step}.collapse.${subStep}.title`)}</span>
    </div>
  );
}
