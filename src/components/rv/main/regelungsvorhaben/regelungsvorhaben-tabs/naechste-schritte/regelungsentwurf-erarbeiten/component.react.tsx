// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  DokumentenmappeDTO,
  RegelungsvorhabenBestandsrechtTableDTO,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityFullResponseDTO,
} from '@plateg/rest-api';
import { displayMessage, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { getCollapseItem, NEXT_STEP_SUB_TYPE, NEXT_STEP_TYPE, NextStepsContentMain, NextStepsTabsNameEnum } from '../';
import { RvErrorController } from '../../../../../../../utils/rv-error-controller';
import { BestandsRechtModal } from '../../bestandsrecht/bestandsrecht-modal/component.react';

interface RegelungsentwurfErarbeitenComponentProps {
  letzteDokumentmappe?: DokumentenmappeDTO;
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  bestandsrechtData: RegelungsvorhabenBestandsrechtTableDTO[];
  setBestandsrechtData: (vals: RegelungsvorhabenBestandsrechtTableDTO[]) => void;
  hideProgress?: boolean;
}

export function RegelungsentwurfErarbeitenComponent(
  props: RegelungsentwurfErarbeitenComponentProps,
): React.ReactElement {
  const { t } = useTranslation();

  const [isBestandsrechtModalVisible, setIsBestandsrechtModalVisible] = useState<boolean>(false);
  const mostRecentEditorDoc = props.letzteDokumentmappe?.dokumente?.reduce((a, b) => {
    return new Date(a.bearbeitetAm) > new Date(b.bearbeitetAm) ? a : b;
  });
  const rvCtrl = GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi());
  const loadingStatusCtrl = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());

  let rvSub: Subscription;
  const getBestandsrechte = (showModal: boolean) => {
    if (rvSub) {
      rvSub.unsubscribe();
    }
    loadingStatusCtrl.setLoadingStatus(true);

    rvSub = rvCtrl.getRegelungsvorhabenBestandsrechtTable({ id: props.regelungsvorhaben.base.id }).subscribe({
      next: (data) => {
        props.setBestandsrechtData(data);
        setIsBestandsrechtModalVisible(showModal);
      },
      complete: () => {
        loadingStatusCtrl.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        rvErrorCtrl.handleError(error);
      },
    });
  };
  const saveBestandsrechte = (elis: string[]) => {
    loadingStatusCtrl.setLoadingStatus(true);
    setIsBestandsrechtModalVisible(false);
    rvCtrl
      .createRegelungsvorhabenBestandsrecht({
        id: props.regelungsvorhaben.base.id,
        regelungsvorhabenBestandsrechtCreateDTO: elis
          .filter((item) => !props.bestandsrechtData.find((element) => element.eli === item))
          .map((item) => ({
            eli: item,
          })),
      })
      .subscribe({
        next: () => {
          getBestandsrechte(false);
          displayMessage(
            t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.successMessage', {
              rvName: props.regelungsvorhaben.dto.abkuerzung,
            }),
            'success',
          );
        },
        complete: () => {
          loadingStatusCtrl.setLoadingStatus(false);
        },
        error: (error: AjaxError) => {
          loadingStatusCtrl.setLoadingStatus(false);
          rvErrorCtrl.handleError(error);
        },
      });
  };

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.REGELUNGSENTWURFERARBEITEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFERARBEITEN].REGELUNGSTEXT,
      !props.letzteDokumentmappe ? `/editor` : `/editor/document/${mostRecentEditorDoc?.id as string}`,
      !!props.letzteDokumentmappe,
      undefined,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGELUNGSENTWURFERARBEITEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFERARBEITEN].BESTANDSRECHT,
      '#',
      !!props.bestandsrechtData.length,
      false,
      undefined,
      undefined,
      undefined,
      props.hideProgress,
      undefined,
      (e) => {
        e?.preventDefault();
        getBestandsrechte(true);
      },
    ),
  ];

  return (
    <>
      <BestandsRechtModal
        isVisible={isBestandsrechtModalVisible}
        setIsVisible={setIsBestandsrechtModalVisible}
        rvName={props.regelungsvorhaben.dto.abkuerzung}
        saveBestandsrechte={saveBestandsrechte}
        rvBestandsrechte={props.bestandsrechtData}
      />
      <NextStepsContentMain
        stepType={NEXT_STEP_TYPE.REGELUNGSENTWURFERARBEITEN}
        collapseItems={collapseItems}
        evirSectionTabname={NextStepsTabsNameEnum.REGELUNGSENTWURF_ERARBEITEN}
      />
    </>
  );
}
