// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './naechste-schritte.less';

import { Layout, Menu } from 'antd';
import { MenuInfo } from 'rc-menu/lib/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { RegelungsvorhabenBestandsrechtTableDTO, RvProzesshilfeOverviewDTO } from '@plateg/rest-api';
import { AriaController } from '@plateg/theme';
import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../../../../shares/routes';
import { MainRoutesNaechsteSchritte, MenuTitle, ProgressLevel } from './';
import { NEXT_STEP_PROGRESS_TYPE, NEXT_STEP_TYPE } from './utils';

interface NaechsteSchritteProps extends RvProzesshilfeOverviewDTO {
  bestandsrechtData: RegelungsvorhabenBestandsrechtTableDTO[];
  setBestandsrechtData: (vals: RegelungsvorhabenBestandsrechtTableDTO[]) => void;
}
export interface MenuItem {
  label: React.ReactNode;
  key: React.Key;
  icon?: React.ReactNode;
  children?: MenuItem[];
  type?: 'group';
  disabled?: boolean;
}
export function NaechsteSchritte(props: NaechsteSchritteProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcher = useRouteMatch<{ id: string; tabName: string; action: string; subTab: string }>(
    '/regelungsvorhaben/:tabName/:id/:action/:subTab',
  );

  const appStore = useAppSelector((state) => state.user);
  const [currentUserIsBTUser, setCurrentUserIsBTUser] = useState<boolean>(false);
  const [currentUserIsBRUser, setCurrentUserIsBRUser] = useState<boolean>(false);
  const [openKeys, setOpenKeys] = useState(['menuItemsBundesregierung']);
  const [defaultSelectedKeys, setDefaultSelectedKeys] = useState<string>(routes.DOKUMENTEERSTELLEN);
  const tabName: string = routeMatcher?.params.subTab ? routeMatcher?.params.subTab : defaultSelectedKeys;
  const mainTabName = routeMatcher?.params.tabName ? routeMatcher?.params.tabName : routes.MEINE_REGELUNGSVORHABEN;
  const rootSubmenuKeys = ['menuItemsBundesregierung', 'menuItemsBundestag', 'menuItemsBundesrat'];
  let dokumenteErstellenProgress: ProgressLevel | undefined;
  const isLetzteZeitplanungActive = props.letzteZeitplanung && props.letzteZeitplanung?.archiviertAm === null;
  const isLetzteEgfaActive = props.letzteEgfa && props.letzteEgfa?.archiviertAm === null;
  if (isLetzteZeitplanungActive && isLetzteEgfaActive) {
    dokumenteErstellenProgress = ProgressLevel.complete;
  } else {
    if (isLetzteZeitplanungActive || isLetzteEgfaActive) {
      dokumenteErstellenProgress = ProgressLevel.partial;
    }
  }
  let abstimmungProgress: ProgressLevel | undefined;
  if (props.letzteHausabstimmung && props.letzteRessortabstimmung) {
    abstimmungProgress = ProgressLevel.complete;
  } else {
    if (props.letzteHausabstimmung || props.letzteRessortabstimmung) {
      abstimmungProgress = ProgressLevel.partial;
    }
  }

  let dokumentMappeProgress: ProgressLevel | undefined;
  if (props.letzteDokumentenmappe && props.bestandsrechtData.length) {
    dokumentMappeProgress = ProgressLevel.complete;
  } else {
    if (props.letzteDokumentenmappe || props.bestandsrechtData.length) {
      dokumentMappeProgress = ProgressLevel.partial;
    }
  }

  let kabinettVerfahrenProgress: ProgressLevel | undefined;

  if (
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].SCHLUSSABSTIMMUNG_DURCHFUEHREN(
      props.letzteSchlussabstimmung,
    ) &&
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_ERSTELLEN(
      props.letzteDokumentenmappe,
    ) &&
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN(
      props.letzteAbstimmungDerVorlageFuerDenRegierungsentwurf,
    ) &&
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_PKPSENDEN(
      props.letzteZuleitung,
    ) &&
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].KABINETTBESCHLUSS(props.letzteZuleitung)
  ) {
    kabinettVerfahrenProgress = ProgressLevel.complete;
  } else if (
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].SCHLUSSABSTIMMUNG_DURCHFUEHREN(
      props.letzteSchlussabstimmung,
    ) ||
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_ERSTELLEN(
      props.letzteDokumentenmappe,
    ) ||
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_HAUSLEITUNGSENDEN(
      props.letzteAbstimmungDerVorlageFuerDenRegierungsentwurf,
    ) ||
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].VORLAGEFREGIERUNGSENTWURF_PKPSENDEN(
      props.letzteZuleitung,
    ) ||
    NEXT_STEP_PROGRESS_TYPE[NEXT_STEP_TYPE.KABINETTVERFAHREN].KABINETTBESCHLUSS(props.letzteZuleitung)
  ) {
    kabinettVerfahrenProgress = ProgressLevel.partial;
  }

  const onOpenChange = (keys: string[]) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (latestOpenKey && rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const onClick = (item: MenuInfo) => {
    history.push(
      `/regelungsvorhaben/${mainTabName}/${props.regelungsvorhaben.base.id}/${routes.NAECHSTESCHRITTE}/${item.key}`,
    );
  };

  useEffect(() => {
    switch (appStore.user?.dto.ressort?.kurzbezeichnung) {
      case 'BT':
        setCurrentUserIsBTUser(true);
        setCurrentUserIsBRUser(false);
        setOpenKeys([routes.GESETZGEBUNGSVERFAHRENBT]);
        setDefaultSelectedKeys(routes.PARLAMENTSVERFAHREN_VORABFASSUNG);
        break;
      case 'BR':
        setCurrentUserIsBTUser(false);
        setCurrentUserIsBRUser(true);
        setOpenKeys([routes.STELLUNGNAHMEBR]);
        setDefaultSelectedKeys(routes.REGIERUNGSENTWUERFE);
        break;
      default:
        setCurrentUserIsBTUser(false);
        setCurrentUserIsBRUser(false);
        setOpenKeys([routes.EINLEITUNGSVERFAHREN]);
        setDefaultSelectedKeys(routes.DOKUMENTEERSTELLEN);
    }
  }, [appStore.user]);

  useEffect(() => {
    document.querySelectorAll('.ant-menu-item').forEach((item) => {
      return item.removeAttribute('aria-current');
    });
    AriaController.setAriaAttrByClassName('ant-menu-item-selected', 'current', 'page');
  }, [tabName]);

  const menuItems: MenuItem[] = [
    {
      label: (
        <MenuTitle
          title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.einleitungsverfahren')}
          hideIcon
          progress={currentUserIsBTUser || currentUserIsBRUser ? ProgressLevel.locked : undefined}
        />
      ),
      key: routes.EINLEITUNGSVERFAHREN,
      icon: undefined,
      children:
        !currentUserIsBTUser && !currentUserIsBRUser
          ? [
              {
                key: routes.DOKUMENTEERSTELLEN,
                label: (
                  <MenuTitle
                    title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.dokumenteErstellen.title')}
                    progress={currentUserIsBTUser ? undefined : dokumenteErstellenProgress}
                  />
                ),
              },
              {
                key: routes.REGELUNGSENTWURFERARBEITEN,
                label: (
                  <MenuTitle
                    progress={currentUserIsBTUser ? undefined : dokumentMappeProgress}
                    title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regelungsentwurfErarbeiten.title')}
                  />
                ),
              },
              {
                key: routes.REGELUNGSENTWURFABSTIMMEN,
                label: (
                  <MenuTitle
                    progress={currentUserIsBTUser ? undefined : abstimmungProgress}
                    title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regelungsentwurfAbstimmen.title')}
                  />
                ),
              },
              {
                key: routes.KABINETTVERFAHREN_DURCHFUEHREN,
                label: (
                  <MenuTitle
                    progress={currentUserIsBTUser ? undefined : kabinettVerfahrenProgress}
                    title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.kabinettverfahren.title')}
                  />
                ),
              },
              {
                disabled: true,
                key: 'brStellungnahme',
                label: (
                  <MenuTitle
                    title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.brStellungnahme.title')}
                    disabled
                  />
                ),
              },
            ]
          : undefined,
    },
    {
      label: (
        <MenuTitle
          title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.stellungnahmeBR')}
          hideIcon
          progress={!currentUserIsBRUser ? ProgressLevel.locked : undefined}
        />
      ),
      key: routes.STELLUNGNAHMEBR,
      icon: undefined,
      children: currentUserIsBRUser
        ? [
            {
              label: (
                <MenuTitle title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.regierungsentwuerfe.title')} />
              ),
              key: routes.REGIERUNGSENTWUERFE,
            },
            {
              label: (
                <MenuTitle
                  title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.ausschussverfahrenDurchfuehren.title')}
                />
              ),
              key: routes.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
            },
            {
              label: (
                <MenuTitle
                  title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.beratungPlenumVorbereiten.title')}
                />
              ),
              key: routes.BERATUNG_PLENUM_VORBEREITEN,
            },
            {
              label: (
                <MenuTitle
                  title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.plenarsitzungDurchfuehren.title')}
                />
              ),
              key: routes.PLENARSITZUNG_DURCHFUEHREN,
            },
            {
              label: (
                <MenuTitle
                  title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.notifizierungUebersenden.title')}
                />
              ),
              key: routes.NOTIFIZIERUNG_UEBERSENDEN,
            },
          ]
        : undefined,
    },
    {
      label: (
        <MenuTitle
          title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.gesetzgebungsverfahrenBT')}
          hideIcon
          progress={!currentUserIsBTUser ? ProgressLevel.locked : undefined}
        />
      ),
      key: routes.GESETZGEBUNGSVERFAHRENBT,
      icon: undefined,
      children: currentUserIsBTUser
        ? [
            {
              label: (
                <MenuTitle
                  title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.parlamentsverfahrenVorabfassung.title')}
                />
              ),
              key: routes.PARLAMENTSVERFAHREN_VORABFASSUNG,
            },
            {
              label: (
                <MenuTitle
                  title={t(
                    'rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.parlamentsverfahrenEndgueltigeFassung.title',
                  )}
                />
              ),
              key: routes.PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG,
            },
          ]
        : undefined,
    },
    {
      label: (
        <MenuTitle
          title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.gesetzgebungsverfahrenBRAT')}
          progress={ProgressLevel.locked}
          hideIcon
        />
      ),
      key: routes.GESETZGEBUNGSVERFAHRENBRAT,
      icon: undefined,
      children: undefined,
    },
    {
      label: (
        <MenuTitle
          title={t('rv.regelungsvorhabenTabs.tabs.naechsteSchritte.tabs.abschlussverfahren')}
          progress={currentUserIsBTUser || currentUserIsBRUser ? ProgressLevel.locked : undefined}
          disabled={!currentUserIsBTUser && !currentUserIsBRUser}
          hideIcon={currentUserIsBTUser || currentUserIsBRUser}
        />
      ),
      key: routes.ABSCHLUSSVERFAHREN,
      icon: undefined,
      disabled: !currentUserIsBTUser && !currentUserIsBRUser,
      children: undefined,
    },
  ];

  return (
    <Layout className="naechste-schritte">
      <Layout.Sider className="naechste-schritte-menu" theme="light" width="25%">
        <Menu
          expandIcon={
            <DirectionRightOutlinedNew style={{ width: 22, height: 22 }} className="ant-menu-submenu-arrow" />
          }
          mode="inline"
          onClick={onClick}
          openKeys={openKeys}
          onOpenChange={onOpenChange}
          defaultSelectedKeys={[tabName]}
          selectedKeys={[tabName]}
          items={menuItems}
        />
      </Layout.Sider>
      <Layout.Content>
        <MainRoutesNaechsteSchritte {...props} />
      </Layout.Content>
    </Layout>
  );
}
