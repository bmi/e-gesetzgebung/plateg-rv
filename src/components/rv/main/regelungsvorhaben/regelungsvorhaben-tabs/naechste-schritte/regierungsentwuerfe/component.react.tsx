// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface RegierungsentwuerfeProps {
  hideProgess?: boolean;
}

export function RegierungsentwuerfeComponent(props: RegierungsentwuerfeProps): React.ReactElement {
  const { t } = useTranslation();

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.REGIERUNGSENTWUERFE,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].REGIERUNGSENTWURFERHALTEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGIERUNGSENTWUERFE)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].REGIERUNGSENTWURFERHALTEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGIERUNGSENTWUERFE,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].DATENGESETZESVORLAGEERFASSEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGIERUNGSENTWUERFE)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].DATENGESETZESVORLAGEERFASSEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGIERUNGSENTWUERFE,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].UMLAUFDURCHFUEHREN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGIERUNGSENTWUERFE)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].UMLAUFDURCHFUEHREN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGIERUNGSENTWUERFE,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].BRGRUNDDRUCKSACHEERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGIERUNGSENTWUERFE)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].BRGRUNDDRUCKSACHEERSTELLEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGIERUNGSENTWUERFE,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].BRDRUCKSACHEVERTEILEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGIERUNGSENTWUERFE)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].BRDRUCKSACHEVERTEILEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGIERUNGSENTWUERFE,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].NACHTRAEGLICHEMITBERATUNGSBITTE,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGIERUNGSENTWUERFE)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGIERUNGSENTWUERFE].NACHTRAEGLICHEMITBERATUNGSBITTE}`,
      ),
      props.hideProgess,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.REGIERUNGSENTWUERFE}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.REGIERUNGSENTWUERFE}
    />
  );
}
