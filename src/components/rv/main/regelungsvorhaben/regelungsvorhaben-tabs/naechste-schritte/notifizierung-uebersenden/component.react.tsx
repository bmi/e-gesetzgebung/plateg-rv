// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface NotifizierungUebersendenProps {
  hideProgess?: boolean;
}

export function NotifizierungUebersendenComponent(props: NotifizierungUebersendenProps): React.ReactElement {
  const { t } = useTranslation();

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN].NOTIFIZIERUNG_VORBEREITEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN].NOTIFIZIERUNG_VORBEREITEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN].NOTIFIZIERUNG_AN_BKAMT_VERSENDEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN].NOTIFIZIERUNG_AN_BKAMT_VERSENDEN}`,
      ),
      props.hideProgess,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.NOTIFIZIERUNG_UEBERSENDEN}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.NOTIFIZIERUNG_UEBERSENDEN}
    />
  );
}
