// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenEntityFullResponseDTO, RvProzesshilfeAbstimmungDTO } from '@plateg/rest-api';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface RegelungsentwurfAbstimmenProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  hausabstimmung?: RvProzesshilfeAbstimmungDTO;
  ressortabstimmung?: RvProzesshilfeAbstimmungDTO;
  vorhabenclearing?: RvProzesshilfeAbstimmungDTO;
  hideProgress?: boolean;
}

export function RegelungsentwurfAbstimmenComponent(props: RegelungsentwurfAbstimmenProps): React.ReactElement {
  const { t } = useTranslation();
  const isArchivedHra = props.hausabstimmung?.archiviertAm ? 'hra/archiv' : 'hra/meineAbstimmung';

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN].HAUSABSTIMMUNG,
      props.hausabstimmung
        ? `/${isArchivedHra}/${props.hausabstimmung.abstimmungId}/abstimmungsrunde`
        : `/hra/meineAbstimmung/abstimmungAnlegen/hausabstimmung/${props.regelungsvorhaben.base.id}`,
      !!props.hausabstimmung,
      false,
      undefined,
      true,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN].HAUSABSTIMMUNG
        }.${props.hausabstimmung ? 'aktuellenHausabstimmung' : 'createLink'}`,
      ),
      props.hideProgress,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN].VORHABENCLEARING,
      props.vorhabenclearing
        ? `/hra/meineAbstimmung/${props.vorhabenclearing.abstimmungId}/abstimmungsrunde`
        : `/hra/meineAbstimmung/abstimmungAnlegen/vorhabenclearing/${props.regelungsvorhaben.base.id}`,
      !!props.vorhabenclearing,
      false,
      undefined,
      true,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN].VORHABENCLEARING
        }.${props.vorhabenclearing ? 'aktuellenVorhabenclearing' : 'createLink'}`,
      ),
      props.hideProgress,
    ),

    getCollapseItem(
      NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN].RESSORTABSTIMMUNG,
      props.ressortabstimmung
        ? `/hra/meineAbstimmung/${props.ressortabstimmung.abstimmungId}/abstimmungsrunde`
        : `/hra/meineAbstimmung/abstimmungAnlegen/ressortabstimmung/${props.regelungsvorhaben.base.id}`,
      !!props.ressortabstimmung,
      false,
      undefined,
      true,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN)}.${
          NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN].RESSORTABSTIMMUNG
        }.${props.ressortabstimmung ? 'aktuellenRessortabstimmung' : 'createLink'}`,
      ),
      props.hideProgress,
    ),
  ];

  return (
    <>
      <NextStepsContentMain
        stepType={NEXT_STEP_TYPE.REGELUNGSENTWURFABSTIMMEN}
        collapseItems={collapseItems}
        evirSectionTabname={NextStepsTabsNameEnum.REGELUNGSENTWURF_ABSTIMMEN}
      />
    </>
  );
}
