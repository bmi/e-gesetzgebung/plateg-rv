// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  getCollapseItem,
  NEXT_STEP_SUB_TYPE,
  NEXT_STEP_TYPE,
  NextStepsContentMain,
  NextStepsTabsNameEnum,
  translStringCollapse,
} from '../';

interface AusschussverfahrenDurchfuehrenProps {
  hideProgess?: boolean;
}

export function AusschussverfahrenDurchfuehrenComponent(
  props: AusschussverfahrenDurchfuehrenProps,
): React.ReactElement {
  const { t } = useTranslation();

  const collapseItems: CollapseProps['items'] = [
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].TAGESORDNUNG_ERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].TAGESORDNUNG_ERSTELLEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].NACHTRAEGETAGESORDNUNG_ERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].NACHTRAEGETAGESORDNUNG_ERSTELLEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].UNTERAUSSCHUSSSITZUNG_DURCHFUEHREN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].UNTERAUSSCHUSSSITZUNG_DURCHFUEHREN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].ENTWURFNIEDERSCHRIFT_ERSTELLEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].ENTWURFNIEDERSCHRIFT_ERSTELLEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].SITZUNGEN_DURCHFUEHREN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].SITZUNGEN_DURCHFUEHREN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].NIEDERSCHRIFT_FINALISIEREN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].NIEDERSCHRIFT_FINALISIEREN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].UMFRAGEVERFAHREN_DURCHFUEHREN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].UMFRAGEVERFAHREN_DURCHFUEHREN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].EMPFEHLUNGEN_AUSSCHUESSE,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].EMPFEHLUNGEN_AUSSCHUESSE}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].EMPFEHLUNGSDRUCKSACHE_PRUEFEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].EMPFEHLUNGSDRUCKSACHE_PRUEFEN}`,
      ),
      props.hideProgess,
    ),
    getCollapseItem(
      NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN,
      NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].EMPFEHLUNGSDRUCKSACHE_VERTEILEN,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      t(
        `${translStringCollapse(NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN)}
                .${NEXT_STEP_SUB_TYPE[NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN].EMPFEHLUNGSDRUCKSACHE_VERTEILEN}`,
      ),
      props.hideProgess,
    ),
  ];

  return (
    <NextStepsContentMain
      stepType={NEXT_STEP_TYPE.AUSSCHUSSVERFAHREN_DURCHFUEHREN}
      collapseItems={collapseItems}
      evirSectionTabname={NextStepsTabsNameEnum.AUSSCHUSSVERFAHREN_DURCHFUEHREN}
    />
  );
}
