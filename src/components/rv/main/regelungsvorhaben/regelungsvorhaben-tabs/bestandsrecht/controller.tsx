// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/es/table';
import i18n from 'i18next';

import { RegelungsvorhabenBestandsrechtTableDTO } from '@plateg/rest-api';
import { compareDates, getDateTimeString } from '@plateg/theme';

import { TableCompPropsRvBestandsrecht } from './component.react';

export const getBestandsrechtTableVals = (
  content: RegelungsvorhabenBestandsrechtTableDTO[],
): TableCompPropsRvBestandsrecht => {
  const columns: ColumnsType<RegelungsvorhabenBestandsrechtTableDTO> = [
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.header.gesetz').toString(),
      key: 'gesetz',
      sorter: (a, b) => a.titelLang.localeCompare(b.titelLang),
      render: (record: RegelungsvorhabenBestandsrechtTableDTO) => {
        return record.titelLang;
      },
    },
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.header.eli').toString(),
      key: 'eli',
      sorter: (a, b) => a.eli.localeCompare(b.eli),
      render: (record: RegelungsvorhabenBestandsrechtTableDTO) => {
        return record.eli;
      },
    },
    {
      title: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.header.importiertAm').toString(),
      key: 'importiertAm',
      sorter: (a, b) => compareDates(a.importiertAm, b.importiertAm),
      render: (record: RegelungsvorhabenBestandsrechtTableDTO) => {
        return getDateTimeString(record.importiertAm);
      },
    },
  ];

  return {
    id: 'rv-bestandsrecht-table',
    expandable: false,
    columns,
    content,
    filteredColumns: [{ name: 'schwebendeAenderung', columnIndex: 1 }],
    sorterOptions: [
      {
        columnKey: 'importiertAm',
        titleAsc: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.importiertAmAsc'),
        titleDesc: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.importiertAmDesc'),
      },
      {
        columnKey: 'gesetz',
        titleAsc: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.nameAsc'),
        titleDesc: i18n.t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.table.nameDesc'),
      },
    ],
    customDefaultSortIndex: 2,
  };
};
