// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './bestandsrecht-review.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { NormShortDTO } from '@plateg/rest-api';

interface BestandsrechtReviewProps {
  nueRises: NormShortDTO[];
}

export function BestandsrechtReview(props: BestandsrechtReviewProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <div className="bestandsrect-review">
        <Title>{t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.reviewTitle')}</Title>
        <div className="bestandsrechtreview-content">
          <div>{t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.resultTitle')}:</div>
          <ul>
            {props.nueRises.map((item) => (
              <li key={item.guid}>{item.officialLongTitle}</li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
}
