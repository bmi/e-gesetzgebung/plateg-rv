// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './tagsList.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { NormShortDTO } from '@plateg/rest-api';
import { AriaController, CloseOutlined } from '@plateg/theme';

interface NeuRisTagsListInterface {
  selectedNeuRises: NormShortDTO[];
  deleteUser: (user: NormShortDTO) => void;
  fieldName: string;
  disabled?: boolean;
  defaultElis: string[];
}

export function NeuRisTagsList(props: NeuRisTagsListInterface): React.ReactElement {
  const { t } = useTranslation();
  const defaultVisibleItems = 8;
  const [neuRisesList, setNeuRisesList] = useState<NormShortDTO[]>([]);
  const [hiddenClass, setHiddenClass] = useState('hidden');

  useEffect(() => {
    setNeuRisesList(props.selectedNeuRises);
    AriaController.setAttrByClassName('tag-holder', 'role', 'list');
    AriaController.setAttrByClassName('tag-element', 'role', 'listitem');
  }, [props.selectedNeuRises]);

  return (
    <div className="tag-holder">
      {!!neuRisesList.length && (
        <span className="bestandsrecht-tags-label">
          {t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.resultTitle')}*
        </span>
      )}

      {neuRisesList.map((item, index) => {
        const deleteMsg = t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.delete');
        const name = item.officialLongTitle;
        return (
          <div
            className={`tag-element ${index + 1 > defaultVisibleItems ? hiddenClass : ''}`}
            key={index}
            id={`selected-address-${props.fieldName}-${index}`}
          >
            {name}
            <Button
              disabled={props.defaultElis.includes(item.eli)}
              className="btn-close"
              type="text"
              onClick={() => props.deleteUser(item)}
              aria-label={`${deleteMsg} ${name}`}
              id={`delete-address-${props.fieldName}-${index}`}
            >
              <CloseOutlined />
            </Button>
          </div>
        );
      })}
      {neuRisesList.length > defaultVisibleItems && hiddenClass ? (
        <Button
          disabled={props.disabled}
          type="text"
          className="show-more-btn"
          onClick={() => setHiddenClass('')}
          id={`show-more-${props.fieldName}`}
        >
          + {neuRisesList.length - defaultVisibleItems}{' '}
          {t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.moreItems')}
        </Button>
      ) : null}
      <div className="div-no-addresses">
        {!neuRisesList.length ? t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.noNeuRises') : null}
      </div>
    </div>
  );
}
