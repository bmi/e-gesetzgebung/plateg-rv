// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';

import { Configuration, NeuRisControllerApi, NeurisSearchResult } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

export class NeuRisModalController {
  private readonly neuRisControllerApi = GlobalDI.getOrRegister(
    'neuRisControllerApi',
    () => new NeuRisControllerApi(new Configuration()),
  );

  public NeuRisSearch(suchString: string): Observable<NeurisSearchResult> {
    return this.neuRisControllerApi.doNeuRisSuche({ suchString });
  }
}
