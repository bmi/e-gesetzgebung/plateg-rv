// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './searchField.less';

import { Button } from 'antd';
import Search from 'antd/lib/input/Search';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { NormShortDTO } from '@plateg/rest-api';
import { AriaController, CheckOutlined, ErrorController, GlobalDI } from '@plateg/theme';

import { NeuRisModalController } from '../controller';

interface SearchFieldInterface {
  onApply: (val: NormShortDTO) => void;
  selectedNeuRises: NormShortDTO[];
  fieldName: string;
  disabled?: boolean;
}

export function SearchFieldComponent(props: SearchFieldInterface): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = new NeuRisModalController();
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [neuRisList, setNeuRisList] = useState<NormShortDTO[]>([]);
  const [fetchingMsg, setFetchMsg] = useState<string>('');
  const [valueSearch, setValueSearch] = useState<string>('');

  const searchLoaderAriaChange = () => {
    document
      .getElementById(`bestandsrecht-search-field-${props.fieldName}`)
      ?.setAttribute('aria-describedby', `bestandsrecht-search-field-${props.fieldName}-error`);
    if (fetchingMsg.includes('10')) {
      AriaController.setAttrByClassName('search-loader', 'role', 'region');
    } else {
      AriaController.setAttrByClassName('search-loader', 'role', 'alert');
    }
  };

  useEffect(() => {
    searchLoaderAriaChange();
  }, []);

  useEffect(() => {
    searchLoaderAriaChange();
  }, [fetchingMsg]);

  const updateFetchingMsg = (dataLength: number) => {
    setFetchMsg('');
    if (!dataLength) {
      setFetchMsg(t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.notFound'));
    } else if (dataLength > 9) {
      setFetchMsg(t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.moreThan10'));
    }
  };

  const getSuggestions = (neuRises: NormShortDTO[]): React.ReactElement | null => {
    if (!neuRises.length) {
      return null;
    }

    return (
      <>
        <strong className="title">
          {neuRisList.length}{' '}
          {neuRisList.length > 1
            ? t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.results.plural')
            : t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.results.singular')}
        </strong>
        <ul className="suggestions-list" id={`suggestions-list-${props.fieldName}`}>
          {neuRises?.map((neuRis, index) => {
            return (
              <li key={index} id={`suggestion-${props.fieldName}-${index}`}>
                <span className="name">{neuRis.officialLongTitle}</span>
                {getStatusBtn(neuRis)}
              </li>
            );
          })}
        </ul>
      </>
    );
  };

  const getStatusBtn = (neuRis: NormShortDTO) => {
    // Check is user already selected and show check icon
    const isAlreadyAdded = props.selectedNeuRises.map((item) => item.eli).includes(neuRis.eli);
    const btnApplyLabel = t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.btnApply');
    let statusHolder = (
      <Button
        disabled={props.disabled}
        type="text"
        onClick={() => props.onApply(neuRis)}
        aria-label={`${btnApplyLabel} ${neuRis.officialLongTitle} `}
      >
        {btnApplyLabel}
      </Button>
    );
    if (isAlreadyAdded) {
      statusHolder = (
        <span className="already-added">
          <CheckOutlined />
        </span>
      );
    }

    return statusHolder;
  };

  const startSearch = (value: string) => {
    setValueSearch(value);
    setNeuRisList([]);
    setFetchMsg('');
    if (value.length && value.length < 3) {
      setFetchMsg(t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.tooShort'));
    } else if (value) {
      setFetchMsg(t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.load'));
      ctrl.NeuRisSearch(value).subscribe({
        next: (data) => {
          //teporary filter data to fix elis duplicated issue otherwise we need to use all data
          const temporarData: NormShortDTO[] = [];
          for (const iterator of data.dtos) {
            if (!temporarData.find((item) => item.eli === iterator.eli)) {
              temporarData.push(iterator);
            }
          }
          setNeuRisList(temporarData);
          updateFetchingMsg(temporarData.length);
        },
        error: (error: AjaxError) => {
          setFetchMsg('');
          errorCtrl.displayErrorMsg(error, 'theme.generalErrorMsg');
          console.error(error);
        },
      });
    }
  };

  return (
    <div className="nueris-search-holder">
      <Search
        disabled={props.disabled}
        className="search-field"
        placeholder={t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.neuRisSearch.placeholder')}
        size="large"
        enterButton={t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.neuRisSearch.btnText')}
        onSearch={startSearch}
        value={valueSearch}
        onChange={(e) => setValueSearch(e.target.value)}
        suffix={
          <Button
            disabled={props.disabled}
            className={`btn-clear ${!valueSearch ? 'hidden' : ''}`}
            type="text"
            onClick={() => startSearch('')}
          >
            {t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.neuRisSearch.btnClear')}
          </Button>
        }
        id={`bestandsrecht-search-field-${props.fieldName}`}
      />
      <div className={`search-loader`} aria-live="polite" id={`bestandsrecht-search-field-${props.fieldName}-error`}>
        {fetchingMsg}
      </div>

      <div className="suggestions-holder">{getSuggestions(neuRisList)}</div>
    </div>
  );
}
