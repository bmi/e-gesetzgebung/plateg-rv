// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './bestandsrecht-modal.less';

import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { NormShortDTO, RegelungsvorhabenBestandsrechtTableDTO } from '@plateg/rest-api';
import { FormItemWithInfo, GeneralFormWrapper, MultiPageModalComponent } from '@plateg/theme';

import { BestandsrechtReview } from './bestandsrecht-review/component.react';
import { NeuRisTagsList } from './neuris-tags-list/component.react';
import { SearchFieldComponent } from './search-field/component.react';

interface BestandsRechtModalProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  rvName: string;
  saveBestandsrechte: (elis: string[]) => void;
  rvBestandsrechte: RegelungsvorhabenBestandsrechtTableDTO[];
}

export function BestandsRechtModal(props: BestandsRechtModalProps): React.ReactElement {
  const { t } = useTranslation();
  const [nueRises, setNueRises] = useState<NormShortDTO[]>([]);
  const [nxtBtnDisables, setNxtBtnDisables] = useState<boolean>(true);
  const [activePageIndex, setActivePageIndex] = useState<number>(0);

  const saveBestandsrechte = () => {
    props.saveBestandsrechte(nueRises.map((item) => item.eli));
  };
  const selectNueRis = (nueRis: NormShortDTO) => {
    if (
      !props.rvBestandsrechte.map((item) => item.eli).includes(nueRis.eli) &&
      !nueRises.find((item) => item.eli === nueRis.eli)
    ) {
      setNueRises([...nueRises, nueRis]);
    }
  };
  const deleteNueRis = (nueRis: NormShortDTO) => {
    setNueRises([...nueRises.filter((item) => item.eli !== nueRis.eli)]);
  };
  useEffect(() => {
    checkNextBtnDisabled();
  }, [nueRises]);
  useEffect(() => {
    if (props.isVisible) {
      setActivePageIndex(0);
      setNueRises(
        props.rvBestandsrechte.map((item) => {
          return {
            guid: item.guid,
            eli: item.eli,
            officialLongTitle: item.titelLang,
          };
        }),
      );
    }
  }, [props.isVisible]);
  const checkNextBtnDisabled = () => {
    const sorter = (
      a: RegelungsvorhabenBestandsrechtTableDTO | NormShortDTO,
      b: RegelungsvorhabenBestandsrechtTableDTO | NormShortDTO,
    ) => {
      if (a.eli > b.eli) {
        return 1;
      }
      if (b.eli > a.eli) {
        return -1;
      }
      return 0;
    };
    props.rvBestandsrechte.sort(sorter);
    nueRises.sort(sorter);
    setNxtBtnDisables(
      props.rvBestandsrechte.map((item) => item.eli).join(',') === nueRises.map((item) => item.eli).join(','),
    );
  };
  const getToImportNeurises = () => {
    return nueRises.filter((item) => !props.rvBestandsrechte.find((element) => element.eli === item.eli));
  };
  return (
    <MultiPageModalComponent
      key="bestandsrecht-dialog"
      componentReferenceContext={'bestandsrecht-dialog' + props.isVisible.toString()}
      isVisible={props.isVisible}
      setIsVisible={props.setIsVisible}
      title={t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.title', { rvName: props.rvName })}
      cancelBtnText={t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.cancelBtnText')}
      nextBtnText={t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.pruefenBtn')}
      prevBtnText={t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.previousBtn')}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      className="bestandsrecht-modal"
      pages={[
        {
          content: (
            <>
              <GeneralFormWrapper
                id={`bestandsrecht-modal-page1`}
                name="bestandsrecht-modal-page1"
                className="bestandsrecht-modal-page1"
                layout="vertical"
              >
                <Title>{t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.contentTitle')}</Title>
                <p className="content-description">
                  {t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.contentDescription')}
                </p>
                <FormItemWithInfo
                  name="bestandsrecht-suchen"
                  label={
                    <span className="bestandsrecht-search-label">
                      <span>{t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.searchLabel')}</span>
                      <span>{t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.searchDescription')}</span>
                    </span>
                  }
                >
                  <span>
                    <SearchFieldComponent
                      onApply={selectNueRis}
                      selectedNeuRises={nueRises}
                      fieldName={'neuris-search-field'}
                    />
                    <NeuRisTagsList
                      selectedNeuRises={getToImportNeurises()}
                      deleteUser={deleteNueRis}
                      fieldName={'neuris-tags-field'}
                      defaultElis={props.rvBestandsrechte.map((item) => item.eli)}
                    />
                  </span>
                </FormItemWithInfo>
              </GeneralFormWrapper>
            </>
          ),
          primaryInsteadNextBtn: {
            disabled: nxtBtnDisables,
            buttonText: t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.pruefenBtn'),
            shouldNavToNextPage: true,
          },
          nextOnClick: () => {
            setActivePageIndex(activePageIndex + 1);
          },
        },
        {
          content: <BestandsrechtReview nueRises={getToImportNeurises()} />,
          primaryInsteadNextBtn: {
            buttonText: t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.importBtn'),
          },
          nextOnClick: () => {
            saveBestandsrechte();
          },
        },
      ]}
    />
  );
}
