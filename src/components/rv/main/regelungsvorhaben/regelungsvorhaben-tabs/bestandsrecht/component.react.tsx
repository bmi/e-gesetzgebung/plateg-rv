// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './bestandsrecht.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AktionType,
  RegelungsvorhabenBestandsrechtTableDTO,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityFullResponseDTO,
} from '@plateg/rest-api';
import { displayMessage, GlobalDI, LoadingStatusController, TableComponent, TableComponentProps } from '@plateg/theme';

import { RvErrorController } from '../../../../../../utils/rv-error-controller';
import { BestandsRechtModal } from './bestandsrecht-modal/component.react';
import { getBestandsrechtTableVals } from './controller';

interface BestandsrechtProps {
  regelungsvorhaben: RegelungsvorhabenEntityFullResponseDTO;
  bestandsrechtData: RegelungsvorhabenBestandsrechtTableDTO[];
  setBestandsrechtData: (vals: RegelungsvorhabenBestandsrechtTableDTO[]) => void;
}

export type TableCompPropsRvBestandsrecht = TableComponentProps<RegelungsvorhabenBestandsrechtTableDTO>;

export function Bestandsrecht(props: BestandsrechtProps): React.ReactElement {
  const { t } = useTranslation();
  const [isBestandsrechtModalVisible, setIsBestandsrechtModalVisible] = useState<boolean>(false);
  const [bestandsrechtTableVals, setBestandsrechtTableVals] = useState<TableCompPropsRvBestandsrecht>({
    columns: [],
    content: [],
    id: '',
    expandable: false,
  });

  const rvCtrl = GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi());
  const loadingStatusCtrl = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  let rvSub: Subscription;

  useEffect(() => {
    if (props.bestandsrechtData.length) {
      setBestandsrechtTableVals(getBestandsrechtTableVals(props.bestandsrechtData));
    }
  }, [props.bestandsrechtData]);

  const fetchBestandsrechte = () => {
    if (rvSub) {
      rvSub.unsubscribe();
    }
    loadingStatusCtrl.setLoadingStatus(true);

    rvSub = rvCtrl.getRegelungsvorhabenBestandsrechtTable({ id: props.regelungsvorhaben.base.id }).subscribe({
      next: (data) => {
        props.setBestandsrechtData(data);
      },
      complete: () => {
        loadingStatusCtrl.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        rvErrorCtrl.handleError(error);
      },
    });
  };
  const saveBestandsrechte = (elis: string[]) => {
    loadingStatusCtrl.setLoadingStatus(true);
    setIsBestandsrechtModalVisible(false);
    rvCtrl
      .createRegelungsvorhabenBestandsrecht({
        id: props.regelungsvorhaben.base.id,
        regelungsvorhabenBestandsrechtCreateDTO: elis
          .filter((item) => !props.bestandsrechtData.find((element) => element.eli === item))
          .map((item) => ({
            eli: item,
          })),
      })
      .subscribe({
        next: () => {
          fetchBestandsrechte();
          displayMessage(
            t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.modal.successMessage', {
              rvName: props.regelungsvorhaben.dto.abkuerzung,
            }),
            'success',
          );
        },
        complete: () => {
          loadingStatusCtrl.setLoadingStatus(false);
        },
        error: (error: AjaxError) => {
          loadingStatusCtrl.setLoadingStatus(false);
          rvErrorCtrl.handleError(error);
        },
      });
  };
  return (
    <>
      <div className="table-container">
        <Button
          className="bestandsrecht-button"
          size={'middle'}
          type="primary"
          onClick={() => setIsBestandsrechtModalVisible(true)}
          disabled={!props.regelungsvorhaben.dto.aktionen?.includes(AktionType.ImportNeuris)}
        >
          {t('rv.regelungsvorhabenTabs.tabs.bestandsrecht.button')}
        </Button>
        <BestandsRechtModal
          isVisible={isBestandsrechtModalVisible}
          setIsVisible={setIsBestandsrechtModalVisible}
          rvName={props.regelungsvorhaben.dto.abkuerzung}
          saveBestandsrechte={saveBestandsrechte}
          rvBestandsrechte={props.bestandsrechtData}
        />

        {bestandsrechtTableVals.content.length > 0 &&
          props.regelungsvorhaben.dto.aktionen?.includes(AktionType.BestandsrechtLesen) && (
            <TableComponent
              id={bestandsrechtTableVals.id}
              expandable={false}
              columns={bestandsrechtTableVals.columns}
              content={bestandsrechtTableVals.content}
              sorterOptions={bestandsrechtTableVals.sorterOptions}
              filteredColumns={bestandsrechtTableVals.filteredColumns}
              customDefaultSortIndex={bestandsrechtTableVals.customDefaultSortIndex}
            />
          )}
      </div>
    </>
  );
}
