// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './my-regelungsvorhaben-bundesrat-tab.less';

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, RegelungsvorhabenBundestagTableDTO } from '@plateg/rest-api/models/';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { getFilterButton, getFilteredRows, setArchived } from '../../tabs/controller.react';
import { FreigabeModalRV } from '../../tabs/freigabe-modal/component.react';
import { getMyRegelungsvorhabenBundesratTableVals } from './controller.react';

interface MyRegelungsvorhabenTabProps {
  myRegelungsvorhabenData: RegelungsvorhabenBundestagTableDTO[];
  setMyRegelungsvorhabenData: (content: RegelungsvorhabenBundestagTableDTO[]) => void;
  resetTab: (tab: string) => void;
  currentUserIsStellvertreter: boolean;
  tabKey: string;
  loadContent: () => void;
}
export type TableCompPropsRvTableWithoutId = Omit<TableComponentProps<RegelungsvorhabenBundestagTableDTO>, 'id'>;

export function MyRegelungsvorhabenBundesratTab(props: MyRegelungsvorhabenTabProps): React.ReactElement {
  const { t } = useTranslation();
  const { Paragraph } = Typography;

  const [confirmArchiveModalIsVisible, setConfirmArchiveModalIsVisible] = useState(false);
  const [selectedRegelungsvorhaben, setSelectedRegelungsvorhaben] = useState<RegelungsvorhabenBundestagTableDTO>(
    {} as RegelungsvorhabenBundestagTableDTO,
  );
  const [myRegelungsvorhabenTableVals, setMyRegelungsvorhabenTableVals] = useState<TableCompPropsRvTableWithoutId>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const [isFreigebenModalVisible, setIsFreigebenModalVisible] = useState<boolean>(false);
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setMyRegelungsvorhabenTableVals(
      getMyRegelungsvorhabenBundesratTableVals(
        props.myRegelungsvorhabenData,
        setConfirmArchiveModalIsVisible,
        setSelectedRegelungsvorhaben,
        setIsFreigebenModalVisible,
        props.currentUserIsStellvertreter,
      ),
    );
  }, [props.myRegelungsvorhabenData]);
  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <>
          <ArchivConfirmComponent
            okText={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archiveConfirm')}
            cancelText={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archiveCancel')}
            modalTitle={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.modalTitle')}
            actionTitle={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archiveActionTitle')}
            visible={confirmArchiveModalIsVisible}
            setVisible={setConfirmArchiveModalIsVisible}
            setArchived={() => {
              setArchived(
                selectedRegelungsvorhaben.id,
                props.myRegelungsvorhabenData,
                props.setMyRegelungsvorhabenData,
                props.loadContent,
              );
              props.resetTab('archiv');
              setConfirmArchiveModalIsVisible(false);
            }}
          />
          <FreigabeModalRV
            disabled={!selectedRegelungsvorhaben.aktionen?.includes(AktionType.BerechtigungenAendern)}
            isVisible={isFreigebenModalVisible}
            setIsVisible={setIsFreigebenModalVisible}
            objectTitle={selectedRegelungsvorhaben.kurzbezeichnung}
            objectId={selectedRegelungsvorhaben.id}
            selectedRvId={selectedRegelungsvorhaben.id}
            allowChanges={selectedRegelungsvorhaben.aktionen?.includes(AktionType.BerechtigungenAendern)}
          />
          <TableComponent
            tabKey={props.tabKey}
            bePagination
            className="rv-bundestag-table"
            id="rv-own-rvs-table"
            expandable={myRegelungsvorhabenTableVals.expandable}
            columns={myRegelungsvorhabenTableVals.columns}
            content={myRegelungsvorhabenTableVals.content}
            filteredColumns={myRegelungsvorhabenTableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            sorterOptions={myRegelungsvorhabenTableVals.sorterOptions}
            customDefaultSortIndex={2}
            userIsStellvertreter={props.currentUserIsStellvertreter}
          ></TableComponent>
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.BR.imgText1'),
            imgSrc:
              require('../../../../../media/empty-content-images/myRegelungsvorhabenBt/img1-myRegelungsvorhabenBt.svg') as ImageModule,
            height: 104,
          },
        ]}
      >
        <Paragraph className="info-text">
          <p
            className="info-text-bt"
            dangerouslySetInnerHTML={{
              __html: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.BR.text1'),
            }}
          />
          <p
            className="info-text-bt"
            dangerouslySetInnerHTML={{
              __html: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.BR.text2'),
            }}
          />
        </Paragraph>
      </EmptyContentComponent>
    </>
  );
}
