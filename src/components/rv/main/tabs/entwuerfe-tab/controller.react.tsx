// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React from 'react';

import { RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api';
import { DropdownMenu } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { deleteRegelungsvorhaben } from '../controller.react';
import { getBRegTableItems } from '../general-items/controller.react';
import { TableCompPropsRvTableWithoutId } from '../my-regelungsvorhaben-tab/component.react';

const history = createHashHistory();
export function getEntwuerfeTableVals(
  content: RegelungsvorhabenBundesregierungTableDTO[],
  setRegelungsvorhabenData: (entwuerfeData: RegelungsvorhabenBundesregierungTableDTO[]) => void,
  loadContent?: () => void,
): TableCompPropsRvTableWithoutId {
  const columns: ColumnsType<RegelungsvorhabenBundesregierungTableDTO> = [
    ...getBRegTableItems(true),
    {
      title: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.actions').toString(),
      key: 'actions',
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        return (
          <div className="actions-holder">
            <Button
              id="rv-myRegelungsVorhaben-entwurfBearbeiten-btn"
              disabled={!record.allowedToModify}
              className="ant-btn-secondary"
              onClick={() =>
                history.push(
                  `/regelungsvorhaben/${routes.ENTWURFE}/${record.id}/${routes.BEARBEITEN_REGELUNGSVORHABEN_ENTWURF}`,
                )
              }
            >
              {i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.btnBearbeiten').toString()}
            </Button>
            <DropdownMenu
              openLink={`/regelungsvorhaben/${routes.ENTWURFE}/${record.id}/${routes.DATENBLATT}`}
              items={[
                {
                  element: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.delete'),
                  onClick: () => deleteRegelungsvorhaben(record.id, content, setRegelungsvorhabenData, loadContent),
                },
              ]}
              elementId={record.id}
            />
          </div>
        );
      },
    },
  ];

  return {
    expandable: false,
    columns,
    content,
    filteredColumns: [{ name: 'vorhabentyp', columnIndex: 0 }],
    sorterOptions: [
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.bearbeitetamAsc'),
        titleDesc: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.bearbeitetamDesc'),
      },
      {
        columnKey: 'erstellt',
        titleAsc: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.erstelltamAsc'),
        titleDesc: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.erstelltamDesc'),
      },
    ],
  };
}
