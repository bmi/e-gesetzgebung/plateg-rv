// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api/models/';
import { EmptyContentComponent, ImageModule, TableComponent } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { getFilterButton, getFilteredRows } from '../controller.react';
import { TableCompPropsRvTableWithoutId } from '../my-regelungsvorhaben-tab/component.react';
import { getEntwuerfeTableVals } from './controller.react';

interface EntwuerfeTabProps {
  entwuerfeData: RegelungsvorhabenBundesregierungTableDTO[];
  setEntwuerfeData: (content: RegelungsvorhabenBundesregierungTableDTO[]) => void;
  tabKey: string;
  loadContent: () => void;
}
export function EntwuerfeTab(props: EntwuerfeTabProps): React.ReactElement {
  const { t } = useTranslation();

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  const [entwuerfeTableVals, setEntwuerfeTableVals] = useState<TableCompPropsRvTableWithoutId>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });

  useEffect(() => {
    setEntwuerfeTableVals(getEntwuerfeTableVals(props.entwuerfeData, props.setEntwuerfeData, props.loadContent));
  }, [props.entwuerfeData]);
  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <TableComponent
          tabKey={props.tabKey}
          bePagination
          id="rv-entwuerfe-table"
          expandable={entwuerfeTableVals.expandable}
          columns={entwuerfeTableVals.columns}
          content={entwuerfeTableVals.content}
          filteredColumns={entwuerfeTableVals.filteredColumns}
          filterRowsMethod={getFilteredRows}
          prepareFilterButtonMethod={getFilterButton}
          sorterOptions={entwuerfeTableVals.sorterOptions}
          customDefaultSortIndex={2}
        ></TableComponent>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t('rv.myRegelungsvorhaben.tabs.entwuerfe.imgText1'),
            imgSrc: require(`../../../../../media/empty-content-images/entwurfe/img1-entwurfe.svg`) as ImageModule,
            height: 132,
          },
          {
            label: t('rv.myRegelungsvorhaben.tabs.entwuerfe.imgText2'),
            imgSrc: require(`../../../../../media/empty-content-images/entwurfe/img2-entwurfe.svg`) as ImageModule,
            height: 124,
          },
        ]}
      >
        <p className="info-text">{t('rv.myRegelungsvorhaben.tabs.entwuerfe.text')}</p>
      </EmptyContentComponent>
    </>
  );
}
