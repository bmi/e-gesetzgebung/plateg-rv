// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import i18n from 'i18next';
import React from 'react';

import { RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api';
import { compareDates, DropdownMenu, getDateTimeString, TableComponentProps } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { getArchiveTableItems } from '../general-items/controller.react';

export function getArchivTableVals(
  content: RegelungsvorhabenBundesregierungTableDTO[],
): TableComponentProps<RegelungsvorhabenBundesregierungTableDTO> {
  const columns: ColumnsType<RegelungsvorhabenBundesregierungTableDTO> = [
    ...getArchiveTableItems(true),
    {
      title: i18n.t('rv.myRegelungsvorhaben.tabs.archiv.table.archiviertAm'),
      key: 'archiviertAm',
      sorter: (a, b) => compareDates(a.archiviertAm, b.archiviertAm),
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        if (record.archiviertAm) {
          return getDateTimeString(record.archiviertAm);
        } else {
          return {
            colSpan: 0,
          };
        }
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.table.actions'),
      key: 'actions',
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        return (
          <div className="actions-holder">
            <DropdownMenu
              openLink={`/regelungsvorhaben/${routes.ARCHIV}/${record.id}/${routes.DATENBLATT}`}
              items={[]}
              elementId={record.id}
            />
          </div>
        );
      },
    },
  ];

  return {
    expandable: false,
    columns,
    content,
    filteredColumns: [{ name: 'vorhabentyp', columnIndex: 0 }],
    sorterOptions: [
      {
        columnKey: 'archiviertAm',
        titleAsc: i18n.t('rv.myRegelungsvorhaben.tabs.archiv.table.archiviertamAsc'),
        titleDesc: i18n.t('rv.myRegelungsvorhaben.tabs.archiv.table.archiviertamDesc'),
      },
    ],
  };
}
