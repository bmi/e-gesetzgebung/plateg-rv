// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api/models/';
import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { getFilterButton, getFilteredRows } from '../controller.react';
import { getArchivTableVals } from './controller.react';

interface ArchivTabProps {
  archivData: RegelungsvorhabenBundesregierungTableDTO[];
  tabKey: string;
}
export function ArchivTab(props: ArchivTabProps): React.ReactElement {
  const { t } = useTranslation();

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  const [archivTableVals, setArchivTableVals] = useState<TableComponentProps<RegelungsvorhabenBundesregierungTableDTO>>(
    {
      id: 'Regelungsvorhaben Archiv Tabelle',
      columns: [],
      content: [],
      expandable: false,
      sorterOptions: [],
    },
  );

  useEffect(() => {
    setArchivTableVals(getArchivTableVals(props.archivData));
  }, [props.archivData]);
  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <TableComponent
          tabKey={props.tabKey}
          bePagination
          id="rv-archiv-table"
          expandable={archivTableVals.expandable}
          columns={archivTableVals.columns}
          content={archivTableVals.content}
          filteredColumns={archivTableVals.filteredColumns}
          filterRowsMethod={getFilteredRows}
          prepareFilterButtonMethod={getFilterButton}
          sorterOptions={archivTableVals.sorterOptions}
          customDefaultSortIndex={2}
        ></TableComponent>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t('rv.myRegelungsvorhaben.tabs.archiv.imgText1'),
            imgSrc: require(`../../../../../media/empty-content-images/archiv/img1-archiv.svg`) as ImageModule,
            height: 132,
          },
          {
            label: t('rv.myRegelungsvorhaben.tabs.archiv.imgText2'),
            imgSrc: require(`../../../../../media/empty-content-images/archiv/img2-archiv.svg`) as ImageModule,
            height: 104,
          },
        ]}
      >
        <p className="info-text">{t('rv.myRegelungsvorhaben.tabs.archiv.text')}</p>
      </EmptyContentComponent>
    </>
  );
}
