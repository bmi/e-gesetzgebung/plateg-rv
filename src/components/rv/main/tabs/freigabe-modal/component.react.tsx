// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import './freigabe-modal.less';

import { t } from 'i18next';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import {
  BerechtigungAnBenutzerDTO,
  BerechtigungResponseDTO,
  FreigabeEntityResponseDTO,
  RolleLokalType,
  UserEntityDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { FreigabeModal } from '@plateg/theme';
import { DeleteConfirmContent } from '@plateg/theme/src/components/delete-confirm-popover/component.react';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

import { FreigabeModalRVController } from './controller';

interface FreigabeModalRVProps {
  disabled?: boolean;
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  objectId: string | undefined;
  objectTitle: string | undefined;
  selectedRvId: string;
  allowChanges?: boolean;
  revokeObserverRight?: boolean;
  loadContent?: () => void;
}

export function FreigabeModalRV(props: FreigabeModalRVProps): React.ReactElement {
  const history = useHistory();
  const freigebenModalController = GlobalDI.getOrRegister(
    'rvFreigebenModalController',
    () => new FreigabeModalRVController(),
  );
  const appStore = useAppSelector((state) => state.user);
  const [federfuehrung, setFederfuehrung] = useState<UserEntityDTO | undefined>();
  const [freigaben, setFreigaben] = useState<FreigabeEntityResponseDTO[]>([]);
  const [checkOwner, setCheckOwner] = useState(true);
  const [documentEditor, setDocumentEditor] = useState<UserEntityResponseDTO | undefined>();
  const [localDocumentEditor, setLocalDocumentEditor] = useState<UserEntityResponseDTO | undefined>();

  const getFreigaben = () => {
    if (props.selectedRvId) {
      freigebenModalController.getFreigaben(props.selectedRvId, (value: BerechtigungResponseDTO[]) => {
        const foundEditorFreigabe = value.find(
          (item) =>
            item.berechtigter.base.id === appStore.user?.base.id && item.rolleType === RolleLokalType.Mitarbeiter,
        );

        if (foundEditorFreigabe) {
          setCheckOwner(false);
          setDocumentEditor(foundEditorFreigabe.berechtigter);
          setLocalDocumentEditor(foundEditorFreigabe.berechtigter);
          value = value.filter((item) => item.berechtigter.base.id !== foundEditorFreigabe.berechtigter.base.id);
        } else {
          setCheckOwner(true);
          setDocumentEditor(undefined);
        }

        const federfuehrung = value.filter((item) => item.rolleType === RolleLokalType.Federfuehrer);
        if (federfuehrung.length > 0) {
          setFederfuehrung(federfuehrung[0].berechtigter.dto);
        }

        setFreigaben(
          value
            .filter((item) => item.rolleType !== RolleLokalType.Federfuehrer)
            .map((item) => {
              return {
                base: item.berechtigter.base,
                dto: {
                  readOnlyAccess: item.rolleType === RolleLokalType.Gast,
                  user: item.berechtigter,
                },
              };
            }),
        );
      });
    }
  };
  useEffect(() => {
    if (props.selectedRvId && props.isVisible !== false) {
      getFreigaben();
    }
  }, [props.selectedRvId, props.isVisible]);
  const texts = {
    title: `Regelungsvorhaben „${props.objectTitle || ''}“`,
  };

  const freigeben = (vals: BerechtigungAnBenutzerDTO[], resetModal: (isVisible: boolean) => void) => {
    const oldUsers: BerechtigungAnBenutzerDTO[] = freigaben.map((item) => {
      return {
        email: item.dto.user.dto.email,
        rolleType: item.dto.readOnlyAccess ? RolleLokalType.Gast : RolleLokalType.Mitarbeiter,
      };
    });

    const resultOfComparing = Filters.prepareBerechtigungenRequestBody(vals, oldUsers);

    if (!documentEditor && localDocumentEditor) {
      resultOfComparing.remove.push({
        email: localDocumentEditor.dto.email,
        rolleType: RolleLokalType.Gast,
      });
      setLocalDocumentEditor(undefined);
    }

    if (props.objectId) {
      freigebenModalController.vorlageFreigeben(
        props.objectTitle,
        props.objectId,
        resultOfComparing,
        resetModal,
        props.revokeObserverRight ? goToHomepage : getFreigaben,
      );
    }
  };

  const goToHomepage = () => {
    history.push('/cockpit');
  };

  return (
    <FreigabeModal
      ersteller={federfuehrung}
      checkOwner={checkOwner}
      {...props}
      ressortCompare={true}
      freigaben={freigaben}
      leserechtOnly={false}
      className="rv-freigabe-modal"
      texts={texts}
      freigeben={freigeben}
      documentEditor={documentEditor}
      setDocumentEditor={setDocumentEditor}
      isRBACActivated={true}
      allowChanges={props.allowChanges}
      revokeObserverRight={props.revokeObserverRight}
      deleteConfirmContent={
        {
          title: t('rv.freigabe.freigabeTitel'),
          infoText: t('rv.freigabe.infoText'),
          yesBtnText: t('rv.freigabe.entziehenButton'),
          noBtnText: t('rv.freigabe.abbrechenButton'),
        } as DeleteConfirmContent
      }
    />
  );
}
