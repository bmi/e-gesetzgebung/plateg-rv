// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import {
  BerechtigungAnBenutzerDTO,
  BerechtigungControllerApi,
  RolleLokalType,
  UpdatePermissionsDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../../general.test';
import { FreigabeModalRVController } from './controller';

describe('FreigabeModalRVController testen', () => {
  let ctrl: FreigabeModalRVController;
  let httpStub: sinon.SinonStub;
  let setIsVisibleStub: sinon.SinonStub;
  let loadDataStub: sinon.SinonStub;

  before(() => {
    ctrl = new FreigabeModalRVController();
    const berechtigungController = GlobalDI.getOrRegister<BerechtigungControllerApi>(
      'berechtigungControllerApi',
      () => new BerechtigungControllerApi(),
    );
    setIsVisibleStub = sinon.stub();
    loadDataStub = sinon.stub();
    httpStub = sinon.stub(berechtigungController, 'setBerechtigungen');
  });
  after(() => {
    httpStub.restore();
  });
  describe('TEST: vorlageFreigaben - Success', () => {
    before(() => {
      httpStub.returns(of(void 0));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
    });
    after(() => {
      setLoadingStatusStub.reset();
      httpStub.resetBehavior();
    });
    it('check if REST API is called - single adressat', (done) => {
      const vorlageTitle = 'abc';
      const vorlageId = 'xyz';
      const freigebenValues: UpdatePermissionsDTO = {
        add: [{ rolleType: RolleLokalType.Gast, email: 'test@test.test' }] as BerechtigungAnBenutzerDTO[],
        modify: [] as BerechtigungAnBenutzerDTO[],
        remove: [] as BerechtigungAnBenutzerDTO[],
      };
      ctrl.vorlageFreigeben(vorlageTitle, vorlageId, freigebenValues, setIsVisibleStub);
      setTimeout(() => {
        sinon.assert.called(setLoadingStatusStub);
        sinon.assert.called(displayErrorMsgStub);
        sinon.assert.notCalled(loadDataStub);
        done();
      }, 20);
    });
  });
  describe('TEST: vorlageFreigaben - Failure', () => {
    before(() => {
      httpStub.returns(throwError({ status: 404 }));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      setLoadingStatusStub.reset();
      httpStub.resetBehavior();
    });
    it('check if REST API is called - single adressat', (done) => {
      const vorlageTitle = 'abc';
      const vorlageId = 'xyz';
      const freigebenValues: UpdatePermissionsDTO = {
        add: [{ rolleType: RolleLokalType.Gast, email: 'test@test.test' }] as BerechtigungAnBenutzerDTO[],
        modify: [] as BerechtigungAnBenutzerDTO[],
        remove: [] as BerechtigungAnBenutzerDTO[],
      };
      ctrl.vorlageFreigeben(vorlageTitle, vorlageId, freigebenValues, setIsVisibleStub);
      setTimeout(() => {
        sinon.assert.called(setLoadingStatusStub);
        sinon.assert.called(displayErrorMsgStub);
        sinon.assert.notCalled(loadDataStub);
        done();
      }, 20);
    });
  });
});
