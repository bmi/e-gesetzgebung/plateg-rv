// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  BerechtigungControllerApi,
  BerechtigungListResponseDTO,
  BerechtigungResponseDTO,
  RessourceType,
  UpdatePermissionsDTO,
} from '@plateg/rest-api';
import { displayMessage, ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

export class FreigabeModalRVController {
  private readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );

  private readonly berechtigungController = GlobalDI.getOrRegister<BerechtigungControllerApi>(
    'berechtigungControllerApi',
    () => new BerechtigungControllerApi(),
  );

  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  public getFreigaben(rvId: string, setFreigaben: (value: BerechtigungResponseDTO[]) => void) {
    this.berechtigungController
      .getBerechtigungen({
        id: rvId,
        type: RessourceType.Regelungsvorhaben,
      })
      .subscribe({
        next: (data: BerechtigungListResponseDTO) => {
          setFreigaben(data.dtos);
        },
        error: (error: AjaxError) => {
          setFreigaben([]);
          this.errorCallback(error);
        },
      });
  }
  public vorlageFreigeben(
    objectTitle: string | undefined,
    objectId: string,
    freigebenValues: UpdatePermissionsDTO,
    setIsVisible: (visible: boolean) => void,
    reloadFreigeben?: () => void,
  ): void {
    setIsVisible(false);
    this.loadingStatusController.setLoadingStatus(true);

    const freigabeRequest = this.berechtigungController.setBerechtigungen({
      type: RessourceType.Regelungsvorhaben,
      id: objectId,
      updatePermissionsDTO: freigebenValues,
    });

    freigabeRequest.subscribe({
      next: () => {
        this.handleSave(objectTitle || '');
        if (reloadFreigeben) {
          reloadFreigeben();
        }
      },
      error: (error: AjaxError) => this.errorCallback(error),
    });
  }

  private handleSave(title: string): void {
    this.loadingStatusController.setLoadingStatus(false);
    displayMessage(this.getSuccessMsg(title), 'success');
  }

  private errorCallback(error: AjaxError): void {
    this.loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    this.errorCtrl.displayErrorMsg(error, 'rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.freigabe.error');
  }

  private getSuccessMsg(title: string): string {
    return i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.freigabe.success', {
      title: title,
    });
  }
}
