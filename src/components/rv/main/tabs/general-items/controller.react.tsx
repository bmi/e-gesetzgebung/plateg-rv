// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import {
  RegelungsvorhabenBundesratTableDTO,
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenBundestagTableDTO,
  UserEntityDTO,
  UserEntityDTOStatusEnum,
  VorhabenStatusType,
} from '@plateg/rest-api';
import { compareDates, ContactPerson, getDateTimeString, InfoComponent, TextAndContact } from '@plateg/theme';

import { RegelungsvorhabenItem } from '../general-items/regelungsvorhaben/component.react';
import { VorhabenartTableItem } from '../general-items/vorhabenart/component.react';
import { PkpSentStatus } from './pkp-sent-status/component.react';

interface ContactPersonInterface extends UserEntityDTO {
  additionalInfo: string;
  adresse: string;
}

/**
 * Defines column renderers of RV table for BR application.
 * Column "Aktionen" is added elsewhere additionally to the columns defined here.
 */
export function getBRTableItems(): ColumnsType<RegelungsvorhabenBundesratTableDTO> {
  return [
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.regelungsvorhaben').toString(),
      key: 'regelungsvorhaben',
      fixed: 'left',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <RegelungsvorhabenItem record={record} />;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.vorhabentyp').toString(),
      className: 'vorhabentyp',
      key: 'vorhabentyp',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <VorhabenartTableItem vorhabenart={record.vorhabenart} />;
      },
    },
    {
      /*
       * - dateReceivedOfficially:  At Bundesrat this shall be called 'Zuleitungsdatum'.
       *                            At Bundestag this shall be called 'Eingangsdatum'.
       * - dateReceivedTechnically: At Bundesrat this shall be called 'Eingangsdatum'.
       *                            At Bundestag this shall be called 'Zuleitungsdatum'.
       */
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.eingegangen').toString(),
      className: 'eingegangen',
      key: 'eingegangen',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <Text>{getDateTimeString(record.dateReceivedTechnically)}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.zugeleitet').toString(),
      key: 'zuleitung',
      sorter: (a, b) => compareDates(a.dateReceivedOfficially, b.dateReceivedOfficially),
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return (
          <TextAndContact
            drawerId={`zuleitung-drawer-${record.id}`}
            drawerTitle={i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.contactPersonTitle')}
            firstRow={getDateTimeString(
              record.fristBeginn ?? record.dateReceivedOfficially ?? record.dateReceivedTechnically,
            )}
            user={record.zuleitungVon.dto}
          />
        );
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.federfuehrendesRessort').toString(),
      key: 'federfuehrendesRessort',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <Text>{record.ffRessort}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.federfuehrenderAusschussBR').toString(),
      key: 'federfuehrenderAusschussBR',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <Text>{record.ffAusschussBR ?? '-'}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.federfuehrenderAusschussBT').toString(),
      key: 'federfuehrenderAusschussBT',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <Text>{record.ffAusschussBT ?? '-'}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.dblnr').toString(),
      className: 'dblnr',
      key: 'dblnr',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => {
        return <Text>{record.dblnr}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BRTableItems.status').toString(),
      key: 'status',
      render: (record: RegelungsvorhabenBundesratTableDTO): ReactElement => (
        <Text>
          {i18n.t(`rv.myRegelungsvorhaben.BRTableItems.statuses.${record.status as VorhabenStatusType}`).toString()}
        </Text>
      ),
    },
  ];
}

export function getBTTableItems(): ColumnsType<RegelungsvorhabenBundestagTableDTO> {
  return [
    {
      title: i18n.t('rv.myRegelungsvorhaben.BTTableItems.regelungsvorhaben').toString(),
      key: 'regelungsvorhaben',
      fixed: 'left',
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        return <RegelungsvorhabenItem record={record} />;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BTTableItems.vorhabentyp').toString(),
      className: 'vorhabentyp',
      key: 'vorhabentyp',
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        return <VorhabenartTableItem vorhabenart={record.vorhabenart} />;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BTTableItems.zugeleitet').toString(),
      key: 'zuleitungAm',
      sorter: (a, b) => compareDates(a.zuleitungAm, b.zuleitungAm),
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        return <Text>{getDateTimeString(record.zuleitungAm)}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BTTableItems.federfuehrendesReferat').toString(),
      key: 'federfuehrendesReferat',
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        if (record.technischesFfReferatBezeichnung) {
          const technischesFfReferatBezeichnungUser: ContactPersonInterface = {
            name: record.technischesFfReferatBezeichnung,
            email: '',
            adresse: '',
            additionalInfo: '',
            status: '' as UserEntityDTOStatusEnum,
          };
          return (
            <>
              <InfoComponent
                isContactPerson={true}
                title={i18n.t('rv.newRegelungsvorhaben.btns.continueLater.hint.title')}
                buttonText={record.technischesFfReferatBezeichnung}
                id={record.id}
              >
                <ContactPerson user={technischesFfReferatBezeichnungUser} />
              </InfoComponent>
            </>
          );
        } else {
          return <></>;
        }
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BTTableItems.dblnr').toString(),
      className: 'dblnr',
      key: 'dblnr',
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        return <Text>{record.dblnr}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.BTTableItems.status').toString(),
      key: 'status',
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        return <Text>{i18n.t(`rv.myRegelungsvorhaben.BTTableItems.statuses.${record.status}`).toString()}</Text>;
      },
    },
    {
      title: (
        <>
          <span className="info-column">
            {i18n.t('rv.myRegelungsvorhaben.BTTableItems.meinezugriffsrechte').toString()}
            <InfoComponent title={i18n.t('rv.myRegelungsvorhaben.BTTableItems.zugriffsrechtInfo.title').toString()}>
              <div
                dangerouslySetInnerHTML={{
                  __html: i18n.t('rv.myRegelungsvorhaben.BTTableItems.zugriffsrechtInfo.drawerText'),
                }}
              ></div>
            </InfoComponent>
          </span>
        </>
      ),
      key: 'status',
      render: (record: RegelungsvorhabenBundestagTableDTO): ReactElement => {
        return <Text>{i18n.t(`rv.myRegelungsvorhaben.zugriffsrechte.${record.rolleTyp}`).toString()}</Text>;
      },
    },
  ];
}

export function getBRegTableItems(isEntwurf?: boolean): ColumnsType<RegelungsvorhabenBundesregierungTableDTO> {
  return [
    {
      title: i18n.t('rv.myRegelungsvorhaben.generalTableItems.regelungsvorhaben').toString(),
      key: 'regelungsvorhaben',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): ReactElement => {
        return <RegelungsvorhabenItem record={record} />;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.generalTableItems.vorhabentyp').toString(),
      className: 'vorhabentyp',
      key: 'vorhabentyp',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): ReactElement => {
        return <VorhabenartTableItem vorhabenart={record.vorhabenart} />;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.generalTableItems.erstelltam').toString(),
      key: 'erstellt',
      sorter: (a, b) => compareDates(a.erstelltAm, b.erstelltAm),
      render: (record: RegelungsvorhabenBundesregierungTableDTO): ReactElement => {
        return (
          <TextAndContact
            drawerId={`erstellt-drawer-${record.id}`}
            drawerTitle={i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.contactPersonTitle')}
            firstRow={getDateTimeString(record.erstelltAm)}
            user={record.erstelltVon.dto}
          />
        );
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.generalTableItems.bearbeitetam').toString(),
      key: 'bearbeitetam',
      sorter: (a, b) => compareDates(a.bearbeitetAm, b.bearbeitetAm),
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        if (record.bearbeitetAm) {
          const user = record.bearbeitetVonStellvertreter || record.bearbeitetVon || record.erstelltVon;
          return (
            <>
              <TextAndContact
                drawerId={`bearbeitetam-drawer-${record.id}`}
                drawerTitle={i18n.t('rv.myRegelungsvorhaben.tabs.entwuerfe.contactPersonTitle')}
                firstRow={getDateTimeString(record.bearbeitetAm)}
                user={user.dto}
                isStellvertretung={!!record.bearbeitetVonStellvertreter}
              />
            </>
          );
        } else {
          return <></>;
        }
      },
    },
    {
      title: (
        <>
          {i18n.t('rv.myRegelungsvorhaben.generalTableItems.zugriffsrecht').toString()}
          <InfoComponent title={i18n.t('rv.myRegelungsvorhaben.generalTableItems.zugriffsrechtInfo.title')}>
            <div
              dangerouslySetInnerHTML={{
                __html: i18n.t('rv.myRegelungsvorhaben.generalTableItems.zugriffsrechtInfo.drawerText'),
              }}
            ></div>
          </InfoComponent>
        </>
      ),
      key: 'zugriffsrecht',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): ReactElement => {
        return <Text>{i18n.t(`rv.myRegelungsvorhaben.zugriffsrechte.${record.rolleTyp}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.tableColLabel').toString(),
      key: 'an-pkp-gesendet',
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        return (
          <PkpSentStatus
            pkpStatus={record.pkpStatus}
            pkpGesendetAm={record.pkpGesendetAm}
            pkpWorkflowStatus={record.pkpWorkflowStatus}
          />
        );
      },
    },
  ];
}

export function getArchiveTableItems(isEntwurf?: boolean): ColumnsType<RegelungsvorhabenBundesregierungTableDTO> {
  return [
    {
      title: i18n.t('rv.myRegelungsvorhaben.generalTableItems.regelungsvorhaben').toString(),
      key: 'regelungsvorhaben',
      fixed: 'left',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): ReactElement => {
        return <RegelungsvorhabenItem record={record} />;
      },
    },
    {
      title: i18n.t('rv.myRegelungsvorhaben.generalTableItems.vorhabentyp').toString(),
      key: 'vorhabentyp',
      render: (record: RegelungsvorhabenBundesregierungTableDTO): ReactElement => {
        return <VorhabenartTableItem vorhabenart={record.vorhabenart} />;
      },
    },
  ];
}
