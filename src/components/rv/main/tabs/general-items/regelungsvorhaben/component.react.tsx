// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import {
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenBundestagTableDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';
import { TwoLineText } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';

interface RegelungsvorhabenItemrops {
  record: RegelungsvorhabenBundesregierungTableDTO | RegelungsvorhabenBundestagTableDTO;
}

export function RegelungsvorhabenItem(props: RegelungsvorhabenItemrops): React.ReactElement {
  const route = (): string => {
    if (props.record.status === VorhabenStatusType.Entwurf) {
      return `${routes.ENTWURFE}/${props.record.id}/${routes.DATENBLATT}`;
    } else if (props.record.status === VorhabenStatusType.Archiviert && props.record.archiviertAm) {
      return `${routes.ARCHIV}/${props.record.id}/${routes.DATENBLATT}`;
    } else {
      return `${routes.MEINE_REGELUNGSVORHABEN}/${props.record.id}/${routes.NAECHSTESCHRITTE}`;
    }
  };

  return (
    <TwoLineText
      firstRowLink={`#/regelungsvorhaben/${route()}`}
      firstRow={props.record.abkuerzung || ''}
      firstRowBold={false}
      secondRow={props.record.kurzbezeichnung || ''}
      secondRowBold={false}
      secondRowLight={false}
      elementId={props.record.id}
    ></TwoLineText>
  );
}
