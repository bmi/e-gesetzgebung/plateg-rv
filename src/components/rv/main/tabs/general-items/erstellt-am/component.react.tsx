// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import React from 'react';

import { Filters } from '@plateg/theme/src/shares/filters';

interface ErstelltAmTableItemProps {
  erstelltAm: string;
}
export function ErstelltAmTableItem(props: ErstelltAmTableItemProps): React.ReactElement {
  return <Text>{Filters.date(new Date(props.erstelltAm))}</Text>;
}
