// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import React from 'react';

interface VorhabenartProps {
  vorhabenart?: string;
}
export function VorhabenartTableItem(props: VorhabenartProps): React.ReactElement {
  return <Text>{props.vorhabenart ? props.vorhabenart.charAt(0) + props.vorhabenart.slice(1).toLowerCase() : ''}</Text>;
}
