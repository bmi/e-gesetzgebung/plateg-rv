// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import i18next from 'i18next';
import React from 'react';

import { VorhabenStatusType } from '@plateg/rest-api';

interface StatusTableItemProps {
  status?: VorhabenStatusType;
}
export function StatusTableItem(props: StatusTableItemProps): React.ReactElement {
  return props.status ? (
    <Text>{i18next.t(`rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.statusValues.${props.status}`)}</Text>
  ) : (
    <></>
  );
}
