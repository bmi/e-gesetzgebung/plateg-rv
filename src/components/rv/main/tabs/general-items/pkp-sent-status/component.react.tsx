// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { PkpGesendetStatusType, PkpWorkflowStatusType } from '@plateg/rest-api';
import { getDateTimeString } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { isRelevantKurzbezeichnung } from '../../../regelungsvorhaben/controller';

interface Props {
  pkpStatus: PkpGesendetStatusType;
  pkpGesendetAm?: string;
  pkpWorkflowStatus?: PkpWorkflowStatusType;
}

export function PkpSentStatus({ pkpStatus, pkpGesendetAm, pkpWorkflowStatus }: Props): React.ReactElement {
  const { t } = useTranslation();
  const appStore = useAppSelector((state) => state.user);
  const isBRorBTuser = isRelevantKurzbezeichnung(appStore.user?.dto.ressort?.kurzbezeichnung);
  const getSentStatus = () => {
    switch (pkpStatus) {
      case PkpGesendetStatusType.Offen:
        return t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.status.notSent');
      case PkpGesendetStatusType.Fehler:
        return t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.status.failed');
      case PkpGesendetStatusType.Aktualisiert:
      case PkpGesendetStatusType.Erstellt: {
        let pkpWorkFlowText = t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.status.success.IV');
        if (pkpWorkflowStatus) {
          pkpWorkFlowText = t(
            `rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.status.success.${pkpWorkflowStatus}`,
          );
        }
        return (
          pkpGesendetAm && (
            <>
              {`${getDateTimeString(pkpGesendetAm)} Uhr`}
              <span className="status-send-to-pkp-span">{pkpWorkFlowText}</span>
            </>
          )
        );
      }
      default:
        return;
    }
  };

  const getSentStatusBundestag = () => {
    return (
      pkpGesendetAm && (
        <>
          {`${getDateTimeString(pkpGesendetAm)} Uhr`}
          <span className="status-send-to-pkp-span">
            {t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.status.successBT')}
          </span>
        </>
      )
    );
  };

  return <>{isBRorBTuser ? getSentStatusBundestag() : getSentStatus()}</>;
}
