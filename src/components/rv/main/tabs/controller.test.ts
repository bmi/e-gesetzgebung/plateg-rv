// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { assert, expect } from 'chai';
import i18n from 'i18next';
import { Observable, of } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
import sinon from 'sinon';

import {
  Configuration,
  PaginierungDTOSortByEnum,
  PaginierungDTOSortDirectionEnum,
  PkpGesendetStatusType,
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenTypType,
  VorhabenStatusType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../general.test';
import {
  compareStatus,
  deleteRegelungsvorhaben,
  deleteRegelungsvorhabenCall,
  getArchivBundestagCall,
  getArchivCall,
  getEntwuerfeCall,
  getFilterButton,
  getFilteredRows,
  getMyRegelungsvorhabenBundesregierungCall,
  getMyRegelungsvorhabenBundestagCall,
  getMyRegelungsvorhabenBundesratCall,
  sendRvToPkp,
  setArchived,
  setArchivedCall,
} from './controller.react';

const regelungsvorhabenController = GlobalDI.getOrRegister(
  'regelungsvorhabenController',
  () => new RegelungsvorhabenControllerApi(new Configuration()),
);

const archiveRegelungsvorhabenStub = sinon.stub(regelungsvorhabenController, 'archiveRegelungsvorhaben');
const deleteRegelungsvorhabenStub = sinon.stub(regelungsvorhabenController, 'deleteRegelungsvorhaben');
const sendRvToPkpStub = sinon.stub(regelungsvorhabenController, 'sendRvToPkp');
const i18nStub = sinon.stub(i18n, 't');
const date = '2021-06-08 08:03:09';
const content: RegelungsvorhabenBundesregierungTableDTO[] = [
  {
    id: '1',
    bearbeitetAm: date,
    allowedToModify: true,
    vorhabenart: RegelungsvorhabenTypType.Gesetz,
  },
  {
    id: '2',
    bearbeitetAm: date,
    allowedToModify: true,
    vorhabenart: RegelungsvorhabenTypType.Rechtsverordnung,
  },
  {
    id: '3',
    bearbeitetAm: date,
    allowedToModify: true,
    vorhabenart: RegelungsvorhabenTypType.Rechtsverordnung,
  },
  {
    id: '4',
    bearbeitetAm: date,
    allowedToModify: true,
    vorhabenart: RegelungsvorhabenTypType.Verwaltungsvorschrift,
  },
];

const requestPaginationObj = {
  pageNumber: 0,
  pageSize: 20,
  filters: {},
  sortBy: PaginierungDTOSortByEnum.ZuletztBearbeitet,
  sortDirection: PaginierungDTOSortDirectionEnum.Desc,
};

describe('TEST: getMyRegelungsvorhabenBundesregierungCall', () => {
  const getMeineRegelungsvorhabenStub = sinon.stub(regelungsvorhabenController, 'getMeineRegelungsvorhaben');
  it('check if REST API is called', () => {
    getMyRegelungsvorhabenBundesregierungCall(requestPaginationObj);
    sinon.assert.calledOnce(getMeineRegelungsvorhabenStub);
    getMeineRegelungsvorhabenStub.restore();
  });
});

describe('TEST: getMyRegelungsvorhabenBundestagCall', () => {
  const getMeineRegelungsvorhabenBundestagStub = sinon.stub(
    regelungsvorhabenController,
    'getMeineRegelungsvorhabenBundestag',
  );
  it('check if REST API is called', () => {
    getMyRegelungsvorhabenBundestagCall(requestPaginationObj);
    sinon.assert.calledOnce(getMeineRegelungsvorhabenBundestagStub);
    getMeineRegelungsvorhabenBundestagStub.restore();
  });
});

describe('TEST: getMyRegelungsvorhabenBundesratCall', () => {
  const getMeineRegelungsvorhabenBundesratGETStub = sinon.stub(
    regelungsvorhabenController,
    'getMeineRegelungsvorhabenBundesratGET',
  );

  it('check if GET method is called', () => {
    getMyRegelungsvorhabenBundesratCall(requestPaginationObj);
    // Called twiced because of pre-flight HEAD request (possibly).
    sinon.assert.calledOnce(getMeineRegelungsvorhabenBundesratGETStub);
    getMeineRegelungsvorhabenBundesratGETStub.restore();
  });
});

describe('TEST: getEntwuerfeCall', () => {
  const getEntwuerfeStub = sinon.stub(regelungsvorhabenController, 'getEntwuerfe');
  it('check if REST API is called', () => {
    getEntwuerfeCall(requestPaginationObj);
    sinon.assert.calledOnce(getEntwuerfeStub);
    getEntwuerfeStub.restore();
  });
});

describe('TEST: getArchivCall', () => {
  const getArchivStub = sinon.stub(regelungsvorhabenController, 'getArchiv');
  it('check if REST API is called', () => {
    getArchivCall(requestPaginationObj);
    sinon.assert.calledOnce(getArchivStub);
    getArchivStub.restore();
  });
});

describe('TEST: getArchivBundestagCall', () => {
  const getArchivStub = sinon.stub(regelungsvorhabenController, 'getArchivBundestag');
  it('check if REST API is called', () => {
    getArchivBundestagCall(requestPaginationObj);
    sinon.assert.calledOnce(getArchivStub);
    getArchivStub.restore();
  });
});

describe('TEST: deleteRegelungsvorhabenCall', () => {
  it('check if REST API is called', () => {
    deleteRegelungsvorhabenCall('0');
    sinon.assert.calledOnce(deleteRegelungsvorhabenStub);
    deleteRegelungsvorhabenStub.reset();
  });
});

describe('TEST: setArchivedCall', () => {
  it('check if REST API is called', () => {
    setArchivedCall('0');
    sinon.assert.calledOnce(archiveRegelungsvorhabenStub);
    archiveRegelungsvorhabenStub.reset();
  });
});

describe('TEST: compareStatus', () => {
  it('a not InBearbeitung, b InBearbeitung -> should return 1', () => {
    expect(compareStatus(VorhabenStatusType.Entwurf, VorhabenStatusType.InBearbeitung)).to.eql(1);
  });

  it('a InBearbeitung, b not InBearbeitung -> should return -1', () => {
    expect(compareStatus(VorhabenStatusType.InBearbeitung, VorhabenStatusType.Entwurf)).to.eql(-1);
  });

  it('a InBearbeitung, b InBearbeitung -> should return 0', () => {
    expect(compareStatus(VorhabenStatusType.InBearbeitung, VorhabenStatusType.InBearbeitung)).to.eql(0);
  });

  it('a undefined, b -> should return 0', () => {
    expect(compareStatus()).to.eql(0);
  });

  it('a undefined, b InBearbeitung -> should return 1', () => {
    expect(compareStatus(undefined, VorhabenStatusType.InBearbeitung)).to.eql(1);
  });

  it('a InBearbeitung, b undefined -> should return -1', () => {
    expect(compareStatus(VorhabenStatusType.InBearbeitung, undefined)).to.eql(-1);
  });
});

describe('TEST: setArchived', () => {
  const id = 'test-id';
  const newStatus = VorhabenStatusType.Entwurf;
  const content: RegelungsvorhabenBundesregierungTableDTO[] = [];
  const setContent = (content: Array<RegelungsvorhabenBundesregierungTableDTO>) => {};
  const setContentSpy = sinon.spy(setContent);

  afterEach(() => {
    setLoadingStatusStub.resetHistory();
    displayErrorMsgStub.resetHistory();
    setContentSpy.resetHistory();
  });

  after(() => {
    setLoadingStatusStub.reset();
    archiveRegelungsvorhabenStub.reset();
  });

  it('successful call, correct functions should be called', () => {
    archiveRegelungsvorhabenStub.returns(of(undefined));
    setArchived(id, content, setContentSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    expect(setContentSpy.called).to.be.true;
  });

  it('unsuccessful call, correct functions should be called', () => {
    archiveRegelungsvorhabenStub.callsFake(() => {
      return new Observable<void>((observer) => observer.error());
    });
    setArchived(id, content, setContentSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(displayErrorMsgStub);
    expect(setContentSpy.called).to.be.false;
  });
});
describe('TEST: send to pkp', () => {
  beforeEach(() => {
    sendRvToPkpStub.resetHistory();
    displayErrorMsgStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    i18nStub.resetHistory();
  });

  after(() => {
    setLoadingStatusStub.reset();
    sendRvToPkpStub.reset();
    i18nStub.reset();
  });

  it('error call, send to pkp', (done) => {
    sendRvToPkpStub.callsFake(() => {
      return new Observable<AjaxResponse<PkpGesendetStatusType>>((observer) => {
        setTimeout(function () {
          observer.error();
        }, 10);
      });
    });

    sendRvToPkp('ttt', 'name');
    setTimeout(function () {
      const x = displayErrorMsgStub?.getCall?.(0)?.args?.[0];
      sinon.assert.calledTwice(setLoadingStatusStub);
      expect(i18nStub?.getCall?.(0)?.args[0]).to.equal(
        'rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.popupMessage.error',
      );
      done();
    }, 10);
  });
  it('error 1067 call, send to pkp', (done) => {
    sendRvToPkpStub.callsFake(() => {
      return new Observable<AjaxResponse<PkpGesendetStatusType>>((observer) => {
        setTimeout(function () {
          observer.error({ response: { status: 1067 } });
        }, 10);
      });
    });

    sendRvToPkp('ttt', 'name');
    setTimeout(function () {
      const x = displayErrorMsgStub?.getCall?.(0)?.args?.[0];
      sinon.assert.calledTwice(setLoadingStatusStub);
      expect(i18nStub?.getCall?.(0)?.args[0]).to.equal(
        'rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.popupMessage.error1067',
      );
      done();
    }, 10);
  });
});

describe('TEST: deleteRegelungsvorhaben', () => {
  const id = 'test-id';
  const content: RegelungsvorhabenBundesregierungTableDTO[] = [];
  const setRegelungsvorhabenData = (entwuerfeData: RegelungsvorhabenBundesregierungTableDTO[]) => {};
  const setRegelungsvorhabenDataSpy = sinon.spy(setRegelungsvorhabenData);

  beforeEach(() => {
    setLoadingStatusStub.resetHistory();
    displayErrorMsgStub.resetHistory();
    setRegelungsvorhabenDataSpy.resetHistory();
    deleteRegelungsvorhabenStub.resetHistory();
  });

  after(() => {
    setLoadingStatusStub.reset();
    deleteRegelungsvorhabenStub.resetHistory();
  });

  it('successful call, correct functions should be called', (done) => {
    deleteRegelungsvorhabenStub.callsFake(() => {
      return new Observable<void>((observer) => {
        setTimeout(function () {
          observer.next();
        }, 10);
      });
    });
    deleteRegelungsvorhaben(id, content, setRegelungsvorhabenDataSpy);
    setTimeout(function () {
      sinon.assert.calledTwice(setLoadingStatusStub);
      expect(setRegelungsvorhabenDataSpy.called).to.be.true;
      done();
    }, 10);
  });

  it('unsuccessful call, correct functions should be called', (done) => {
    deleteRegelungsvorhabenStub.callsFake(() => {
      return new Observable<void>((observer) => {
        setTimeout(function () {
          observer.error();
        }, 10);
      });
    });
    deleteRegelungsvorhaben(id, content, setRegelungsvorhabenDataSpy);

    setTimeout(function () {
      sinon.assert.calledTwice(setLoadingStatusStub);
      sinon.assert.calledOnce(displayErrorMsgStub);
      expect(setRegelungsvorhabenDataSpy.called).to.be.false;
      done();
    }, 10);
  });
});

describe('TEST: getFilteredRows - filter rows by value for specified column', () => {
  it('filter rows by value for specified column - check for correct length', () => {
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Gesetz, {
        name: 'vorhabentyp',
        columnIndex: 2,
      }).length,
    ).to.equal(1);
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Rechtsverordnung, {
        name: 'vorhabentyp',
        columnIndex: 2,
      }).length,
    ).to.equal(2);
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Verwaltungsvorschrift, {
        name: 'vorhabentyp',
        columnIndex: 2,
      }).length,
    ).to.equal(1);
  });
  it('filter rows by value for specified column - check correct content is returned', () => {
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Gesetz, {
        name: 'vorhabentyp',
        columnIndex: 2,
      })[0],
    ).to.equal(content[0]);
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Rechtsverordnung, {
        name: 'vorhabentyp',
        columnIndex: 2,
      })[0],
    ).to.equal(content[1]);
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Verwaltungsvorschrift, {
        name: 'vorhabentyp',
        columnIndex: 2,
      })[0],
    ).to.equal(content[3]);
  });

  it('filter rows by value for unknown column - check if empty array is returned', () => {
    expect(
      getFilteredRows(content, RegelungsvorhabenTypType.Verwaltungsvorschrift, {
        name: 'abc',
        columnIndex: 2,
      }).length,
    ).to.equal(0);
  });
});

describe('TEST: getFilterButton - get correct filter button properties', () => {
  it('get filter button props for vorhabentyp', () => {
    const filterButton = getFilterButton(content, {
      name: 'vorhabentyp',
      columnIndex: 2,
    });
    expect(filterButton.displayName).to.equal(
      i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.filter.displayname.vorhabentyp'),
    );
    expect(filterButton.labelContent).to.equal(
      i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.filter.labelContent'),
    );
    expect(Array.from(filterButton.options)[0]).to.equal('Gesetz');
    expect(Array.from(filterButton.options)[1]).to.equal('Rechtsverordnung');
    expect(Array.from(filterButton.options)[2]).to.equal('Verwaltungsvorschrift');
  });
});
