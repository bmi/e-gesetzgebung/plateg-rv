// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { RegelungsvorhabenControllerApi } from '@plateg/rest-api';
import {
  PaginierungDTO,
  PkpGesendetStatusType,
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenBundesregierungTableDTOs,
  RegelungsvorhabenBundestagTableDTO,
  VorhabenStatusType,
  RegelungsvorhabenBundestagTableDTOs,
  RegelungsvorhabenBundesratTableDTO,
  RegelungsvorhabenBundesratTableDTOs,
} from '@plateg/rest-api/models';
import { displayMessage, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { RvErrorController } from '../../../../utils/rv-error-controller';
import { PkpSendStatusState } from './my-regelungsvorhaben-tab/pkp-confirm-modal-wrapper-rv-list/component.react';

export function getFilterButton<
  T extends
    | RegelungsvorhabenBundesregierungTableDTO
    | RegelungsvorhabenBundestagTableDTO
    | RegelungsvorhabenBundesratTableDTO,
>(
  initialContent: T[],
  column: { name: string; columnIndex: number },
): { displayName: string; labelContent: string; options: Set<string> } {
  const displayName = i18n.t(`rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.filter.displayname.${column.name}`);
  let options: Set<string> = new Set();
  const labelContent = i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.filter.labelContent');

  if (column.name === 'vorhabentyp') {
    options = new Set(
      initialContent.map((row) => {
        return row.vorhabenart ? `${row.vorhabenart.charAt(0)}${row.vorhabenart.slice(1).toLowerCase()}` : '';
      }),
    );
  }
  return { displayName, labelContent, options };
}

export function getFilteredRows<
  T extends
    | RegelungsvorhabenBundesregierungTableDTO
    | RegelungsvorhabenBundestagTableDTO
    | RegelungsvorhabenBundesratTableDTO,
>(actualContent: T[], filteredBy: string, column: { name: string; columnIndex: number }): T[] {
  let newRows: Set<T> = new Set();
  if (column.name === 'vorhabentyp') {
    newRows = new Set(
      actualContent.filter((row) => {
        const vorhabenart = row.vorhabenart ?? '';
        return vorhabenart.toString() === filteredBy.toUpperCase();
      }),
    );
  }
  return Array.from(newRows);
}

const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
const loadingStatusController = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());

export function deleteRegelungsvorhaben(
  id: string,
  content: RegelungsvorhabenBundesregierungTableDTO[],
  setRegelungsvorhabenData: (entwuerfeData: RegelungsvorhabenBundesregierungTableDTO[]) => void,
  loadContent?: () => void,
): void {
  loadingStatusController.setLoadingStatus(true);
  const deleteRvSubscription = deleteRegelungsvorhabenCall(id).subscribe({
    next: () => {
      setRegelungsvorhabenData(
        content.filter((entry) => {
          return entry.id !== id;
        }),
      );
      displayMessage(i18n.t('rv.myRegelungsvorhaben.deleteMsg.success'), 'success');
      loadingStatusController.setLoadingStatus(false);
      loadContent?.();
      deleteRvSubscription.unsubscribe();
    },
    error: (error: AjaxError) => {
      loadingStatusController.setLoadingStatus(false);
      console.error(error, error);
      rvErrorCtrl.handleError(error, 'rv.myRegelungsvorhaben.deleteMsg.error');
      deleteRvSubscription.unsubscribe();
    },
  });
}

export function setArchived<
  T extends
    | RegelungsvorhabenBundesregierungTableDTO
    | RegelungsvorhabenBundestagTableDTO
    | RegelungsvorhabenBundesratTableDTO,
>(id: string, content: T[], setContent: (content: T[]) => void, loadContent?: () => void): void {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorController', () => new RvErrorController());
  loadingStatusController.setLoadingStatus(true);
  setArchivedCall(id).subscribe({
    next: () => {
      const filteredCont = content.filter((entry: T) => {
        return entry.id !== id;
      });
      const deletedRv = content.find((entry: T) => {
        return entry.id === id;
      });
      setContent(filteredCont);
      displayMessage(
        i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archivedSuccess', {
          title: deletedRv?.kurzbezeichnung,
        }),
        'success',
      );
      loadingStatusController.setLoadingStatus(false);
      loadContent?.();
    },
    error: (error: AjaxError) => {
      console.error('Error sub', error);
      loadingStatusController.setLoadingStatus(false);
      rvErrorCtrl.handleError(error);
    },
  });
}

export function compareStatus(a?: string, b?: string): number {
  const statusA = a || '';
  const statusB = b || '';
  if (statusA !== VorhabenStatusType.InBearbeitung.toString() && statusB !== statusA) {
    return 1;
  }
  if (statusB !== VorhabenStatusType.InBearbeitung.toString() && statusB !== statusA) {
    return -1;
  }
  return 0;
}

export function getMyRegelungsvorhabenBundesregierungCall(
  paginierungDTO: PaginierungDTO,
): Observable<RegelungsvorhabenBundesregierungTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');

  return regelungsvorhabenController.getMeineRegelungsvorhaben({ paginierungDTO });
}

export function getEntwuerfeCall(
  paginierungDTO: PaginierungDTO,
): Observable<RegelungsvorhabenBundesregierungTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.getEntwuerfe({ paginierungDTO });
}

export function getArchivCall(paginierungDTO: PaginierungDTO): Observable<RegelungsvorhabenBundesregierungTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.getArchiv({ paginierungDTO });
}

export function deleteRegelungsvorhabenCall(id: string): Observable<void> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.deleteRegelungsvorhaben({ id });
}

export function setArchivedCall(id: string): Observable<void> {
  const abstimmungController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return abstimmungController.archiveRegelungsvorhaben({ id });
}

// BT
export function getMyRegelungsvorhabenBundestagCall(
  paginierungDTO: PaginierungDTO,
): Observable<RegelungsvorhabenBundestagTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.getMeineRegelungsvorhabenBundestag({ paginierungDTO });
}

export function getArchivBundestagCall(
  paginierungDTO: PaginierungDTO,
): Observable<RegelungsvorhabenBundestagTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.getArchivBundestag({ paginierungDTO });
}

// BR
export function getMyRegelungsvorhabenBundesratCall(
  paginierungDTO: PaginierungDTO,
): Observable<RegelungsvorhabenBundesratTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.getMeineRegelungsvorhabenBundesratGET({
    page: paginierungDTO.pageNumber,
    size: paginierungDTO.pageSize,
    sort: paginierungDTO.sortBy ? [`${paginierungDTO.sortBy},${paginierungDTO.sortDirection ?? 'ASC'}`] : [],
    eRSTELLER: paginierungDTO.filters?.ERSTELLER,
    iNITIANTUSER: paginierungDTO.filters?.INITIANT_USER,
    iNITIANTINSTITUTION: paginierungDTO.filters?.INITIANT_INSTITUTION,
    vORHABENART: paginierungDTO.filters?.VORHABENART,
  });
}

export function getArchivBundesratCall(
  paginierungDTO: PaginierungDTO,
): Observable<RegelungsvorhabenBundesratTableDTOs> {
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  return regelungsvorhabenController.getArchivBundesrat({ paginierungDTO });
}

export function sendRvToPkp(
  id: string,
  rvNameAbkz: string,
  setPkpSendStatus?: (val: PkpSendStatusState) => void,
  onClickModal?: () => void,
) {
  loadingStatusController.setLoadingStatus(true);

  const updatePkpSendStatus = (value: PkpGesendetStatusType) => {
    if (setPkpSendStatus) {
      setPkpSendStatus({
        pkpSendStatus: value,
        gesendetAm: new Date().toISOString(),
        bearbeitetAm: new Date().toISOString(),
      });
    }
  };

  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  const restponseSendToPkpSub = regelungsvorhabenController.sendRvToPkp({ id }).subscribe({
    next: (value) => {
      updatePkpSendStatus(value);
    },
    complete: () => {
      onClickModal && onClickModal();
      loadingStatusController.setLoadingStatus(false);
      displayMessage(
        i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.popupMessage.success', {
          rvNameAbkz,
        }),
        'success',
      );
      restponseSendToPkpSub.unsubscribe();
    },
    error: (error: AjaxError) => {
      updatePkpSendStatus(PkpGesendetStatusType.Fehler);
      loadingStatusController.setLoadingStatus(false);
      const status = (error?.response as { status: number })?.status;
      displayMessage(
        status === 1067
          ? i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.popupMessage.error1067', {
              rvNameAbkz,
            })
          : i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.popupMessage.error', {
              rvNameAbkz,
            }),
        'error',
      );
      restponseSendToPkpSub.unsubscribe();
    },
  });
}
