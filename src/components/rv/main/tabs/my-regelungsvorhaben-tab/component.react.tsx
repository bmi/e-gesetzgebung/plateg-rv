// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api/models/';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { getFilterButton, getFilteredRows, setArchived } from '../controller.react';
import { FreigabeModalRV } from '../freigabe-modal/component.react';
import { getMyRegelungsvorhabenTableVals } from './controller.react';
import { PkpComfirmModalWrapperRvList } from './pkp-confirm-modal-wrapper-rv-list/component.react';

interface MyRegelungsvorhabenTabProps {
  myRegelungsvorhabenData: RegelungsvorhabenBundesregierungTableDTO[];
  setMyRegelungsvorhabenData: (content: RegelungsvorhabenBundesregierungTableDTO[]) => void;
  resetTab: (tab: string) => void;
  currentUserIsStellvertreter: boolean;
  tabKey: string;
  loadContent: () => void;
}
export type TableCompPropsRvTableWithoutId = Omit<TableComponentProps<RegelungsvorhabenBundesregierungTableDTO>, 'id'>;

export function MyRegelungsvorhabenTab(props: MyRegelungsvorhabenTabProps): React.ReactElement {
  const { t } = useTranslation();
  const { Paragraph } = Typography;

  const [confirmArchiveModalIsVisible, setConfirmArchiveModalIsVisible] = useState(false);
  const [confirmSentRvToPkpModalVisible, setConfirmSentRvToPkpModalVisible] = useState(false);
  const [selectedRegelungsvorhaben, setSelectedRegelungsvorhaben] = useState<RegelungsvorhabenBundesregierungTableDTO>(
    {} as RegelungsvorhabenBundesregierungTableDTO,
  );
  const [myRegelungsvorhabenTableVals, setMyRegelungsvorhabenTableVals] = useState<TableCompPropsRvTableWithoutId>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const [isFreigebenModalVisible, setIsFreigebenModalVisible] = useState<boolean>(false);

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setMyRegelungsvorhabenTableVals(
      getMyRegelungsvorhabenTableVals(
        props.myRegelungsvorhabenData,
        setConfirmArchiveModalIsVisible,
        setSelectedRegelungsvorhaben,
        setIsFreigebenModalVisible,
        setConfirmSentRvToPkpModalVisible,
        props.currentUserIsStellvertreter,
      ),
    );
  }, [props.myRegelungsvorhabenData]);
  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <>
          <PkpComfirmModalWrapperRvList
            rvsData={props.myRegelungsvorhabenData}
            setRegelungsvorhabenData={props.setMyRegelungsvorhabenData}
            rv={selectedRegelungsvorhaben}
            setVisible={setConfirmSentRvToPkpModalVisible}
            visible={confirmSentRvToPkpModalVisible}
          />
          <ArchivConfirmComponent
            okText={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archiveConfirm')}
            cancelText={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archiveCancel')}
            actionTitle={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.archiveActionTitle')}
            visible={confirmArchiveModalIsVisible}
            setVisible={setConfirmArchiveModalIsVisible}
            setArchived={() => {
              setArchived(
                selectedRegelungsvorhaben.id,
                props.myRegelungsvorhabenData,
                props.setMyRegelungsvorhabenData,
                props.loadContent,
              );
              props.resetTab('archiv');
              setConfirmArchiveModalIsVisible(false);
            }}
          />
          <FreigabeModalRV
            disabled={!selectedRegelungsvorhaben.aktionen?.includes(AktionType.BerechtigungenAendern)}
            isVisible={isFreigebenModalVisible}
            setIsVisible={setIsFreigebenModalVisible}
            objectTitle={selectedRegelungsvorhaben.kurzbezeichnung}
            objectId={selectedRegelungsvorhaben.id}
            selectedRvId={selectedRegelungsvorhaben.id}
            allowChanges={
              selectedRegelungsvorhaben.aktionen?.includes(AktionType.BerechtigungenAendern) ||
              selectedRegelungsvorhaben.aktionen?.includes(AktionType.EigeneBerechtigungenLoeschen)
            }
            revokeObserverRight={selectedRegelungsvorhaben.aktionen?.includes(AktionType.EigeneBerechtigungenLoeschen)}
            loadContent={props.loadContent}
          />
          <TableComponent
            tabKey={props.tabKey}
            bePagination
            id="rv-own-rvs-table"
            expandable={myRegelungsvorhabenTableVals.expandable}
            columns={myRegelungsvorhabenTableVals.columns}
            content={myRegelungsvorhabenTableVals.content}
            filteredColumns={myRegelungsvorhabenTableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            sorterOptions={myRegelungsvorhabenTableVals.sorterOptions}
            customDefaultSortIndex={2}
            userIsStellvertreter={props.currentUserIsStellvertreter}
          ></TableComponent>
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.imgText1'),
            imgSrc: require(
              `../../../../../media/empty-content-images/myRegelungsvorhaben/img1-myRegelungsvorhaben.svg`,
            ) as ImageModule,
            height: 104,
          },
          {
            label: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.imgText2'),
            imgSrc: require(
              `../../../../../media/empty-content-images/myRegelungsvorhaben/img2-myRegelungsvorhaben.svg`,
            ) as ImageModule,
            height: 104,
          },
          {
            label: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.imgText3'),
            imgSrc: require(
              `../../../../../media/empty-content-images/myRegelungsvorhaben/img3-myRegelungsvorhaben.svg`,
            ) as ImageModule,
            height: 104,
          },
        ]}
      >
        <Paragraph className="info-text">
          <p
            className="info-text"
            dangerouslySetInnerHTML={{
              __html: t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.text'),
            }}
          />
        </Paragraph>
      </EmptyContentComponent>
    </>
  );
}
