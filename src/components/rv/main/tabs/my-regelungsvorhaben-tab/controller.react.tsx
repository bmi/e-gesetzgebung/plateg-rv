// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React from 'react';

import { AktionType, RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api';
import { DropdownMenu } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { getBRegTableItems } from '../general-items/controller.react';
import { TableCompPropsRvTableWithoutId } from './component.react';

const history = createHashHistory();

export function getMyRegelungsvorhabenTableVals(
  content: RegelungsvorhabenBundesregierungTableDTO[],
  setConfirmArchiveModalIsVisible: (confirmArchiveModalIsVisible: boolean) => void,
  setSelectedRegelungsvorhaben: (selectedAbstimmung: RegelungsvorhabenBundesregierungTableDTO) => void,
  setIsFreigebenModalVisible: (value: boolean) => void,
  setConfirmSentRvToPkpModalVisible: (value: boolean) => void,
  currentUserIsStellvertreter: boolean,
): TableCompPropsRvTableWithoutId {
  const columns: ColumnsType<RegelungsvorhabenBundesregierungTableDTO> = [
    ...getBRegTableItems(),
    {
      title: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.actions').toString(),
      key: 'actions',
      render: (record: RegelungsvorhabenBundesregierungTableDTO) => {
        return (
          <div className="actions-holder">
            <span>
              <Button
                id={`rv-myRegelungsVorhabenBearbeiten-btn-${record.id}`}
                disabled={!record.allowedToModify}
                className="ant-btn-secondary"
                onClick={() =>
                  history.push(
                    `/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}/${record.id}/${routes.BEARBEITEN_REGELUNGSVORHABEN}`,
                  )
                }
              >
                {i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.btnBearbeiten').toString()}
              </Button>
            </span>
            <DropdownMenu
              openLink={`/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}/${record.id}/${routes.NAECHSTESCHRITTE}`}
              items={[
                {
                  element: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.freigeben'),
                  onClick: () => {
                    setIsFreigebenModalVisible(true);
                    setSelectedRegelungsvorhaben(record);
                  },
                },
                {
                  element: i18n
                    .t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.ddMenuItemLabel')
                    .toString(),
                  onClick: () => {
                    setConfirmSentRvToPkpModalVisible(true);
                    setSelectedRegelungsvorhaben(record);
                  },
                  disabled: () => !record.aktionen?.includes(AktionType.AnPkpSenden) || currentUserIsStellvertreter,
                },

                {
                  element: i18n.t(
                    'rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.archiveAction.menuItemArchive',
                  ),
                  onClick: () => {
                    setSelectedRegelungsvorhaben(record);
                    setConfirmArchiveModalIsVisible(true);
                  },
                  disabled: () => !record.allowedToArchive,
                },
              ]}
              elementId={record.id}
            />
          </div>
        );
      },
    },
  ];

  return {
    expandable: false,
    columns,
    content,
    filteredColumns: [{ name: 'vorhabentyp', columnIndex: 0 }],
    sorterOptions: [
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.bearbeitetamAsc'),
        titleDesc: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.bearbeitetamDesc'),
      },
      {
        columnKey: 'erstellt',
        titleAsc: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.erstelltamAsc'),
        titleDesc: i18n.t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.erstelltamDesc'),
      },
    ],
  };
}
