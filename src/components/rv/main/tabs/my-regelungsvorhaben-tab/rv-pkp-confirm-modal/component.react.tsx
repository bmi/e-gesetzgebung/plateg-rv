// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createHashHistory } from 'history';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';

import {
  AbteilungDTO,
  CodelistenResponseDTO,
  ReferatDTO,
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenEntityFullDTO,
  RessortFullDTO,
} from '@plateg/rest-api';
import { CodeListenController, GlobalDI, LoadingStatusController, ModalWrapper } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';
import { sendRvToPkp } from '../../controller.react';
import { PkpSendStatusState } from '../pkp-confirm-modal-wrapper-rv-list/component.react';

interface Props {
  rv: RegelungsvorhabenBundesregierungTableDTO | RegelungsvorhabenEntityFullDTO;
  setVisible: (value: boolean) => void;
  visible: boolean;
  setPkpSendStatus?: (val: PkpSendStatusState) => void;
  rvId?: string;
  onClickModal?: () => void;
}
const history = createHashHistory();

export function RvPkpConfirmModal({ rv, setVisible, visible, setPkpSendStatus, rvId, onClickModal }: Props) {
  const { t } = useTranslation();

  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const codeListenCtrl = GlobalDI.getOrRegister('codeListenController', () => new CodeListenController());
  const [ressorts, setRessorts] = useState<RessortFullDTO[]>([]);
  const [abteilungDeleted, setAbteilungDeleted] = useState<AbteilungDTO | undefined | null>();
  const [referatDeleted, setReferatDeleted] = useState<ReferatDTO | undefined | null>();

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const codeListsub: Subscription = codeListenCtrl.getCodeListen().subscribe({
      next: (codeListResponse: CodelistenResponseDTO) => {
        setRessorts(codeListResponse.ressorts);
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error) => {
        console.error(error);
      },
    });
    return () => {
      codeListsub?.unsubscribe();
    };
  }, []);

  useEffect(() => {
    // Find the selected ressort and its abteilungen
    const selectedRessortAbteilungen = ressorts.find((ressort) => ressort.id === rv.technischesFfRessort)?.abteilungen;

    // Find the specific abteilung within the selected ressort
    const selectedAbteilung = selectedRessortAbteilungen?.find(
      (abteilung) => abteilung.id === rv.technischeFfAbteilung,
    );

    // Gather all referats from the selected abteilung
    const referats = [
      ...(selectedAbteilung?.referate || []), // Include its direct referate
      ...(selectedAbteilung?.unterabteilungen?.flatMap((item) => item.referate) || []), // Include referate from unterabteilungen
    ];

    // Find the specific referat within the gathered referats
    const selectedReferat = referats.find((referat) => referat?.id === rv.technischesFfReferat);

    // Update state based on deletion flags
    setAbteilungDeleted(selectedAbteilung?.deleted ? selectedAbteilung : null);
    setReferatDeleted(selectedReferat?.deleted ? selectedReferat : null);
  }, [rv, ressorts]);

  const ffAbteilungAndReferatSet = rv.technischeFfAbteilung && rv.technischesFfReferat;
  const rvID = rvId || (rv as RegelungsvorhabenBundesregierungTableDTO).id;

  const isAbteilungOrReferatDeleted = abteilungDeleted?.deleted || referatDeleted?.deleted;

  let modalContent: string | React.ReactElement = ffAbteilungAndReferatSet
    ? t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.modal.contentOk')
    : t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.modal.contentNotOk');
  const okBtnLabel =
    ffAbteilungAndReferatSet && !isAbteilungOrReferatDeleted
      ? t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.modal.okBtnOk')
      : t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.modal.okBtnDatafile');

  if (isAbteilungOrReferatDeleted) {
    modalContent = (
      <>
        {abteilungDeleted?.deleted && (
          <>
            {t('rv.newRegelungsvorhaben.participants.ffAbteilung.deletedDepartmentMsg', {
              abteilung: abteilungDeleted?.bezeichnung,
            })}
            {<br />}
          </>
        )}
        {referatDeleted?.deleted && (
          <>
            {t('rv.newRegelungsvorhaben.participants.ffReferat.deletedDepartmentMsg', {
              referat: referatDeleted?.bezeichnung,
            })}
            {<br />}
          </>
        )}
        {t(`rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.modal.correctInfoMsg`)}
      </>
    );
  }
  let rvNameAbkz = rv.abkuerzung || rv.kurzbezeichnung || '';
  rvNameAbkz = rvNameAbkz.length > 30 ? `${rvNameAbkz.slice(0, 30)}...` : rvNameAbkz;

  const onOkClickAction = () => {
    if (ffAbteilungAndReferatSet && !isAbteilungOrReferatDeleted) {
      sendRvToPkp(rvID, rvNameAbkz, setPkpSendStatus, onClickModal);
      setVisible(false);
    } else {
      history.push(
        `/regelungsvorhaben/${routes.MEINE_REGELUNGSVORHABEN}/${rvID}/${routes.BEARBEITEN_REGELUNGSVORHABEN}`,
      );
    }
  };

  return (
    <ModalWrapper
      onOk={onOkClickAction}
      title={t('rv.myRegelungsvorhaben.tabs.myRegelungsvorhaben.table.sendRvToPkp.modal.title')}
      okText={okBtnLabel}
      open={visible}
      onCancel={() => setVisible(false)}
    >
      {modalContent}
    </ModalWrapper>
  );
}
