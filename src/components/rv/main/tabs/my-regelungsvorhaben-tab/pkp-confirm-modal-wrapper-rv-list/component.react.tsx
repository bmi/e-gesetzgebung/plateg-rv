// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';

import { PkpGesendetStatusType, RegelungsvorhabenBundesregierungTableDTO } from '@plateg/rest-api';

import { RvPkpConfirmModal } from '../rv-pkp-confirm-modal/component.react';

interface Props {
  setVisible: (value: boolean) => void;
  visible: boolean;
  rv: RegelungsvorhabenBundesregierungTableDTO;
  rvsData: RegelungsvorhabenBundesregierungTableDTO[];
  setRegelungsvorhabenData: (val: RegelungsvorhabenBundesregierungTableDTO[]) => void;
}

export interface PkpSendStatusState {
  pkpSendStatus: PkpGesendetStatusType;
  gesendetAm?: string;
  bearbeitetAm: string;
}

export function PkpComfirmModalWrapperRvList({
  rv,
  visible,
  setVisible,
  setRegelungsvorhabenData,
  rvsData,
}: Props): React.ReactElement {
  const [pkpSendStatus, setPkpSendStatus] = useState<PkpSendStatusState>({
    pkpSendStatus: rv.pkpStatus,
    gesendetAm: rv.pkpGesendetAm,
    bearbeitetAm: rv.bearbeitetAm,
  });

  useEffect(() => {
    if (pkpSendStatus.pkpSendStatus) {
      updateRvData();
    }
  }, [pkpSendStatus]);

  const updateRvData = () => {
    const updateCondition =
      pkpSendStatus.pkpSendStatus !== PkpGesendetStatusType.Fehler || rv.pkpStatus !== pkpSendStatus.pkpSendStatus;

    const updatedData = rvsData.map((item) => {
      if (item.id === rv.id && updateCondition) {
        return {
          ...item,
          pkpGesendetAm: pkpSendStatus.gesendetAm,
          pkpStatus: pkpSendStatus.pkpSendStatus,
          bearbeitetAm: pkpSendStatus.bearbeitetAm,
        };
      }
      return item;
    });

    setRegelungsvorhabenData(updatedData);
  };

  return <RvPkpConfirmModal rv={rv} setVisible={setVisible} visible={visible} setPkpSendStatus={setPkpSendStatus} />;
}
