// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Layout, Spin } from 'antd';
import { Content } from 'antd/es/layout/layout';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { HeaderComponent, HeaderController, InfoComponent, LoadingStatusController } from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../shares/routes';
import { RvErrorController } from '../../utils/rv-error-controller';
import { MainRoutesRv } from './main/component.react';
import { RvHelpComponent } from './main/help/component.react';

export function RvHelpLink(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <InfoComponent key="rv-help" title={t('rv.help.drawerTitle')} buttonText={t('rv.header.linkHelp')}>
      <RvHelpComponent />
    </InfoComponent>
  );
}

export function TabLink(props: { tabName: string }): React.ReactElement {
  const { t } = useTranslation();
  return (
    <Link id="rv-meineRegelungsVorhaben-link" key={`rv-${props.tabName}`} to={`/regelungsvorhaben/${props.tabName}`}>
      {t('rv.breadcrumbs.projectName')} - {t(`rv.breadcrumbs.tabName.${props.tabName}`)}
    </Link>
  );
}

export function ElementLink(props: { tabName: string; elId?: string | null; abkuerzung: string }): React.ReactElement {
  const url = props.elId ? `${props.elId}/datenblatt` : routes.NEUES_REGELUNGSVORHABEN;
  return (
    <Link
      id="rv-element-link"
      key={`rv-${props.tabName}-${props.abkuerzung}`}
      to={`/regelungsvorhaben/${props.tabName}/${url}`}
    >
      {props.abkuerzung}
    </Link>
  );
}

export function EditElementLink(props: { tabName: string; action: string; elId?: string | null }): React.ReactElement {
  const { t } = useTranslation();
  const url = props.elId ? `${props.elId}/${props.action}` : routes.NEUES_REGELUNGSVORHABEN;
  return (
    <Link
      id="rv-element-edit-link"
      key={`rv-${props.tabName}-edit-element`}
      to={`/regelungsvorhaben/${props.tabName}/${url}`}
    >
      {t(`rv.breadcrumbs.action.${props.action}`)}
    </Link>
  );
}

export function BreadcrumbTitle(props: { action: string; abkuerzung: string }): React.ReactElement {
  const { t } = useTranslation();
  return (
    <span key={`vote-${props.action}`}>
      {props.abkuerzung ? `${props.abkuerzung} - ` : ''}
      {t(`rv.breadcrumbs.action.${props.action}`)}
    </span>
  );
}

export function LayoutWrapperRv(): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('rvHeadercontroller');
  const rvErrorCtrl = GlobalDI.getOrRegister('rvErrorCtrl', () => new RvErrorController());
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);

  useEffect(() => {
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error: AjaxError) => {
        console.error('Error sub', error);
        rvErrorCtrl.handleError(error);
      },
    });
    loadingStatusController.initLoadingStatus();
    return () => subs.unsubscribe();
  }, []);

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{ height: '100%', display: loadingStatus === true ? 'none' : 'unset' }}
        className="site-layout-background"
      >
        <HeaderComponent ctrl={headerController} headerId="ev-header" />

        <Content className="main-content-area has-drawer">
          <div className="ant-layout-content">
            <MainRoutesRv />
          </div>
        </Content>
      </Layout>
    </Spin>
  );
}
