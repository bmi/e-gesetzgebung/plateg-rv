// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import sinon from 'sinon';

import { ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';
// Diese Testklasse enthält keine Testfälle, stattdessen werden hier alle Mocks initialisiert,
// die von den anderen Testklassen benötigt werden
const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
export const setLoadingStatusStub = sinon.stub(loadingStatusCtrl, 'setLoadingStatus');

const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
export const displayErrorMsgStub = sinon.stub(errorCtrl, 'displayErrorMsg');
