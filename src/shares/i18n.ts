// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import { messages } from '../messages';

const loadMessages = i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: messages,
    lng: 'de',
    fallbackLng: 'de',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
