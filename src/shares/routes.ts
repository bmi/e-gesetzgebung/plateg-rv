// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const routes = {
  MEINE_REGELUNGSVORHABEN: 'meineRegelungsvorhaben',
  ENTWURFE: 'entwuerfe',
  ARCHIV: 'archiv',

  REGELUNGSVORHABEN: 'regelungsvorhaben',
  NEUES_REGELUNGSVORHABEN: 'neuesRegelungsvorhaben',
  BEARBEITEN_REGELUNGSVORHABEN: 'bearbeitenRegelungsvorhaben',
  BEARBEITEN_REGELUNGSVORHABEN_ENTWURF: 'bearbeitenRegelungsvorhabenEntwurf',
  PRUEFEN_REGELUNGSVORHABEN: 'pruefenRegelungsvorhaben',
  PRUEFEN_REGELUNGSVORHABEN_ENTWURF: 'pruefenRegelungsvorhabenEntwurf',
  DATENBLATT: 'datenblatt',
  NAECHSTESCHRITTE: 'naechsteschritte',
  ZULEITUNGEN: 'zustellungen',
  BESTANDSRECHT: 'bestandsrecht',
  DOKUMENTEERSTELLEN: 'dokumenteerstellt',
  PARLAMENTSVERFAHREN_VORABFASSUNG: 'parlamentsverfahrenvorbereitenVorabfassung',
  PARLAMENTSVERFAHREN_ENDGUELTIGE_FASSUNG: 'parlamentsverfahrenvorbereitenEntgueltigeFassung',
  REGELUNGSENTWURFERARBEITEN: 'regelungsentwurferarbeiten',
  REGELUNGSENTWURFABSTIMMEN: 'regelungsentwurfabstimmen',
  KABINETTVERFAHREN_DURCHFUEHREN: 'kabinettverfahrenDurchfuehren',
  EINLEITUNGSVERFAHREN: 'einleitungsverfahren',
  GESETZGEBUNGSVERFAHRENBT: 'gesetzgebungsverfahrenBT',
  GESETZGEBUNGSVERFAHRENBRAT: 'gesetzgebungsverfahrenBRAT',
  ABSCHLUSSVERFAHREN: 'abschlussverfahren',
  STELLUNGNAHMEBR: 'stellungnahmeBR',
  REGIERUNGSENTWUERFE: 'regierungsentwuerfe',
  AUSSCHUSSVERFAHREN_DURCHFUEHREN: 'ausschussverfahrenDurchfuehren',
  BERATUNG_PLENUM_VORBEREITEN: 'beratungPlenumVorbereiten',
  PLENARSITZUNG_DURCHFUEHREN: 'plenarsitzungDurchfuehren',
  NOTIFIZIERUNG_UEBERSENDEN: 'notifizierungUebersenden',
  ERFOLG: 'erfolg',
  ERFOLGPKP: 'erfolgPKP',
  ERFOLGUPD: 'erfolgAktualisiert',
  FEHLER: 'fehler',

  HILFE: 'hilfe',
};
