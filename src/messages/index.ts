// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de as theme_de } from '@plateg/theme';

import { de } from './de';

export const messages = {
  de: {
    translation: { ...de, ...theme_de },
  },
};
