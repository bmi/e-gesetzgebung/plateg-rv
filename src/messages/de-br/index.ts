// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const br = {
  rv: {
    breadcrumbs: {
      action: {
        datenblatt: 'Datenblatt',
        zustellungen: 'Zuleitungen',
      },
      projectName: 'Vorlagen',
      tabName: {
        archiv: 'Archiv',
        entwuerfe: 'Entwürfe',
        meineRegelungsvorhaben: 'Vorlagen',
      },
    },
    help: {
      section1:
        '<p>In der Anwendung Vorlage können Sie ein neues Vorlage empfangen und öffnen. </p><p>Nach einem Klick auf ein einzelnes Vorlage erhalten Sie im Bereich „Nächste Schritte“ einen Überblick über den derzeitigen Stand. </p><p>Steht ein Vorlage auf der Plenartagesordnung, können Sie diese Information hier hinterlegen. Auf dem „Datenblatt der Bundesregierung“ sehen Sie die Eckdaten der Bundesregierung für dieses Vorhaben. </p><p>Extern zugeleitete Dokumente empfangen Sie im Reiter Zuleitungen. Diese können hier geöffnet und heruntergeladen werden. </p><p>In der Anwendung werden Sie durch spezifische Hilfetexte zu den wichtigsten Begrifflichkeiten unterstützt.</p>',
    },
    myRegelungsvorhaben: {
      BRTableItems: {
        dblnr: 'DBlNr.',
        eingegangen: 'Eingegangen',
        federfuehrenderAusschussBR: 'Federführender Ausschuss BR',
        federfuehrenderAusschussBT: 'Federführender Ausschuss BT',
        federfuehrendesRessort: 'Federführendes Ressort',
        meinezugriffsrechte: 'Meine Zugriffsrechte',
        regelungsvorhaben: 'Vorlage',
        status: 'Status',
        statuses: {
          ARCHIVIERT: 'Archiviert',
          BUNDESTAG: 'Bundestag',
          IN_BEARBEITUNG: 'In Bearbeitung',
          WEITERGELEITET: 'Weitergeleitet',
        },
        vorhabentyp: 'Vorhabentyp',
        zugeleitet: 'Zugeleitet',
        zugriffsrechtInfo: {
          drawerText: `<h2>Mögliche Zugriffsrechte</h2>
          <ul>
          <li>„Lesen“: Sie können die Vorlage öffnen und die sich darin befindenden Dokumente einsehen. </li>
          <li>„Schreiben“: Das Datenblatt können Sie um die benötigten Metadaten für den Bundestag ergänzen.</li>
          <li>„Ersteller“ (künftig verfügbar): Sie können eine Vorlage anlegen und sind damit Erstellerin oder Ersteller. Als Erstellerin oder Ersteller können Sie etwa die Vorlage abbrechen oder abschließen.</li>
          </ul>
          <h2>Zugriffsrechte ändern</h2>
          <p>Über den Button "Berechtigungen" ist es möglich anderen Personen Rechte für die Vorlage zu geben.</p>`,
          title: 'Meine Zugriffsrechte',
        },
      },
      generalTableItems: {
        regelungsvorhaben: 'Vorlage',
      },
      mainTitle: 'Vorlagen',
      tabs: {
        archiv: {
          imgText1: 'Vorlagen archivieren',
          tabNav: 'Archiv',
          text: 'Vorlagen, die Sie nicht mehr bearbeiten möchten, können Sie in das Archiv sortieren. Alle Nutzenden haben nach der Archivierung nur noch lesenden Zugriff und können keine Änderungen mehr vornehmen. Alle weiteren Dokumente der Vorlage (z.B. Zeitplanungen, Abstimmungen) können ebenfalls nur noch im Lese-Zugriff geöffnet werden. Eine Archivierung kann nicht rückgängig gemacht werden.',
        },
        myRegelungsvorhaben: {
          BR: {
            imgText1: 'Vorlagen empfangen und einsehen',
            table: {
              actions: {
                menuItemArchive: 'Vorlage archivieren',
                statusAendern: 'Status ändern',
              },
            },
            text1: 'In dieser Anwendung empfangen und öffnen Sie Vorlagen.',
            text2:
              'Nach einem Klick auf eine einzelne Vorlage erhalten Sie im Bereich „Nächste Schritte“ einen Überblick über den derzeitigen Stand. Auf dem „Datenblatt“ sehen Sie die Eckdaten der Vorlage.',
          },
          imgText3: 'Zustellungen an PKP senden',
          tabNav: 'Vorlagen',
          text: 'In dieser Anwendung legen Sie eine neue Vorlage an. Dabei erfassen Sie alle relevanten Daten zu Ihrer Vorlage auf einem Datenblatt.<br> Innerhalb einer Vorlage behalten Sie den Status mithilfe der Übersicht „Nächste Schritte“ stets im Blick. Versenden Sie von dort außerdem Zuleitungen an PKP.',
        },
      },
    },
    newRegelungsvorhaben: {
      participants: {
        involvedRessorts: {
          label: 'Beteiligte Ressorts',
        },
      },
    },
    newZuleitung: {
      modal: {
        page2: {
          pruefenBtn: 'Dokumente zuleiten',
        },
        title: 'Vorlage "{{rvName}}": Dokumente zuleiten',
      },
    },
    regelungsvorhabenTabs: {
      tabs: {
        datenblatt: {
          title: 'Datenblatt der Bundesregierung',
        },
        naechsteSchritte: {
          tabs: {
            kabinettverfahren: {
              collapse: {
                kabinettbeschluss: {
                  createLink: 'Zum Tab „Zuleitungen“',
                },
                vorlageFRegierungsentwurf_PKPSenden: {
                  createLink: 'Zum Tab „Zuleitungen“',
                  text: `<p>Die Kabinettvorlage senden Sie im Tab „Zuleitungen“ in der Anwendung Vorlage an PKP.</p>
                  <p>Wenn aus den Beratungen im Kabinett Änderungswünsche entstehen, müssen Sie die Kabinettvorlage anpassen
                  und erneut an PKP senden. Dieser Prozess wiederholt sich, bis das Kabinett den Regierungsentwurf beschließt.</p>`,
                },
              },
            },
            lockReason: {
              bRegReason:
                'Der Zugriff auf diese Seite ist ausschließlich der Instanz vorbehalten, die die Vorlage eingeleitet hat.',
            },
          },
        },
        zuleitungen: {
          sendZultngBtn: 'Dokumente zuleiten',
          title: 'Zuleitungen',
        },
      },
    },
    rvErrorController: {
      error1063:
        'Das von Ihnen gewählte Vorlage befindet sich bei PKP intern noch nicht im richtigen Status für eine Zuleitung.  {{fehlercode}}',
      error1069: 'Das Dokument für die Zuleitung hat ein ungültiges Dateiformat.  {{fehlercode}}',
    },
  },
};
