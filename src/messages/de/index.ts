// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { br } from '../de-br';
import { bt } from '../de-bt';

export const de = {
  rv: {
    br: { ...br },
    breadcrumbs: {
      action: {
        bearbeitenRegelungsvorhaben: 'Bearbeiten',
        bearbeitenRegelungsvorhabenEntwurf: 'Entwurf bearbeiten',
        bestandsrecht: 'Bestandsrecht',
        datenblatt: 'Datenblatt',
        naechsteschritte: 'Nächste Schritte',
        neuesRegelungsvorhaben: 'Neues Regelungsvorhaben anlegen',
        prozesshilfe: 'Prozesshilfe',
        pruefenRegelungsvorhaben: 'Eingaben prüfen',
        pruefenRegelungsvorhabenEntwurf: 'Eingaben prüfen',
        // NOTE: both de-br and de-bt override key 'zustellungen' with 'Zuleitungen'.
        zustellungen: 'Zustellungen',
      },
      projectName: 'Regelungsvorhaben',
      tabName: {
        archiv: 'Archiv',
        entwuerfe: 'Entwürfe',
        meineRegelungsvorhaben: 'Regelungsvorhaben',
      },
    },
    bt: { ...bt },
    datenblattRegelungsvorhaben: {
      btns: {
        archiv: 'Zurück zum Archiv',
        edit: 'Eingaben bearbeiten',
        entwuerfe: 'Zurück zu den Entwürfen',
        meineRegelungsvorhaben: 'Zurück zur Startseite Regelungsvorhaben',
        PKP: 'Regelungsvorhaben an PKP senden',
      },
      confirmPKPModal: {
        actionTitle: 'Möchten Sie das Vorhaben an PKP übertragen?',
        cancelText: 'Abbrechen',
        okText: 'Ja, jetzt übertragen',
      },
      sendToPKP: {
        failure: 'Oh je, die Übertragung ist fehlgeschlagen.',
        success: 'Ihr Regelungsvorhaben {{rvName}} wurde erfolgreich an PKP übertragen.',
      },
      title: '{{rvTitle}} - Datenblatt',
    },
    fileUpload: {
      dmTitle: 'Dokumentenmappe mit Regierungsentwurf',
      missingDmHintContent:
        'Um eine Zuleitung durchführen zu können, muss sich eine Dokumentenmappe zu diesem Regelungsvorhaben im Status „Bereit für Kabinettverfahren“ befinden.',
      missingDmHintTitle: 'Fehlende Dokumentenmappe',
      typeOfDocument: {
        error: 'Bitte geben Sie die Art des Dokuments an.',
        label: 'Format des Dokuments',
        typeEditorDocument: 'Editor-Dokument',
        typeEnormDocument: 'eNorm-Dokument',
      },
      uploadAttachmentsTitle: 'Hochladen von Anlagen (PDF/Word-Dokumente, max. 20 MB)',
      uploadBtnRegelungsentwurf: 'Regierungsentwurf hochladen',
      uploadDraftDrawerText:
        'Hier kann nur ein Regierungsentwurf hochgeladen werden. Bitte wählen Sie eine Datei aus. Bei Bedarf können Sie die Bezeichnung der Datei über das Stift-Icon anpassen.',
      uploadDraftDrawerTitle: 'Regierungsentwurf hochladen',
      uploadDraftErrorTitle: 'Abschnitt "Regierungsentwurf hochladen"',
      uploadDraftFormatShort: 'DOCX (eNorm oder Word)',
      uploadDraftTitle: 'Regierungsentwurf hochladen (PDF/Word-Dokument, max. 20 MB)',
      uploadedAttachmentsTitle: 'Hochgeladene Anlagen',
      uploadedDraftTitle: 'Hochgeladener Regierungsentwurf',
      uploadHintRegelungsentwurf:
        'Ziehen Sie den Regierungsentwurf einfach in dieses Feld, um ihn hochzuladen, oder klicken Sie hinein, um den Auswahldialog zu öffnen. Eine Mehrfachauswahl ist nicht möglich.',
    },
    freigabe: {
      abbrechenButton: 'Abrechen',
      entziehenButton: 'Entziehen',
      freigabeTitel: 'Möchten Sie die Berechtigungen entziehen?',
      infoText:
        'Wenn Sie der Person die Berechtigung zur Beobachtung des Regelungsvorhabens entziehen, verliert sie auch die Berechtigung zur Mitarbeit an einzelnen Modulen der  Gesetzesfolgenabschätzung (falls vorhanden).',
    },
    header: {
      exportButton: 'Datenexport (PDF)',
      freigabe: 'Berechtigungen',
      linkHelp: 'Hilfe',
      linkHome: 'Startseite Regelungsvorhaben',
    },
    help: {
      drawerTitle: 'Regelungsvorhaben',
      section1: `<p>In der Anwendung Regelungsvorhaben können Sie ein neues Regelungsvorhaben anlegen.
        Ein Regelungsvorhaben dient dazu, alle notwendigen Dokumente eines Regelungsentwurfs in einem Ordner zusammenzufassen. Beim Anlegen werden Sie gebeten, relevante Eckdaten zur geplanten Regelung in einem Datenblatt einzugeben.
        Diese Informationen werden dazu verwendet, Sie durch den weiteren Rechtsetzungsprozess zu leiten. Sie können diese Eckdaten im Laufe des Rechtsetzungsprozesses weiterhin bearbeiten und anpassen.
        Ab bestimmten Punkten im Verfahren sind einzelne Informationen dann jedoch festgelegt und nicht mehr bearbeitbar.</p>
        <p>Regelungsvorhaben können archiviert werden, wenn die Bearbeitung abgeschlossen ist. Sie behalten weiterhin Lese-Zugriff auf archivierte Regelungsvorhaben.</p>
        <p>In der Anwendung werden Sie durch spezifische Hilfetexte zu den wichtigsten Begrifflichkeiten unterstützt.</p>`,
    },
    myRegelungsvorhaben: {
      // Declares tombstone that must be overridden in de-br. Checked by mocha test.
      BRTableItems: {},
      btnNewRegelungsvorhaben: 'Neues Regelungsvorhaben anlegen',
      contactPersonDrawerTitle: 'Kontakt',
      deleteMsg: {
        error: 'Der Entwurf konnte nicht gelöscht werden {{fehlercode}}',
        success: 'Der Entwurf wurde gelöscht',
      },
      generalTableItems: {
        bearbeitetam: 'Zuletzt bearbeitet',
        erstelltam: 'Erstellt',
        federfuhrer: 'Federführung',
        regelungsvorhaben: 'Regelungsvorhaben',
        vorhabentyp: 'Vorhabentyp',
        zugriffsrecht: 'Meine Zugriffsrechte',
        zugriffsrechtInfo: {
          drawerText: `<h2>Mögliche Zugriffsrechte</h2>
          <ul>
            <li>„Federführung“: Sie können das Datenblatt des Regelungsvorhabens öffnen und bearbeiten. Sie können das Regelungsvorhaben an PKP senden und archivieren. Sie können Berechtigungen für das Regelungsvorhaben zuweisen.</li>
            <li>„Mitarbeit“: Sie können das Datenblatt des Regelungsvorhabens öffnen und bearbeiten.</li>
            <li>„Beobachtung“: Sie können das Datenblatt des Regelungsvorhabens öffnen.</li>
          </ul>
          <h2>Zugriffsrechte ändern</h2>
          <p>Öffnen Sie im Aktionen-Menü (Drei-Punkte-Menü) den Dialog „Berechtigungen“. Dort können Sie die Zugriffsrechte einsehen und bearbeiten.</p>
          `,
          title: 'Meine Zugriffsrechte',
        },
      },
      mainTitle: 'Regelungsvorhaben',
      tabs: {
        archiv: {
          imgText1: 'Regelungsvorhaben archivieren',
          imgText2: 'Lese-Zugriff auf das Datenblatt bleibt erhalten',
          table: {
            archiviertAm: 'Archiviert',
            archiviertamAsc: 'Archiviert (ältestes zuerst)',
            archiviertamDesc: 'Archiviert (neuestes zuerst)',
            bearbeitetamAsc: 'Zuletzt bearbeitet (ältestes zuerst)',
            bearbeitetamDesc: 'Zuletzt bearbeitet (neuestes zuerst)',
            erstelltamAsc: 'Erstellt (ältestes zuerst)',
            erstelltamDesc: 'Erstellt (neuestes zuerst)',
            filter: {
              displayname: {
                einleiter: 'Einleitung',
                vorhabentyp: 'Vorhabentyp',
              },
              labelContent: 'Filtern nach',
            },
            sorterTitle: 'Sortieren nach',
          },
          tabNav: 'Archiv',
          text: 'Regelungsvorhaben, die Sie nicht mehr bearbeiten möchten, können Sie in das Archiv sortieren. Alle Nutzenden haben nach der Archivierung nur noch lesenden Zugriff und können keine Änderungen mehr vornehmen. Alle weiteren Dokumente des Regelungsvorhaben (z.B. Zeitplanungen, Abstimmungen) können ebenfalls nur noch im Lese-Zugriff geöffnet werden. Eine Archivierung kann nicht rückgängig gemacht werden.',
        },
        entwuerfe: {
          contactPersonTitle: 'Kontakt',
          imgText1: 'Zwischenstände beim Anlegen von Datenblättern speichern',
          imgText2: 'Fortsetzung später problemlos möglich',
          table: {
            actions: 'Aktionen',
            bearbeitetamAsc: 'Zuletzt bearbeitet (ältestes zuerst)',
            bearbeitetamDesc: 'Zuletzt bearbeitet (neuestes zuerst)',
            btnBearbeiten: 'Entwurf\u00A0bearbeiten',
            delete: 'Entwurf löschen',
            erstelltamAsc: 'Erstellt (ältestes zuerst)',
            erstelltamDesc: 'Erstellt (neuestes zuerst)',
            filter: {
              displayname: {
                einleiter: 'Einleitung',
                vorhabentyp: 'Vorhabentyp',
              },
              labelContent: 'Filtern nach',
            },
            sorterTitle: 'Sortieren nach',
            status: 'Status',
          },
          tabNav: 'Entwürfe',
          text: 'Beim initialen Anlegen eines Regelungsvorhabens können Sie zwischenspeichern und das Datenblatt zu einem späteren Zeitpunkt vervollständigen. Sie finden Ihren gespeicherten Zwischenstand in diesem Bereich „Entwürfe“.',
        },
        myRegelungsvorhaben: {
          // Declares tombstone that must be overridden in de-br. Checked by mocha test.
          BR: {},
          freigabe: {
            error:
              'Die Anzeige der Berechtigungen ist gerade nicht möglich. Bitte versuchen Sie es später noch einmal.',
            success: 'Sie haben die Berechtigungen für das Regelungvorhaben „{{title}}“ aktualisiert.',
          },
          imgText1: 'Neues Regelungsvorhaben anlegen',
          imgText2: '„Nächste Schritte“ Ihres Regelungsvorhabens einsehen',
          imgText3: 'Zustellungen an PKP senden',
          statusValues: {
            ABGEBROCHEN: 'Abgebrochen',
            ABGESCHLOSSEN: 'Geschlossen',
            ARCHIVIERT: 'Archiviert',
            ENTWURF: 'Entwurf',
            IN_BEARBEITUNG: 'Aktiv',
          },
          table: {
            actions: 'Aktionen',
            archiveAction: {
              archiveActionTitle: `<p>Hiermit beenden Sie das Vorhaben und verschieben das Datenblatt ins Archiv. Damit wird der laufende Prozess abgebrochen. Eine Archivierung gilt für alle Personen mit Berechtigungen an dem Regelungsvorhaben. Eine Archivierung können Sie nicht rückgängig machen.</p>
                <p>Im Archiv kann das Datenblatt noch eingesehen, aber nicht mehr bearbeitet werden.</p>
                <p>Alle Elemente, die Sie für dieses Vorhaben angelegt haben (z.&nbsp;B. Dokumentenmappen, Zeitplanungen und zugehörige Billigungsanfragen, Abstimmungen und Gesetzesfolgenabschätzungen) werden ebenfalls ins Archiv verschoben.</p>
                <p>Die Archivierung des Regelungsvorhabens ist keine Veraktung und ersetzt nicht die Veraktung in der E‑Akte. Eine Veraktung der Elemente in der E‑Akte muss unabhängig davon manuell vorgenommen werden.</p>
                <p>Möchten Sie das Regelungsvorhaben archivieren?</p>`,
              archiveCancel: 'Abbrechen',
              archiveConfirm: 'Ja, jetzt archivieren',
              archivedSuccess: 'Sie haben das Regelungsvorhaben „{{title}}“ erfolgreich archiviert',
              freigeben: 'Berechtigungen',
              menuItemArchive: 'Regelungsvorhaben archivieren',
              modalTitle: 'Regelungsvorhaben abschließen',
            },
            bearbeitetamAsc: 'Zuletzt bearbeitet (ältestes zuerst)',
            bearbeitetamDesc: 'Zuletzt bearbeitet (neuestes zuerst)',
            btnBearbeiten: 'Bearbeiten',
            erstelltamAsc: 'Erstellt (ältestes zuerst)',
            erstelltamDesc: 'Erstellt (neuestes zuerst)',
            filter: {
              displayname: {
                einleiter: 'Erstellerin oder Ersteller',
                vorhabentyp: 'Vorhabentyp',
              },
              labelContent: 'Filtern nach',
            },
            sendRvToPkp: {
              ddMenuItemLabel: 'Regelungsvorhaben an PKP senden',
              modal: {
                contentNotOk: `Die Informationen zu „Federführende Abteilung“ und „Federführendes Referat“ liegen noch nicht vor. Bitte tragen Sie die entsprechenden Informationen ein.`,
                contentOk: 'Möchten Sie das Vorhaben an PKP übertragen?',
                correctInfoMsg: 'Bitte korrigieren Sie die entsprechenden Informationen.',
                okBtnDatafile: 'Datenblatt bearbeiten',
                okBtnOk: 'Ja, jetzt übertragen',
                title: 'Regelungsvorhaben an PKP senden',
              },
              popupMessage: {
                error: `Oh je, die Übertragung ist fehlgeschlagen.
                Ihr Regelungsvorhaben {{rvNameAbkz}} konnte nicht an PKP gesendet werden. Bitte versuchen Sie es erneut.`,
                error1067:
                  'Der Wert für den Titel bzw. den Langtitel des Regelungsvorhabens {{rvNameAbkz}} kann nicht mehr geändert werden, da es sich bereits im Meldestatus üV oder KMGGV befindet. (1067)',
                success: `Ihr Regelungsvorhaben {{rvNameAbkz}} wurde erfolgreich an PKP übertragen.`,
              },
              status: {
                failed: 'Fehlgeschlagen',
                notSent: 'Nicht gesendet',
                success: {
                  DELETED: 'Regelungsvorhaben erfolgreich an PKP übertragen: gelöscht',
                  IV: 'Regelungsvorhaben erfolgreich an PKP übertragen: iV',
                  KMGGV: 'Regelungsvorhaben erfolgreich an PKP übertragen: Kabinettverfahren',
                  UV: 'Regelungsvorhaben erfolgreich an PKP übertragen: üV',
                },
                successBT: 'Regelungsvorhaben erfolgreich von PKP empfangen',
              },
              tableColLabel: 'An PKP gesendet',
            },
            sorterTitle: 'Sortieren nach',
            status: 'Status',
            statusAsc: 'Aktiv',
            statusDesc: 'Geschlossen',
            zuleitungsdatumAsc: 'Zuleitungsdatum (ältestes zuerst)',
            zuleitungsdatumDesc: 'Zuleitungsdatum (neuestes zuerst)',
          },
          tabNav: 'Regelungsvorhaben',
          text: 'In dieser Anwendung legen Sie ein neues Regelungsvorhaben an. Dabei erfassen Sie alle relevanten Daten zu Ihrem Vorhaben auf einem Datenblatt.<br> Innerhalb eines Regelungsvorhabens behalten Sie den Status mithilfe der Übersicht „Nächste Schritte“ stets im Blick. Versenden Sie von dort außerdem Zustellungen an PKP.',
        },
      },
      zugriffsrechte: {
        ERSTELLER: 'Ersteller',
        FEDERFUEHRER: 'Federführung',
        GAST: 'Beobachtung',
        KEINE: 'Keine',
        LESER: 'Lesen',
        MITARBEITER: 'Mitarbeit',
        SCHREIBER: 'Schreiben',
      },
    },
    na: 'keine Angabe',
    naDatenblattNr: 'Datenblattnummer noch nicht von PKP vergeben',
    newRegelungsvorhaben: {
      bearbeitenRegelungsvorhaben: {
        mainTitle: 'Bearbeiten',
      },
      bearbeitenRegelungsvorhabenEntwurf: {
        mainTitle: 'Entwurf bearbeiten',
      },
      beteiligungBundesrat: {
        hint: {
          content: `<p style="margin-top: 0">Die Beteiligung des Bundesrates ist von der Vorhabenart abhängig.</p>
            <h1>Gesetz</h1><p style="margin-top: 0">Gesetze werden in Zustimmungs- und Einspruchsgesetze unterschieden. Diese Unterscheidung bestimmt die Art des Zusammenwirkens von Bundestag und Bundesrat im Rahmen des Gesetzgebungsverfahrens. Der Grundfall sind Einspruchsgesetze, für Zustimmungsgesetze müssen bestimmte Voraussetzungen erfüllt sein.</p>
            <p>Zustimmungsgesetze kommen nur zustande, wenn der Bundesrat dem Gesetz zustimmt, bei Einspruchsgesetzen hingegen kann der Bundesrat Einspruch einlegen, der Bundestag kann ihn jedoch überstimmen.</p>
            <p>Zustimmungspflichtig sind</p>
            <ul><li>Gesetze, die die Verfassung ändern.<br>Hier muss der Bundesrat sogar mit einer Zweidrittelmehrheit (46 Stimmen) zustimmen (<a id="link_art79gg" target="_blank" href={{link_art79gg}}>Artikel 79 Abs. 2 GG</a>).</li>
            <li>Gesetze, die in bestimmter Weise Auswirkungen auf die Finanzen der Länder haben.<br>Hierunter fallen auf der Einnahmeseite alle Gesetze über Steuern, an deren Aufkommen die Länder oder Gemeinden beteiligt sind (<a id="link_art105gg" target="_blank" href={{link_art105gg}}>Artikel 105 Abs. 3 GG</a>): zum Beispiel die Lohn- und Einkommensteuer, die Mehrwertsteuer und die Gewerbesteuer.<br>Auf der Ausgabenseite zählen hierzu alle Bundesgesetze, die Pflichten der Länder zur Erbringung von Geldleistungen, geldwerten Sachleistungen oder vergleichbare Dienstleistungen gegenüber Dritten begründen (<a id="link_art104agg" target="_blank" href={{link_art104agg}}>Artikel 104a Abs. 4 GG</a>).</li>
            <li>Gesetze, für deren Umsetzung in die Organisations- und Verwaltungshoheit der Länder eingegriffen wird.<br>Die Länder haben das Recht, von bundesgesetzlichen Regelungen über die Einrichtung der Behörden und über das Verwaltungsverfahren durch Landesgesetz abweichen zu dürfen. Die Zustimmung des Bundesrates ist insoweit nur erforderlich, wenn im Bundesgesetz wegen eines besonderen Bedürfnisses nach bundeseinheitlicher Regelung das Verwaltungsverfahren ausnahmsweise ohne Abweichungsmöglichkeit für die Länder geregelt wird (<a id="link_art84gg_1" target="_blank" href={{link_art84gg}}>Artikel 84 Abs. 1 GG</a>).</li></ul>
            <p>Weitere Informationen dazu sind im Verfahrensassistenten für Gesetze unter Schritt <a id="link_evir22beratungbundesrat" target="_blank" href={{link_evir22beratungbundesrat}}>2.2 Beratung Bundesrat</a> zu finden.</p>
            <h1>Rechtsverordnung</h1><p style="margin-top: 0">Rechtsverordnungen der Bundesregierung oder einer Bundesministerin bzw. eines Bundesministers bedürfen in bestimmten Fällen der Zustimmung des Bundesrates. Zum einen kann das ermächtigende Parlamentsgesetz vorsehen, dass die Rechtsverordnung der Zustimmung des Bundesrates bedarf. Zum anderen beinhaltet <a id="link_art80gg" target="_blank" href={{link_art80gg}}>Artikel 80 Absatz 2 GG</a> bestimmte Katalogtatbestände, für die Rechtsverordnungen zustimmungsbedürftig sind, sofern ein Bundesgesetz diese Zustimmungspflicht nicht aufgehoben hat.</p>
            <p>Der Schritt <a id="link_evir18befassungbundesrat" target="_blank" href={{link_evir18befassungbundesrat}}>1.8 Befassung Bundesrat</a> im Verfahrensassistenten für Rechtsverordnungen bietet weitere Informationen zum Verfahren. </p>
            <h1>Verwaltungsvorschrift</h1><p style="margin-top: 0">Die Beteiligung des Bundesrates ist in <a id="link_art84gg_2" target="_blank" href={{link_art84gg}}>Art. 84 Abs. 2</a> sowie in <a id="link_art85gg" target="_blank" href={{link_art85gg}}>Art. 85 Abs. 2 GG</a> geregelt. Für allgemeine Verwaltungsvorschriften nach <a id="link_art86gg" target="_blank" href={{link_art86gg}}>Art. 86 GG</a> ist keine Zustimmung des Bundesrates erforderlich. Die allgemeinen Verwaltungsvorschriften nach <a id="link_art108gg" target="_blank" href={{link_art108gg}}>Artikel 108 Absatz 7 GG</a> bedürfen der Zustimmung des Bundesrates, soweit sie für die Verwaltung der Landesfinanzbehörden oder der Gemeinden und Gemeindeverbände gelten.</p>
            <p>Weitere Informationen sind im Verfahrensassistenten für Verwaltungsvorschriften unter Schritt <a id="link_evir152befassungbundesrat" target="_blank" href={{link_evir152befassungbundesrat}}>1.5.2 Beteiligung des Bundesrates</a> zu finden.</p>`,
          title: 'Beteiligungserfordernis Bundesrat',
        },
        options: {
          BETEILIGUNGSERFORDERNIS_SPAETER_FESTLEGEN: {
            label: 'Beteiligungserfordernis des Bundesrats später festlegen',
          },
          EINSPRUCHSGESETZ: {
            label: 'Einspruchsgesetz',
          },
          ZUSTIMMUNGERFORDERNIS: {
            label: 'Zustimmungserfordernis',
          },
          ZUSTIMMUNGSGESETZ: {
            label: 'Zustimmungsgesetz',
          },
        },
        title: 'Beteiligungserfordernis Bundesrat',
      },
      beteiligungBundestag: {
        hint: {
          content: `<p style="margin-top: 0">Die Beteiligung des Deutschen Bundestages ist von der Vorhabenart abhängig.</p>
            <h1>Gesetz</h1><p style="margin-top: 0">Gesetze werden vom Deutschen Bundestag beschlossen. Das Verfahren ist im Verfahrensassistenten unter Phase <a id="link_evir21beratungbundestag" target="_blank" href={{link_evir21beratungbundestag}}>2.1 Beratung Bundestag</a> beschrieben.</p>
            <h1>Rechtsverordnung</h1><p style="margin-top: 0">Die Mitwirkung des Bundestages bei Rechtsverordnungen richtet sich nach der gesetzlichen Ermächtigung für die Rechtsverordnung.</p>
            <p>Unterschieden wird bei der Mitwirkung in vier Vorbehaltstypen:</p>
            <ul><li>Kenntnisvorbehalt - Die Verordnungsgeberin oder der Verordnungsgeber ist verpflichtet, den Verordnungsentwurf dem Bundestag zur Kenntnis vorzulegen, der so die Möglichkeit zur Stellungnahme oder ggf. zu weiteren Maßnahmen hat.</li>
            <li>Zustimmungsvorbehalt - Die Zustimmung durch den Bundestag ist ausdrücklich zu erteilen oder wird nach Ablauf einer bestimmten Frist fingiert.</li>
            <li>Ablehnungsvorbehalt - Der Bundestag ist ermächtigt, Rechtsverordnungen abzulehnen. Sieht das Gesetz einen Änderungsvorbehalt vor, ist der Bundestag zur inhaltlichen Änderung des Verordnungsentwurfs befugt oder kann die Änderung verlangen. Änderungs- und Ablehnungsvorbehalte sind zumeist kombiniert in der gesetzlichen Ermächtigung vorgesehen.</li>
            <li>Aufhebungsvorbehalt - Der Bundestag kann eine Rechtsverordnung nach deren Erlass durch Beschluss aufheben oder deren Aufhebung verlangen.</li></ul>
            <p>Weitere Informationen sind im Verfahrensassistenten für Rechtverordnungen unter <a id="link_evir17befassungbundestag" target="_blank" href={{link_evir17befassungbundestag}}>Schritt 1.7 Befassung Bundestag</a> zu finden.</p>`,
          title: 'Beteiligungserfordernis Deutscher Bundestag',
        },
        options: {
          ABLEHNUNGSVORBEHALT: {
            label: 'Ablehnungsvorbehalt',
          },
          AUFHEBUNGSVORBEHALT: {
            label: 'Aufhebungsvorbehalt',
          },
          BUNDESTAG: {
            label: 'Deutscher Bundestag',
          },
          KENNTNISVORBEHALT: {
            label: 'Kenntnisvorbehalt',
          },
          ZUSTIMMUNGSVORBEHALT: {
            label: 'Zustimmungsvorbehalt',
          },
        },
        title: 'Beteiligungserfordernis Deutscher Bundestag',
      },
      btns: {
        cancel: 'Abbrechen',
        continueLater: {
          bntText: 'Zwischenspeichern',
          hint: {
            content:
              'Speichern Sie Ihren Zwischenstand, um das Formular zu einem späteren Zeitpunkt abzuschicken. Unter dem Menüpunkt „Regelungsvorhaben“ im Reiter „Entwürfe“ können Sie es erneut aufrufen.',
            title: 'Regelungsvorhaben anlegen später fortsetzen',
          },
          title: 'Regelungsvorhaben anlegen später fortsetzen?',
        },
        submit: 'Eingaben prüfen',
      },
      confirmModal: {
        btnCancelConfirmNo: 'Nein, nicht abbrechen',
        btnCancelConfirmOk: 'Ja, abbrechen',
        title: 'Möchten Sie das Anlegen des Regelungsvorhabens wirklich abbrechen?',
      },
      designation: {
        amtlAbbreviation: {
          error: 'Bitte geben Sie die Abkürzung des Regelungvorhabens an.',
          hint: {
            content:
              '<p>Geben Sie hier die Abkürzung des Regelungsvorhabens an.</p>' +
              '<p>Beispiel: StrRehaG</p>' +
              '<p>Das Handbuch der Rechtsförmlichkeit enthält Hinweise zu Abkürzungen zu den verschiedenen Regelungsarten. Grundlegende Hinweise zur Bildung einer Abkürzung finden sich im Abschnitt zu Stammgesetzen unter <a target="_blank" href={{link1}}>HdR Rn. 341 ff</a>. Ablösungsgesetze erhalten die selbe Abkürzung wie das abgelöste Gesetz (<a target="_blank" href={{link2}}>HdR Rn. 509</a>).</p>' +
              '<p>Auch wenn Einzelnovellen (<a target="_blank" href={{link3}}>HdR Rn. 533</a>), Mantelgesetze (<a target="_blank" href={{link4}}>Hdr Rn 729</a>) und Einführungsgesetze (<a target="_blank" href={{link5}}>HdR Rn 756 ff</a>) aus Sicht der Rechtsförmlichkeit grundsätzlich keine Abkürzung benötigen, ist im System der E-Gesetzgebung eine Abkürzung als Pflichtangabe notwendig, damit das Regelungsvorhaben in Tabellen und Verlinkungen dargestellt werden kann.</p>',
            title: 'Abkürzung',
          },
          label: 'Abkürzung',
        },
        amtlDesignation: {
          error: 'Bitte geben Sie die Bezeichnung des Regelungvorhabens an.',
          hint: {
            content:
              '<p>Geben Sie hier die Bezeichnung des Regelungsvorhabens in der Langform an (<a target="_blank" href={{link1}}>HdR Rn. 324 ff.</a>).</p>' +
              '<p>Beispiel: Gesetz über die Rehabilitierung und Entschädigung von Opfern rechtsstaatswidriger Strafverfolgungsmaßnahmen im Beitrittsgebiet.</p>',
            contentInfoDisabled: `Wenn das Regelungsvorhaben in PKP veröffentlicht wurde, darf die Bezeichnung nicht mehr geändert werden. Aus diesem Grund ist das Feld Bezeichnung ab der Veröffentlichung in PKP für Eingaben gesperrt.`,
            title: 'Bezeichnung',
          },
          label: 'Bezeichnung',
        },
        datenblattNr: {
          infoText:
            'Die Datenblattnummer wird in der Anwendung PKP beim Übergang von der internen in die übergreifende Vorhabenplanung vergeben, da sie hier zwingend vorliegen muss. Solange in PKP keine Datenblattnummer vorliegt, wird dies beim Regelungsvorhaben entsprechend gekennzeichnet.',
          label: 'Datenblattnummer',
        },
        regDesignation: {
          error: 'Bitte geben Sie die Kurzbezeichnung des Regelungvorhabens an.',
          hint: {
            content:
              '<p>Geben Sie hier die Bezeichnung des Regelungsvorhabens in der Kurzform an.</p>' +
              '<p>Beispiel: Strafrechtliches Rehabilitierungsgesetz</p>' +
              '<p>Das Handbuch der Rechtsförmlichkeit enthält Hinweise zu Kurzbezeichnungen zu den verschiedenen Regelungsarten. Grundlegende Hinweise zur Bildung von Kurzbezeichnungen finden sich im Abschnitt zu Stammgesetzen unter <a target="_blank" href={{link1}}>HdR Rn. 331 ff</a>. Ablösungsgesetze erhalten die selbe (Kurz-) Bezeichnung wie das abgelöste Gesetz (<a target="_blank" href={{link2}}>HdR Rn. 508 ff</a>).</p>' +
              '<p>Auch wenn Einzelnovellen (<a target="_blank" href={{link3}}>HdR Rn. 532</a>), Mantelgesetze (<a target="_blank" href={{link4}}>Hdr Rn 724</a>) und Einführungsgesetze (<a target="_blank" href={{link5}}>HdR Rn 756 ff</a>) aus Sicht der Rechtsförmlichkeit grundsätzlich keine Kurzbezeichnung benötigen, ist im System der E-Gesetzgebung eine Kurzbezeichnung als Pflichtangabe notwendig, damit das Regelungsvorhaben in Tabellen und Verlinkungen dargestellt werden kann.</p>',
            contentInfoDisabled:
              'Wenn das Regelungsvorhaben in PKP veröffentlicht wurde, darf die Kurzbezeichnung nicht mehr geändert werden. Aus diesem Grund ist das Feld Kurzbezeichnung ab der Veröffentlichung in PKP für Eingaben gesperrt.',
            title: 'Kurzbezeichnung',
          },
          label: 'Kurzbezeichnung',
          lengthError: 'Maximale Länge von {{maxChars}} Zeichen für {{fieldname}} wurde überschritten.',
        },
        rvsInPKP: {
          besondereUmstaende: {
            hinweis: {
              content:
                'Das Regelungsvorhaben ist in PKP als eilbedürftig gekennzeichnet worden. Prüfen Sie vor der erneuten Übertragung an PKP, ob Sie das Regelungsvorhaben in der E-Gesetzgebung unter "Besondere Umstände" als Eilbedürftig kennzeichnen möchten. Bedenken Sie dabei, dass die Eilbedürftigkeit bei bestimmten Regelungstypen ausgeschlossen ist.',
            },
          },
          btnApply: 'Diese Daten verwenden',
          columns: {
            aktion: 'Aktion',
            kurzbezeichnung: 'Kurzbezeichnung',
            regelungsvorhaben: 'Regelungsvorhaben',
            vorhabenart: 'Vorhabenart',
          },
          drawerSwitchText:
            'Hier können Sie sich bereits in PKP angelegte Regelungsvorhaben anzeigen lassen. Deren Metadaten können Sie dann in das Formular unten übernehmen. Es werden Ihnen nur Regelungsvorhaben angezeigt, die auch in PKP für Sie sichtbar sind.',
          drawerSwitchTextStellvertreter:
            'Wenn Sie als Stellvertretung eingeloggt sind, können Sie keine Regelungsvorhaben anzeigen lassen, die bereits in PKP angelegt sind.',
          drawerSwitchTitle: 'In PKP verfügbare Regelungsvorhaben',
          noItems: 'Keine Vorhaben vorhanden.',
          selectedItem: {
            drawerText:
              'Das Formular wird mit den verfügbaren Informationen des Regelungsvorhabens aus PKP vorbefüllt. Sie können diese Informationen anschließend anpassen. Wenn Sie das Regelungsvorhaben anschließend in der E-Gesetzgebung speichern, sind die Regelungsvorhaben zwischen den Plattformen miteinander verknüpft.',
            noItem: 'Es wurde noch kein in PKP angelegtes Regelungsvorhaben ausgewählt.',
            title: 'Ausgewähltes Regelungsvorhaben aus PKP',
          },
          switchLabel: 'Für mich in PKP verfügbare Regelungsvorhaben anzeigen',
        },
        shortDescription: {
          hint: {
            content:
              'Geben Sie hier in kurzen Worten den Inhalt des Regelungsvorhabens wieder. Dies soll anderen insbesondere dabei helfen, einen kurzen Überblick über die zentralen Regelungen des Vorhabens zu gewinnen.',
            title: 'Kurzbeschreibung',
          },
          label: 'Kurzbeschreibung',
        },
        title: 'Eckdaten',
      },
      msgs: {
        successCached: 'Änderungen gespeichert',
        successCreatedMsg: 'Das Regelungsvorhaben wurde erfolgreich angelegt.',
        successUpdatedMsg: 'Das Regelungsvorhaben wurde erfolgreich aktualisiert.',
      },
      neuesRegelungsvorhaben: {
        mainTitle: 'Neues Regelungsvorhaben anlegen',
      },
      participants: {
        beteiligungsErfordernis: {
          errorMsg:
            'Sie haben die Auswahl über eine Fristverkürzung ausgewählt. Bitte wählen Sie im Feld Beteiligungserfordernis „Bundesrat“ aus.',
          hint: {
            content:
              '<p>An dieser Stelle haben Sie die Möglichkeit anzugeben, ob eine Beteiligung des Bundestags und/oder des Bundesrats im Rechtsetzungsprozess erforderlich ist.</p>' +
              '<p>Im Gesetzgebungsprozess sind Bundestag und Bundesrat stets am Rechtsetzungsprozess zu beteiligen.</p>' +
              '<p>Verordnungen bedürfen unter den in <a target="_blank" href={{link}}>Art. 80 Abs. 2 GG</a> definierten Voraussetzungen der Zustimmung des Bundesrates. Eine Mitwirkungspflicht des Bundestages ergibt sich unter Umständen aus der Verordnung zu Grunde liegenden gesetzlichen Ermächtigung.</p>' +
              '<p>Bezüglich der Einbindung des Bundesrates bei der Erstellung von Verwaltungsvorschriften sind <a target="_blank" href={{link2}}>Art. 84 Abs. 2 GG</a>, <a target="_blank" href={{link3}}>Art. 85 Abs. 2 GG</a> und Art. <a target="_blank" href={{link4}}>108 Abs. 7 GG</a> zu beachten. Eine Einbindung des Bundestages erfolgt nicht. </p>',
            title: 'Beteiligungserfordernis',
          },
          itemType: 'Beteiligung',
          label: 'Beteiligungserfordernis',
          placeholder: 'Bitte auswählen',
        },
        ffAbteilung: {
          deletedDepartmentMsg: `Die ausgewählte federführende Abteilung "{{abteilung}}" wurde gelöscht.`,
          error: 'Bitte geben Sie die federführende Abteilung des Regelungsvorhabens an.',
          hint: {
            content:
              '<p>Diese Angabe hilft anderen Stellen, sich bei Rückfragen zu dem Regelungsvorhaben an die richtige Stelle zu wenden.</p>' +
              '<p>Falls Sie Ihre Abteilung nicht finden können, wenden Sie sich bitte an die <a target="_blank" href={{link1}}>Fachadministration des Planungs- und Kabinettmanagement-Programms</a> (PKP). Diese kann in der Ressortverwaltung Änderungen vornehmen.</p>',
            title: 'Federführende Abteilung',
          },
          label: 'Federführende Abteilung',
          placeholder: 'Bitte auswählen',
        },
        ffReferat: {
          deletedDepartmentMsg: `Das ausgewählte federführende Referat "{{referat}}" wurde gelöscht.`,
          error: 'Bitte geben Sie das federführende Referat des Regelungsvorhabens an.',
          hint: {
            content:
              '<p>Diese Angabe hilft anderen Stellen, sich bei Rückfragen zu dem Regelungsvorhaben an die richtige Stelle zu wenden.</p>' +
              '<p>Falls Sie Ihre Abteilung nicht finden können, wenden Sie sich bitte an die <a target="_blank" href={{link1}}>Fachadministration des Planungs- und Kabinettmanagement-Programms</a> (PKP). Diese kann in der Ressortverwaltung Änderungen vornehmen.</p>',
            title: 'Federführendes Referat',
          },
          label: 'Federführendes Referat',
          placeholder: 'Bitte auswählen',
        },
        ffRessort: {
          error: 'Bitte geben Sie die Federführung des Regelungsvorhabens an.',
          itemType: 'Ressort',
          label: 'Federführung',
          placeholder: 'Bitte auswählen',
        },
        initiator: {
          error: 'Bitte geben Sie die Regelungsinitiative des Regelungsvorhabens an.',
          label: 'Regelungsinitiative',
          options: {
            bundesrat: 'Bundesrat',
            bundesregierung: 'Bundesregierung',
            bundestag: 'Deutscher Bundestag',
          },
        },
        involvedRessorts: {
          hint: {
            content: `<p class="p-no-style">An dieser Stelle haben Sie die Möglichkeit, ein oder mehrere zu beteiligende Ressorts auszuwählen. Die Beteiligungen der Ressorts zum Entwurf einer Regelungsvorlage sind in <a target="_blank" href={{link_GGO45_Seite36}}>§ 45 Absatz 1 GGO</a> in Verbindung mit <a target="_blank" href={{link_GGO45_Anlage6}}>Anlage 6 zu § 45 Absatz 1</a> (Achtung: nicht mehr aktuell!), <a target="_blank" href={{link_GGO74_Seite55}}>§ 74 Absatz 5 GGO</a> und den Organisationserlassen der Bundeskanzlerin vom 17. Dezember 2013 und vom 14. März 2018 sowie des Bundeskanzlers vom 8. Dezember 2021 detailliert geregelt.</p>
            <p>Weitere Informationen zur Beteiligung bieten auch im Falle von Gesetzen die Schritte <a target="_blank" href={{link_gesetz_113_materialrecherche}}>1.1.3 Materialrecherche</a> und <a target="_blank" href={{link_gesetz_141_ressortabstimmung}}>1.4.1 Ressortabstimmung</a> des Verfahrensassistenten für Gesetze, im Falle von Rechtsverordnungen die Schritte <a target="_blank" href={{link_rechtsverordnung_113_materialrecherche}}>1.1.3 Materialrecherche</a> und <a target="_blank" href={{link_rechtsverordnung_141_ressortabstimmung}}>1.4.1 Ressortabstimmung</a> des Verfahrensassistenten für Rechtsverordnungen.</p>
            <p>Bei Verwaltungsvorschriften gibt Schritt <a target="_blank" href={{link_verwaltungsvorschrift_15_verfahren}}>1.5 Verfahren beim Erlass von Verwaltungsvorschriften</a> zusätzliche Informationen, jedoch sind in der Regel hier keine Beteiligungen vorgesehen.</p>`,
            title: 'Zu beteiligende Ressorts',
          },
          itemType: 'Ressort',
          label: 'Zu beteiligende Ressorts',
          placeholder: 'Bitte auswählen',
        },
        subjectAreas: {
          hint: {
            content: `<p>An dieser Stelle haben Sie die Möglichkeit, ein oder mehrere Sachgebiet/e auszuwählen. Sollte keines der systemseitig vorgeschlagenen Sachgebiete zu dem Themenkomplex Ihres Regelungsvorhabens passen, lassen Sie das Feld unausgefüllt.</p>
            <p>Diese Information hilft nachfolgenden Stellen, wie zum Beispiel anderen Ressorts, dem Bundestag oder dem Bundesrat, die Regelung inhaltlich zuzuordnen.</p>`,
            title: 'Sachgebiete',
          },
          itemType: 'Sachgebiet',
          label: 'Sachgebiete',
          placeholder: 'Bitte auswählen',
        },
        techFfRessort: {
          error: 'Bitte geben Sie die koordinierende Federführung des Regelungsvorhabens an.',
          hint: {
            content:
              'Wählen Sie aus den von Ihnen angegebenen fachlich federführenden Ressorts das Ressort aus, das die koordinierende Federführung inne hat. Das koordinierende federführende Ressort koordiniert das Regelungsvorhaben. Es kann nur eine koordinierende Federführung geben. <br> Mit dem „Anlegen“ eines Regelungsvorhabens wird die koordinierende Federführung bzw. die alleinige ausgewählte Federführung als „Federführung“ in die Anwendung PKP übernommen.',
            title: 'Koordinierende Federführung',
          },
          label: 'Koordinierende Federführung',
          placeholder: 'Bitte auswählen',
        },
        title: 'Beteiligte',
      },
      status: {
        error: 'Bitte geben Sie den Status des Regelungsvorhabens an.',
        hint: {
          content:
            '<p>Sie müssen dem Regelungsvorhaben einen der folgenden Status zuweisen:</p>' +
            '<ul><li>Aktiv: Das Regelungsvorhaben wird angelegt und kann bearbeitet werden. In anderen Anwendungen der E-Gesetzgebung kann es (z.B. für Editor-Dokumente, Zeitplanungen etc.) ausgewählt werden, so dass diese Dokumente dem Regelungsvorhaben automatisch zugeordnet werden können.</li>' +
            '<li>Geschlossen: Die Beurteilung, ob ein Regelungsvorhaben abgeschlossen ist, obliegt grundsätzlich der für das Regelungsvorhaben verantwortlichen Stelle. Ein Regelungsvorhaben kann als abgeschlossen betrachtet werden, wenn es verworfen oder verkündet wurde. Wenn Sie diesen Status wählen, kann das Regelungsvorhaben nicht mehr bearbeitet werden.</li></ul>' +
            '<p>Das System weist dem Regelungsvorhaben automatisch den Status „Aktiv” zu. Sie können den Status je nach Bedarf anpassen.</p>' +
            '<p>Wenn Sie eine aktive Abstimmung oder eine aktive Gesetzesfolgenabschätzung für dieses Regelungsvorhaben erstellt haben, dürfen Sie den Status nicht mehr von aktiv zu geschlossen ändern.</p>',
          title: 'Status des Regelungsvorhabens',
        },
        label: 'Status des Regelungsvorhabens',
        options: {
          ARCHIVIERT: 'Archiviert',
          BUNDESTAG: 'Bundestag',
          ENTWURF: 'Entwurf',
          IN_BEARBEITUNG: 'Aktiv',
        },
        title: 'Status',
      },
      vorhabenart: {
        art: {
          error: 'Bitte geben Sie den Vorhabentyp des Regelungsvorhabens an.',
          hint: {
            content:
              'Der Typ eines Regelungsvorhabens bezieht sich auf die Form der Rechtsetzung. Es werden die drei Vorhabentypen Gesetz, Rechtsverordnung und Verwaltungsvorschrift unterschieden.',
            title: 'Vorhabentyp',
          },
          label: 'Vorhabentyp',
          options: {
            gesetz: 'Gesetz',
            rechtsverordnung: 'Rechtsverordnung',
            verwaltungsvorschrift: 'Verwaltungsvorschrift',
          },
        },
        baseType: {
          error: 'Bitte geben Sie den Grundtyp/Besonderer Typ des Regelungsvorhabens an.',
          hint: {
            content: `<p class="p-no-style">An dieser Stelle haben Sie die Möglichkeit, den von Ihnen ausgewählten Vorhabentyp zu spezifizieren.</p>
            <p>Besondere Regelungstypen können Auswirkungen auf das Regelungsverfahren haben. Für bestimmte Regelungstypen gelten zum Beispiel besondere Fristen oder es sind Fristverkürzungen ausgeschlossen.</p>
            <p>Beachten Sie, dass die Auswahl rein informatorischen Wert für nachfolgende Prozessbeteiligte im Rechtsetzungsverfahren hat. Sie bewirkt nicht automatisch eine Änderung im Verfahren, sondern es sind danach weitere Abstimmungsprozesse erforderlich. Beachten Sie die Vorgaben Ihres Hauses zum Umgang mit Regelungstypen im Rechtsetzungsverfahren, insbesondere bzgl. möglicher Abstimmungs- und Freigaberegelungen.</p>
            <p>Der Vorhabentyp kann sich manchmal im Verlauf eines Verfahrens ändern. Sie können diese Auswahl im weiteren Verfahren noch anpassen. Die Auswahlmöglichkeiten sind von der Wahl des Vorhabentyps abhängig.</p>`,
            title: 'Grundtyp/Besonderer Typ',
          },
          label: 'Grundtyp/Besonderer Typ',
          placeholder: 'Bitte auswählen',
        },
        executionEuLaw: {
          hint: {
            content: `<p class="p-no-style">An dieser Stelle haben Sie die Möglichkeit, zu spezifizieren, ob es sich bei dem Regelungsverfahren um die Umsetzung von EU-Recht in nationales Recht handelt.</p>
            <p>Bei der Umsetzung von EU-Recht sind besondere Regeln zur rechtsförmlichen Erstellung des Regelungsentwurfs zu beachten.</p>`,
            title: 'Umsetzung von EU-Recht',
          },
          label: 'Umsetzung von EU-Recht',
          options: {
            leer: 'Nein',
            umsetzung: 'Ja',
            vorbereitung: 'Vorbereitung',
          },
        },
        specialCircumstances: {
          btnAdd: '+ Besondere Umstände hinzufügen',
          hint: {
            content: `<p>An dieser Stelle haben Sie die Möglichkeit, besondere Umstände Ihres Regelungsvorhabens zu spezifizieren.</p>
            <p>Besondere Umstände können Auswirkungen auf das Regelungsverfahren haben. Zum Beispiel hat die Eilbedürftigkeit als besonderer Umstand einen Einfluss auf Verfahrensfristen.</p>
            <p>Beachten Sie, dass die Auswahl einen rein informatorischen Wert für nachfolgende Prozessbeteiligte im Rechtsetzungsverfahren hat. Sie bewirkt nicht automatisch eine Änderung im Verfahren, sondern es sind danach weitere Abstimmungsprozesse erforderlich. Beachten Sie die Vorgaben Ihres Hauses zum Umgang mit besonderen Umständen im Rechtsetzungsverfahren, insbesondere bzgl. möglicher Abstimmungs- und Freigaberegelungen.</p>
            <p>Besondere Umstände ergeben sich manchmal erst im Verlauf eines Verfahrens. Sie können diese Auswahl im weiteren Verfahren noch anpassen. Die Auswahlmöglichkeiten sind von der Wahl des Vorhabentyps abhängig.</p>`,
            title: 'Besondere Umstände',
          },
          itemType: 'besonderer Umstand',
          label: 'Besondere Umstände',
          placeholder: 'Bitte auswählen',
        },
        specialType: {
          error: 'Bitte geben Sie den besonderen Typ des Regelungsvorhabens an.',
          hint: {
            content:
              'Ein Regelungsvorhaben kann einem besonderen Typen zugeordnet werden, wenn der Verfahrensablauf von denen der möglichen Grundtypen abweicht. Dies ist zum Beispiel bei Haushaltsgesetzen der Fall. Die Auswahlmöglichkeiten sind von der Wahl des Vorhabentyps abhängig.',
            title: 'Besonderer Typ',
          },
          label: 'Besonderer Typ',
          placeholder: 'Bitte auswählen',
        },
        title: 'Weitere Eckdaten',
      },
    },
    newZuleitung: {
      modal: {
        cancelBtnText: 'Abbrechen',
        nextBtnText: 'Nächster Schritt',
        page1: {
          item1: {
            error: 'Bitte geben Sie die Anliegen an.',
            label: 'Anliegen wählen',
            options: {
              BKAMT: 'Kabinettvorlage an BKAmt (via PKP) senden',
              BUNDESRAT: 'BR-Drucksache an Bundesrat (via PKP) senden',
            },
          },
          requiredInfo: 'Pflichtfelder sind mit einem * gekennzeichnet',
          title: 'Schritt 1 von 2: Anliegen und Dokument auswählen',
        },
        page2: {
          anliegen: 'Anliegen: ',
          formatDocument: 'Format des Dokuments: ',
          noAnswer: 'Keine Angabe',
          pruefenBtn: 'Dokumente zustellen',
          selectedRE: 'Ausgewähltes Dokument:',
          title: 'Schritt 2 von 2: Eingaben prüfen',
          typeDoc: {
            EDITOR: 'Editor',
            ENORM: 'eNorm',
          },
          uploadedAttachments: 'Hochgeladene Anlagen:',
          uploadedDraft: 'Hochgeladener Regierungsentwurf:',
        },
        prevBtnText: 'Vorheriger Schritt',
        pruefenBtn: 'Eingaben prüfen',
        succsessMsg: 'Ihre Dokumente wurden erfolgreich zugestellt.',
        title: 'Regelungsvorhaben "{{rvName}}": Dokumente zustellen',
      },
    },
    notificationPages: {
      error: {
        buttonText: 'Regelungsvorhaben korrigieren',
        text: 'Ihr Regelungsvorhaben konnte nicht angelegt werden. Bitte korrigieren Sie Ihre Eingaben im Formular „Neues Regelungsvorhaben anlegen“.',
        title: 'Achtung, es ist ein Fehler aufgetreten.',
      },
      success: {
        buttonText: 'Zur Übersicht „Regelungsvorhaben“',
        text: 'Sie können das Regelungsvorhaben nun in allen Anwendungen der <span class="no-wrap">E-Gesetzgebung</span> auswählen und ausarbeiten.',
        title: 'Das Regelungsvorhaben wurde erfolgreich {{type}}.',
      },
    },
    promise: 'wird später eingetragen',
    pruefenRegelungsvorhaben: {
      btns: {
        cancel: 'Eingaben bearbeiten',
        submit: 'Regelungsvorhaben jetzt anlegen',
        submitEdit: 'Regelungsvorhaben jetzt aktualisieren',
        submitItemFromPKP: 'Regelungsvorhaben aus PKP jetzt anlegen',
      },
      itemFromPKPHinweis:
        'Sie übernehmen ein Regelungsvorhaben aus PKP. Das Regelungsvorhaben wird beim Anlegen einmalig an PKP gesendet. Später können Sie das Regelungsvorhaben erneut manuell an PKP senden, um dort die Daten zu aktualisieren.',
      mainTitle: 'Eingaben prüfen',
    },
    regelungsvorhabenTabs: {
      rvInfoDrawer: {
        infoRvLabel: 'Informationen zum Regelungsvorhaben',
        infoRvTitle: 'Informationen zum Regelungsvorhaben',
      },
      tabs: {
        bestandsrecht: {
          button: 'Bestandsrecht aus Neu-RIS importieren',
          modal: {
            btnApply: 'Übernehmen',
            cancelBtnText: 'Abbrechen',
            contentDescription: 'Pflichtfelder sind mit einem * gekennzeichnet',
            contentTitle: 'Bestandsrecht auswählen',
            delete: 'Löschen',
            importBtn: 'Ausgewähltes Bestandsrecht jetzt importieren',
            load: 'Laden...',
            moreItems: 'weitere einblenden',
            moreThan10: 'Es gibt mehr als 10 Ergebnisse. Bitte verfeinern Sie Ihre Suche.',
            neuRisSearch: {
              btnClear: 'Suche zurücksetzen',
              btnText: 'Suchen',
              placeholder: 'Suchbegriff eingeben',
            },
            noNeuRises: 'Es wurde noch kein Neu-Ris übernommen.',
            notFound: 'Es wurde kein Ergebnis gefunden.',
            previousBtn: 'Vorheriger Schritt',
            pruefenBtn: 'Eingaben prüfen',
            results: {
              plural: 'Ergebnisse',
              singular: 'Ergebnis',
            },
            resultTitle: 'Zu importierendes Bestandsrecht',
            reviewTitle: 'Eingaben prüfen',
            searchDescription: '(z. B. Kurzbezeichnung, Abkürzung, Stichworte)',
            searchLabel: 'Suche nach Bestandsrecht in Neu-RIS',
            successMessage:
              'Die ausgewählten Bestandsrechte wurden erfolgreich aus Neu-RIS in Ihr Regelungsvorhaben „{{rvName}}“ importiert.',
            title: 'Regelungsvorhaben „{{rvName}}“: Bestandsrecht aus Neu-RIS importieren',
            tooShort: 'Der Suchstring ist zu kurz. Für die Suche muss mindestens ein Wort 3 Zeichen oder länger sein',
          },
          table: {
            header: {
              eli: 'ELI',
              gesetz: 'Gesetz',
              importiertAm: 'Importiert am',
            },
            importiertAmAsc: 'Zuletzt importiert (ältestest zuerst)',
            importiertAmDesc: 'Zuletzt importiert (neuestes zuerst)',
            nameAsc: 'Alphabetisch (aufsteigend A - Z)',
            nameDesc: 'Alphabetisch (absteigend Z - A)',
          },
          title: 'Bestandsrecht',
        },
        datenblatt: {
          title: 'Datenblatt',
        },
        naechsteSchritte: {
          tabs: {
            abschlussverfahren: 'Abschlussverfahren',
            abschlussverfahrenZugriff: {
              evir: {
                list: {
                  ausfertigung: '3.1 Ausfertigung und Verkündung',
                },
              },
              title: 'Abschlussverfahren (kein Zugriff)',
            },
            ausschussverfahrenDurchfuehren: {
              collapse: {
                empfehlungenAusschuesse: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Zusammenstellen der Empfehlungen aller Ausschüsse zu einer Drucksache',
                },
                empfehlungsdrucksachePruefen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Empfehlungsdrucksache prüfen',
                },
                empfehlungsdrucksacheVerteilen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Empfehlungsdrucksache verteilen',
                },
                entwurfNiederschriftErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Entwurf der Niederschrift anfertigen',
                },
                nachtraegeTagesordnungErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Ggf. Nachträge zur Tagesordnung erstellen und versenden',
                },
                niederschriftFinalisieren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Niederschrift aktualisieren, finalisieren und versenden',
                },
                sitzungenDurchfuehren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Sitzungen durchführen',
                },
                tagesordnungErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Tagesordnung erstellen und versenden',
                },
                umfrageverfahrenDurchfuehren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Ggf. Umfrageverfahren durchführen',
                },
                unterausschusssitzungDurchfuehren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Ggf. Unterausschusssitzung durchführen',
                },
              },
              evir: {
                list: {
                  stellungnahme: '1.7 Stellungnahme Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Ausschussverfahren durchführen',
            },
            beratungPlenumVorbereiten: {
              collapse: {
                erlaeuterungenTagesordnungErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Erläuterungen zu TOPs erstellen, prüfen und einstellen',
                },
                sprechzettelUndRednerliste: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Sprechzettel und Rednerliste fertigen, prüfen und ablegen',
                },
                tagesordnungAnlegen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Tagesordnung anlegen',
                },
                tagesordnungVeroeffentlichen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Tagesordnung an Ländern und Bundesregierung versenden und veröffentlichen',
                },
              },
              evir: {
                list: {
                  stellungnahme: '1.7 Stellungnahme Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Beratung im Plenum vorbereiten',
            },
            brStellungnahme: {
              title: 'BR-Stellungnahme einholen',
            },
            dokumenteErstellen: {
              collapse: {
                gesetzesfolgenabschaetzung: {
                  aktuellenEgfa: 'Zur aktuellen Gesetzesfolgenabschätzung',
                  createLink: 'Gesetzesfolgenabschätzung anlegen',
                  text: 'Analysieren, berechnen und beschreiben Sie die Auswirkungen Ihres Gesetzesvorhabens mithilfe einer Gesetzesfolgenabschätzung.',
                  title: 'Gesetzesfolgenabschätzung anlegen',
                },
                regelungsalternativen: {
                  createLink: 'Regelungsalternativen in der Anwendung „Vorbereitung“ prüfen',
                  text: 'In der Anwendung „Vorbereitung“ können Sie Ihre Fragestellung aus unterschiedlichen Blickwinkeln beleuchten. So finden Sie die beste Regelungsalternative und entwickeln Eckpunkte für Ihr Vorhaben.',
                  title: 'Regelungsalternativen prüfen',
                },
                zeitplanung: {
                  aktuellenZeitplanung: 'Zur aktuellen Zeitplanung',
                  createLink: 'Zeitplanung anlegen',
                  praxistipp: 'Praxistipp',
                  text: 'Planen Sie mithilfe einer oder mehrerer Zeitplanungen, wie viel Zeit Ihr Gesetzesvorhaben in Anspruch nehmen wird. Sie können Phasen und Termine für den Rechtsetzungsprozess planen und managen. Die Zeitplanung dient der internen Planung und hat keine Bindungswirkung innerhalb des eigenen Hauses oder für andere Verfassungsorgane.',
                  textAusblenden: 'Text ausblenden',
                  textEinblenden: 'Text einblenden',
                  textTip:
                    'Nutzen Sie die angebotenen Vorlagen. Diese enthalten alle wesentlichen Phasen und Termine. Bei Bedarf können Sie die Vorlagen individuell zu den Rahmenbedingen des jeweiligen Gesetzesvorhaben anpassen.',
                  title: 'Zeitplanung anlegen',
                },
              },
              evir: {
                list: {
                  eckpunkte: '1.1.4 Eckpunkte und prospektive Folgenabschätzung',
                  gesetzesfolgenabschaetzung: '1.2.2 Gesetzesfolgenabschätzung',
                  zeitplanung: '1.1.2 Zeitplanung',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Dokumente erstellen',
            },
            einleitungsverfahren: 'Einleitungsverfahren',
            einleitungsverfahrenKeinZugriff: {
              evir: {
                list: {
                  arbeitsentwurf: '1.2 Arbeitsentwurf',
                  einbringung: '1.9 Einbringung in den Bundestag',
                  gegenaeusserung: '1.8 Gegenäußerung Bundesregierung',
                  hausabstimmung: '1.3 Hausabstimmung',
                  kabinettbeschluss: '1.6 Kabinettbeschluss',
                  laender: '1.5 Länder- und Verbändebeteiligung',
                  ressortabstimmung: '1.4 Ressortabstimmung und weitere Beteiligungen',
                  stellungnahme: '1.7 Stellungnahme Bundesrat',
                  vorueberlegungen: '1.1 Vorüberlegungen',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Einleitungsverfahren (kein Zugriff)',
            },
            gesetzgebungsverfahrenBRAT: 'Gesetzgebungsverfahren Bundesrat',
            gesetzgebungsverfahrenBRATZugriff: {
              evir: {
                list: {
                  beratung: '2.2 Beratung Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Gesetzgebungsverfahren Bundesrat (kein Zugriff)',
            },
            gesetzgebungsverfahrenBT: 'Gesetzgebungsverfahren Bundestag',
            gesetzgebungsverfahrenBTAGZugriff: {
              evir: {
                list: {
                  beratung: '2.1 Beratung Bundestag',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Gesetzgebungsverfahren Bundestag (kein Zugriff)',
            },
            kabinettverfahren: {
              collapse: {
                kabinettbeschluss: {
                  createLink: 'Zum Tab „Zustellungen“',
                  text: `Wenn das Kabinett den Regierungsentwurf beschlossen und Ihnen das Zuleitungsschreiben gesendet hat, sind die Ressorts an die beschlossene Fassung gebunden.`,
                  title: `Kabinettbeschluss`,
                },
                schlussabstimmung: {
                  aktuellenSchlussabstimmung: 'Zur aktuellen Schlussabstimmung',
                  createLink: 'Neue Abstimmung einleiten',
                  text: `Binden Sie alle am Vorhaben beteiligten Ressorts in einer abschließenden Abstimmung erneut ein, um die Kabinettreife herbeizuführen. Das Bundesministerium der Justiz bestätigt spätestens jetzt die Durchführung der rechtssystematischen und rechtsförmlichen Prüfung.`,
                  title: `Schlussabstimmung zur Herbeiführung der Kabinettreife durchführen`,
                },
                vorlageFRegierungsentwurf_Erstellen: {
                  createLink: 'Anwendung „Editor“ öffnen',
                  text: `Nach Abschluss der Abstimmungen erstellen Sie im „Editor“ nun die gewünschte Dokumentenmappe mit dem Status „Bereit für Kabinett“. So erstellen Sie die Vorlage für den Regierungsentwurf. Ergänzen Sie bei Bedarf dabei die weiteren Dokumente, die für eine Kabinettvorlage benötigt werden, und passen Sie die Struktur an.`,
                  textTip: `Eine Kabinettvorlage enthält:
                  <ul> 
                    <li> Ministeranschreiben an Chefin oder Chef des Bundeskanzleramts </li>
                    <li> Beschlussvorschlag </li>
                    <li> Sprechzettel für Regierungssprecherin oder Regierungssprecher </li>
                    <li> Dokumentenmappe (Vorblatt, Regelungsentwurf, Begründung) </li>
                    <li> ggf. Stellungnahme des Normenkontrollrats </li>
                    <li> ggf. Gegenäußerung der Bundesregierung zur Stellungnahme des Normenkontrollrats </li>
                  </ul>
                  `,
                  title: `Kabinettvorlage erstellen`,
                },
                vorlageFRegierungsentwurf_HausleitungSenden: {
                  aktuelleAbstimmung: 'Zur aktuellen Abstimmung mit der Hausleitung',
                  createLink: 'Neue Abstimmung einleiten',
                  text: `Versenden Sie die Kabinettvorlage nun an Ihre Hausleitung zur Disposition.`,
                  title: `Kabinettvorlage an Hausleitung senden`,
                },
                vorlageFRegierungsentwurf_PKPSenden: {
                  createLink: 'Zum Tab „Zustellungen“',
                  text: `<p>Die Kabinettvorlage senden Sie im Tab „Zustellungen“ in der Anwendung Regelungsvorhaben an PKP.</p>
                  <p>Wenn aus den Beratungen im Kabinett Änderungswünsche entstehen, müssen Sie die Kabinettvorlage anpassen
                  und erneut an PKP senden. Dieser Prozess wiederholt sich, bis das Kabinett den Regierungsentwurf beschließt.</p>`,
                  title: `Kabinettvorlage an PKP senden`,
                },
              },
              evir: {
                list: {
                  kabinettbeschluss: '1.6.4 Kabinettbeschluss',
                  kvBundeskanzleramt: '1.6.3 Kabinettvorlage an Bundeskanzleramt',
                  kvHausleitung: '1.6.2 Kabinettvorlage an Hausleitung',
                  schlussabstimmung: '1.6.1 Schlussabstimmung',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Kabinettverfahren durchführen',
            },
            keinZugriff: '(kein Zugriff)',
            kuenftigVerfuegbar: '(künftig verfügbar)',
            lockReason: {
              bAmtReason:
                'Der Zugriff auf diese Seite ist ausschließlich der Bundesregierung und dem Bundespräsidialamt vorbehalten. ',
              bRatReason: 'Der Zugriff auf diese Seite ist ausschließlich dem Bundesrat vorbehalten.',
              bRegReason: 'Der Zugriff auf diese Seite ist ausschließlich der Bundesregierung vorbehalten.',
              bTagReason: 'Der Zugriff auf diese Seite ist ausschließlich dem Bundestag vorbehalten.',
              title: 'Hinweis',
            },
            notifizierungUebersenden: {
              collapse: {
                notifizierungAnBKAmtVersenden: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Notifizierung an Bundeskanzleramt versenden',
                },
                notifizierungVorbereiten: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Notifizierung vorbereiten',
                },
              },
              evir: {
                list: {
                  stellungnahme: '1.7 Stellungnahme Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Notifizierung übersenden',
            },
            parlamentsverfahrenEndgueltigeFassung: {
              collapse: {
                aenderungenAbstimmen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Änderungen mit dem Initianten abstimmen',
                },
                druckAuftragGeben: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Druck für endgültige Fassung in Auftrag geben',
                },
                druckfahneErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Druckfahne erstellen',
                },
                endgueltigeFassungErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Endgültige Fassung erstellen',
                },
                fassungVerteilen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Endgültige Fassung verteilen',
                },
                vorlageLektorieren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Vorlage lektorieren',
                },
              },
              evir: {
                list: {
                  beratungBundestag: '2.1 Beratung Bundestag',
                  einbringungBundestag: '1.9 Einbringung in den Bundestag',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Parlamentsverfahren vorbereiten (endgültige Fassung)',
            },
            parlamentsverfahrenVorabfassung: {
              collapse: {
                bundestagsDrucksacheErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Bundestagsdrucksache aus der Dokumentenmappe erzeugen (Manuskript erstellen)',
                },
                checkboxTip: {
                  text: 'Die Vorlage steht auf der Tagesordnung',
                  title: 'Tagesordnungsrelevanz festlegen',
                },
                datenErfassen: {
                  datenblattAnzeigen: 'Angaben im Modul Bundestagsdrucksachen ergänzen',
                  text: 'Ergänzen Sie benötigte Angaben im Modul Bundestagsdrucksachen. Vergeben Sie insbesondere eine Drucksachennummer.',
                  title: 'Daten zur Vorlage erfassen',
                },
                schnelldruckFuerVorabfassungGeben: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Bei Tagesordnungsrelevanz: Schnelldruck für Vorabfassung in Auftrag geben',
                },
                vorabfassungErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Vorabfassung erstellen',
                },
                vorabfassungVerteilen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Vorabfassung verteilen',
                },
                vorlageEmpfangen: {
                  text: 'Sie haben eine Vorlage empfangen. Zusätzlich wurden Ihnen ggf. weitere Dokumente (Anhänge) zugeleitet.',
                  title: 'Vorlage empfangen',
                },
                vorpruefungDurchfuehren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Vorprüfung der Vorlage durchführen',
                },
                zusaetzlicheVorpruefungDurchfuehren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Bei Tagesordnungsrelevanz: Zusätzliche Vorprüfung der Vorlage durchführen',
                },
              },
              evir: {
                list: {
                  beratungBundestag: '2.1 Beratung Bundestag',
                  einbringungBundestag: '1.9 Einbringung in den Bundestag',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Parlamentsverfahren vorbereiten (Vorabfassung)',
            },
            plenarsitzungDurchfuehren: {
              collapse: {
                beschluesseErfassen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Beschlüsse erfassen (und freigeben)',
                },
                beschlussdrucksacheErzeugen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Beschlussdrucksache erzeugen',
                },
                beschlussdrucksachePruefen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Prüfung der Beschlussdrucksache durchführen',
                },
                beschlussdrucksacheVersenden: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Beschlussdrucksache an Bundesregierung, Länder, Bundestag senden',
                },
                plenarantragPruefen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Plenarantrag prüfen',
                },
                plenarantragStellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Plenarantrag stellen',
                },
                sprechzettelUndRednerlisteAnpassen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Sprechzettel und Rednerliste anpassen',
                },
                tagesordnungFreigeben: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Tagesordnung freigeben',
                },
              },
              evir: {
                list: {
                  stellungnahme: '1.7 Stellungnahme Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Plenarsitzung durchführen',
            },
            regelungsentwurfAbstimmen: {
              collapse: {
                hausabstimmung: {
                  aktuellenHausabstimmung: 'Zur aktuellen Hausabstimmung',
                  createLink: 'Neue Abstimmung einleiten',
                  text: '<p>Beteiligen Sie nach Erstellung eines Arbeitsentwurfs weitere zuständige Organisationseinheiten Ihres Hauses.</p><p> Unterrichten Sie nach abgeschlossener Hausabstimmung die Hausleitung über das Ergebnis. Holen Sie gleichzeitig mit der Unterrichtung die Zustimmung zur Einleitung der Ressortabstimmung ein.</p><p>Nach Möglichkeit umfasst Ihr Entwurf bereits ein befülltes Vorblatt sowie einen ausformulierten Begründungsteil.</p>',
                  title: 'Hausabstimmung durchführen',
                },
                ressortabstimmung: {
                  aktuellenRessortabstimmung: 'Zur aktuellen Ressortabstimmung',
                  createLink: 'Neue Abstimmung einleiten',
                  text: '<p>Leiten Sie nun die Ressortabstimmung ein. Hierzu senden Sie den von der Hausleitung gebilligten Ressortentwurf an alle Ressorts, deren Zuständigkeiten berührt sind.</p><p>Die Verfassungsressorts (BMI und BMJ) sind dabei immer zu beteiligen.</p><p> Beteiligen Sie in diesem Schritt auch den Normenkontrollrat. Fügen Sie ihn dafür einfach als Adressaten zu Ihrer Ressortabstimmung hinzu. Er erhält mit den gleichen Fristen Gelegenheit zur Stellungnahme, insbesondere zum zu erwartenden Erfüllungsaufwand.</p><p> Der Ressortentwurf sollte nun ein befülltes Vorblatt und eine ausformulierte Begründung enthalten.</p>',
                  title: 'Ressortabstimmung durchführen',
                },
                vorhabenclearing: {
                  aktuellenVorhabenclearing: 'Zum aktuellen Vorhabenclearing',
                  createLink: 'Neue Abstimmung einleiten',
                  text: '<p>Vor der Einleitung der Ressortabstimmung sendet das federführende Ressort den Gesetzentwurf an das Bundeskanzleramt, BMWK und BMF (= Vorhabenclearing). Dies geschieht mindestens drei Werktage vorab</p><p>Sie haben die Möglichkeit eine Abstimmung vor dem Versand als Entwurf zwischenzuspeichern und die Bearbeitung zu einem späteren Zeitpunkt fortzusetzen.</p><p>Bitte beachten Sie, dass das Vorhabenclearing erst nach abgeschlossener  Hausabstimmung eingeleitet werden kann.</p>',
                  title: 'Vorhabenclearing durchführen',
                },
              },
              evir: {
                list: {
                  entscheidung: '1.3.2 Entscheidung Hausleitung',
                  hausabstimmung: '1.3 Hausabstimmung',
                  ressortabstimmungen: '1.4 Ressortabstimmungen und weitere Beteiligungen',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Regelungsentwurf abstimmen',
            },
            regelungsentwurfErarbeiten: {
              collapse: {
                bestandsrecht: {
                  createLink: 'Bestandsrecht aus Neu-RIS importieren',
                  text: 'Wenn Sie ein Änderungsgesetz erstellen, können Sie außerdem benötigtes Bestandsrecht korrekt formatiert aus Neu-RIS importieren. Das importierte Bestandsrecht ist dann schon mit Ihrem Regelungsvorhaben verknüpft. So können Sie im „Editor“ schnell und komfortabel die Texterstellung fortführen.',
                  title: 'Bestandsrecht importieren',
                },
                regelungstext: {
                  createLink: 'Anwendung „Editor“ öffnen',
                  text: 'Beginnen Sie die Texterstellung, indem Sie im „Editor“ eine Dokumentenmappe anlegen. Der Regelungstext ist automatisch in der Dokumentenmappe enthalten.',
                  title: 'Regelungstext anlegen',
                },
              },
              evir: {
                list: {
                  ersterEntwurf: '1.2.1 Erster Entwurf',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Regelungsentwurf erarbeiten',
            },
            regierungsentwuerfe: {
              collapse: {
                brDrucksacheVerteilen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Bundesratsdrucksache an Landesvertretungen und Bundesregierung verteilen',
                },
                brGrunddrucksacheErstellen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Bundesratsgrunddrucksache erstellen',
                },
                datenGesetzesvorlageErfassen: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Daten zur Gesetzesvorlage erfassen',
                },
                nachtraeglicheMitberatungsbitte: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Ggf. Nachträgliche Mitberatungsbitte',
                },
                regierungsentwurfErhalten: {
                  text: 'Sie haben einen Regierungsentwurf erhalten. Zusätzlich wurden Ihnen ggf. weitere Dokumente (Anhänge) zugeleitet.',
                  title: 'Regierungsentwurf erhalten',
                },
                umlaufDurchfuehren: {
                  text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                  title: 'Umlauf durchführen',
                },
              },
              evir: {
                list: {
                  stellungnahme: '1.7 Stellungnahme Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Regierungsentwürfe empfangen und verteilen',
            },
            stellungnahmeBR: 'Erster Durchgang im Bundesrat (Stellungnahme)',
            stellungnahmeBRKeinZugriff: {
              evir: {
                list: {
                  beratung: '1.7 Stellungnahme Bundesrat',
                },
                title: 'Zugehörige Schritte des Rechtsetzungsverfahrens im Verfahrensassistent:',
              },
              title: 'Erster Durchgang im Bundesrat (Stellungnahme) (kein Zugriff)',
            },
          },
          title: 'Nächste Schritte',
        },
        zuleitungen: {
          counter: {
            pl: 'Einträge',
            sgl: 'Eintrag',
          },
          sendZultngBtn: 'Dokumente zustellen',
          table: {
            body: {
              actions: {
                feedbackDocFolder: 'Rückmeldung einer Dokumentenmappe zuweisen',
                openFeedback: 'Stellungnahme öffnen',
                statusSetzenBRWeitergeleitet: 'An Bundesrat zuleiten',
                statusSetzenBTWeitergeleitet: 'An Bundestag zuleiten',
                versionHistory: 'Versionshistorie öffnen',
              },
              approved: {
                p1: '(P1)',
                pd1: '(PD1)',
                pkp: '(Via PKP)',
              },
              brWeitergeleitet: {
                successMessage: 'Das Regelungsvorhaben wurde erfolgreich an den Bundesrat zugeleitet.',
              },
              btWeitergeleitet: {
                successMessage: 'Das Regelungsvorhaben wurde erfolgreich an den Bundestag zugeleitet.',
              },
              docs: {
                showDocsReceived: 'Empfangene Dokumente anzeigen',
                showDocsSent: 'Gesendete Dokumente anzeigen',
              },
              processSteps: {
                BKAMT: 'Kabinettverfahren durchführen / Kabinettvorlage an BKAmt senden',
                BUNDESRAT: 'an Bundesrat zuleiten',
                BUNDESTAG: 'an Bundestag zuleiten',
              },
            },
            header: {
              actions: 'Aktionen',
              approved: 'Freigegeben für',
              docs: 'Dokumente',
              process: 'Prozessschritt',
              status: 'Status',
            },
            // labels are a union of ProzesschrittType | FreigegebenType | RueckmeldungType.
            labels: {
              ABSCHLUSSVERFAHREN_DURCHFUEHREN: 'Abschlussverfahren durchführen',
              AENDERUNG_GEWUENSCHT_FEDERFUEHRER: 'Änderungsbedarf durch Federführung gemeldet',
              AENDERUNG_GEWUENSCHT_PKP: 'Änderungsbedarf durch BKAmt gemeldet',
              BKAMT: 'BKAmt',
              BR_STELLUNGNAHME: 'BR-Stellungnahme',
              BUNDESRAT: 'Bundesrat',
              BUNDESTAG: 'Bundestag',
              DOKUMENT_ERSTELLEN: 'Dokument erstellen',
              KABINETTVERFAHREN_DURCHFUEHREN: 'Kabinettverfahren durchführen',
              KABINETTVORLAGE_BESCHLOSSEN: 'Kabinettvorlage beschlossen',
              PARLAMENTSVERFAHREN_DURCHFUEHREN: 'Parlamentsverfahren durchführen',
              REGELUNGSENTWURF_ABSTIMMEN: 'Regelungsentwurf abstimmen',
              REGELUNGSENTWURF_ERARBEITEN: 'Regelungsentwurf erarbeiten',
              STELLUNGNAHME_ERHALTEN: 'Stellungnahme erhalten',
              ZUGESTELLT: 'Zugestellt an PKP',
              ZULEITUNG_EMPFANGEN: 'Zuleitung empfangen',
            },
          },
          // NOTE: both de-br and de-bt override key 'title' with 'Zuleitungen'.
          title: 'Zustellungen',
        },
      },
    },
    rvErrorController: {
      error1061:
        'PKP ist zurzeit leider nicht erreichbar. Versuchen Sie es zu einem späteren Zeitpunkt erneut. {{fehlercode}}',
      error1062:
        'Die von Ihnen ausgewählte federführende Abteilung gehört nicht zum federführenden Ressort. {{fehlercode}}',
      error1063:
        'Das von Ihnen gewählte Regelungsvorhaben befindet sich bei PKP intern noch nicht im richtigen Status für eine Zustellung.  {{fehlercode}}',
      error1064: 'Für das von Ihnen ausgewählte Ressort ist kein Meldestatus konfiguriert.  {{fehlercode}}',
      error1065:
        'Sie haben leider bei PKP intern keine Zugriffsberechtigung auf das Regelungsvorhaben.  {{fehlercode}}',
      error1069: 'Das Dokument für die Zustellung hat ein ungültiges Dateiformat.  {{fehlercode}}',
      errorDefault:
        'Ein Fehler ist aufgetreten. Bitte versuchen Sie es zu einem späteren Zeitpunkt erneut. {{fehlercode}}',
    },
  },
};
